<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
    Route::get('/', function () {
        return redirect('/login');
    });

    /**
     * Applying role authentication to all the route in this closure.
     */
    Route::group(['namespace' => 'Admin', 'middleware' => ['role', 'auth']], function () {

        // Route to dashboard page.
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

        /**
         * Routes for ROLE management page.
         */

        // Ajax route to list all roles.
        Route::get('/roles/list', 'RoleController@list');

        // Route to delete a role.
        Route::get('/roles/{id}/delete', 'RoleController@destroy')->name('roles.destroy');

        // Route to display permission's page.
        Route::get('/roles/permission', 'RoleController@permission');

        // Routes to manage all role's CRUD operations.
        Route::resource('/roles', 'RoleController');

        /**
         * Routes for USER management pages.
         */

        // Ajax route to list all users.
        Route::get('/users/list', 'UserController@list');

        // Route to delete a user.
        Route::get('/users/{id}/delete', 'UserController@destroy')->name('users.destroy');

        // Route to show a role that is assigned to a user.
        Route::get('/users/{id}/roles', 'UserController@roles')->name('users.roles');

        // Route to assign a roles to a user.
        Route::patch('/users/{id}/roles', 'UserController@saveRole')->name('users.saverole');

        // Routes to manage all user's CRUD operations, like create, show, update and delete.
        Route::resource('/users', 'UserController');
    });

    Auth::routes();
    Route::get('logs1919', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::resource('airports', 'AirportController');