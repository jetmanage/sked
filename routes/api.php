<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::post('recover', 'AuthController@recover');

Route::group(['middleware' => ['jwt.auth', 'two.factor']], function () {
    Route::get('status', 'AuthController@status');
    Route::post('twofactor', 'AuthController@twoFactor');

    Route::resource('addresses', 'AddressAPIController');
    Route::resource('phones', 'PhoneAPIController');
});

Route::resource('individuals', 'IndividualAPIController');
Route::resource('countries', 'CountryAPIController');
Route::resource('emails', 'EmailAPIController');
Route::resource('notes', 'NoteAPIController');
Route::resource('tags', 'TagAPIController');
Route::resource('aircrafts', 'AircraftAPIController');
Route::resource('trips', 'TripAPIController');
Route::resource('legs', 'LegAPIController');
Route::resource('passengers', 'PassengerAPIController');
Route::resource('airports', 'AirportAPIController');

Route::post('individuals/tag/{id}/{tagId}', 'IndividualAPIController@addTag');
Route::delete('individuals/tag/{id}/{tagId}', 'IndividualAPIController@removeTag');

Route::post('upload', 'UploadController@uploadDocument');

Route::get('logout', 'AuthController@logout');
Route::get('airports/search/{query}', 'AirportAPIController@search');
