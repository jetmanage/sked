<?php

use App\Models\Aircraft;
use App\Repositories\AircraftRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AircraftRepositoryTest extends TestCase
{
    use MakeAircraftTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AircraftRepository
     */
    protected $aircraftRepo;

    public function setUp()
    {
        parent::setUp();
        $this->aircraftRepo = App::make(AircraftRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAircraft()
    {
        $aircraft = $this->fakeAircraftData();
        $createdAircraft = $this->aircraftRepo->create($aircraft);
        $createdAircraft = $createdAircraft->toArray();
        $this->assertArrayHasKey('id', $createdAircraft);
        $this->assertNotNull($createdAircraft['id'], 'Created Aircraft must have id specified');
        $this->assertNotNull(Aircraft::find($createdAircraft['id']), 'Aircraft with given id must be in DB');
        $this->assertModelData($aircraft, $createdAircraft);
    }

    /**
     * @test read
     */
    public function testReadAircraft()
    {
        $aircraft = $this->makeAircraft();
        $dbAircraft = $this->aircraftRepo->find($aircraft->id);
        $dbAircraft = $dbAircraft->toArray();
        $this->assertModelData($aircraft->toArray(), $dbAircraft);
    }

    /**
     * @test update
     */
    public function testUpdateAircraft()
    {
        $aircraft = $this->makeAircraft();
        $fakeAircraft = $this->fakeAircraftData();
        $updatedAircraft = $this->aircraftRepo->update($fakeAircraft, $aircraft->id);
        $this->assertModelData($fakeAircraft, $updatedAircraft->toArray());
        $dbAircraft = $this->aircraftRepo->find($aircraft->id);
        $this->assertModelData($fakeAircraft, $dbAircraft->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAircraft()
    {
        $aircraft = $this->makeAircraft();
        $resp = $this->aircraftRepo->delete($aircraft->id);
        $this->assertTrue($resp);
        $this->assertNull(Aircraft::find($aircraft->id), 'Aircraft should not exist in DB');
    }
}
