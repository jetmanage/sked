<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AircraftApiTest extends TestCase
{
    use MakeAircraftTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAircraft()
    {
        $aircraft = $this->fakeAircraftData();
        $this->json('POST', '/api/v1/aircrafts', $aircraft);

        $this->assertApiResponse($aircraft);
    }

    /**
     * @test
     */
    public function testReadAircraft()
    {
        $aircraft = $this->makeAircraft();
        $this->json('GET', '/api/v1/aircrafts/'.$aircraft->id);

        $this->assertApiResponse($aircraft->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAircraft()
    {
        $aircraft = $this->makeAircraft();
        $editedAircraft = $this->fakeAircraftData();

        $this->json('PUT', '/api/v1/aircrafts/'.$aircraft->id, $editedAircraft);

        $this->assertApiResponse($editedAircraft);
    }

    /**
     * @test
     */
    public function testDeleteAircraft()
    {
        $aircraft = $this->makeAircraft();
        $this->json('DELETE', '/api/v1/aircrafts/'.$aircraft->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/aircrafts/'.$aircraft->id);

        $this->assertResponseStatus(404);
    }
}
