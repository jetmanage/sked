<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AirportApiTest extends TestCase
{
    use MakeAirportTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAirport()
    {
        $airport = $this->fakeAirportData();
        $this->json('POST', '/api/v1/airports', $airport);

        $this->assertApiResponse($airport);
    }

    /**
     * @test
     */
    public function testReadAirport()
    {
        $airport = $this->makeAirport();
        $this->json('GET', '/api/v1/airports/'.$airport->id);

        $this->assertApiResponse($airport->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAirport()
    {
        $airport = $this->makeAirport();
        $editedAirport = $this->fakeAirportData();

        $this->json('PUT', '/api/v1/airports/'.$airport->id, $editedAirport);

        $this->assertApiResponse($editedAirport);
    }

    /**
     * @test
     */
    public function testDeleteAirport()
    {
        $airport = $this->makeAirport();
        $this->json('DELETE', '/api/v1/airports/'.$airport->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/airports/'.$airport->id);

        $this->assertResponseStatus(404);
    }
}
