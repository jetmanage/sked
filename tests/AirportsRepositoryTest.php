<?php

use App\Models\Airports;
use App\Repositories\AirportsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AirportsRepositoryTest extends TestCase
{
    use MakeAirportsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AirportsRepository
     */
    protected $airportsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->airportsRepo = App::make(AirportsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAirports()
    {
        $airports = $this->fakeAirportsData();
        $createdAirports = $this->airportsRepo->create($airports);
        $createdAirports = $createdAirports->toArray();
        $this->assertArrayHasKey('id', $createdAirports);
        $this->assertNotNull($createdAirports['id'], 'Created Airports must have id specified');
        $this->assertNotNull(Airports::find($createdAirports['id']), 'Airports with given id must be in DB');
        $this->assertModelData($airports, $createdAirports);
    }

    /**
     * @test read
     */
    public function testReadAirports()
    {
        $airports = $this->makeAirports();
        $dbAirports = $this->airportsRepo->find($airports->id);
        $dbAirports = $dbAirports->toArray();
        $this->assertModelData($airports->toArray(), $dbAirports);
    }

    /**
     * @test update
     */
    public function testUpdateAirports()
    {
        $airports = $this->makeAirports();
        $fakeAirports = $this->fakeAirportsData();
        $updatedAirports = $this->airportsRepo->update($fakeAirports, $airports->id);
        $this->assertModelData($fakeAirports, $updatedAirports->toArray());
        $dbAirports = $this->airportsRepo->find($airports->id);
        $this->assertModelData($fakeAirports, $dbAirports->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAirports()
    {
        $airports = $this->makeAirports();
        $resp = $this->airportsRepo->delete($airports->id);
        $this->assertTrue($resp);
        $this->assertNull(Airports::find($airports->id), 'Airports should not exist in DB');
    }
}
