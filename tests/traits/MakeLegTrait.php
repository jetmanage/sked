<?php

use Faker\Factory as Faker;
use App\Models\Leg;
use App\Repositories\LegRepository;

trait MakeLegTrait
{
    /**
     * Create fake instance of Leg and save it in database
     *
     * @param array $legFields
     * @return Leg
     */
    public function makeLeg($legFields = [])
    {
        /** @var LegRepository $legRepo */
        $legRepo = App::make(LegRepository::class);
        $theme = $this->fakeLegData($legFields);
        return $legRepo->create($theme);
    }

    /**
     * Get fake instance of Leg
     *
     * @param array $legFields
     * @return Leg
     */
    public function fakeLeg($legFields = [])
    {
        return new Leg($this->fakeLegData($legFields));
    }

    /**
     * Get fake data of Leg
     *
     * @param array $postFields
     * @return array
     */
    public function fakeLegData($legFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'trip_id' => $fake->randomDigitNotNull,
            'trip_leg_order' => $fake->randomDigitNotNull,
            'from_airport_id' => $fake->randomDigitNotNull,
            'to_airport_id' => $fake->randomDigitNotNull,
            'depart_date' => $fake->date('Y-m-d H:i:s'),
            'out_time' => $fake->date('Y-m-d H:i:s'),
            'off_time' => $fake->date('Y-m-d H:i:s'),
            'arrival_date' => $fake->date('Y-m-d H:i:s'),
            'on_time' => $fake->date('Y-m-d H:i:s'),
            'in_time' => $fake->date('Y-m-d H:i:s'),
            'out_fuel' => $fake->randomDigitNotNull,
            'in_fuel' => $fake->randomDigitNotNull,
            'out_hobbs' => $fake->randomDigitNotNull,
            'in_hobbs' => $fake->randomDigitNotNull,
            'out_tach' => $fake->randomDigitNotNull,
            'in_tach' => $fake->randomDigitNotNull,
            'regs' => $fake->word,
            'aircraft_id' => $fake->randomDigitNotNull,
            'registration' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $legFields);
    }
}
