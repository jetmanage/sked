<?php

use Faker\Factory as Faker;
use App\Models\Aircraft;
use App\Repositories\AircraftRepository;

trait MakeAircraftTrait
{
    /**
     * Create fake instance of Aircraft and save it in database
     *
     * @param array $aircraftFields
     * @return Aircraft
     */
    public function makeAircraft($aircraftFields = [])
    {
        /** @var AircraftRepository $aircraftRepo */
        $aircraftRepo = App::make(AircraftRepository::class);
        $theme = $this->fakeAircraftData($aircraftFields);
        return $aircraftRepo->create($theme);
    }

    /**
     * Get fake instance of Aircraft
     *
     * @param array $aircraftFields
     * @return Aircraft
     */
    public function fakeAircraft($aircraftFields = [])
    {
        return new Aircraft($this->fakeAircraftData($aircraftFields));
    }

    /**
     * Get fake data of Aircraft
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAircraftData($aircraftFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'registration' => $fake->word,
            'type_id' => $fake->word,
            'serial_number' => $fake->word,
            'name' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $aircraftFields);
    }
}
