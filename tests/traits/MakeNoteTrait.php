<?php

use Faker\Factory as Faker;
use App\Models\Note;
use App\Repositories\NoteRepository;

trait MakeNoteTrait
{
    /**
     * Create fake instance of Note and save it in database
     *
     * @param array $noteFields
     * @return Note
     */
    public function makeNote($noteFields = [])
    {
        /** @var NoteRepository $noteRepo */
        $noteRepo = App::make(NoteRepository::class);
        $theme = $this->fakeNoteData($noteFields);
        return $noteRepo->create($theme);
    }

    /**
     * Get fake instance of Note
     *
     * @param array $noteFields
     * @return Note
     */
    public function fakeNote($noteFields = [])
    {
        return new Note($this->fakeNoteData($noteFields));
    }

    /**
     * Get fake data of Note
     *
     * @param array $postFields
     * @return array
     */
    public function fakeNoteData($noteFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'ind_id' => $fake->randomDigitNotNull,
            'note_type' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $noteFields);
    }
}
