<?php

use Faker\Factory as Faker;
use App\Models\Trip;
use App\Repositories\TripRepository;

trait MakeTripTrait
{
    /**
     * Create fake instance of Trip and save it in database
     *
     * @param array $tripFields
     * @return Trip
     */
    public function makeTrip($tripFields = [])
    {
        /** @var TripRepository $tripRepo */
        $tripRepo = App::make(TripRepository::class);
        $theme = $this->fakeTripData($tripFields);
        return $tripRepo->create($theme);
    }

    /**
     * Get fake instance of Trip
     *
     * @param array $tripFields
     * @return Trip
     */
    public function fakeTrip($tripFields = [])
    {
        return new Trip($this->fakeTripData($tripFields));
    }

    /**
     * Get fake data of Trip
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTripData($tripFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'aircraft_id' => $fake->randomDigitNotNull,
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $tripFields);
    }
}
