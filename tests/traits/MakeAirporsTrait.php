<?php

use Faker\Factory as Faker;
use App\Models\Airpors;
use App\Repositories\AirporsRepository;

trait MakeAirporsTrait
{
    /**
     * Create fake instance of Airpors and save it in database
     *
     * @param array $airporsFields
     * @return Airpors
     */
    public function makeAirpors($airporsFields = [])
    {
        /** @var AirporsRepository $airporsRepo */
        $airporsRepo = App::make(AirporsRepository::class);
        $theme = $this->fakeAirporsData($airporsFields);
        return $airporsRepo->create($theme);
    }

    /**
     * Get fake instance of Airpors
     *
     * @param array $airporsFields
     * @return Airpors
     */
    public function fakeAirpors($airporsFields = [])
    {
        return new Airpors($this->fakeAirporsData($airporsFields));
    }

    /**
     * Get fake data of Airpors
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAirporsData($airporsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'airport_entry_id' => $fake->randomDigitNotNull,
            'code' => $fake->word,
            'city' => $fake->word,
            'country' => $fake->word,
            'faaCode' => $fake->word,
            'iata' => $fake->word,
            'icao' => $fake->word,
            'landingFacilityId' => $fake->word,
            'latitude' => $fake->randomDigitNotNull,
            'longitude' => $fake->randomDigitNotNull,
            'typicallyHasMetar' => $fake->word,
            'typicallyHasTaf' => $fake->word,
            'name' => $fake->word,
            'state' => $fake->word,
            'zip' => $fake->word,
            'fullCountryNameLowerCase' => $fake->word,
            'magVarWest' => $fake->randomDigitNotNull,
            'access' => $fake->word,
            'type' => $fake->word,
            'hasFuel' => $fake->word,
            'fuelTypes' => $fake->word,
            'longestRunwayLength' => $fake->randomDigitNotNull,
            'longestRunwaySurfaceType' => $fake->word,
            'nearestlandingFacilityIds' => $fake->word,
            'elevationFt' => $fake->randomDigitNotNull,
            'procedureTypes' => $fake->word,
            'towered' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $airporsFields);
    }
}
