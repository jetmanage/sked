<?php

use Faker\Factory as Faker;
use App\Models\Airport;
use App\Repositories\AirportRepository;

trait MakeAirportTrait
{
    /**
     * Create fake instance of Airport and save it in database
     *
     * @param array $airportFields
     * @return Airport
     */
    public function makeAirport($airportFields = [])
    {
        /** @var AirportRepository $airportRepo */
        $airportRepo = App::make(AirportRepository::class);
        $theme = $this->fakeAirportData($airportFields);
        return $airportRepo->create($theme);
    }

    /**
     * Get fake instance of Airport
     *
     * @param array $airportFields
     * @return Airport
     */
    public function fakeAirport($airportFields = [])
    {
        return new Airport($this->fakeAirportData($airportFields));
    }

    /**
     * Get fake data of Airport
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAirportData($airportFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'airport_entry_id' => $fake->randomDigitNotNull,
            'code' => $fake->word,
            'city' => $fake->word,
            'country' => $fake->word,
            'faaCode' => $fake->word,
            'iata' => $fake->word,
            'icao' => $fake->word,
            'landingFacilityId' => $fake->word,
            'latitude' => $fake->randomDigitNotNull,
            'longitude' => $fake->randomDigitNotNull,
            'typicallyHasMetar' => $fake->word,
            'typicallyHasTaf' => $fake->word,
            'name' => $fake->word,
            'state' => $fake->word,
            'zip' => $fake->word,
            'fullCountryNameLowerCase' => $fake->word,
            'magVarWest' => $fake->randomDigitNotNull,
            'access' => $fake->word,
            'type' => $fake->word,
            'hasFuel' => $fake->word,
            'fuelTypes' => $fake->word,
            'longestRunwayLength' => $fake->randomDigitNotNull,
            'longestRunwaySurfaceType' => $fake->word,
            'nearestlandingFacilityIds' => $fake->word,
            'elevationFt' => $fake->randomDigitNotNull,
            'procedureTypes' => $fake->word,
            'towered' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $airportFields);
    }
}
