<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PassengerApiTest extends TestCase
{
    use MakePassengerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePassenger()
    {
        $passenger = $this->fakePassengerData();
        $this->json('POST', '/api/v1/passengers', $passenger);

        $this->assertApiResponse($passenger);
    }

    /**
     * @test
     */
    public function testReadPassenger()
    {
        $passenger = $this->makePassenger();
        $this->json('GET', '/api/v1/passengers/'.$passenger->id);

        $this->assertApiResponse($passenger->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePassenger()
    {
        $passenger = $this->makePassenger();
        $editedPassenger = $this->fakePassengerData();

        $this->json('PUT', '/api/v1/passengers/'.$passenger->id, $editedPassenger);

        $this->assertApiResponse($editedPassenger);
    }

    /**
     * @test
     */
    public function testDeletePassenger()
    {
        $passenger = $this->makePassenger();
        $this->json('DELETE', '/api/v1/passengers/'.$passenger->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/passengers/'.$passenger->id);

        $this->assertResponseStatus(404);
    }
}
