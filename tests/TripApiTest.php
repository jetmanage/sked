<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TripApiTest extends TestCase
{
    use MakeTripTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTrip()
    {
        $trip = $this->fakeTripData();
        $this->json('POST', '/api/v1/trips', $trip);

        $this->assertApiResponse($trip);
    }

    /**
     * @test
     */
    public function testReadTrip()
    {
        $trip = $this->makeTrip();
        $this->json('GET', '/api/v1/trips/'.$trip->id);

        $this->assertApiResponse($trip->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTrip()
    {
        $trip = $this->makeTrip();
        $editedTrip = $this->fakeTripData();

        $this->json('PUT', '/api/v1/trips/'.$trip->id, $editedTrip);

        $this->assertApiResponse($editedTrip);
    }

    /**
     * @test
     */
    public function testDeleteTrip()
    {
        $trip = $this->makeTrip();
        $this->json('DELETE', '/api/v1/trips/'.$trip->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/trips/'.$trip->id);

        $this->assertResponseStatus(404);
    }
}
