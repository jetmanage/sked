<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IndividualApiTest extends TestCase
{
    use MakeIndividualTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateIndividual()
    {
        $individual = $this->fakeIndividualData();
        $this->json('POST', '/api/v1/individuals', $individual);

        $this->assertApiResponse($individual);
    }

    /**
     * @test
     */
    public function testReadIndividual()
    {
        $individual = $this->makeIndividual();
        $this->json('GET', '/api/v1/individuals/'.$individual->id);

        $this->assertApiResponse($individual->toArray());
    }

    /**
     * @test
     */
    public function testUpdateIndividual()
    {
        $individual = $this->makeIndividual();
        $editedIndividual = $this->fakeIndividualData();

        $this->json('PUT', '/api/v1/individuals/'.$individual->id, $editedIndividual);

        $this->assertApiResponse($editedIndividual);
    }

    /**
     * @test
     */
    public function testDeleteIndividual()
    {
        $individual = $this->makeIndividual();
        $this->json('DELETE', '/api/v1/individuals/'.$individual->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/individuals/'.$individual->id);

        $this->assertResponseStatus(404);
    }
}
