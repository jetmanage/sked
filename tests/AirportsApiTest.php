<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AirportsApiTest extends TestCase
{
    use MakeAirportsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAirports()
    {
        $airports = $this->fakeAirportsData();
        $this->json('POST', '/api/v1/airports', $airports);

        $this->assertApiResponse($airports);
    }

    /**
     * @test
     */
    public function testReadAirports()
    {
        $airports = $this->makeAirports();
        $this->json('GET', '/api/v1/airports/'.$airports->id);

        $this->assertApiResponse($airports->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAirports()
    {
        $airports = $this->makeAirports();
        $editedAirports = $this->fakeAirportsData();

        $this->json('PUT', '/api/v1/airports/'.$airports->id, $editedAirports);

        $this->assertApiResponse($editedAirports);
    }

    /**
     * @test
     */
    public function testDeleteAirports()
    {
        $airports = $this->makeAirports();
        $this->json('DELETE', '/api/v1/airports/'.$airports->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/airports/'.$airports->id);

        $this->assertResponseStatus(404);
    }
}
