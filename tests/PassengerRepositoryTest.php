<?php

use App\Models\Passenger;
use App\Repositories\PassengerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PassengerRepositoryTest extends TestCase
{
    use MakePassengerTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PassengerRepository
     */
    protected $passengerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->passengerRepo = App::make(PassengerRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePassenger()
    {
        $passenger = $this->fakePassengerData();
        $createdPassenger = $this->passengerRepo->create($passenger);
        $createdPassenger = $createdPassenger->toArray();
        $this->assertArrayHasKey('id', $createdPassenger);
        $this->assertNotNull($createdPassenger['id'], 'Created Passenger must have id specified');
        $this->assertNotNull(Passenger::find($createdPassenger['id']), 'Passenger with given id must be in DB');
        $this->assertModelData($passenger, $createdPassenger);
    }

    /**
     * @test read
     */
    public function testReadPassenger()
    {
        $passenger = $this->makePassenger();
        $dbPassenger = $this->passengerRepo->find($passenger->id);
        $dbPassenger = $dbPassenger->toArray();
        $this->assertModelData($passenger->toArray(), $dbPassenger);
    }

    /**
     * @test update
     */
    public function testUpdatePassenger()
    {
        $passenger = $this->makePassenger();
        $fakePassenger = $this->fakePassengerData();
        $updatedPassenger = $this->passengerRepo->update($fakePassenger, $passenger->id);
        $this->assertModelData($fakePassenger, $updatedPassenger->toArray());
        $dbPassenger = $this->passengerRepo->find($passenger->id);
        $this->assertModelData($fakePassenger, $dbPassenger->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePassenger()
    {
        $passenger = $this->makePassenger();
        $resp = $this->passengerRepo->delete($passenger->id);
        $this->assertTrue($resp);
        $this->assertNull(Passenger::find($passenger->id), 'Passenger should not exist in DB');
    }
}
