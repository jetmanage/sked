<?php

use App\Models\Leg;
use App\Repositories\LegRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LegRepositoryTest extends TestCase
{
    use MakeLegTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var LegRepository
     */
    protected $legRepo;

    public function setUp()
    {
        parent::setUp();
        $this->legRepo = App::make(LegRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateLeg()
    {
        $leg = $this->fakeLegData();
        $createdLeg = $this->legRepo->create($leg);
        $createdLeg = $createdLeg->toArray();
        $this->assertArrayHasKey('id', $createdLeg);
        $this->assertNotNull($createdLeg['id'], 'Created Leg must have id specified');
        $this->assertNotNull(Leg::find($createdLeg['id']), 'Leg with given id must be in DB');
        $this->assertModelData($leg, $createdLeg);
    }

    /**
     * @test read
     */
    public function testReadLeg()
    {
        $leg = $this->makeLeg();
        $dbLeg = $this->legRepo->find($leg->id);
        $dbLeg = $dbLeg->toArray();
        $this->assertModelData($leg->toArray(), $dbLeg);
    }

    /**
     * @test update
     */
    public function testUpdateLeg()
    {
        $leg = $this->makeLeg();
        $fakeLeg = $this->fakeLegData();
        $updatedLeg = $this->legRepo->update($fakeLeg, $leg->id);
        $this->assertModelData($fakeLeg, $updatedLeg->toArray());
        $dbLeg = $this->legRepo->find($leg->id);
        $this->assertModelData($fakeLeg, $dbLeg->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteLeg()
    {
        $leg = $this->makeLeg();
        $resp = $this->legRepo->delete($leg->id);
        $this->assertTrue($resp);
        $this->assertNull(Leg::find($leg->id), 'Leg should not exist in DB');
    }
}
