<?php

use App\Models\Note;
use App\Repositories\NoteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NoteRepositoryTest extends TestCase
{
    use MakeNoteTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var NoteRepository
     */
    protected $noteRepo;

    public function setUp()
    {
        parent::setUp();
        $this->noteRepo = App::make(NoteRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateNote()
    {
        $note = $this->fakeNoteData();
        $createdNote = $this->noteRepo->create($note);
        $createdNote = $createdNote->toArray();
        $this->assertArrayHasKey('id', $createdNote);
        $this->assertNotNull($createdNote['id'], 'Created Note must have id specified');
        $this->assertNotNull(Note::find($createdNote['id']), 'Note with given id must be in DB');
        $this->assertModelData($note, $createdNote);
    }

    /**
     * @test read
     */
    public function testReadNote()
    {
        $note = $this->makeNote();
        $dbNote = $this->noteRepo->find($note->id);
        $dbNote = $dbNote->toArray();
        $this->assertModelData($note->toArray(), $dbNote);
    }

    /**
     * @test update
     */
    public function testUpdateNote()
    {
        $note = $this->makeNote();
        $fakeNote = $this->fakeNoteData();
        $updatedNote = $this->noteRepo->update($fakeNote, $note->id);
        $this->assertModelData($fakeNote, $updatedNote->toArray());
        $dbNote = $this->noteRepo->find($note->id);
        $this->assertModelData($fakeNote, $dbNote->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteNote()
    {
        $note = $this->makeNote();
        $resp = $this->noteRepo->delete($note->id);
        $this->assertTrue($resp);
        $this->assertNull(Note::find($note->id), 'Note should not exist in DB');
    }
}
