const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var res = 'resources/assets/';

/**
 * This js and css is for core files
 */
mix.js(res + 'js/app.js', 'public/js')
    .sass(res + 'sass/app.scss', 'public/css');

/**
 * Copy required icons for the website
 */
mix.copy(res + 'fonts/brand-icons/brand-icons.woff2', 'public/css/');
mix.copy(res + 'fonts/brand-icons/brand-icons.woff', 'public/css/');
mix.copy(res + 'fonts/brand-icons/brand-icons.ttf', 'public/css/');
mix.copy(res + 'fonts/material-design/Material-Design-Iconic-Font.woff2', 'public/css/');
mix.copy(res + 'fonts/material-design/Material-Design-Iconic-Font.woff', 'public/css/');
mix.copy(res + 'fonts/material-design/Material-Design-Iconic-Font.ttf', 'public/css/');
mix.copy(res + 'fonts/web-icons/web-icons.woff2', 'public/css/');
mix.copy(res + 'fonts/web-icons/web-icons.woff', 'public/css/');
mix.copy(res + 'fonts/web-icons/web-icons.ttf', 'public/css/');
mix.copy(res + 'fonts/font-awesome/fontawesome-webfont.woff2', 'public/css/');
mix.copy(res + 'fonts/font-awesome/fontawesome-webfont.woff', 'public/css/');
mix.copy(res + 'fonts/font-awesome/fontawesome-webfont.ttf', 'public/css/');
mix.copy(res + 'global/jstree/throbber.gif', 'public/css/');
mix.copy(res + 'global/jstree/32px.png', 'public/css/');

/**
 * Main Site
 */
mix.styles([
    res + 'css/bootstrap.min.css',
    res + 'css/bootstrap-extend.min.css',
    res + 'css/site/uikit/icon.css',
    res + 'fonts/font-awesome/font-awesome.css',
    res + 'global/toastr/toastr.css',
    res + 'fonts/web-icons/web-icons.min.css',
    res + 'fonts/material-design/material-design.min.css',
    res + "fonts/brand-icons/brand-icons.min.css",
    res + "global/bootstrap-select/bootstrap-select.css",
    res + "global/formvalidation/formValidation.css",
    res + "global/bootstrap-datepicker/bootstrap-datepicker.css",
    res + "global/bootstrap-table/bootstrap-table.css",
    res + "global/bootstrap-treeview/bootstrap-treeview.css",
    res + "global/notie/notie.css",
    res + "css/site/style.css",
    res + "global/select2/select2.css",
    res + "webui-popover/webui-popover.css",
    res + "global/datatables-bootstrap/dataTables.bootstrap.css",
    res + "global/datatables-fixedheader/dataTables.fixedHeader.css",
    res + "global/datatables-responsive/dataTables.responsive.css",
    res + "global/jstree/jstree.min.css",
    res + "global/summernote/summernote.css",
    res + "css/site/site.css",
], 'public/css/vendor.css').sourceMaps();

mix.scripts([
    // 'summernote/dist/summernote.js',
    // 'jsgrid/dist/jsgrid.js',
    // 'clipboard/dist/clipboard.js',
    // res + "js/bootstrap.js",
    res + "js/site.js",
    res + "global/formvalidation/formValidation.min.js",
    res + "global/formvalidation/framework/bootstrap4.min.js",
    res + "global/bootstrap-table/bootstrap-table.min.js",
    res + "global/bootstrap-table/bootstrap-table-locale-all.js",
    res + "global/bootstrap-table/extensions/mobile/bootstrap-table-mobile.js",
    res + "global/bootstrap-treeview/bootstrap-treeview.min.js",
    res + "global/bootstrap-select/bootstrap-select.js",
    res + "global/bootstrap-datepicker/bootstrap-datepicker.js",
    res + "global/toastr/toastr.js",
    res + "js/lang.dist.js",
    res + "js/tables/bootstrap.js",
    res + "js/forms/validation.js",
    res + "global/notie/notie.js",
    res + "global/select2/select2.full.min.js",
    res + "global/webui-popover/jquery.webui-popover.min.js",
    res + "global/datatables/jquery.dataTables.js",
    res + "global/datatables-fixedheader/dataTables.fixedHeader.js",
    res + "global/datatables-bootstrap/dataTables.bootstrap.js",
    res + "global/datatables-responsive/dataTables.responsive.js",
    res + "global/datatables-tabletools/dataTables.tableTools.js",
    res + "global/bootstrap-select/bootstrap-select.js",
    res + "global/jstree/jstree.min.js",
    res + "js/clipboardjs/clipboard.min.js",
    res + "js/advanced/treeview.js",
    res + "js/site.js",
], 'public/js/vendor.js').sourceMaps();


/**
 * Login specific css and js
 */
mix.styles([
    res + 'css/bootstrap.min.css',
    res + 'css/bootstrap-extend.min.css',
    res + "fonts/material-design/material-design.min.css",
    res + "fonts/brand-icons/brand-icons.min.css",
    res + "fonts/font-awesome/font-awesome.css",
    res + "fonts/web-icons/web-icons.min.css",
    res + "global/toastr/toastr.css",
    res + "css/login/site.css",
    res + "css/login/login.css"
], 'public/css/login.css');

mix.scripts([
    // res + "js/bootstrap.js",
    res + "global/toastr/toastr.js"
], 'public/js/login.js');