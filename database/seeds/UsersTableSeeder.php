<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        DB::table('users')->insertGetId([
            'name' => 'test',
            'email' => 'test@sked.dev',
            'password' => bcrypt('123'),
            'remember_token' => str_random(10),
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now(),
            'country_code'=> '1',
            'phone' => '9492953949',
            'is_verified' => '1'
        ]);

        foreach (range(1,10) as $index) {
            DB::table('users')->insertGetId([
                'name' => 'test',
                'email' => $faker->userName . '@sked.dev',
                'password' => bcrypt('123'),
                'remember_token' => str_random(10),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                'country_code'=> '1',
                'phone' => '9492953949',
                'is_verified' => '1',
            ]);
        }

    }
}