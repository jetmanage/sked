<?php

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * -----------------------------------------------------------------------
     * To make the permissions work we need to follow below steps
     * - A middleware need to be defined to check permission of any user
     * - Code needs to be added to get permission from the database and add
     * into the system, which can be defined in AuthServiceProvider.
     * -----------------------------------------------------------------------
     *
     * @return void
     */
    public function run()
    {
        // Array to create multiple roles, with array's index as role name and array's
        // value as role display name.
        $roles = [
            'super-admin' => 'Super Admin',
            'admin'       => 'Admin',
        ];

        // Creating multiple roles and return array with all roles object.
        $rolesObj = Role::createRoles($roles);

        // Creating specific role objects from returned role array, which will be used
        // to assign permissions to roles.
        $superAdmin = $rolesObj['super-admin'];
        $admin = $rolesObj['admin'];

        // Array to create complete crud permissions for specific controller, with
        // array's index as permission's name and array's value as permission's
        // display name.
        $crudPermissionArray = [
            'user' => 'User crud operations.',
            'role' => 'Role crud operations.',
        ];

        // Creating multiple curd permissions for specific controllers and returning
        // array with permission's object.
        $crudPermissions = Permission::createCruds($crudPermissionArray);

        // Array with single permission, which need to be added separately.
        $permissions = [
            'dashboard'       => 'Permission to show dashboard charts.',
            'user-role'       => 'Permission to view and assign users to roles.',
            'role-permission' => 'Permission to assign permissions to roles.',
        ];

        // Creating individual permissions.
        $normalPermissions = Permission::createPermissions($permissions);

        // Assigning all crud permissions to super admin.
        $this->giveCrudPermissions($superAdmin, $crudPermissions);

        // Assigning all single permissions to super admin.
        $this->givePermissions($superAdmin, $normalPermissions);

        // Assigning all crud permissions to to admin.
        $this->giveCrudPermissions($admin, $crudPermissions);

        // Assigning all single permissions to admin.
        $this->givePermissions($admin, $normalPermissions);

        // Array which contains crud permissions name or single permission name which
        // needs to be removed from certain roles.
        $notAllow = ['user', 'role'];

        // This method will assign crud permissions to agent role, it will remove
        // permissions which are added to $notAllow array.
        //$this->giveCrudPermissions($agent, $crudPermissions, $notAllow);

        // Creating supervisor's permissions array.
        $limitedPermissions = [
            'user-display',
            'role-display',
        ];

        // Assigning specific permissions to supervisor role.
        // $this->giveNewPermissions($supervisor, $supervisorPermissions);

        // Retrieving users.
        // Assigning user one to super admin role.
        $userOne = User::find(1);
        $userOne->roles()->save($superAdmin);

        // Creating a new user.
        $superAdminUser = User::create([
            'name'     => "Super Admin",
            'email'    => "superadmin@sked.dev",
            'password' => bcrypt("123"),
            'is_verified' => '1'
        ]);

        // Assigning new user to super admin role.
        $superAdminUser->giveRoleTo($superAdmin);

        // Creating a new user.
        $adminUser = User::create([
            'name'     => "Admin",
            'email'    => "admin@sked.dev",
            'password' => bcrypt("123"),
            'is_verified' => '1'
        ]);

        // Assigning new user to admin role.
        $adminUser->giveRoleTo($admin);

    }

    /**
     * Assigning multiple crud permissions to a single role.
     *
     * @author Jesse V
     * @param  Role  $role     Model Role object.
     * @param  array $crud     Array with crud permission names
     * @param  array $notAllow Array with permission name which are not allowed.
     * @return void
     */
    public function giveCrudPermissions(Role $role, $crud, $notAllow = [])
    {
        foreach ($crud as $crudName => $objCrud) {
            if (!in_array($crudName, $notAllow)) {
                $this->givePermissions($role, $objCrud, $notAllow);
            }
        }
    }

    /**
     * Assigning roles to a single permission array.
     *
     * @author Jesse V
     * @param  Role  $role             Model Role object.
     * @param  array $singlePermission Array which contains permission object.
     * @param  array $notAllow         Array containing permission that are not allowed.
     */
    public function givePermissions(Role $role, $singlePermission, $notAllow = [])
    {
        foreach ($singlePermission as $perName => $singlePer) {
            if (!in_array($perName, $notAllow)) {
                $role->givePermissionTo($singlePer);
            }
        }
    }

    /**
     * Assigning permissions to roles by permission name in an array.
     *
     * @author Jesse V
     * @param  Role  $role     Model Role object.
     * @param  array $perArray Array with permission names only.
     */
    public function giveNewPermissions(Role $role, $perArray)
    {
        foreach ($perArray as $perName) {
            $role->givePermissionTo(Permission::getPermission($perName));
        }
    }
}