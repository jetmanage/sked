<?php
use Illuminate\Database\Seeder;
use App\Country;

class CountriesTableSeeder extends Seeder {
    public function run()
    {
        Country::insert([
            [
                'full_name' => 'Afghanistan',
                'iso_3166_2' => 'AF',
            ],
            [
                'full_name' => 'Albania',
                'iso_3166_2' => 'AL',
            ],
            [
                'full_name' => 'Algeria',
                'iso_3166_2' => 'DZ',
            ],
            [
                'full_name' => 'American Samoa',
                'iso_3166_2' => 'DS',
            ],
            [
                'full_name' => 'Andorra',
                'iso_3166_2' => 'AD',
            ],
            [
                'full_name' => 'Angola',
                'iso_3166_2' => 'AO',
            ],
            [
                'full_name' => 'Anguilla',
                'iso_3166_2' => 'AI',
            ],
            [
                'full_name' => 'Antarctica',
                'iso_3166_2' => 'AQ',
            ],
            [
                'full_name' => 'Antigua and/or Barbuda',
                'iso_3166_2' => 'AG',
            ],
            [
                'full_name' => 'Argentina',
                'iso_3166_2' => 'AR',
            ],
            [
                'full_name' => 'Armenia',
                'iso_3166_2' => 'AM',
            ],
            [
                'full_name' => 'Aruba',
                'iso_3166_2' => 'AW',
            ],
            [
                'full_name' => 'Australia',
                'iso_3166_2' => 'AU',
            ],
            [
                'full_name' => 'Austria',
                'iso_3166_2' => 'AT',
            ],
            [
                'full_name' => 'Azerbaijan',
                'iso_3166_2' => 'AZ',
            ],
            [
                'full_name' => 'Bahamas',
                'iso_3166_2' => 'BS',
            ],
            [
                'full_name' => 'Bahrain',
                'iso_3166_2' => 'BH',
            ],
            [
                'full_name' => 'Bangladesh',
                'iso_3166_2' => 'BD',
            ],
            [
                'full_name' => 'Barbados',
                'iso_3166_2' => 'BB',
            ],
            [
                'full_name' => 'Belarus',
                'iso_3166_2' => 'BY',
            ],
            [
                'full_name' => 'Belgium',
                'iso_3166_2' => 'BE',
            ],
            [
                'full_name' => 'Belize',
                'iso_3166_2' => 'BZ',
            ],
            [
                'full_name' => 'Benin',
                'iso_3166_2' => 'BJ',
            ],
            [
                'full_name' => 'Bermuda',
                'iso_3166_2' => 'BM',
            ],
            [
                'full_name' => 'Bhutan',
                'iso_3166_2' => 'BT',
            ],
            [
                'full_name' => 'Bolivia',
                'iso_3166_2' => 'BO',
            ],
            [
                'full_name' => 'Bosnia and Herzegovina',
                'iso_3166_2' => 'BA',
            ],
            [
                'full_name' => 'Botswana',
                'iso_3166_2' => 'BW',
            ],
            [
                'full_name' => 'Bouvet Island',
                'iso_3166_2' => 'BV',
            ],
            [
                'full_name' => 'Brazil',
                'iso_3166_2' => 'BR',
            ],
            [
                'full_name' => 'British lndian Ocean Territory',
                'iso_3166_2' => 'IO',
            ],
            [
                'full_name' => 'Brunei Darussalam',
                'iso_3166_2' => 'BN',
            ],
            [
                'full_name' => 'Bulgaria',
                'iso_3166_2' => 'BG',
            ],
            [
                'full_name' => 'Burkina Faso',
                'iso_3166_2' => 'BF',
            ],
            [
                'full_name' => 'Burundi',
                'iso_3166_2' => 'BI',
            ],
            [
                'full_name' => 'Cambodia',
                'iso_3166_2' => 'KH',
            ],
            [
                'full_name' => 'Cameroon',
                'iso_3166_2' => 'CM',
            ],
            [
                'full_name' => 'Canada',
                'iso_3166_2' => 'CA',
            ],
            [
                'full_name' => 'Cape Verde',
                'iso_3166_2' => 'CV',
            ],
            [
                'full_name' => 'Cayman Islands',
                'iso_3166_2' => 'KY',
            ],
            [
                'full_name' => 'Central African Republic',
                'iso_3166_2' => 'CF',
            ],
            [
                'full_name' => 'Chad',
                'iso_3166_2' => 'TD',
            ],
            [
                'full_name' => 'Chile',
                'iso_3166_2' => 'CL',
            ],
            [
                'full_name' => 'China',
                'iso_3166_2' => 'CN',
            ],
            [
                'full_name' => 'Christmas Island',
                'iso_3166_2' => 'CX',
            ],
            [
                'full_name' => 'Cocos (Keeling) Islands',
                'iso_3166_2' => 'CC',
            ],
            [
                'full_name' => 'Colombia',
                'iso_3166_2' => 'CO',
            ],
            [
                'full_name' => 'Comoros',
                'iso_3166_2' => 'KM',
            ],
            [
                'full_name' => 'Congo',
                'iso_3166_2' => 'CG',
            ],
            [
                'full_name' => 'Cook Islands',
                'iso_3166_2' => 'CK',
            ],
            [
                'full_name' => 'Costa Rica',
                'iso_3166_2' => 'CR',
            ],
            [
                'full_name' => 'Croatia (Hrvatska)',
                'iso_3166_2' => 'HR',
            ],
            [
                'full_name' => 'Cuba',
                'iso_3166_2' => 'CU',
            ],
            [
                'full_name' => 'Cyprus',
                'iso_3166_2' => 'CY',
            ],
            [
                'full_name' => 'Czech Republic',
                'iso_3166_2' => 'CZ',
            ],
            [
                'full_name' => 'Denmark',
                'iso_3166_2' => 'DK',
            ],
            [
                'full_name' => 'Djibouti',
                'iso_3166_2' => 'DJ',
            ],
            [
                'full_name' => 'Dominica',
                'iso_3166_2' => 'DM',
            ],
            [
                'full_name' => 'Dominican Republic',
                'iso_3166_2' => 'DO',
            ],
            [
                'full_name' => 'East Timor',
                'iso_3166_2' => 'TP',
            ],
            [
                'full_name' => 'Ecuador',
                'iso_3166_2' => 'EC',
            ],
            [
                'full_name' => 'Egypt',
                'iso_3166_2' => 'EG',
            ],
            [
                'full_name' => 'El Salvador',
                'iso_3166_2' => 'SV',
            ],
            [
                'full_name' => 'Equatorial Guinea',
                'iso_3166_2' => 'GQ',
            ],
            [
                'full_name' => 'Eritrea',
                'iso_3166_2' => 'ER',
            ],
            [
                'full_name' => 'Estonia',
                'iso_3166_2' => 'EE',
            ],
            [
                'full_name' => 'Ethiopia',
                'iso_3166_2' => 'ET',
            ],
            [
                'full_name' => 'Falkland Islands (Malvinas)',
                'iso_3166_2' => 'FK',
            ],
            [
                'full_name' => 'Faroe Islands',
                'iso_3166_2' => 'FO',
            ],
            [
                'full_name' => 'Fiji',
                'iso_3166_2' => 'FJ',
            ],
            [
                'full_name' => 'Finland',
                'iso_3166_2' => 'FI',
            ],
            [
                'full_name' => 'France',
                'iso_3166_2' => 'FR',
            ],
            [
                'full_name' => 'France, Metropolitan',
                'iso_3166_2' => 'FX',
            ],
            [
                'full_name' => 'French Guiana',
                'iso_3166_2' => 'GF',
            ],
            [
                'full_name' => 'French Polynesia',
                'iso_3166_2' => 'PF',
            ],
            [
                'full_name' => 'French Southern Territories',
                'iso_3166_2' => 'TF',
            ],
            [
                'full_name' => 'Gabon',
                'iso_3166_2' => 'GA',
            ],
            [
                'full_name' => 'Gambia',
                'iso_3166_2' => 'GM',
            ],
            [
                'full_name' => 'Georgia',
                'iso_3166_2' => 'GE',
            ],
            [
                'full_name' => 'Germany',
                'iso_3166_2' => 'DE',
            ],
            [
                'full_name' => 'Ghana',
                'iso_3166_2' => 'GH',
            ],
            [
                'full_name' => 'Gibraltar',
                'iso_3166_2' => 'GI',
            ],
            [
                'full_name' => 'Greece',
                'iso_3166_2' => 'GR',
            ],
            [
                'full_name' => 'Greenland',
                'iso_3166_2' => 'GL',
            ],
            [
                'full_name' => 'Grenada',
                'iso_3166_2' => 'GD',
            ],
            [
                'full_name' => 'Guadeloupe',
                'iso_3166_2' => 'GP',
            ],
            [
                'full_name' => 'Guam',
                'iso_3166_2' => 'GU',
            ],
            [
                'full_name' => 'Guatemala',
                'iso_3166_2' => 'GT',
            ],
            [
                'full_name' => 'Guinea',
                'iso_3166_2' => 'GN',
            ],
            [
                'full_name' => 'Guinea-Bissau',
                'iso_3166_2' => 'GW',
            ],
            [
                'full_name' => 'Guyana',
                'iso_3166_2' => 'GY',
            ],
            [
                'full_name' => 'Haiti',
                'iso_3166_2' => 'HT',
            ],
            [
                'full_name' => 'Heard and Mc Donald Islands',
                'iso_3166_2' => 'HM',
            ],
            [
                'full_name' => 'Honduras',
                'iso_3166_2' => 'HN',
            ],
            [
                'full_name' => 'Hong Kong',
                'iso_3166_2' => 'HK',
            ],
            [
                'full_name' => 'Hungary',
                'iso_3166_2' => 'HU',
            ],
            [
                'full_name' => 'Iceland',
                'iso_3166_2' => 'IS',
            ],
            [
                'full_name' => 'India',
                'iso_3166_2' => 'IN',
            ],
            [
                'full_name' => 'Indonesia',
                'iso_3166_2' => 'ID',
            ],
            [
                'full_name' => 'Iran (Islamic Republic of)',
                'iso_3166_2' => 'IR',
            ],
            [
                'full_name' => 'Iraq',
                'iso_3166_2' => 'IQ',
            ],
            [
                'full_name' => 'Ireland',
                'iso_3166_2' => 'IE',
            ],
            [
                'full_name' => 'Israel',
                'iso_3166_2' => 'IL',
            ],
            [
                'full_name' => 'Italy',
                'iso_3166_2' => 'IT',
            ],
            [
                'full_name' => 'Ivory Coast',
                'iso_3166_2' => 'CI',
            ],
            [
                'full_name' => 'Jamaica',
                'iso_3166_2' => 'JM',
            ],
            [
                'full_name' => 'Japan',
                'iso_3166_2' => 'JP',
            ],
            [
                'full_name' => 'Jordan',
                'iso_3166_2' => 'JO',
            ],
            [
                'full_name' => 'Kazakhstan',
                'iso_3166_2' => 'KZ',
            ],
            [
                'full_name' => 'Kenya',
                'iso_3166_2' => 'KE',
            ],
            [
                'full_name' => 'Kiribati',
                'iso_3166_2' => 'KI',
            ],
            [
                'full_name' => 'Korea, Democratic People\'s Republic of',
                'iso_3166_2' => 'KP',
            ],
            [
                'full_name' => 'Korea, Republic of',
                'iso_3166_2' => 'KR',
            ],
            [
                'full_name' => 'Kosovo',
                'iso_3166_2' => 'XK',
            ],
            [
                'full_name' => 'Kuwait',
                'iso_3166_2' => 'KW',
            ],
            [
                'full_name' => 'Kyrgyzstan',
                'iso_3166_2' => 'KG',
            ],
            [
                'full_name' => 'Lao People\'s Democratic Republic',
                'iso_3166_2' => 'LA',
            ],
            [
                'full_name' => 'Latvia',
                'iso_3166_2' => 'LV',
            ],
            [
                'full_name' => 'Lebanon',
                'iso_3166_2' => 'LB',
            ],
            [
                'full_name' => 'Lesotho',
                'iso_3166_2' => 'LS',
            ],
            [
                'full_name' => 'Liberia',
                'iso_3166_2' => 'LR',
            ],
            [
                'full_name' => 'Libyan Arab Jamahiriya',
                'iso_3166_2' => 'LY',
            ],
            [
                'full_name' => 'Liechtenstein',
                'iso_3166_2' => 'LI',
            ],
            [
                'full_name' => 'Lithuania',
                'iso_3166_2' => 'LT',
            ],
            [
                'full_name' => 'Luxembourg',
                'iso_3166_2' => 'LU',
            ],
            [
                'full_name' => 'Macau',
                'iso_3166_2' => 'MO',
            ],
            [
                'full_name' => 'Macedonia',
                'iso_3166_2' => 'MK',
            ],
            [
                'full_name' => 'Madagascar',
                'iso_3166_2' => 'MG',
            ],
            [
                'full_name' => 'Malawi',
                'iso_3166_2' => 'MW',
            ],
            [
                'full_name' => 'Malaysia',
                'iso_3166_2' => 'MY',
            ],
            [
                'full_name' => 'Maldives',
                'iso_3166_2' => 'MV',
            ],
            [
                'full_name' => 'Mali',
                'iso_3166_2' => 'ML',
            ],
            [
                'full_name' => 'Malta',
                'iso_3166_2' => 'MT',
            ],
            [
                'full_name' => 'Marshall Islands',
                'iso_3166_2' => 'MH',
            ],
            [
                'full_name' => 'Martinique',
                'iso_3166_2' => 'MQ',
            ],
            [
                'full_name' => 'Mauritania',
                'iso_3166_2' => 'MR',
            ],
            [
                'full_name' => 'Mauritius',
                'iso_3166_2' => 'MU',
            ],
            [
                'full_name' => 'Mayotte',
                'iso_3166_2' => 'TY',
            ],
            [
                'full_name' => 'Mexico',
                'iso_3166_2' => 'MX',
            ],
            [
                'full_name' => 'Micronesia, Federated States of',
                'iso_3166_2' => 'FM',
            ],
            [
                'full_name' => 'Moldova, Republic of',
                'iso_3166_2' => 'MD',
            ],
            [
                'full_name' => 'Monaco',
                'iso_3166_2' => 'MC',
            ],
            [
                'full_name' => 'Mongolia',
                'iso_3166_2' => 'MN',
            ],
            [
                'full_name' => 'Montenegro',
                'iso_3166_2' => 'ME',
            ],
            [
                'full_name' => 'Montserrat',
                'iso_3166_2' => 'MS',
            ],
            [
                'full_name' => 'Morocco',
                'iso_3166_2' => 'MA',
            ],
            [
                'full_name' => 'Mozambique',
                'iso_3166_2' => 'MZ',
            ],
            [
                'full_name' => 'Myanmar',
                'iso_3166_2' => 'MM',
            ],
            [
                'full_name' => 'Namibia',
                'iso_3166_2' => 'NA',
            ],
            [
                'full_name' => 'Nauru',
                'iso_3166_2' => 'NR',
            ],
            [
                'full_name' => 'Nepal',
                'iso_3166_2' => 'NP',
            ],
            [
                'full_name' => 'Netherlands',
                'iso_3166_2' => 'NL',
            ],
            [
                'full_name' => 'Netherlands Antilles',
                'iso_3166_2' => 'AN',
            ],
            [
                'full_name' => 'New Caledonia',
                'iso_3166_2' => 'NC',
            ],
            [
                'full_name' => 'New Zealand',
                'iso_3166_2' => 'NZ',
            ],
            [
                'full_name' => 'Nicaragua',
                'iso_3166_2' => 'NI',
            ],
            [
                'full_name' => 'Niger',
                'iso_3166_2' => 'NE',
            ],
            [
                'full_name' => 'Nigeria',
                'iso_3166_2' => 'NG',
            ],
            [
                'full_name' => 'Niue',
                'iso_3166_2' => 'NU',
            ],
            [
                'full_name' => 'Norfork Island',
                'iso_3166_2' => 'NF',
            ],
            [
                'full_name' => 'Northern Mariana Islands',
                'iso_3166_2' => 'MP',
            ],
            [
                'full_name' => 'Norway',
                'iso_3166_2' => 'NO',
            ],
            [
                'full_name' => 'Oman',
                'iso_3166_2' => 'OM',
            ],
            [
                'full_name' => 'Pakistan',
                'iso_3166_2' => 'PK',
            ],
            [
                'full_name' => 'Palau',
                'iso_3166_2' => 'PW',
            ],
            [
                'full_name' => 'Panama',
                'iso_3166_2' => 'PA',
            ],
            [
                'full_name' => 'Papua New Guinea',
                'iso_3166_2' => 'PG',
            ],
            [
                'full_name' => 'Paraguay',
                'iso_3166_2' => 'PY',
            ],
            [
                'full_name' => 'Peru',
                'iso_3166_2' => 'PE',
            ],
            [
                'full_name' => 'Philippines',
                'iso_3166_2' => 'PH',
            ],
            [
                'full_name' => 'Pitcairn',
                'iso_3166_2' => 'PN',
            ],
            [
                'full_name' => 'Poland',
                'iso_3166_2' => 'PL',
            ],
            [
                'full_name' => 'Portugal',
                'iso_3166_2' => 'PT',
            ],
            [
                'full_name' => 'Puerto Rico',
                'iso_3166_2' => 'PR',
            ],
            [
                'full_name' => 'Qatar',
                'iso_3166_2' => 'QR',
            ],
            [
                'full_name' => 'Reunion',
                'iso_3166_2' => 'RE',
            ],
            [
                'full_name' => 'Romania',
                'iso_3166_2' => 'RO',
            ],
            [
                'full_name' => 'Russian Federation',
                'iso_3166_2' => 'RU',
            ],
            [
                'full_name' => 'Rwanda',
                'iso_3166_2' => 'RW',
            ],
            [
                'full_name' => 'Saint Kitts and Nevis',
                'iso_3166_2' => 'KN',
            ],
            [
                'full_name' => 'Saint Lucia',
                'iso_3166_2' => 'LC',
            ],
            [
                'full_name' => 'Saint Vincent and the Grenadines',
                'iso_3166_2' => 'VC',
            ],
            [
                'full_name' => 'Samoa',
                'iso_3166_2' => 'WS',
            ],
            [
                'full_name' => 'San Marino',
                'iso_3166_2' => 'SM',
            ],
            [
                'full_name' => 'Sao Tome and Principe',
                'iso_3166_2' => 'ST',
            ],
            [
                'full_name' => 'Saudi Arabia',
                'iso_3166_2' => 'SA',
            ],
            [
                'full_name' => 'Senegal',
                'iso_3166_2' => 'SN',
            ],
            [
                'full_name' => 'Serbia',
                'iso_3166_2' => 'RS',
            ],
            [
                'full_name' => 'Seychelles',
                'iso_3166_2' => 'SC',
            ],
            [
                'full_name' => 'Sierra Leone',
                'iso_3166_2' => 'SL',
            ],
            [
                'full_name' => 'Singapore',
                'iso_3166_2' => 'SG',
            ],
            [
                'full_name' => 'Slovakia',
                'iso_3166_2' => 'SK',
            ],
            [
                'full_name' => 'Slovenia',
                'iso_3166_2' => 'SI',
            ],
            [
                'full_name' => 'Solomon Islands',
                'iso_3166_2' => 'SB',
            ],
            [
                'full_name' => 'Somalia',
                'iso_3166_2' => 'SO',
            ],
            [
                'full_name' => 'South Africa',
                'iso_3166_2' => 'ZA',
            ],
            [
                'full_name' => 'South Georgia South Sandwich Islands',
                'iso_3166_2' => 'GS',
            ],
            [
                'full_name' => 'Spain',
                'iso_3166_2' => 'ES',
            ],
            [
                'full_name' => 'Sri Lanka',
                'iso_3166_2' => 'LK',
            ],
            [
                'full_name' => 'St. Helena',
                'iso_3166_2' => 'SH',
            ],
            [
                'full_name' => 'St. Pierre and Miquelon',
                'iso_3166_2' => 'PM',
            ],
            [
                'full_name' => 'Sudan',
                'iso_3166_2' => 'SD',
            ],
            [
                'full_name' => 'Suriname',
                'iso_3166_2' => 'SR',
            ],
            [
                'full_name' => 'Svalbarn and Jan Mayen Islands',
                'iso_3166_2' => 'SJ',
            ],
            [
                'full_name' => 'Swaziland',
                'iso_3166_2' => 'SZ',
            ],
            [
                'full_name' => 'Sweden',
                'iso_3166_2' => 'SE',
            ],
            [
                'full_name' => 'Switzerland',
                'iso_3166_2' => 'CH',
            ],
            [
                'full_name' => 'Syrian Arab Republic',
                'iso_3166_2' => 'SY',
            ],
            [
                'full_name' => 'Taiwan',
                'iso_3166_2' => 'TW',
            ],
            [
                'full_name' => 'Tajikistan',
                'iso_3166_2' => 'TJ',
            ],
            [
                'full_name' => 'Tanzania, United Republic of',
                'iso_3166_2' => 'TZ',
            ],
            [
                'full_name' => 'Thailand',
                'iso_3166_2' => 'TH',
            ],
            [
                'full_name' => 'Togo',
                'iso_3166_2' => 'TG',
            ],
            [
                'full_name' => 'Tokelau',
                'iso_3166_2' => 'TK',
            ],
            [
                'full_name' => 'Tonga',
                'iso_3166_2' => 'TO',
            ],
            [
                'full_name' => 'Trinidad and Tobago',
                'iso_3166_2' => 'TT',
            ],
            [
                'full_name' => 'Tunisia',
                'iso_3166_2' => 'TN',
            ],
            [
                'full_name' => 'Turkey',
                'iso_3166_2' => 'TR',
            ],
            [
                'full_name' => 'Turkmenistan',
                'iso_3166_2' => 'TM',
            ],
            [
                'full_name' => 'Turks and Caicos Islands',
                'iso_3166_2' => 'TC',
            ],
            [
                'full_name' => 'Tuvalu',
                'iso_3166_2' => 'TV',
            ],
            [
                'full_name' => 'Uganda',
                'iso_3166_2' => 'UG',
            ],
            [
                'full_name' => 'Ukraine',
                'iso_3166_2' => 'UA',
            ],
            [
                'full_name' => 'United Arab Emirates',
                'iso_3166_2' => 'AE',
            ],
            [
                'full_name' => 'United Kingdom',
                'iso_3166_2' => 'GB',
            ],
            [
                'full_name' => 'United States',
                'iso_3166_2' => 'US',
            ],
            [
                'full_name' => 'United States minor outlying islands',
                'iso_3166_2' => 'UM',
            ],
            [
                'full_name' => 'Uruguay',
                'iso_3166_2' => 'UY',
            ],
            [
                'full_name' => 'Uzbekistan',
                'iso_3166_2' => 'UZ',
            ],
            [
                'full_name' => 'Vanuatu',
                'iso_3166_2' => 'VU',
            ],
            [
                'full_name' => 'Vatican City State',
                'iso_3166_2' => 'VA',
            ],
            [
                'full_name' => 'Venezuela',
                'iso_3166_2' => 'VE',
            ],
            [
                'full_name' => 'Vietnam',
                'iso_3166_2' => 'VN',
            ],
            [
                'full_name' => 'Virgin Islands (British)',
                'iso_3166_2' => 'VG',
            ],
            [
                'full_name' => 'Virgin Islands (U.S.)',
                'iso_3166_2' => 'VI',
            ],
            [
                'full_name' => 'Wallis and Futuna Islands',
                'iso_3166_2' => 'WF',
            ],
            [
                'full_name' => 'Western Sahara',
                'iso_3166_2' => 'EH',
            ],
            [
                'full_name' => 'Yemen',
                'iso_3166_2' => 'YE',
            ],
            [
                'full_name' => 'Yugoslavia',
                'iso_3166_2' => 'YU',
            ],
            [
                'full_name' => 'Zaire',
                'iso_3166_2' => 'ZR',
            ],
            [
                'full_name' => 'Zambia',
                'iso_3166_2' => 'ZM',
            ],
            [
                'full_name' => 'Zimbabwe',
                'iso_3166_2' => 'ZW',
            ],
        ]);
    }
}