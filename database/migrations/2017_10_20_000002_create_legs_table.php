<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_id')->unsigned();
            $table->integer('trip_leg_order')->unsigned('');
            $table->integer('from_airport_id')->unsigned('');
            $table->integer('to_airport_id')->unsigned('');
            $table->dateTime('depart_date');
            $table->dateTime('out_time');
            $table->dateTime('off_time');
            $table->dateTime('arrival_date');
            $table->dateTime('on_time');
            $table->dateTime('in_time');
            $table->integer('out_fuel')->unsigned();
            $table->integer('in_fuel')->unsigned();
            $table->integer('out_hobbs')->unsigned();
            $table->integer('in_hobbs')->unsigned();
            $table->integer('out_tach')->unsigned();
            $table->integer('in_tach')->unsigned();
            $table->string('regs')->default('');

            $table->integer('co_pilot_id')->unsigned()->nullable();
            $table->string('registration')->default('');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('legs', function (Blueprint $table) {
            $table->foreign('trip_id')
                ->references('id')
                ->on('trips');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legs');
    }
}

