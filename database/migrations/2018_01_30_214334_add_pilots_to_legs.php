<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPilotsToLegs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('legs', function($table) {
            $table->integer('aircraft_id')->unsigned();
            $table->integer('pilot_id')->unsigned()->nullable();
        });

        Schema::table('legs', function (Blueprint $table) {
            $table->foreign('pilot_id')
                ->references('id')
                ->on('individuals');
        });

        Schema::table('legs', function (Blueprint $table) {
            $table->foreign('co_pilot_id')
                ->references('id')
                ->on('individuals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('legs', function (Blueprint $table) {
            $table->foreign('aircraft_id')
                ->references('id')
                ->on('aircrafts');
        });
        Schema::table('legs', function($table) {
            $table->dropColumn('aircraft_id');
            $table->dropColumn('pilot_id');
        });
    }
}
