<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ind_id')->unsigned();
            $table->string('phone');
            $table->tinyInteger('phone_type');
            $table->boolean('is_textable');
            $table->boolean('is_verified');
            $table->boolean('is_primary');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('phones', function (Blueprint $table) {
            $table->foreign('ind_id')
                ->references('id')
                ->on('individuals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phones');
    }
}
