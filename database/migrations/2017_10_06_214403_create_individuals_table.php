<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndividualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('individuals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->tinyInteger('gender');
            $table->dateTime('dob');
            $table->integer('weight');
            $table->integer('height');
            $table->integer('res_country_id')->unsigned();
            $table->integer('birth_country_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('individuals', function (Blueprint $table) {
            $table->foreign('res_country_id')
                ->references('id')
                ->on('countries');

            $table->foreign('birth_country_id')
                ->references('id')
                ->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('individuals');
    }
}