<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirportsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $file = base_path().'/database/sql/airports.sql';
        exec("mysql -usked -ppMzR2JpWOtY5qkOs9ArN sked < ". $file);
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('airports')) {
            Schema::drop('airports');
        }
    }
}