<?php

class AicraftTypeConfigurationCopyRequest extends AircraftTypeCopyRequest
{

    /**
     * @var string $EngineName
     */
    protected $EngineName = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getEngineName()
    {
      return $this->EngineName;
    }

    /**
     * @param string $EngineName
     * @return AicraftTypeConfigurationCopyRequest
     */
    public function setEngineName($EngineName)
    {
      $this->EngineName = $EngineName;
      return $this;
    }

}
