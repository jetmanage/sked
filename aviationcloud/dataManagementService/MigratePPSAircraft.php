<?php

class MigratePPSAircraft
{

    /**
     * @var PPSAircraftMigrationRequest $request
     */
    protected $request = null;

    /**
     * @param PPSAircraftMigrationRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return PPSAircraftMigrationRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param PPSAircraftMigrationRequest $request
     * @return MigratePPSAircraft
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
