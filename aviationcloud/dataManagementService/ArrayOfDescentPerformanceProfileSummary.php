<?php

class ArrayOfDescentPerformanceProfileSummary implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DescentPerformanceProfileSummary[] $DescentPerformanceProfileSummary
     */
    protected $DescentPerformanceProfileSummary = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DescentPerformanceProfileSummary[]
     */
    public function getDescentPerformanceProfileSummary()
    {
      return $this->DescentPerformanceProfileSummary;
    }

    /**
     * @param DescentPerformanceProfileSummary[] $DescentPerformanceProfileSummary
     * @return ArrayOfDescentPerformanceProfileSummary
     */
    public function setDescentPerformanceProfileSummary(array $DescentPerformanceProfileSummary = null)
    {
      $this->DescentPerformanceProfileSummary = $DescentPerformanceProfileSummary;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DescentPerformanceProfileSummary[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DescentPerformanceProfileSummary
     */
    public function offsetGet($offset)
    {
      return $this->DescentPerformanceProfileSummary[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DescentPerformanceProfileSummary $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DescentPerformanceProfileSummary[] = $value;
      } else {
        $this->DescentPerformanceProfileSummary[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DescentPerformanceProfileSummary[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DescentPerformanceProfileSummary Return the current element
     */
    public function current()
    {
      return current($this->DescentPerformanceProfileSummary);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DescentPerformanceProfileSummary);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DescentPerformanceProfileSummary);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DescentPerformanceProfileSummary);
    }

    /**
     * Countable implementation
     *
     * @return DescentPerformanceProfileSummary Return count of elements
     */
    public function count()
    {
      return count($this->DescentPerformanceProfileSummary);
    }

}
