<?php

class ETOPSCertification extends AircraftCertification
{

    /**
     * @var Weight $ETOPSApproachFuel
     */
    protected $ETOPSApproachFuel = null;

    /**
     * @var Length $EmergencyFlightDistance
     */
    protected $EmergencyFlightDistance = null;

    /**
     * @var Time $EmergencyFlightTime
     */
    protected $EmergencyFlightTime = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getETOPSApproachFuel()
    {
      return $this->ETOPSApproachFuel;
    }

    /**
     * @param Weight $ETOPSApproachFuel
     * @return ETOPSCertification
     */
    public function setETOPSApproachFuel($ETOPSApproachFuel)
    {
      $this->ETOPSApproachFuel = $ETOPSApproachFuel;
      return $this;
    }

    /**
     * @return Length
     */
    public function getEmergencyFlightDistance()
    {
      return $this->EmergencyFlightDistance;
    }

    /**
     * @param Length $EmergencyFlightDistance
     * @return ETOPSCertification
     */
    public function setEmergencyFlightDistance($EmergencyFlightDistance)
    {
      $this->EmergencyFlightDistance = $EmergencyFlightDistance;
      return $this;
    }

    /**
     * @return Time
     */
    public function getEmergencyFlightTime()
    {
      return $this->EmergencyFlightTime;
    }

    /**
     * @param Time $EmergencyFlightTime
     * @return ETOPSCertification
     */
    public function setEmergencyFlightTime($EmergencyFlightTime)
    {
      $this->EmergencyFlightTime = $EmergencyFlightTime;
      return $this;
    }

}
