<?php

class AircraftStructureUnit
{

    /**
     * @var string $Identifier
     */
    protected $Identifier = null;

    /**
     * @var ArmDefinition $Arm
     */
    protected $Arm = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
      return $this->Identifier;
    }

    /**
     * @param string $Identifier
     * @return AircraftStructureUnit
     */
    public function setIdentifier($Identifier)
    {
      $this->Identifier = $Identifier;
      return $this;
    }

    /**
     * @return ArmDefinition
     */
    public function getArm()
    {
      return $this->Arm;
    }

    /**
     * @param ArmDefinition $Arm
     * @return AircraftStructureUnit
     */
    public function setArm($Arm)
    {
      $this->Arm = $Arm;
      return $this;
    }

}
