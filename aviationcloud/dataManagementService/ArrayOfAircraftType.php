<?php

class ArrayOfAircraftType implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AircraftType[] $AircraftType
     */
    protected $AircraftType = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftType[]
     */
    public function getAircraftType()
    {
      return $this->AircraftType;
    }

    /**
     * @param AircraftType[] $AircraftType
     * @return ArrayOfAircraftType
     */
    public function setAircraftType(array $AircraftType = null)
    {
      $this->AircraftType = $AircraftType;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AircraftType[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AircraftType
     */
    public function offsetGet($offset)
    {
      return $this->AircraftType[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AircraftType $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AircraftType[] = $value;
      } else {
        $this->AircraftType[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AircraftType[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AircraftType Return the current element
     */
    public function current()
    {
      return current($this->AircraftType);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AircraftType);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AircraftType);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AircraftType);
    }

    /**
     * Countable implementation
     *
     * @return AircraftType Return count of elements
     */
    public function count()
    {
      return count($this->AircraftType);
    }

}
