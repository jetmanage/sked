<?php

class AircraftEnvelopePoint
{

    /**
     * @var float $ArmLimit
     */
    protected $ArmLimit = null;

    /**
     * @var Weight $Weight
     */
    protected $Weight = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getArmLimit()
    {
      return $this->ArmLimit;
    }

    /**
     * @param float $ArmLimit
     * @return AircraftEnvelopePoint
     */
    public function setArmLimit($ArmLimit)
    {
      $this->ArmLimit = $ArmLimit;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getWeight()
    {
      return $this->Weight;
    }

    /**
     * @param Weight $Weight
     * @return AircraftEnvelopePoint
     */
    public function setWeight($Weight)
    {
      $this->Weight = $Weight;
      return $this;
    }

}
