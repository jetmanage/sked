<?php

class DeleteAccount
{

    /**
     * @var RequestBase $request
     */
    protected $request = null;

    /**
     * @param RequestBase $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return RequestBase
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param RequestBase $request
     * @return DeleteAccount
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
