<?php

class ArrayOfByAltitudeFuelFlow implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ByAltitudeFuelFlow[] $ByAltitudeFuelFlow
     */
    protected $ByAltitudeFuelFlow = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ByAltitudeFuelFlow[]
     */
    public function getByAltitudeFuelFlow()
    {
      return $this->ByAltitudeFuelFlow;
    }

    /**
     * @param ByAltitudeFuelFlow[] $ByAltitudeFuelFlow
     * @return ArrayOfByAltitudeFuelFlow
     */
    public function setByAltitudeFuelFlow(array $ByAltitudeFuelFlow = null)
    {
      $this->ByAltitudeFuelFlow = $ByAltitudeFuelFlow;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ByAltitudeFuelFlow[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ByAltitudeFuelFlow
     */
    public function offsetGet($offset)
    {
      return $this->ByAltitudeFuelFlow[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ByAltitudeFuelFlow $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ByAltitudeFuelFlow[] = $value;
      } else {
        $this->ByAltitudeFuelFlow[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ByAltitudeFuelFlow[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ByAltitudeFuelFlow Return the current element
     */
    public function current()
    {
      return current($this->ByAltitudeFuelFlow);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ByAltitudeFuelFlow);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ByAltitudeFuelFlow);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ByAltitudeFuelFlow);
    }

    /**
     * Countable implementation
     *
     * @return ByAltitudeFuelFlow Return count of elements
     */
    public function count()
    {
      return count($this->ByAltitudeFuelFlow);
    }

}
