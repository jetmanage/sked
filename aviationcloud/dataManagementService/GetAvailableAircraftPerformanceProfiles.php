<?php

class GetAvailableAircraftPerformanceProfiles
{

    /**
     * @var PerformanceProfileListRetrivalRequest $request
     */
    protected $request = null;

    /**
     * @param PerformanceProfileListRetrivalRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return PerformanceProfileListRetrivalRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param PerformanceProfileListRetrivalRequest $request
     * @return GetAvailableAircraftPerformanceProfiles
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
