<?php

class ListOFPLayouts
{

    /**
     * @var OFPLayoutListRequest $aRequest
     */
    protected $aRequest = null;

    /**
     * @param OFPLayoutListRequest $aRequest
     */
    public function __construct($aRequest)
    {
      $this->aRequest = $aRequest;
    }

    /**
     * @return OFPLayoutListRequest
     */
    public function getARequest()
    {
      return $this->aRequest;
    }

    /**
     * @param OFPLayoutListRequest $aRequest
     * @return ListOFPLayouts
     */
    public function setARequest($aRequest)
    {
      $this->aRequest = $aRequest;
      return $this;
    }

}
