<?php

class DeleteAircraft
{

    /**
     * @var AircraftDeletionRequest $request
     */
    protected $request = null;

    /**
     * @param AircraftDeletionRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AircraftDeletionRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AircraftDeletionRequest $request
     * @return DeleteAircraft
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
