<?php

class DataManagementService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array(
        'CreateOFPLayout' => '\\CreateOFPLayout',
        'OFPLayoutCreateRequest' => '\\OFPLayoutCreateRequest',
        'RequestBase' => '\\RequestBase',
        'OFPLayout' => '\\OFPLayout',
        'CreateOFPLayoutResponse' => '\\CreateOFPLayoutResponse',
        'OFPLayoutCreateResponse' => '\\OFPLayoutCreateResponse',
        'UpdateOFPLayout' => '\\UpdateOFPLayout',
        'OFPLayoutUpdateRequest' => '\\OFPLayoutUpdateRequest',
        'UpdateOFPLayoutResponse' => '\\UpdateOFPLayoutResponse',
        'OFPLayoutUpdateResonse' => '\\OFPLayoutUpdateResonse',
        'DeleteOFPLayout' => '\\DeleteOFPLayout',
        'OFPLayoutDeleteRequest' => '\\OFPLayoutDeleteRequest',
        'DeleteOFPLayoutResponse' => '\\DeleteOFPLayoutResponse',
        'OFPLayoutDeleteResponse' => '\\OFPLayoutDeleteResponse',
        'ListOFPLayouts' => '\\ListOFPLayouts',
        'OFPLayoutListRequest' => '\\OFPLayoutListRequest',
        'ListOFPLayoutsResponse' => '\\ListOFPLayoutsResponse',
        'OFPLayoutListResonse' => '\\OFPLayoutListResonse',
        'ArrayOfOFPLayout' => '\\ArrayOfOFPLayout',
        'MigratePPSAircraft' => '\\MigratePPSAircraft',
        'PPSAircraftMigrationRequest' => '\\PPSAircraftMigrationRequest',
        'MigratePPSAircraftResponse' => '\\MigratePPSAircraftResponse',
        'GetAircraftTypeNames' => '\\GetAircraftTypeNames',
        'AircraftTypeRetrivalRequest' => '\\AircraftTypeRetrivalRequest',
        'GetAircraftTypeNamesResponse' => '\\GetAircraftTypeNamesResponse',
        'GetAircraft' => '\\GetAircraft',
        'AircraftRetrivalRequest' => '\\AircraftRetrivalRequest',
        'GetAircraftResponse' => '\\GetAircraftResponse',
        'Aircraft' => '\\Aircraft',
        'AircraftATCInfo' => '\\AircraftATCInfo',
        'AircraftEquipment' => '\\AircraftEquipment',
        'PerformanceBasedNavigationCapabilities' => '\\PerformanceBasedNavigationCapabilities',
        'RadioAndNavigationEquipment' => '\\RadioAndNavigationEquipment',
        'ATCACARSEquipment' => '\\ATCACARSEquipment',
        'CPDLCEquipment' => '\\CPDLCEquipment',
        'RTFEquipment' => '\\RTFEquipment',
        'SSREquipment' => '\\SSREquipment',
        'ADSCapabilities' => '\\ADSCapabilities',
        'ModeSCapabilities' => '\\ModeSCapabilities',
        'AircraftFrequencyAvailability' => '\\AircraftFrequencyAvailability',
        'AircraftLifeJacketEquipment' => '\\AircraftLifeJacketEquipment',
        'RNAVPreferentialRoutes' => '\\RNAVPreferentialRoutes',
        'AircraftSurvivalEquipment' => '\\AircraftSurvivalEquipment',
        'AircraftPerformanceData' => '\\AircraftPerformanceData',
        'ArrayOfAircraftPerformanceProfile' => '\\ArrayOfAircraftPerformanceProfile',
        'AircraftPerformanceProfile' => '\\AircraftPerformanceProfile',
        'BiasedClimbPerformanceProfile' => '\\BiasedClimbPerformanceProfile',
        'ClimbProfile' => '\\ClimbProfile',
        'DiscreteClimbProfile' => '\\DiscreteClimbProfile',
        'ArrayOfClimbDescendDataSeries' => '\\ArrayOfClimbDescendDataSeries',
        'ClimbDescendDataSeries' => '\\ClimbDescendDataSeries',
        'PerformanceDataSeries' => '\\PerformanceDataSeries',
        'Weight' => '\\Weight',
        'HoldingDataSeries' => '\\HoldingDataSeries',
        'ArrayOfHoldingDataPoint' => '\\ArrayOfHoldingDataPoint',
        'HoldingDataPoint' => '\\HoldingDataPoint',
        'PerformanceDataPoint' => '\\PerformanceDataPoint',
        'ISAPoint' => '\\ISAPoint',
        'ClimbDescendDataPoint' => '\\ClimbDescendDataPoint',
        'ClimbDescendData' => '\\ClimbDescendData',
        'Length' => '\\Length',
        'Time' => '\\Time',
        'CruisePerformanceDataPoint' => '\\CruisePerformanceDataPoint',
        'AircraftCruisePerformanceData' => '\\AircraftCruisePerformanceData',
        'HoldingData' => '\\HoldingData',
        'CruisePerformanceDataSeries' => '\\CruisePerformanceDataSeries',
        'ArrayOfCruisePerformanceDataPoint' => '\\ArrayOfCruisePerformanceDataPoint',
        'ArrayOfClimbDescendDataPoint' => '\\ArrayOfClimbDescendDataPoint',
        'SimpleClimbProfile' => '\\SimpleClimbProfile',
        'Speed' => '\\Speed',
        'ByAltitudeClimbProfile' => '\\ByAltitudeClimbProfile',
        'ArrayOfByAltitudeFuelFlow' => '\\ArrayOfByAltitudeFuelFlow',
        'ByAltitudeFuelFlow' => '\\ByAltitudeFuelFlow',
        'ArrayOfByAltitudeClimbPerformance' => '\\ArrayOfByAltitudeClimbPerformance',
        'ByAltitudeClimbPerformance' => '\\ByAltitudeClimbPerformance',
        'BiasedCruisePerformanceProfile' => '\\BiasedCruisePerformanceProfile',
        'CruiseProfile' => '\\CruiseProfile',
        'DiscreteCruiseProfile' => '\\DiscreteCruiseProfile',
        'ArrayOfCruisePerformanceDataSeries' => '\\ArrayOfCruisePerformanceDataSeries',
        'ArrayOfWeightLimitation' => '\\ArrayOfWeightLimitation',
        'WeightLimitation' => '\\WeightLimitation',
        'ETOPSCruiseProfile' => '\\ETOPSCruiseProfile',
        'SimpleCruiseProfile' => '\\SimpleCruiseProfile',
        'Altitude' => '\\Altitude',
        'ByAltitudeCruiseProfile' => '\\ByAltitudeCruiseProfile',
        'ArrayOfByAltitudeCruisePerformance' => '\\ArrayOfByAltitudeCruisePerformance',
        'ByAltitudeCruisePerformance' => '\\ByAltitudeCruisePerformance',
        'BiasedDescentProfile' => '\\BiasedDescentProfile',
        'DescentProfile' => '\\DescentProfile',
        'DiscreteDescentProfile' => '\\DiscreteDescentProfile',
        'SimpleDescentProfile' => '\\SimpleDescentProfile',
        'ByAltitudeDescentProfile' => '\\ByAltitudeDescentProfile',
        'ArrayOfByAltitudeDescentPerformance' => '\\ArrayOfByAltitudeDescentPerformance',
        'ByAltitudeDescentPerformance' => '\\ByAltitudeDescentPerformance',
        'BiasedHoldingPerformanceProfile' => '\\BiasedHoldingPerformanceProfile',
        'HoldingProfile' => '\\HoldingProfile',
        'DiscreteHoldingProfile' => '\\DiscreteHoldingProfile',
        'ArrayOfHoldingDataSeries' => '\\ArrayOfHoldingDataSeries',
        'ReferencedPerformanceProfile' => '\\ReferencedPerformanceProfile',
        'AircraftStructure' => '\\AircraftStructure',
        'FuelTankDefinition' => '\\FuelTankDefinition',
        'SimpleFuelTankDefinition' => '\\SimpleFuelTankDefinition',
        'FuelTankListDefinition' => '\\FuelTankListDefinition',
        'ArrayOfFuelTank' => '\\ArrayOfFuelTank',
        'FuelTank' => '\\FuelTank',
        'AircraftStructureUnit' => '\\AircraftStructureUnit',
        'ArmDefinition' => '\\ArmDefinition',
        'StaticArmDefinition' => '\\StaticArmDefinition',
        'DynamicArmDefinition' => '\\DynamicArmDefinition',
        'ArrayOfWeightDependentArm' => '\\ArrayOfWeightDependentArm',
        'WeightDependentArm' => '\\WeightDependentArm',
        'PassengerCompartment' => '\\PassengerCompartment',
        'CargoCompartment' => '\\CargoCompartment',
        'CrewCompartment' => '\\CrewCompartment',
        'ArrayOfMassAndBalanceConfiguration' => '\\ArrayOfMassAndBalanceConfiguration',
        'MassAndBalanceConfiguration' => '\\MassAndBalanceConfiguration',
        'SimpleMassAndBalanceConfiguration' => '\\SimpleMassAndBalanceConfiguration',
        'MassAndBalanceConfigurationBase' => '\\MassAndBalanceConfigurationBase',
        'CompartmentBasedMassAndBalanceConfiguration' => '\\CompartmentBasedMassAndBalanceConfiguration',
        'ArrayOfPassengerCompartment' => '\\ArrayOfPassengerCompartment',
        'ArrayOfCargoCompartment' => '\\ArrayOfCargoCompartment',
        'ArrayOfCrewCompartment' => '\\ArrayOfCrewCompartment',
        'DefaultWeightParameters' => '\\DefaultWeightParameters',
        'ReferencedMassAndBalanceConfiguration' => '\\ReferencedMassAndBalanceConfiguration',
        'UnspecifiedMassAndBalance' => '\\UnspecifiedMassAndBalance',
        'AircraftEnvelope' => '\\AircraftEnvelope',
        'ArrayOfAircraftEnvelopePoint' => '\\ArrayOfAircraftEnvelopePoint',
        'AircraftEnvelopePoint' => '\\AircraftEnvelopePoint',
        'AircraftEngineSpecification' => '\\AircraftEngineSpecification',
        'AircraftType' => '\\AircraftType',
        'EngineDetails' => '\\EngineDetails',
        'ArrayOfAircraftCertification' => '\\ArrayOfAircraftCertification',
        'AircraftCertification' => '\\AircraftCertification',
        'ETOPSCertification' => '\\ETOPSCertification',
        'GetAircraftTypes' => '\\GetAircraftTypes',
        'GetAircraftTypesResponse' => '\\GetAircraftTypesResponse',
        'ArrayOfAircraftType' => '\\ArrayOfAircraftType',
        'GetAircraftTypeConfiguration' => '\\GetAircraftTypeConfiguration',
        'AircaftTypeConfigurationRetrivalRequest' => '\\AircaftTypeConfigurationRetrivalRequest',
        'GetAircraftTypeConfigurationResponse' => '\\GetAircraftTypeConfigurationResponse',
        'GetAicraftTypeConfigurationIdentifiers' => '\\GetAicraftTypeConfigurationIdentifiers',
        'GetAicraftTypeConfigurationIdentifiersResponse' => '\\GetAicraftTypeConfigurationIdentifiersResponse',
        'ArrayOfAicractTypeConfigurationIdentifier' => '\\ArrayOfAicractTypeConfigurationIdentifier',
        'AicractTypeConfigurationIdentifier' => '\\AicractTypeConfigurationIdentifier',
        'GetRegisteredAircrafts' => '\\GetRegisteredAircrafts',
        'GetRegisteredAircraftsResponse' => '\\GetRegisteredAircraftsResponse',
        'ArrayOfAircraft' => '\\ArrayOfAircraft',
        'GetAllRegisteredTailNumbers' => '\\GetAllRegisteredTailNumbers',
        'GetAllRegisteredTailNumbersResponse' => '\\GetAllRegisteredTailNumbersResponse',
        'DeleteAircraft' => '\\DeleteAircraft',
        'AircraftDeletionRequest' => '\\AircraftDeletionRequest',
        'DeleteAircraftResponse' => '\\DeleteAircraftResponse',
        'RegisterAircraft' => '\\RegisterAircraft',
        'AircraftRegistrationRequest' => '\\AircraftRegistrationRequest',
        'RegisterAircraftResponse' => '\\RegisterAircraftResponse',
        'RegisterAircraftType' => '\\RegisterAircraftType',
        'AircraftTypeRegistrationRequest' => '\\AircraftTypeRegistrationRequest',
        'RegisterAircraftTypeResponse' => '\\RegisterAircraftTypeResponse',
        'RegisterAircraftTypeConfiguration' => '\\RegisterAircraftTypeConfiguration',
        'AircraftTypeConfigurationRegistrationRequest' => '\\AircraftTypeConfigurationRegistrationRequest',
        'RegisterAircraftTypeConfigurationResponse' => '\\RegisterAircraftTypeConfigurationResponse',
        'CopyAicraftType' => '\\CopyAicraftType',
        'AircraftTypeCopyRequest' => '\\AircraftTypeCopyRequest',
        'AicraftTypeConfigurationCopyRequest' => '\\AicraftTypeConfigurationCopyRequest',
        'CopyAicraftTypeResponse' => '\\CopyAicraftTypeResponse',
        'CopyAicraftTypeConfiguration' => '\\CopyAicraftTypeConfiguration',
        'CopyAicraftTypeConfigurationResponse' => '\\CopyAicraftTypeConfigurationResponse',
        'UpdateAircraftTypeConfiguration' => '\\UpdateAircraftTypeConfiguration',
        'UpdateAircraftTypeConfigurationResponse' => '\\UpdateAircraftTypeConfigurationResponse',
        'RetrieveAircraftMetaData' => '\\RetrieveAircraftMetaData',
        'AircraftMetaDataRetrivalRequest' => '\\AircraftMetaDataRetrivalRequest',
        'AircraftSpecification' => '\\AircraftSpecification',
        'TailnumberAircraftSpecification' => '\\TailnumberAircraftSpecification',
        'ICAOIdentAircraftSpecification' => '\\ICAOIdentAircraftSpecification',
        'EmbeddedAircraftSpecification' => '\\EmbeddedAircraftSpecification',
        'AircraftTypeConfigurationSpecification' => '\\AircraftTypeConfigurationSpecification',
        'SimplifiedAircraftSpecification' => '\\SimplifiedAircraftSpecification',
        'UUIDAircraftSpecification' => '\\UUIDAircraftSpecification',
        'ByAltitudeAircraftSpecification' => '\\ByAltitudeAircraftSpecification',
        'ArrayOfByAltitudeClimbProfile' => '\\ArrayOfByAltitudeClimbProfile',
        'ArrayOfByAltitudeCruiseProfile' => '\\ArrayOfByAltitudeCruiseProfile',
        'ArrayOfByAltitudeDescentProfile' => '\\ArrayOfByAltitudeDescentProfile',
        'RetrieveAircraftMetaDataResponse' => '\\RetrieveAircraftMetaDataResponse',
        'AircraftMetaDataRetrivalResponse' => '\\AircraftMetaDataRetrivalResponse',
        'AircraftMetaData' => '\\AircraftMetaData',
        'AircraftTypeDefinition' => '\\AircraftTypeDefinition',
        'ArrayOfClimbPerformanceProfileSummary' => '\\ArrayOfClimbPerformanceProfileSummary',
        'ClimbPerformanceProfileSummary' => '\\ClimbPerformanceProfileSummary',
        'PerformanceProfileSummary' => '\\PerformanceProfileSummary',
        'CruisePerformancePorifleSummary' => '\\CruisePerformancePorifleSummary',
        'DescentPerformanceProfileSummary' => '\\DescentPerformanceProfileSummary',
        'ETOPSProfileSummary' => '\\ETOPSProfileSummary',
        'ArrayOfCruisePerformancePorifleSummary' => '\\ArrayOfCruisePerformancePorifleSummary',
        'ArrayOfDescentPerformanceProfileSummary' => '\\ArrayOfDescentPerformanceProfileSummary',
        'ArrayOfETOPSProfileSummary' => '\\ArrayOfETOPSProfileSummary',
        'ArrayOfMassAndBalanceProfileSummary' => '\\ArrayOfMassAndBalanceProfileSummary',
        'MassAndBalanceProfileSummary' => '\\MassAndBalanceProfileSummary',
        'GetAvailableAircraftPerformanceProfiles' => '\\GetAvailableAircraftPerformanceProfiles',
        'PerformanceProfileListRetrivalRequest' => '\\PerformanceProfileListRetrivalRequest',
        'GetAvailableAircraftPerformanceProfilesResponse' => '\\GetAvailableAircraftPerformanceProfilesResponse',
        'AircraftPerformanceProfileListResponse' => '\\AircraftPerformanceProfileListResponse',
        'ArrayOfAircraftPerformanceProfileIdentifier' => '\\ArrayOfAircraftPerformanceProfileIdentifier',
        'AircraftPerformanceProfileIdentifier' => '\\AircraftPerformanceProfileIdentifier',
        'CreateAccount' => '\\CreateAccount',
        'AccountCreationRequest' => '\\AccountCreationRequest',
        'CreateAccountResponse' => '\\CreateAccountResponse',
        'Account' => '\\Account',
        'GetAccounts' => '\\GetAccounts',
        'GetAccountsResponse' => '\\GetAccountsResponse',
        'ArrayOfAccount' => '\\ArrayOfAccount',
        'DeleteAccount' => '\\DeleteAccount',
        'DeleteAccountResponse' => '\\DeleteAccountResponse',
        'SearchForAccount' => '\\SearchForAccount',
        'AccountSearchRequest' => '\\AccountSearchRequest',
        'SearchForAccountResponse' => '\\SearchForAccountResponse',
        'ArrayOfstring' => '\\ArrayOfstring',
        'TypeReferencedMassAndBalanceConfiguration' => '\\TypeReferencedMassAndBalanceConfiguration',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
        foreach (self::$classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }
        $options = array_merge(array(
            'features' => 1,
        ), $options);
        if (!$wsdl) {
            $wsdl = 'https://testservices.aviationcloud.aero/DataManagementService.svc?singleWsdl';
        }
        parent::__construct($wsdl, $options);
    }

    /**
     *
     * Inserts a new layout into the database.
     *
     * <param name="aRequest">Contains the layout to be inserted. Note that we choose the layout number so this field will not be used
     * The inserted layout. Note that the layout number is now set
     *
     * @param CreateOFPLayout $parameters
     * @return CreateOFPLayoutResponse
     */
    public function CreateOFPLayout(CreateOFPLayout $parameters)
    {
        return $this->__soapCall('CreateOFPLayout', array($parameters));
    }

    /**
     *
     * Update an already exsisting layout
     *
     * <param name="aRequest">Contains the layout to be updated
     *
     *
     * @param UpdateOFPLayout $parameters
     * @return UpdateOFPLayoutResponse
     */
    public function UpdateOFPLayout(UpdateOFPLayout $parameters)
    {
        return $this->__soapCall('UpdateOFPLayout', array($parameters));
    }

    /**
     *
     * Delete a layout
     *
     * <param name="aRequest">Contains the layout to be deleted
     *
     *
     * @param DeleteOFPLayout $parameters
     * @return DeleteOFPLayoutResponse
     */
    public function DeleteOFPLayout(DeleteOFPLayout $parameters)
    {
        return $this->__soapCall('DeleteOFPLayout', array($parameters));
    }

    /**
     *
     * List all layouts
     *
     * <param name="aRequest">
     *
     *
     * @param ListOFPLayouts $parameters
     * @return ListOFPLayoutsResponse
     */
    public function ListOFPLayouts(ListOFPLayouts $parameters)
    {
        return $this->__soapCall('ListOFPLayouts', array($parameters));
    }

    /**
     * @param MigratePPSAircraft $parameters
     * @return MigratePPSAircraftResponse
     */
    public function MigratePPSAircraft(MigratePPSAircraft $parameters)
    {
        return $this->__soapCall('MigratePPSAircraft', array($parameters));
    }

    /**
     *
     * Get names of aircraft type configuration in the specified data level matching the specified search string
     *
     * The aircraft type retrieval request. Account ID must be supplied if data level is set to account. If no search string is
     * specified all aircraft type names at the specified data level is returned
     *
     *
     * @param GetAircraftTypeNames $parameters
     * @return GetAircraftTypeNamesResponse
     */
    public function GetAircraftTypeNames(GetAircraftTypeNames $parameters)
    {
        return $this->__soapCall('GetAircraftTypeNames', array($parameters));
    }

    /**
     *
     * Gets the aircraft specified in the request.
     *
     * The aircraft retrieval request. An account ID must be provided
     * An aircraft with the specified tailnumber
     *
     * @param GetAircraft $parameters
     * @return GetAircraftResponse
     */
    public function GetAircraft(GetAircraft $parameters)
    {
        return $this->__soapCall('GetAircraft', array($parameters));
    }

    /**
     *
     * Get aircraft types at the specified data level matching the specified search string
     *
     * Request specifying data level and search string
     * Aircraft types matching the request
     *
     * @param GetAircraftTypes $parameters
     * @return GetAircraftTypesResponse
     */
    public function GetAircraftTypes(GetAircraftTypes $parameters)
    {
        return $this->__soapCall('GetAircraftTypes', array($parameters));
    }

    /**
     *
     * Gets an aircraft type configuration matching the specified request
     *
     * Request containing aircraft engine name, type name and data level
     * An aircraft type configuration matching the specified request or NULL if non found
     *
     * @param GetAircraftTypeConfiguration $parameters
     * @return GetAircraftTypeConfigurationResponse
     */
    public function GetAircraftTypeConfiguration(GetAircraftTypeConfiguration $parameters)
    {
        return $this->__soapCall('GetAircraftTypeConfiguration', array($parameters));
    }

    /**
     *
     * Retrieves a list of aircraft type configuration matching the specified request
     *
     * A request containing an aircraft engine name type name and datalevel. Search string may be empty
     *
     *
     * @param GetAicraftTypeConfigurationIdentifiers $parameters
     * @return GetAicraftTypeConfigurationIdentifiersResponse
     */
    public function GetAicraftTypeConfigurationIdentifiers(GetAicraftTypeConfigurationIdentifiers $parameters)
    {
        return $this->__soapCall('GetAicraftTypeConfigurationIdentifiers', array($parameters));
    }

    /**
     *
     * Get aircrafts matching the specified request. The service allows a maximum of 10 aircrafts to be
     * retrieving in one request.
     *
     * A request specifying a search string, which may be empty.
     * A list of aircraft containing no more than 10 elements
     *
     * @param GetRegisteredAircrafts $parameters
     * @return GetRegisteredAircraftsResponse
     */
    public function GetRegisteredAircrafts(GetRegisteredAircrafts $parameters)
    {
        return $this->__soapCall('GetRegisteredAircrafts', array($parameters));
    }

    /**
     *
     * Get all registered tailnumbers for a given account matching the specified request
     *
     * A request which may specify a tailnumber search string
     * A list of tailnumbers registered to a given account matching the optional tail number search string
     *
     * @param GetAllRegisteredTailNumbers $parameters
     * @return GetAllRegisteredTailNumbersResponse
     */
    public function GetAllRegisteredTailNumbers(GetAllRegisteredTailNumbers $parameters)
    {
        return $this->__soapCall('GetAllRegisteredTailNumbers', array($parameters));
    }

    /**
     *
     * Deletes the aircraft with the specified tailnumber
     *
     * A request specifying the tailnumber to delete.
     *
     * @param DeleteAircraft $parameters
     * @return DeleteAircraftResponse
     */
    public function DeleteAircraft(DeleteAircraft $parameters)
    {
        return $this->__soapCall('DeleteAircraft', array($parameters));
    }

    /**
     *
     * Registers an aircraft with the services
     *
     * A request containing aircraft data, engine name and aircraft type name. The services must be
     * able to locate a registered aircraft type configuration with the given engine and type name at the specified data level
     *
     * @param RegisterAircraft $parameters
     * @return RegisterAircraftResponse
     */
    public function RegisterAircraft(RegisterAircraft $parameters)
    {
        return $this->__soapCall('RegisterAircraft', array($parameters));
    }

    /**
     *
     * Registers an aircraft type. Data level "global" is only allowed for AFS administrators and privileged partners. If data level
     * is account an account ID for which the calling partner has authorization must be provided
     *
     * Request containing aircraft type data, data level and optional account ID
     *
     * @param RegisterAircraftType $parameters
     * @return RegisterAircraftTypeResponse
     */
    public function RegisterAircraftType(RegisterAircraftType $parameters)
    {
        return $this->__soapCall('RegisterAircraftType', array($parameters));
    }

    /**
     *
     * Registers an aircraft type configuration. Data level "global" is only allowed for AFS administrators and privileged partners. If data level
     * is account an account ID for which the calling partner has authorization must be provided
     *
     * Request containing aircraft type configuration data, data level and optional account ID
     *
     * @param RegisterAircraftTypeConfiguration $parameters
     * @return RegisterAircraftTypeConfigurationResponse
     */
    public function RegisterAircraftTypeConfiguration(RegisterAircraftTypeConfiguration $parameters)
    {
        return $this->__soapCall('RegisterAircraftTypeConfiguration', array($parameters));
    }

    /**
     *
     * Copies aircraft type from one repository to another (e.g. from global repository to account repository)
     *
     * Request containing source level, target level and aircraft type name
     *
     * @param CopyAicraftType $parameters
     * @return CopyAicraftTypeResponse
     */
    public function CopyAicraftType(CopyAicraftType $parameters)
    {
        return $this->__soapCall('CopyAicraftType', array($parameters));
    }

    /**
     *
     * Copies aircraft type configuration from one repository to another (e.g. from global repository to account repository)
     *
     * Request containing source level, target level and aircraft type name
     *
     * @param CopyAicraftTypeConfiguration $parameters
     * @return CopyAicraftTypeConfigurationResponse
     */
    public function CopyAicraftTypeConfiguration(CopyAicraftTypeConfiguration $parameters)
    {
        return $this->__soapCall('CopyAicraftTypeConfiguration', array($parameters));
    }

    /**
     *
     * Updates an existing aircraft type configuration with new data
     *
     * A request containing the data needed to identify the existing configuration and the updated configuration data
     * True if aircraft configuration could be found, otherwise false
     *
     * @param UpdateAircraftTypeConfiguration $parameters
     * @return UpdateAircraftTypeConfigurationResponse
     */
    public function UpdateAircraftTypeConfiguration(UpdateAircraftTypeConfiguration $parameters)
    {
        return $this->__soapCall('UpdateAircraftTypeConfiguration', array($parameters));
    }

    /**
     *
     * Retrieves easy-accessible aircraft meta data such as aircraft structural limits, aircraft equipment etc. Typically used for retrieving meta-data about generic aircraft
     *
     *
     * @param RetrieveAircraftMetaData $parameters
     * @return RetrieveAircraftMetaDataResponse
     */
    public function RetrieveAircraftMetaData(RetrieveAircraftMetaData $parameters)
    {
        return $this->__soapCall('RetrieveAircraftMetaData', array($parameters));
    }

    /**
     *
     * Retrieves a list of available aircraft performance profiles to be used in UUID aircraft specifications
     *
     *
     * @param GetAvailableAircraftPerformanceProfiles $parameters
     * @return GetAvailableAircraftPerformanceProfilesResponse
     */
    public function GetAvailableAircraftPerformanceProfiles(GetAvailableAircraftPerformanceProfiles $parameters)
    {
        return $this->__soapCall('GetAvailableAircraftPerformanceProfiles', array($parameters));
    }

    /**
     * Creates a new account for which aircraft etc. can be registered to
     *
     * <param name="req">
     *
     *
     * @param CreateAccount $parameters
     * @return CreateAccountResponse
     */
    public function CreateAccount(CreateAccount $parameters)
    {
        return $this->__soapCall('CreateAccount', array($parameters));
    }

    /**
     *
     * Returns a list of accounts available for the authorized user
     *
     * @param GetAccounts $parameters
     * @return GetAccountsResponse
     */
    public function GetAccounts(GetAccounts $parameters)
    {
        return $this->__soapCall('GetAccounts', array($parameters));
    }

    /**
     *
     * Deletes the given account.
     * Please notice that an account may only be deleted if no active flights (e.g. filed, non-cancelled flights
     * with an STD 24 hours from current time and onwards)
     *
     * @param DeleteAccount $parameters
     * @return DeleteAccountResponse
     */
    public function DeleteAccount(DeleteAccount $parameters)
    {
        return $this->__soapCall('DeleteAccount', array($parameters));
    }

    /**
     *
     * Searches for account with the specified name. Returns all accounts who starts with the specified name or matches it exactly
     *
     * @param SearchForAccount $parameters
     * @return SearchForAccountResponse
     */
    public function SearchForAccount(SearchForAccount $parameters)
    {
        return $this->__soapCall('SearchForAccount', array($parameters));
    }

}
