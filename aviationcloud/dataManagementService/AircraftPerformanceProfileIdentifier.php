<?php

class AircraftPerformanceProfileIdentifier
{

    /**
     * @var string $ICAOCode
     */
    protected $ICAOCode = null;

    /**
     * @var string $MakeName
     */
    protected $MakeName = null;

    /**
     * @var string $ModelName
     */
    protected $ModelName = null;

    /**
     * @var string $UUID
     */
    protected $UUID = null;

    /**
     * @var string $VariantName
     */
    protected $VariantName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getICAOCode()
    {
      return $this->ICAOCode;
    }

    /**
     * @param string $ICAOCode
     * @return AircraftPerformanceProfileIdentifier
     */
    public function setICAOCode($ICAOCode)
    {
      $this->ICAOCode = $ICAOCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getMakeName()
    {
      return $this->MakeName;
    }

    /**
     * @param string $MakeName
     * @return AircraftPerformanceProfileIdentifier
     */
    public function setMakeName($MakeName)
    {
      $this->MakeName = $MakeName;
      return $this;
    }

    /**
     * @return string
     */
    public function getModelName()
    {
      return $this->ModelName;
    }

    /**
     * @param string $ModelName
     * @return AircraftPerformanceProfileIdentifier
     */
    public function setModelName($ModelName)
    {
      $this->ModelName = $ModelName;
      return $this;
    }

    /**
     * @return string
     */
    public function getUUID()
    {
      return $this->UUID;
    }

    /**
     * @param string $UUID
     * @return AircraftPerformanceProfileIdentifier
     */
    public function setUUID($UUID)
    {
      $this->UUID = $UUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantName()
    {
      return $this->VariantName;
    }

    /**
     * @param string $VariantName
     * @return AircraftPerformanceProfileIdentifier
     */
    public function setVariantName($VariantName)
    {
      $this->VariantName = $VariantName;
      return $this;
    }

}
