<?php

class SearchForAccountResponse
{

    /**
     * @var ArrayOfAccount $SearchForAccountResult
     */
    protected $SearchForAccountResult = null;

    /**
     * @param ArrayOfAccount $SearchForAccountResult
     */
    public function __construct($SearchForAccountResult)
    {
      $this->SearchForAccountResult = $SearchForAccountResult;
    }

    /**
     * @return ArrayOfAccount
     */
    public function getSearchForAccountResult()
    {
      return $this->SearchForAccountResult;
    }

    /**
     * @param ArrayOfAccount $SearchForAccountResult
     * @return SearchForAccountResponse
     */
    public function setSearchForAccountResult($SearchForAccountResult)
    {
      $this->SearchForAccountResult = $SearchForAccountResult;
      return $this;
    }

}
