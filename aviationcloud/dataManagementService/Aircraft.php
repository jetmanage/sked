<?php

class Aircraft
{

    /**
     * @var AircraftATCInfo $ATCData
     */
    protected $ATCData = null;

    /**
     * @var AircraftPerformanceData $AircraftSpecificPerformanceData
     */
    protected $AircraftSpecificPerformanceData = null;

    /**
     * @var AircraftStructure $AircraftStructure
     */
    protected $AircraftStructure = null;

    /**
     * @var AircraftEngineSpecification $AircraftTypeConfiguration
     */
    protected $AircraftTypeConfiguration = null;

    /**
     * @var ArrayOfAircraftCertification $Certifications
     */
    protected $Certifications = null;

    /**
     * @var float $ClimbFuelBias
     */
    protected $ClimbFuelBias = null;

    /**
     * @var float $ClimbSpeedBias
     */
    protected $ClimbSpeedBias = null;

    /**
     * @var float $CruiseFuelBias
     */
    protected $CruiseFuelBias = null;

    /**
     * @var float $CruiseSpeedBias
     */
    protected $CruiseSpeedBias = null;

    /**
     * @var float $DescentFuelBias
     */
    protected $DescentFuelBias = null;

    /**
     * @var float $DescentSpeedBias
     */
    protected $DescentSpeedBias = null;

    /**
     * @var Weight $FixedClimbFuelBias
     */
    protected $FixedClimbFuelBias = null;

    /**
     * @var Time $FixedClimbSpeedBias
     */
    protected $FixedClimbSpeedBias = null;

    /**
     * @var Weight $FixedDescentFuelBias
     */
    protected $FixedDescentFuelBias = null;

    /**
     * @var Time $FixedDescentSpeedBias
     */
    protected $FixedDescentSpeedBias = null;

    /**
     * @var float $FuelBias
     */
    protected $FuelBias = null;

    /**
     * @var float $HoldingFuelBias
     */
    protected $HoldingFuelBias = null;

    /**
     * @var \DateTime $LastUpdatedTime
     */
    protected $LastUpdatedTime = null;

    /**
     * @var AircraftRuleSet $RuleSet
     */
    protected $RuleSet = null;

    /**
     * @var float $SpeedBias
     */
    protected $SpeedBias = null;

    /**
     * @var string $TailNumber
     */
    protected $TailNumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftATCInfo
     */
    public function getATCData()
    {
      return $this->ATCData;
    }

    /**
     * @param AircraftATCInfo $ATCData
     * @return Aircraft
     */
    public function setATCData($ATCData)
    {
      $this->ATCData = $ATCData;
      return $this;
    }

    /**
     * @return AircraftPerformanceData
     */
    public function getAircraftSpecificPerformanceData()
    {
      return $this->AircraftSpecificPerformanceData;
    }

    /**
     * @param AircraftPerformanceData $AircraftSpecificPerformanceData
     * @return Aircraft
     */
    public function setAircraftSpecificPerformanceData($AircraftSpecificPerformanceData)
    {
      $this->AircraftSpecificPerformanceData = $AircraftSpecificPerformanceData;
      return $this;
    }

    /**
     * @return AircraftStructure
     */
    public function getAircraftStructure()
    {
      return $this->AircraftStructure;
    }

    /**
     * @param AircraftStructure $AircraftStructure
     * @return Aircraft
     */
    public function setAircraftStructure($AircraftStructure)
    {
      $this->AircraftStructure = $AircraftStructure;
      return $this;
    }

    /**
     * @return AircraftEngineSpecification
     */
    public function getAircraftTypeConfiguration()
    {
      return $this->AircraftTypeConfiguration;
    }

    /**
     * @param AircraftEngineSpecification $AircraftTypeConfiguration
     * @return Aircraft
     */
    public function setAircraftTypeConfiguration($AircraftTypeConfiguration)
    {
      $this->AircraftTypeConfiguration = $AircraftTypeConfiguration;
      return $this;
    }

    /**
     * @return ArrayOfAircraftCertification
     */
    public function getCertifications()
    {
      return $this->Certifications;
    }

    /**
     * @param ArrayOfAircraftCertification $Certifications
     * @return Aircraft
     */
    public function setCertifications($Certifications)
    {
      $this->Certifications = $Certifications;
      return $this;
    }

    /**
     * @return float
     */
    public function getClimbFuelBias()
    {
      return $this->ClimbFuelBias;
    }

    /**
     * @param float $ClimbFuelBias
     * @return Aircraft
     */
    public function setClimbFuelBias($ClimbFuelBias)
    {
      $this->ClimbFuelBias = $ClimbFuelBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getClimbSpeedBias()
    {
      return $this->ClimbSpeedBias;
    }

    /**
     * @param float $ClimbSpeedBias
     * @return Aircraft
     */
    public function setClimbSpeedBias($ClimbSpeedBias)
    {
      $this->ClimbSpeedBias = $ClimbSpeedBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getCruiseFuelBias()
    {
      return $this->CruiseFuelBias;
    }

    /**
     * @param float $CruiseFuelBias
     * @return Aircraft
     */
    public function setCruiseFuelBias($CruiseFuelBias)
    {
      $this->CruiseFuelBias = $CruiseFuelBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getCruiseSpeedBias()
    {
      return $this->CruiseSpeedBias;
    }

    /**
     * @param float $CruiseSpeedBias
     * @return Aircraft
     */
    public function setCruiseSpeedBias($CruiseSpeedBias)
    {
      $this->CruiseSpeedBias = $CruiseSpeedBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getDescentFuelBias()
    {
      return $this->DescentFuelBias;
    }

    /**
     * @param float $DescentFuelBias
     * @return Aircraft
     */
    public function setDescentFuelBias($DescentFuelBias)
    {
      $this->DescentFuelBias = $DescentFuelBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getDescentSpeedBias()
    {
      return $this->DescentSpeedBias;
    }

    /**
     * @param float $DescentSpeedBias
     * @return Aircraft
     */
    public function setDescentSpeedBias($DescentSpeedBias)
    {
      $this->DescentSpeedBias = $DescentSpeedBias;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFixedClimbFuelBias()
    {
      return $this->FixedClimbFuelBias;
    }

    /**
     * @param Weight $FixedClimbFuelBias
     * @return Aircraft
     */
    public function setFixedClimbFuelBias($FixedClimbFuelBias)
    {
      $this->FixedClimbFuelBias = $FixedClimbFuelBias;
      return $this;
    }

    /**
     * @return Time
     */
    public function getFixedClimbSpeedBias()
    {
      return $this->FixedClimbSpeedBias;
    }

    /**
     * @param Time $FixedClimbSpeedBias
     * @return Aircraft
     */
    public function setFixedClimbSpeedBias($FixedClimbSpeedBias)
    {
      $this->FixedClimbSpeedBias = $FixedClimbSpeedBias;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFixedDescentFuelBias()
    {
      return $this->FixedDescentFuelBias;
    }

    /**
     * @param Weight $FixedDescentFuelBias
     * @return Aircraft
     */
    public function setFixedDescentFuelBias($FixedDescentFuelBias)
    {
      $this->FixedDescentFuelBias = $FixedDescentFuelBias;
      return $this;
    }

    /**
     * @return Time
     */
    public function getFixedDescentSpeedBias()
    {
      return $this->FixedDescentSpeedBias;
    }

    /**
     * @param Time $FixedDescentSpeedBias
     * @return Aircraft
     */
    public function setFixedDescentSpeedBias($FixedDescentSpeedBias)
    {
      $this->FixedDescentSpeedBias = $FixedDescentSpeedBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getFuelBias()
    {
      return $this->FuelBias;
    }

    /**
     * @param float $FuelBias
     * @return Aircraft
     */
    public function setFuelBias($FuelBias)
    {
      $this->FuelBias = $FuelBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getHoldingFuelBias()
    {
      return $this->HoldingFuelBias;
    }

    /**
     * @param float $HoldingFuelBias
     * @return Aircraft
     */
    public function setHoldingFuelBias($HoldingFuelBias)
    {
      $this->HoldingFuelBias = $HoldingFuelBias;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdatedTime()
    {
      if ($this->LastUpdatedTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->LastUpdatedTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $LastUpdatedTime
     * @return Aircraft
     */
    public function setLastUpdatedTime(\DateTime $LastUpdatedTime = null)
    {
      if ($LastUpdatedTime == null) {
       $this->LastUpdatedTime = null;
      } else {
        $this->LastUpdatedTime = $LastUpdatedTime->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return AircraftRuleSet
     */
    public function getRuleSet()
    {
      return $this->RuleSet;
    }

    /**
     * @param AircraftRuleSet $RuleSet
     * @return Aircraft
     */
    public function setRuleSet($RuleSet)
    {
      $this->RuleSet = $RuleSet;
      return $this;
    }

    /**
     * @return float
     */
    public function getSpeedBias()
    {
      return $this->SpeedBias;
    }

    /**
     * @param float $SpeedBias
     * @return Aircraft
     */
    public function setSpeedBias($SpeedBias)
    {
      $this->SpeedBias = $SpeedBias;
      return $this;
    }

    /**
     * @return string
     */
    public function getTailNumber()
    {
      return $this->TailNumber;
    }

    /**
     * @param string $TailNumber
     * @return Aircraft
     */
    public function setTailNumber($TailNumber)
    {
      $this->TailNumber = $TailNumber;
      return $this;
    }

}
