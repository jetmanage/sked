<?php

class ADSCapabilities
{

    /**
     * @var boolean $ADSB1090InOut
     */
    protected $ADSB1090InOut = null;

    /**
     * @var boolean $ADSB1090Out
     */
    protected $ADSB1090Out = null;

    /**
     * @var boolean $ADSBUATInOut
     */
    protected $ADSBUATInOut = null;

    /**
     * @var boolean $ADSBUATOut
     */
    protected $ADSBUATOut = null;

    /**
     * @var boolean $ADSCATN
     */
    protected $ADSCATN = null;

    /**
     * @var boolean $ADSCFANS1A
     */
    protected $ADSCFANS1A = null;

    /**
     * @var boolean $ADSVDLMode4Out
     */
    protected $ADSVDLMode4Out = null;

    /**
     * @var boolean $ADSVDLMode4OutIn
     */
    protected $ADSVDLMode4OutIn = null;

    /**
     * @var string $RTCAAircraftAddressCode
     */
    protected $RTCAAircraftAddressCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getADSB1090InOut()
    {
      return $this->ADSB1090InOut;
    }

    /**
     * @param boolean $ADSB1090InOut
     * @return ADSCapabilities
     */
    public function setADSB1090InOut($ADSB1090InOut)
    {
      $this->ADSB1090InOut = $ADSB1090InOut;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getADSB1090Out()
    {
      return $this->ADSB1090Out;
    }

    /**
     * @param boolean $ADSB1090Out
     * @return ADSCapabilities
     */
    public function setADSB1090Out($ADSB1090Out)
    {
      $this->ADSB1090Out = $ADSB1090Out;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getADSBUATInOut()
    {
      return $this->ADSBUATInOut;
    }

    /**
     * @param boolean $ADSBUATInOut
     * @return ADSCapabilities
     */
    public function setADSBUATInOut($ADSBUATInOut)
    {
      $this->ADSBUATInOut = $ADSBUATInOut;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getADSBUATOut()
    {
      return $this->ADSBUATOut;
    }

    /**
     * @param boolean $ADSBUATOut
     * @return ADSCapabilities
     */
    public function setADSBUATOut($ADSBUATOut)
    {
      $this->ADSBUATOut = $ADSBUATOut;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getADSCATN()
    {
      return $this->ADSCATN;
    }

    /**
     * @param boolean $ADSCATN
     * @return ADSCapabilities
     */
    public function setADSCATN($ADSCATN)
    {
      $this->ADSCATN = $ADSCATN;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getADSCFANS1A()
    {
      return $this->ADSCFANS1A;
    }

    /**
     * @param boolean $ADSCFANS1A
     * @return ADSCapabilities
     */
    public function setADSCFANS1A($ADSCFANS1A)
    {
      $this->ADSCFANS1A = $ADSCFANS1A;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getADSVDLMode4Out()
    {
      return $this->ADSVDLMode4Out;
    }

    /**
     * @param boolean $ADSVDLMode4Out
     * @return ADSCapabilities
     */
    public function setADSVDLMode4Out($ADSVDLMode4Out)
    {
      $this->ADSVDLMode4Out = $ADSVDLMode4Out;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getADSVDLMode4OutIn()
    {
      return $this->ADSVDLMode4OutIn;
    }

    /**
     * @param boolean $ADSVDLMode4OutIn
     * @return ADSCapabilities
     */
    public function setADSVDLMode4OutIn($ADSVDLMode4OutIn)
    {
      $this->ADSVDLMode4OutIn = $ADSVDLMode4OutIn;
      return $this;
    }

    /**
     * @return string
     */
    public function getRTCAAircraftAddressCode()
    {
      return $this->RTCAAircraftAddressCode;
    }

    /**
     * @param string $RTCAAircraftAddressCode
     * @return ADSCapabilities
     */
    public function setRTCAAircraftAddressCode($RTCAAircraftAddressCode)
    {
      $this->RTCAAircraftAddressCode = $RTCAAircraftAddressCode;
      return $this;
    }

}
