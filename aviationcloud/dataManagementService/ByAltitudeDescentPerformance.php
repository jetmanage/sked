<?php

class ByAltitudeDescentPerformance
{

    /**
     * @var float $AltitudeFeet
     */
    protected $AltitudeFeet = null;

    /**
     * @var float $Isa
     */
    protected $Isa = null;

    /**
     * @var float $RateOfClimbFeetPerMinute
     */
    protected $RateOfClimbFeetPerMinute = null;

    /**
     * @var float $SpeedKnots
     */
    protected $SpeedKnots = null;

    /**
     * @var float $WeightPounds
     */
    protected $WeightPounds = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getAltitudeFeet()
    {
      return $this->AltitudeFeet;
    }

    /**
     * @param float $AltitudeFeet
     * @return ByAltitudeDescentPerformance
     */
    public function setAltitudeFeet($AltitudeFeet)
    {
      $this->AltitudeFeet = $AltitudeFeet;
      return $this;
    }

    /**
     * @return float
     */
    public function getIsa()
    {
      return $this->Isa;
    }

    /**
     * @param float $Isa
     * @return ByAltitudeDescentPerformance
     */
    public function setIsa($Isa)
    {
      $this->Isa = $Isa;
      return $this;
    }

    /**
     * @return float
     */
    public function getRateOfClimbFeetPerMinute()
    {
      return $this->RateOfClimbFeetPerMinute;
    }

    /**
     * @param float $RateOfClimbFeetPerMinute
     * @return ByAltitudeDescentPerformance
     */
    public function setRateOfClimbFeetPerMinute($RateOfClimbFeetPerMinute)
    {
      $this->RateOfClimbFeetPerMinute = $RateOfClimbFeetPerMinute;
      return $this;
    }

    /**
     * @return float
     */
    public function getSpeedKnots()
    {
      return $this->SpeedKnots;
    }

    /**
     * @param float $SpeedKnots
     * @return ByAltitudeDescentPerformance
     */
    public function setSpeedKnots($SpeedKnots)
    {
      $this->SpeedKnots = $SpeedKnots;
      return $this;
    }

    /**
     * @return float
     */
    public function getWeightPounds()
    {
      return $this->WeightPounds;
    }

    /**
     * @param float $WeightPounds
     * @return ByAltitudeDescentPerformance
     */
    public function setWeightPounds($WeightPounds)
    {
      $this->WeightPounds = $WeightPounds;
      return $this;
    }

}
