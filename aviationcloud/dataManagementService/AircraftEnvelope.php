<?php

class AircraftEnvelope
{

    /**
     * @var ArrayOfAircraftEnvelopePoint $EnvelopePoints
     */
    protected $EnvelopePoints = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAircraftEnvelopePoint
     */
    public function getEnvelopePoints()
    {
      return $this->EnvelopePoints;
    }

    /**
     * @param ArrayOfAircraftEnvelopePoint $EnvelopePoints
     * @return AircraftEnvelope
     */
    public function setEnvelopePoints($EnvelopePoints)
    {
      $this->EnvelopePoints = $EnvelopePoints;
      return $this;
    }

}
