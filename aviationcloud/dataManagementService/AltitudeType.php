<?php

class AltitudeType
{
    const __default = 'FlightlevelFeet';
    const FlightlevelFeet = 'FlightlevelFeet';
    const FlightlevelMetric = 'FlightlevelMetric';
    const Feet = 'Feet';
    const Meters = 'Meters';
    const VFR = 'VFR';


}
