<?php

class CopyAicraftType
{

    /**
     * @var AircraftTypeCopyRequest $request
     */
    protected $request = null;

    /**
     * @param AircraftTypeCopyRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AircraftTypeCopyRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AircraftTypeCopyRequest $request
     * @return CopyAicraftType
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
