<?php

class UpdateOFPLayout
{

    /**
     * @var OFPLayoutUpdateRequest $aRequest
     */
    protected $aRequest = null;

    /**
     * @param OFPLayoutUpdateRequest $aRequest
     */
    public function __construct($aRequest)
    {
      $this->aRequest = $aRequest;
    }

    /**
     * @return OFPLayoutUpdateRequest
     */
    public function getARequest()
    {
      return $this->aRequest;
    }

    /**
     * @param OFPLayoutUpdateRequest $aRequest
     * @return UpdateOFPLayout
     */
    public function setARequest($aRequest)
    {
      $this->aRequest = $aRequest;
      return $this;
    }

}
