<?php

class PPSAircraftMigrationRequest extends RequestBase
{

    /**
     * @var base64Binary $AC6Data
     */
    protected $AC6Data = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return base64Binary
     */
    public function getAC6Data()
    {
      return $this->AC6Data;
    }

    /**
     * @param base64Binary $AC6Data
     * @return PPSAircraftMigrationRequest
     */
    public function setAC6Data($AC6Data)
    {
      $this->AC6Data = $AC6Data;
      return $this;
    }

}
