<?php

class AircaftTypeConfigurationRetrivalRequest extends RequestBase
{

    /**
     * @var string $AircraftTypeName
     */
    protected $AircraftTypeName = null;

    /**
     * @var DataLevel $DataLevel
     */
    protected $DataLevel = null;

    /**
     * @var string $EngineName
     */
    protected $EngineName = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getAircraftTypeName()
    {
      return $this->AircraftTypeName;
    }

    /**
     * @param string $AircraftTypeName
     * @return AircaftTypeConfigurationRetrivalRequest
     */
    public function setAircraftTypeName($AircraftTypeName)
    {
      $this->AircraftTypeName = $AircraftTypeName;
      return $this;
    }

    /**
     * @return DataLevel
     */
    public function getDataLevel()
    {
      return $this->DataLevel;
    }

    /**
     * @param DataLevel $DataLevel
     * @return AircaftTypeConfigurationRetrivalRequest
     */
    public function setDataLevel($DataLevel)
    {
      $this->DataLevel = $DataLevel;
      return $this;
    }

    /**
     * @return string
     */
    public function getEngineName()
    {
      return $this->EngineName;
    }

    /**
     * @param string $EngineName
     * @return AircaftTypeConfigurationRetrivalRequest
     */
    public function setEngineName($EngineName)
    {
      $this->EngineName = $EngineName;
      return $this;
    }

}
