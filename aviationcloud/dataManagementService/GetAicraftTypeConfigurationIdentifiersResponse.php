<?php

class GetAicraftTypeConfigurationIdentifiersResponse
{

    /**
     * @var ArrayOfAicractTypeConfigurationIdentifier $GetAicraftTypeConfigurationIdentifiersResult
     */
    protected $GetAicraftTypeConfigurationIdentifiersResult = null;

    /**
     * @param ArrayOfAicractTypeConfigurationIdentifier $GetAicraftTypeConfigurationIdentifiersResult
     */
    public function __construct($GetAicraftTypeConfigurationIdentifiersResult)
    {
      $this->GetAicraftTypeConfigurationIdentifiersResult = $GetAicraftTypeConfigurationIdentifiersResult;
    }

    /**
     * @return ArrayOfAicractTypeConfigurationIdentifier
     */
    public function getGetAicraftTypeConfigurationIdentifiersResult()
    {
      return $this->GetAicraftTypeConfigurationIdentifiersResult;
    }

    /**
     * @param ArrayOfAicractTypeConfigurationIdentifier $GetAicraftTypeConfigurationIdentifiersResult
     * @return GetAicraftTypeConfigurationIdentifiersResponse
     */
    public function setGetAicraftTypeConfigurationIdentifiersResult($GetAicraftTypeConfigurationIdentifiersResult)
    {
      $this->GetAicraftTypeConfigurationIdentifiersResult = $GetAicraftTypeConfigurationIdentifiersResult;
      return $this;
    }

}
