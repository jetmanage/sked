<?php

class CruisePerformancePorifleSummary extends PerformanceProfileSummary
{

    /**
     * @var int $Index
     */
    protected $Index = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getIndex()
    {
      return $this->Index;
    }

    /**
     * @param int $Index
     * @return CruisePerformancePorifleSummary
     */
    public function setIndex($Index)
    {
      $this->Index = $Index;
      return $this;
    }

}
