<?php

class HoldingData
{

    /**
     * @var Weight $FuelFlow
     */
    protected $FuelFlow = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getFuelFlow()
    {
      return $this->FuelFlow;
    }

    /**
     * @param Weight $FuelFlow
     * @return HoldingData
     */
    public function setFuelFlow($FuelFlow)
    {
      $this->FuelFlow = $FuelFlow;
      return $this;
    }

}
