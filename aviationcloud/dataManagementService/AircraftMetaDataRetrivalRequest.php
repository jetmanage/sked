<?php

class AircraftMetaDataRetrivalRequest extends RequestBase
{

    /**
     * @var AircraftSpecification $AircraftDefinition
     */
    protected $AircraftDefinition = null;

    /**
     * @var WeightUnit $WeightOutputUnit
     */
    protected $WeightOutputUnit = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return AircraftSpecification
     */
    public function getAircraftDefinition()
    {
      return $this->AircraftDefinition;
    }

    /**
     * @param AircraftSpecification $AircraftDefinition
     * @return AircraftMetaDataRetrivalRequest
     */
    public function setAircraftDefinition($AircraftDefinition)
    {
      $this->AircraftDefinition = $AircraftDefinition;
      return $this;
    }

    /**
     * @return WeightUnit
     */
    public function getWeightOutputUnit()
    {
      return $this->WeightOutputUnit;
    }

    /**
     * @param WeightUnit $WeightOutputUnit
     * @return AircraftMetaDataRetrivalRequest
     */
    public function setWeightOutputUnit($WeightOutputUnit)
    {
      $this->WeightOutputUnit = $WeightOutputUnit;
      return $this;
    }

}
