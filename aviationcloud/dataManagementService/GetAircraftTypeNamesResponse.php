<?php

class GetAircraftTypeNamesResponse
{

    /**
     * @var ArrayOfstring $GetAircraftTypeNamesResult
     */
    protected $GetAircraftTypeNamesResult = null;

    /**
     * @param ArrayOfstring $GetAircraftTypeNamesResult
     */
    public function __construct($GetAircraftTypeNamesResult)
    {
      $this->GetAircraftTypeNamesResult = $GetAircraftTypeNamesResult;
    }

    /**
     * @return ArrayOfstring
     */
    public function getGetAircraftTypeNamesResult()
    {
      return $this->GetAircraftTypeNamesResult;
    }

    /**
     * @param ArrayOfstring $GetAircraftTypeNamesResult
     * @return GetAircraftTypeNamesResponse
     */
    public function setGetAircraftTypeNamesResult($GetAircraftTypeNamesResult)
    {
      $this->GetAircraftTypeNamesResult = $GetAircraftTypeNamesResult;
      return $this;
    }

}
