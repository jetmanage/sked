<?php

class AircraftEngineSpecification
{

    /**
     * @var AircraftType $AircraftType
     */
    protected $AircraftType = null;

    /**
     * @var AircraftATCInfo $DefaultATCData
     */
    protected $DefaultATCData = null;

    /**
     * @var EngineDetails $EngineSpecification
     */
    protected $EngineSpecification = null;

    /**
     * @var \DateTime $LastUpdatedTime
     */
    protected $LastUpdatedTime = null;

    /**
     * @var AircraftPerformanceData $PerformanceData
     */
    protected $PerformanceData = null;

    /**
     * @var AircraftStructure $StructureTemplate
     */
    protected $StructureTemplate = null;

    /**
     * @var Weight $TaxiFuelBurnRatePerMinute
     */
    protected $TaxiFuelBurnRatePerMinute = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftType
     */
    public function getAircraftType()
    {
      return $this->AircraftType;
    }

    /**
     * @param AircraftType $AircraftType
     * @return AircraftEngineSpecification
     */
    public function setAircraftType($AircraftType)
    {
      $this->AircraftType = $AircraftType;
      return $this;
    }

    /**
     * @return AircraftATCInfo
     */
    public function getDefaultATCData()
    {
      return $this->DefaultATCData;
    }

    /**
     * @param AircraftATCInfo $DefaultATCData
     * @return AircraftEngineSpecification
     */
    public function setDefaultATCData($DefaultATCData)
    {
      $this->DefaultATCData = $DefaultATCData;
      return $this;
    }

    /**
     * @return EngineDetails
     */
    public function getEngineSpecification()
    {
      return $this->EngineSpecification;
    }

    /**
     * @param EngineDetails $EngineSpecification
     * @return AircraftEngineSpecification
     */
    public function setEngineSpecification($EngineSpecification)
    {
      $this->EngineSpecification = $EngineSpecification;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdatedTime()
    {
      if ($this->LastUpdatedTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->LastUpdatedTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $LastUpdatedTime
     * @return AircraftEngineSpecification
     */
    public function setLastUpdatedTime(\DateTime $LastUpdatedTime = null)
    {
      if ($LastUpdatedTime == null) {
       $this->LastUpdatedTime = null;
      } else {
        $this->LastUpdatedTime = $LastUpdatedTime->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return AircraftPerformanceData
     */
    public function getPerformanceData()
    {
      return $this->PerformanceData;
    }

    /**
     * @param AircraftPerformanceData $PerformanceData
     * @return AircraftEngineSpecification
     */
    public function setPerformanceData($PerformanceData)
    {
      $this->PerformanceData = $PerformanceData;
      return $this;
    }

    /**
     * @return AircraftStructure
     */
    public function getStructureTemplate()
    {
      return $this->StructureTemplate;
    }

    /**
     * @param AircraftStructure $StructureTemplate
     * @return AircraftEngineSpecification
     */
    public function setStructureTemplate($StructureTemplate)
    {
      $this->StructureTemplate = $StructureTemplate;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTaxiFuelBurnRatePerMinute()
    {
      return $this->TaxiFuelBurnRatePerMinute;
    }

    /**
     * @param Weight $TaxiFuelBurnRatePerMinute
     * @return AircraftEngineSpecification
     */
    public function setTaxiFuelBurnRatePerMinute($TaxiFuelBurnRatePerMinute)
    {
      $this->TaxiFuelBurnRatePerMinute = $TaxiFuelBurnRatePerMinute;
      return $this;
    }

}
