<?php

class GetAircraftTypeNames
{

    /**
     * @var AircraftTypeRetrivalRequest $request
     */
    protected $request = null;

    /**
     * @param AircraftTypeRetrivalRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AircraftTypeRetrivalRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AircraftTypeRetrivalRequest $request
     * @return GetAircraftTypeNames
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
