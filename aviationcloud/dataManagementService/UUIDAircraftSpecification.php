<?php

class UUIDAircraftSpecification extends AircraftSpecification
{

    /**
     * @var AircraftFuelType $AircraftFuelType
     */
    protected $AircraftFuelType = null;

    /**
     * @var string $AircraftUUID
     */
    protected $AircraftUUID = null;

    /**
     * @var float $ClimbFuelBias
     */
    protected $ClimbFuelBias = null;

    /**
     * @var float $ClimbSpeedBias
     */
    protected $ClimbSpeedBias = null;

    /**
     * @var float $CruiseFuelBias
     */
    protected $CruiseFuelBias = null;

    /**
     * @var float $CruiseSpeedBias
     */
    protected $CruiseSpeedBias = null;

    /**
     * @var float $DescentFuelBias
     */
    protected $DescentFuelBias = null;

    /**
     * @var float $DescentSpeedBias
     */
    protected $DescentSpeedBias = null;

    /**
     * @var AircraftATCInfo $Equipment
     */
    protected $Equipment = null;

    /**
     * @var Weight $FixedClimbFuelBias
     */
    protected $FixedClimbFuelBias = null;

    /**
     * @var Time $FixedClimbSpeedBias
     */
    protected $FixedClimbSpeedBias = null;

    /**
     * @var Weight $FixedDescentFuelBias
     */
    protected $FixedDescentFuelBias = null;

    /**
     * @var Time $FixedDescentSpeedBias
     */
    protected $FixedDescentSpeedBias = null;

    /**
     * @var Weight $FixedHoldingFuelBias
     */
    protected $FixedHoldingFuelBias = null;

    /**
     * @var float $FuelBias
     */
    protected $FuelBias = null;

    /**
     * @var FuelTankDefinition $FuelTankDefinition
     */
    protected $FuelTankDefinition = null;

    /**
     * @var float $HoldingFuelBias
     */
    protected $HoldingFuelBias = null;

    /**
     * @var MassAndBalanceConfigurationBase $MassAndBalanceData
     */
    protected $MassAndBalanceData = null;

    /**
     * @var DataLevel $PreferedDataLevel
     */
    protected $PreferedDataLevel = null;

    /**
     * @var float $SpeedBias
     */
    protected $SpeedBias = null;

    /**
     * @var string $Tailnumber
     */
    protected $Tailnumber = null;

    /**
     * @var Weight $MaxRampMass
     */
    protected $MaxRampMass = null;

    /**
     * @var Weight $MaxTakeOffMass
     */
    protected $MaxTakeOffMass = null;

    /**
     * @var Weight $MaxZeroFuelMass
     */
    protected $MaxZeroFuelMass = null;

    /**
     * @var Weight $MaxLandingMass
     */
    protected $MaxLandingMass = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftFuelType
     */
    public function getAircraftFuelType()
    {
      return $this->AircraftFuelType;
    }

    /**
     * @param AircraftFuelType $AircraftFuelType
     * @return UUIDAircraftSpecification
     */
    public function setAircraftFuelType($AircraftFuelType)
    {
      $this->AircraftFuelType = $AircraftFuelType;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftUUID()
    {
      return $this->AircraftUUID;
    }

    /**
     * @param string $AircraftUUID
     * @return UUIDAircraftSpecification
     */
    public function setAircraftUUID($AircraftUUID)
    {
      $this->AircraftUUID = $AircraftUUID;
      return $this;
    }

    /**
     * @return float
     */
    public function getClimbFuelBias()
    {
      return $this->ClimbFuelBias;
    }

    /**
     * @param float $ClimbFuelBias
     * @return UUIDAircraftSpecification
     */
    public function setClimbFuelBias($ClimbFuelBias)
    {
      $this->ClimbFuelBias = $ClimbFuelBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getClimbSpeedBias()
    {
      return $this->ClimbSpeedBias;
    }

    /**
     * @param float $ClimbSpeedBias
     * @return UUIDAircraftSpecification
     */
    public function setClimbSpeedBias($ClimbSpeedBias)
    {
      $this->ClimbSpeedBias = $ClimbSpeedBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getCruiseFuelBias()
    {
      return $this->CruiseFuelBias;
    }

    /**
     * @param float $CruiseFuelBias
     * @return UUIDAircraftSpecification
     */
    public function setCruiseFuelBias($CruiseFuelBias)
    {
      $this->CruiseFuelBias = $CruiseFuelBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getCruiseSpeedBias()
    {
      return $this->CruiseSpeedBias;
    }

    /**
     * @param float $CruiseSpeedBias
     * @return UUIDAircraftSpecification
     */
    public function setCruiseSpeedBias($CruiseSpeedBias)
    {
      $this->CruiseSpeedBias = $CruiseSpeedBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getDescentFuelBias()
    {
      return $this->DescentFuelBias;
    }

    /**
     * @param float $DescentFuelBias
     * @return UUIDAircraftSpecification
     */
    public function setDescentFuelBias($DescentFuelBias)
    {
      $this->DescentFuelBias = $DescentFuelBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getDescentSpeedBias()
    {
      return $this->DescentSpeedBias;
    }

    /**
     * @param float $DescentSpeedBias
     * @return UUIDAircraftSpecification
     */
    public function setDescentSpeedBias($DescentSpeedBias)
    {
      $this->DescentSpeedBias = $DescentSpeedBias;
      return $this;
    }

    /**
     * @return AircraftATCInfo
     */
    public function getEquipment()
    {
      return $this->Equipment;
    }

    /**
     * @param AircraftATCInfo $Equipment
     * @return UUIDAircraftSpecification
     */
    public function setEquipment($Equipment)
    {
      $this->Equipment = $Equipment;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFixedClimbFuelBias()
    {
      return $this->FixedClimbFuelBias;
    }

    /**
     * @param Weight $FixedClimbFuelBias
     * @return UUIDAircraftSpecification
     */
    public function setFixedClimbFuelBias($FixedClimbFuelBias)
    {
      $this->FixedClimbFuelBias = $FixedClimbFuelBias;
      return $this;
    }

    /**
     * @return Time
     */
    public function getFixedClimbSpeedBias()
    {
      return $this->FixedClimbSpeedBias;
    }

    /**
     * @param Time $FixedClimbSpeedBias
     * @return UUIDAircraftSpecification
     */
    public function setFixedClimbSpeedBias($FixedClimbSpeedBias)
    {
      $this->FixedClimbSpeedBias = $FixedClimbSpeedBias;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFixedDescentFuelBias()
    {
      return $this->FixedDescentFuelBias;
    }

    /**
     * @param Weight $FixedDescentFuelBias
     * @return UUIDAircraftSpecification
     */
    public function setFixedDescentFuelBias($FixedDescentFuelBias)
    {
      $this->FixedDescentFuelBias = $FixedDescentFuelBias;
      return $this;
    }

    /**
     * @return Time
     */
    public function getFixedDescentSpeedBias()
    {
      return $this->FixedDescentSpeedBias;
    }

    /**
     * @param Time $FixedDescentSpeedBias
     * @return UUIDAircraftSpecification
     */
    public function setFixedDescentSpeedBias($FixedDescentSpeedBias)
    {
      $this->FixedDescentSpeedBias = $FixedDescentSpeedBias;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFixedHoldingFuelBias()
    {
      return $this->FixedHoldingFuelBias;
    }

    /**
     * @param Weight $FixedHoldingFuelBias
     * @return UUIDAircraftSpecification
     */
    public function setFixedHoldingFuelBias($FixedHoldingFuelBias)
    {
      $this->FixedHoldingFuelBias = $FixedHoldingFuelBias;
      return $this;
    }

    /**
     * @return float
     */
    public function getFuelBias()
    {
      return $this->FuelBias;
    }

    /**
     * @param float $FuelBias
     * @return UUIDAircraftSpecification
     */
    public function setFuelBias($FuelBias)
    {
      $this->FuelBias = $FuelBias;
      return $this;
    }

    /**
     * @return FuelTankDefinition
     */
    public function getFuelTankDefinition()
    {
      return $this->FuelTankDefinition;
    }

    /**
     * @param FuelTankDefinition $FuelTankDefinition
     * @return UUIDAircraftSpecification
     */
    public function setFuelTankDefinition($FuelTankDefinition)
    {
      $this->FuelTankDefinition = $FuelTankDefinition;
      return $this;
    }

    /**
     * @return float
     */
    public function getHoldingFuelBias()
    {
      return $this->HoldingFuelBias;
    }

    /**
     * @param float $HoldingFuelBias
     * @return UUIDAircraftSpecification
     */
    public function setHoldingFuelBias($HoldingFuelBias)
    {
      $this->HoldingFuelBias = $HoldingFuelBias;
      return $this;
    }

    /**
     * @return MassAndBalanceConfigurationBase
     */
    public function getMassAndBalanceData()
    {
      return $this->MassAndBalanceData;
    }

    /**
     * @param MassAndBalanceConfigurationBase $MassAndBalanceData
     * @return UUIDAircraftSpecification
     */
    public function setMassAndBalanceData($MassAndBalanceData)
    {
      $this->MassAndBalanceData = $MassAndBalanceData;
      return $this;
    }

    /**
     * @return DataLevel
     */
    public function getPreferedDataLevel()
    {
      return $this->PreferedDataLevel;
    }

    /**
     * @param DataLevel $PreferedDataLevel
     * @return UUIDAircraftSpecification
     */
    public function setPreferedDataLevel($PreferedDataLevel)
    {
      $this->PreferedDataLevel = $PreferedDataLevel;
      return $this;
    }

    /**
     * @return float
     */
    public function getSpeedBias()
    {
      return $this->SpeedBias;
    }

    /**
     * @param float $SpeedBias
     * @return UUIDAircraftSpecification
     */
    public function setSpeedBias($SpeedBias)
    {
      $this->SpeedBias = $SpeedBias;
      return $this;
    }

    /**
     * @return string
     */
    public function getTailnumber()
    {
      return $this->Tailnumber;
    }

    /**
     * @param string $Tailnumber
     * @return UUIDAircraftSpecification
     */
    public function setTailnumber($Tailnumber)
    {
      $this->Tailnumber = $Tailnumber;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxRampMass()
    {
      return $this->MaxRampMass;
    }

    /**
     * @param Weight $MaxRampMass
     * @return UUIDAircraftSpecification
     */
    public function setMaxRampMass($MaxRampMass)
    {
      $this->MaxRampMass = $MaxRampMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxTakeOffMass()
    {
      return $this->MaxTakeOffMass;
    }

    /**
     * @param Weight $MaxTakeOffMass
     * @return UUIDAircraftSpecification
     */
    public function setMaxTakeOffMass($MaxTakeOffMass)
    {
      $this->MaxTakeOffMass = $MaxTakeOffMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxZeroFuelMass()
    {
      return $this->MaxZeroFuelMass;
    }

    /**
     * @param Weight $MaxZeroFuelMass
     * @return UUIDAircraftSpecification
     */
    public function setMaxZeroFuelMass($MaxZeroFuelMass)
    {
      $this->MaxZeroFuelMass = $MaxZeroFuelMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxLandingMass()
    {
      return $this->MaxLandingMass;
    }

    /**
     * @param Weight $MaxLandingMass
     * @return UUIDAircraftSpecification
     */
    public function setMaxLandingMass($MaxLandingMass)
    {
      $this->MaxLandingMass = $MaxLandingMass;
      return $this;
    }

}
