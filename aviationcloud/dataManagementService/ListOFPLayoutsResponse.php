<?php

class ListOFPLayoutsResponse
{

    /**
     * @var OFPLayoutListResonse $ListOFPLayoutsResult
     */
    protected $ListOFPLayoutsResult = null;

    /**
     * @param OFPLayoutListResonse $ListOFPLayoutsResult
     */
    public function __construct($ListOFPLayoutsResult)
    {
      $this->ListOFPLayoutsResult = $ListOFPLayoutsResult;
    }

    /**
     * @return OFPLayoutListResonse
     */
    public function getListOFPLayoutsResult()
    {
      return $this->ListOFPLayoutsResult;
    }

    /**
     * @param OFPLayoutListResonse $ListOFPLayoutsResult
     * @return ListOFPLayoutsResponse
     */
    public function setListOFPLayoutsResult($ListOFPLayoutsResult)
    {
      $this->ListOFPLayoutsResult = $ListOFPLayoutsResult;
      return $this;
    }

}
