<?php

class OFPLayoutCreateRequest extends RequestBase
{

    /**
     * @var OFPLayout $Layout
     */
    protected $Layout = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return OFPLayout
     */
    public function getLayout()
    {
      return $this->Layout;
    }

    /**
     * @param OFPLayout $Layout
     * @return OFPLayoutCreateRequest
     */
    public function setLayout($Layout)
    {
      $this->Layout = $Layout;
      return $this;
    }

}
