<?php

class RTFEquipment
{

    /**
     * @var boolean $RTFSATCOMINMARSAT
     */
    protected $RTFSATCOMINMARSAT = null;

    /**
     * @var boolean $RTFSATCOMIRDIUM
     */
    protected $RTFSATCOMIRDIUM = null;

    /**
     * @var boolean $RTFSATCOMMTSAT
     */
    protected $RTFSATCOMMTSAT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getRTFSATCOMINMARSAT()
    {
      return $this->RTFSATCOMINMARSAT;
    }

    /**
     * @param boolean $RTFSATCOMINMARSAT
     * @return RTFEquipment
     */
    public function setRTFSATCOMINMARSAT($RTFSATCOMINMARSAT)
    {
      $this->RTFSATCOMINMARSAT = $RTFSATCOMINMARSAT;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRTFSATCOMIRDIUM()
    {
      return $this->RTFSATCOMIRDIUM;
    }

    /**
     * @param boolean $RTFSATCOMIRDIUM
     * @return RTFEquipment
     */
    public function setRTFSATCOMIRDIUM($RTFSATCOMIRDIUM)
    {
      $this->RTFSATCOMIRDIUM = $RTFSATCOMIRDIUM;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRTFSATCOMMTSAT()
    {
      return $this->RTFSATCOMMTSAT;
    }

    /**
     * @param boolean $RTFSATCOMMTSAT
     * @return RTFEquipment
     */
    public function setRTFSATCOMMTSAT($RTFSATCOMMTSAT)
    {
      $this->RTFSATCOMMTSAT = $RTFSATCOMMTSAT;
      return $this;
    }

}
