<?php

class GetRegisteredAircraftsResponse
{

    /**
     * @var ArrayOfAircraft $GetRegisteredAircraftsResult
     */
    protected $GetRegisteredAircraftsResult = null;

    /**
     * @param ArrayOfAircraft $GetRegisteredAircraftsResult
     */
    public function __construct($GetRegisteredAircraftsResult)
    {
      $this->GetRegisteredAircraftsResult = $GetRegisteredAircraftsResult;
    }

    /**
     * @return ArrayOfAircraft
     */
    public function getGetRegisteredAircraftsResult()
    {
      return $this->GetRegisteredAircraftsResult;
    }

    /**
     * @param ArrayOfAircraft $GetRegisteredAircraftsResult
     * @return GetRegisteredAircraftsResponse
     */
    public function setGetRegisteredAircraftsResult($GetRegisteredAircraftsResult)
    {
      $this->GetRegisteredAircraftsResult = $GetRegisteredAircraftsResult;
      return $this;
    }

}
