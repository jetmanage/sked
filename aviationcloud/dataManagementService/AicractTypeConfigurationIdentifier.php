<?php

class AicractTypeConfigurationIdentifier
{

    /**
     * @var string $EngineName
     */
    protected $EngineName = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var AircraftType $TypeData
     */
    protected $TypeData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getEngineName()
    {
      return $this->EngineName;
    }

    /**
     * @param string $EngineName
     * @return AicractTypeConfigurationIdentifier
     */
    public function setEngineName($EngineName)
    {
      $this->EngineName = $EngineName;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return AicractTypeConfigurationIdentifier
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return AircraftType
     */
    public function getTypeData()
    {
      return $this->TypeData;
    }

    /**
     * @param AircraftType $TypeData
     * @return AicractTypeConfigurationIdentifier
     */
    public function setTypeData($TypeData)
    {
      $this->TypeData = $TypeData;
      return $this;
    }

}
