<?php

class ArrayOfAircraftCertification implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AircraftCertification[] $AircraftCertification
     */
    protected $AircraftCertification = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftCertification[]
     */
    public function getAircraftCertification()
    {
      return $this->AircraftCertification;
    }

    /**
     * @param AircraftCertification[] $AircraftCertification
     * @return ArrayOfAircraftCertification
     */
    public function setAircraftCertification(array $AircraftCertification = null)
    {
      $this->AircraftCertification = $AircraftCertification;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AircraftCertification[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AircraftCertification
     */
    public function offsetGet($offset)
    {
      return $this->AircraftCertification[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AircraftCertification $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AircraftCertification[] = $value;
      } else {
        $this->AircraftCertification[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AircraftCertification[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AircraftCertification Return the current element
     */
    public function current()
    {
      return current($this->AircraftCertification);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AircraftCertification);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AircraftCertification);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AircraftCertification);
    }

    /**
     * Countable implementation
     *
     * @return AircraftCertification Return count of elements
     */
    public function count()
    {
      return count($this->AircraftCertification);
    }

}
