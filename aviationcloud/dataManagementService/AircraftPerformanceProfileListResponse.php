<?php

class AircraftPerformanceProfileListResponse
{

    /**
     * @var ArrayOfAircraftPerformanceProfileIdentifier $AvaiablePerformanceProfiles
     */
    protected $AvaiablePerformanceProfiles = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAircraftPerformanceProfileIdentifier
     */
    public function getAvaiablePerformanceProfiles()
    {
      return $this->AvaiablePerformanceProfiles;
    }

    /**
     * @param ArrayOfAircraftPerformanceProfileIdentifier $AvaiablePerformanceProfiles
     * @return AircraftPerformanceProfileListResponse
     */
    public function setAvaiablePerformanceProfiles($AvaiablePerformanceProfiles)
    {
      $this->AvaiablePerformanceProfiles = $AvaiablePerformanceProfiles;
      return $this;
    }

}
