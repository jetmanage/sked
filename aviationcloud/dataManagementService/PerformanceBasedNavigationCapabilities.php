<?php

class PerformanceBasedNavigationCapabilities
{

    /**
     * @var boolean $RNAV10
     */
    protected $RNAV10 = null;

    /**
     * @var boolean $RNAV1All
     */
    protected $RNAV1All = null;

    /**
     * @var boolean $RNAV1DMEDME
     */
    protected $RNAV1DMEDME = null;

    /**
     * @var boolean $RNAV1DMEDMEIRU
     */
    protected $RNAV1DMEDMEIRU = null;

    /**
     * @var boolean $RNAV1GNSS
     */
    protected $RNAV1GNSS = null;

    /**
     * @var boolean $RNAV2All
     */
    protected $RNAV2All = null;

    /**
     * @var boolean $RNAV2DMEDME
     */
    protected $RNAV2DMEDME = null;

    /**
     * @var boolean $RNAV2DMEDMEIRU
     */
    protected $RNAV2DMEDMEIRU = null;

    /**
     * @var boolean $RNAV2GNSS
     */
    protected $RNAV2GNSS = null;

    /**
     * @var boolean $RNAV5All
     */
    protected $RNAV5All = null;

    /**
     * @var boolean $RNAV5DME
     */
    protected $RNAV5DME = null;

    /**
     * @var boolean $RNAV5GNSS
     */
    protected $RNAV5GNSS = null;

    /**
     * @var boolean $RNAV5INSOrIRS
     */
    protected $RNAV5INSOrIRS = null;

    /**
     * @var boolean $RNAV5LoranC
     */
    protected $RNAV5LoranC = null;

    /**
     * @var boolean $RNAV5VORDME
     */
    protected $RNAV5VORDME = null;

    /**
     * @var boolean $RNP1All
     */
    protected $RNP1All = null;

    /**
     * @var boolean $RNP1DME
     */
    protected $RNP1DME = null;

    /**
     * @var boolean $RNP1DMEIRU
     */
    protected $RNP1DMEIRU = null;

    /**
     * @var boolean $RNP1GNSS
     */
    protected $RNP1GNSS = null;

    /**
     * @var boolean $RNP4
     */
    protected $RNP4 = null;

    /**
     * @var boolean $RNPARApproachWithRF
     */
    protected $RNPARApproachWithRF = null;

    /**
     * @var boolean $RNPARApproachWithoutRF
     */
    protected $RNPARApproachWithoutRF = null;

    /**
     * @var boolean $RNPApproach
     */
    protected $RNPApproach = null;

    /**
     * @var boolean $RNPApproachBAROVNAV
     */
    protected $RNPApproachBAROVNAV = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getRNAV10()
    {
      return $this->RNAV10;
    }

    /**
     * @param boolean $RNAV10
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV10($RNAV10)
    {
      $this->RNAV10 = $RNAV10;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV1All()
    {
      return $this->RNAV1All;
    }

    /**
     * @param boolean $RNAV1All
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV1All($RNAV1All)
    {
      $this->RNAV1All = $RNAV1All;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV1DMEDME()
    {
      return $this->RNAV1DMEDME;
    }

    /**
     * @param boolean $RNAV1DMEDME
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV1DMEDME($RNAV1DMEDME)
    {
      $this->RNAV1DMEDME = $RNAV1DMEDME;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV1DMEDMEIRU()
    {
      return $this->RNAV1DMEDMEIRU;
    }

    /**
     * @param boolean $RNAV1DMEDMEIRU
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV1DMEDMEIRU($RNAV1DMEDMEIRU)
    {
      $this->RNAV1DMEDMEIRU = $RNAV1DMEDMEIRU;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV1GNSS()
    {
      return $this->RNAV1GNSS;
    }

    /**
     * @param boolean $RNAV1GNSS
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV1GNSS($RNAV1GNSS)
    {
      $this->RNAV1GNSS = $RNAV1GNSS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV2All()
    {
      return $this->RNAV2All;
    }

    /**
     * @param boolean $RNAV2All
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV2All($RNAV2All)
    {
      $this->RNAV2All = $RNAV2All;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV2DMEDME()
    {
      return $this->RNAV2DMEDME;
    }

    /**
     * @param boolean $RNAV2DMEDME
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV2DMEDME($RNAV2DMEDME)
    {
      $this->RNAV2DMEDME = $RNAV2DMEDME;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV2DMEDMEIRU()
    {
      return $this->RNAV2DMEDMEIRU;
    }

    /**
     * @param boolean $RNAV2DMEDMEIRU
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV2DMEDMEIRU($RNAV2DMEDMEIRU)
    {
      $this->RNAV2DMEDMEIRU = $RNAV2DMEDMEIRU;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV2GNSS()
    {
      return $this->RNAV2GNSS;
    }

    /**
     * @param boolean $RNAV2GNSS
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV2GNSS($RNAV2GNSS)
    {
      $this->RNAV2GNSS = $RNAV2GNSS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV5All()
    {
      return $this->RNAV5All;
    }

    /**
     * @param boolean $RNAV5All
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV5All($RNAV5All)
    {
      $this->RNAV5All = $RNAV5All;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV5DME()
    {
      return $this->RNAV5DME;
    }

    /**
     * @param boolean $RNAV5DME
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV5DME($RNAV5DME)
    {
      $this->RNAV5DME = $RNAV5DME;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV5GNSS()
    {
      return $this->RNAV5GNSS;
    }

    /**
     * @param boolean $RNAV5GNSS
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV5GNSS($RNAV5GNSS)
    {
      $this->RNAV5GNSS = $RNAV5GNSS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV5INSOrIRS()
    {
      return $this->RNAV5INSOrIRS;
    }

    /**
     * @param boolean $RNAV5INSOrIRS
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV5INSOrIRS($RNAV5INSOrIRS)
    {
      $this->RNAV5INSOrIRS = $RNAV5INSOrIRS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV5LoranC()
    {
      return $this->RNAV5LoranC;
    }

    /**
     * @param boolean $RNAV5LoranC
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV5LoranC($RNAV5LoranC)
    {
      $this->RNAV5LoranC = $RNAV5LoranC;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNAV5VORDME()
    {
      return $this->RNAV5VORDME;
    }

    /**
     * @param boolean $RNAV5VORDME
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNAV5VORDME($RNAV5VORDME)
    {
      $this->RNAV5VORDME = $RNAV5VORDME;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNP1All()
    {
      return $this->RNP1All;
    }

    /**
     * @param boolean $RNP1All
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNP1All($RNP1All)
    {
      $this->RNP1All = $RNP1All;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNP1DME()
    {
      return $this->RNP1DME;
    }

    /**
     * @param boolean $RNP1DME
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNP1DME($RNP1DME)
    {
      $this->RNP1DME = $RNP1DME;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNP1DMEIRU()
    {
      return $this->RNP1DMEIRU;
    }

    /**
     * @param boolean $RNP1DMEIRU
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNP1DMEIRU($RNP1DMEIRU)
    {
      $this->RNP1DMEIRU = $RNP1DMEIRU;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNP1GNSS()
    {
      return $this->RNP1GNSS;
    }

    /**
     * @param boolean $RNP1GNSS
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNP1GNSS($RNP1GNSS)
    {
      $this->RNP1GNSS = $RNP1GNSS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNP4()
    {
      return $this->RNP4;
    }

    /**
     * @param boolean $RNP4
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNP4($RNP4)
    {
      $this->RNP4 = $RNP4;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNPARApproachWithRF()
    {
      return $this->RNPARApproachWithRF;
    }

    /**
     * @param boolean $RNPARApproachWithRF
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNPARApproachWithRF($RNPARApproachWithRF)
    {
      $this->RNPARApproachWithRF = $RNPARApproachWithRF;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNPARApproachWithoutRF()
    {
      return $this->RNPARApproachWithoutRF;
    }

    /**
     * @param boolean $RNPARApproachWithoutRF
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNPARApproachWithoutRF($RNPARApproachWithoutRF)
    {
      $this->RNPARApproachWithoutRF = $RNPARApproachWithoutRF;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNPApproach()
    {
      return $this->RNPApproach;
    }

    /**
     * @param boolean $RNPApproach
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNPApproach($RNPApproach)
    {
      $this->RNPApproach = $RNPApproach;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRNPApproachBAROVNAV()
    {
      return $this->RNPApproachBAROVNAV;
    }

    /**
     * @param boolean $RNPApproachBAROVNAV
     * @return PerformanceBasedNavigationCapabilities
     */
    public function setRNPApproachBAROVNAV($RNPApproachBAROVNAV)
    {
      $this->RNPApproachBAROVNAV = $RNPApproachBAROVNAV;
      return $this;
    }

}
