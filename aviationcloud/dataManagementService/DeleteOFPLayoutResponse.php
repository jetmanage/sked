<?php

class DeleteOFPLayoutResponse
{

    /**
     * @var OFPLayoutDeleteResponse $DeleteOFPLayoutResult
     */
    protected $DeleteOFPLayoutResult = null;

    /**
     * @param OFPLayoutDeleteResponse $DeleteOFPLayoutResult
     */
    public function __construct($DeleteOFPLayoutResult)
    {
      $this->DeleteOFPLayoutResult = $DeleteOFPLayoutResult;
    }

    /**
     * @return OFPLayoutDeleteResponse
     */
    public function getDeleteOFPLayoutResult()
    {
      return $this->DeleteOFPLayoutResult;
    }

    /**
     * @param OFPLayoutDeleteResponse $DeleteOFPLayoutResult
     * @return DeleteOFPLayoutResponse
     */
    public function setDeleteOFPLayoutResult($DeleteOFPLayoutResult)
    {
      $this->DeleteOFPLayoutResult = $DeleteOFPLayoutResult;
      return $this;
    }

}
