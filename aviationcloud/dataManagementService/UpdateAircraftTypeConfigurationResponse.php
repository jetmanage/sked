<?php

class UpdateAircraftTypeConfigurationResponse
{

    /**
     * @var boolean $UpdateAircraftTypeConfigurationResult
     */
    protected $UpdateAircraftTypeConfigurationResult = null;

    /**
     * @param boolean $UpdateAircraftTypeConfigurationResult
     */
    public function __construct($UpdateAircraftTypeConfigurationResult)
    {
      $this->UpdateAircraftTypeConfigurationResult = $UpdateAircraftTypeConfigurationResult;
    }

    /**
     * @return boolean
     */
    public function getUpdateAircraftTypeConfigurationResult()
    {
      return $this->UpdateAircraftTypeConfigurationResult;
    }

    /**
     * @param boolean $UpdateAircraftTypeConfigurationResult
     * @return UpdateAircraftTypeConfigurationResponse
     */
    public function setUpdateAircraftTypeConfigurationResult($UpdateAircraftTypeConfigurationResult)
    {
      $this->UpdateAircraftTypeConfigurationResult = $UpdateAircraftTypeConfigurationResult;
      return $this;
    }

}
