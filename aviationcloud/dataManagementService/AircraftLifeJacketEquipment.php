<?php

class AircraftLifeJacketEquipment
{

    /**
     * @var boolean $Flourescein
     */
    protected $Flourescein = null;

    /**
     * @var boolean $Lights
     */
    protected $Lights = null;

    /**
     * @var boolean $UHF
     */
    protected $UHF = null;

    /**
     * @var boolean $VHF
     */
    protected $VHF = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getFlourescein()
    {
      return $this->Flourescein;
    }

    /**
     * @param boolean $Flourescein
     * @return AircraftLifeJacketEquipment
     */
    public function setFlourescein($Flourescein)
    {
      $this->Flourescein = $Flourescein;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getLights()
    {
      return $this->Lights;
    }

    /**
     * @param boolean $Lights
     * @return AircraftLifeJacketEquipment
     */
    public function setLights($Lights)
    {
      $this->Lights = $Lights;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUHF()
    {
      return $this->UHF;
    }

    /**
     * @param boolean $UHF
     * @return AircraftLifeJacketEquipment
     */
    public function setUHF($UHF)
    {
      $this->UHF = $UHF;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getVHF()
    {
      return $this->VHF;
    }

    /**
     * @param boolean $VHF
     * @return AircraftLifeJacketEquipment
     */
    public function setVHF($VHF)
    {
      $this->VHF = $VHF;
      return $this;
    }

}
