<?php

class ArrayOfMassAndBalanceProfileSummary implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var MassAndBalanceProfileSummary[] $MassAndBalanceProfileSummary
     */
    protected $MassAndBalanceProfileSummary = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return MassAndBalanceProfileSummary[]
     */
    public function getMassAndBalanceProfileSummary()
    {
      return $this->MassAndBalanceProfileSummary;
    }

    /**
     * @param MassAndBalanceProfileSummary[] $MassAndBalanceProfileSummary
     * @return ArrayOfMassAndBalanceProfileSummary
     */
    public function setMassAndBalanceProfileSummary(array $MassAndBalanceProfileSummary = null)
    {
      $this->MassAndBalanceProfileSummary = $MassAndBalanceProfileSummary;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->MassAndBalanceProfileSummary[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return MassAndBalanceProfileSummary
     */
    public function offsetGet($offset)
    {
      return $this->MassAndBalanceProfileSummary[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param MassAndBalanceProfileSummary $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->MassAndBalanceProfileSummary[] = $value;
      } else {
        $this->MassAndBalanceProfileSummary[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->MassAndBalanceProfileSummary[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return MassAndBalanceProfileSummary Return the current element
     */
    public function current()
    {
      return current($this->MassAndBalanceProfileSummary);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->MassAndBalanceProfileSummary);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->MassAndBalanceProfileSummary);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->MassAndBalanceProfileSummary);
    }

    /**
     * Countable implementation
     *
     * @return MassAndBalanceProfileSummary Return count of elements
     */
    public function count()
    {
      return count($this->MassAndBalanceProfileSummary);
    }

}
