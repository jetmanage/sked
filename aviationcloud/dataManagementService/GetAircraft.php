<?php

class GetAircraft
{

    /**
     * @var AircraftRetrivalRequest $request
     */
    protected $request = null;

    /**
     * @param AircraftRetrivalRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AircraftRetrivalRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AircraftRetrivalRequest $request
     * @return GetAircraft
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
