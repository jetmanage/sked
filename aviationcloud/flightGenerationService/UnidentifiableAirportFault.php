<?php

class UnidentifiableAirportFault
{

    /**
     * @var string $AirportDesignator
     */
    protected $AirportDesignator = null;

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    /**
     * @var Airport $ProvidedAirport
     */
    protected $ProvidedAirport = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAirportDesignator()
    {
      return $this->AirportDesignator;
    }

    /**
     * @param string $AirportDesignator
     * @return UnidentifiableAirportFault
     */
    public function setAirportDesignator($AirportDesignator)
    {
      $this->AirportDesignator = $AirportDesignator;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return UnidentifiableAirportFault
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getProvidedAirport()
    {
      return $this->ProvidedAirport;
    }

    /**
     * @param Airport $ProvidedAirport
     * @return UnidentifiableAirportFault
     */
    public function setProvidedAirport($ProvidedAirport)
    {
      $this->ProvidedAirport = $ProvidedAirport;
      return $this;
    }

}
