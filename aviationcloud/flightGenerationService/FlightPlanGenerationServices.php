<?php

class FlightPlanGenerationServices extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'CalculateFlight' => '\\CalculateFlight',
      'FlightCalculationRequest' => '\\FlightCalculationRequest',
      'RequestBase' => '\\RequestBase',
      'FlightSpecification' => '\\FlightSpecification',
      'ATCData' => '\\ATCData',
      'ArrayOfATCOtherItemField' => '\\ArrayOfATCOtherItemField',
      'ATCOtherItemField' => '\\ATCOtherItemField',
      'Length' => '\\Length',
      'ATCSupplementaryInfo' => '\\ATCSupplementaryInfo',
      'Currency' => '\\Currency',
      'AircraftSpecification' => '\\AircraftSpecification',
      'TailnumberAircraftSpecification' => '\\TailnumberAircraftSpecification',
      'ICAOIdentAircraftSpecification' => '\\ICAOIdentAircraftSpecification',
      'AircraftATCInfo' => '\\AircraftATCInfo',
      'AircraftEquipment' => '\\AircraftEquipment',
      'PerformanceBasedNavigationCapabilities' => '\\PerformanceBasedNavigationCapabilities',
      'RadioAndNavigationEquipment' => '\\RadioAndNavigationEquipment',
      'ATCACARSEquipment' => '\\ATCACARSEquipment',
      'CPDLCEquipment' => '\\CPDLCEquipment',
      'RTFEquipment' => '\\RTFEquipment',
      'SSREquipment' => '\\SSREquipment',
      'ADSCapabilities' => '\\ADSCapabilities',
      'ModeSCapabilities' => '\\ModeSCapabilities',
      'AircraftFrequencyAvailability' => '\\AircraftFrequencyAvailability',
      'AircraftLifeJacketEquipment' => '\\AircraftLifeJacketEquipment',
      'RNAVPreferentialRoutes' => '\\RNAVPreferentialRoutes',
      'AircraftSurvivalEquipment' => '\\AircraftSurvivalEquipment',
      'Weight' => '\\Weight',
      'Time' => '\\Time',
      'FuelTankDefinition' => '\\FuelTankDefinition',
      'SimpleFuelTankDefinition' => '\\SimpleFuelTankDefinition',
      'FuelTankListDefinition' => '\\FuelTankListDefinition',
      'ArrayOfFuelTank' => '\\ArrayOfFuelTank',
      'FuelTank' => '\\FuelTank',
      'AircraftStructureUnit' => '\\AircraftStructureUnit',
      'ArmDefinition' => '\\ArmDefinition',
      'StaticArmDefinition' => '\\StaticArmDefinition',
      'DynamicArmDefinition' => '\\DynamicArmDefinition',
      'ArrayOfWeightDependentArm' => '\\ArrayOfWeightDependentArm',
      'WeightDependentArm' => '\\WeightDependentArm',
      'PassengerCompartment' => '\\PassengerCompartment',
      'CargoCompartment' => '\\CargoCompartment',
      'CrewCompartment' => '\\CrewCompartment',
      'MassAndBalanceConfigurationBase' => '\\MassAndBalanceConfigurationBase',
      'MassAndBalanceConfiguration' => '\\MassAndBalanceConfiguration',
      'SimpleMassAndBalanceConfiguration' => '\\SimpleMassAndBalanceConfiguration',
      'CompartmentBasedMassAndBalanceConfiguration' => '\\CompartmentBasedMassAndBalanceConfiguration',
      'ArrayOfPassengerCompartment' => '\\ArrayOfPassengerCompartment',
      'ArrayOfCargoCompartment' => '\\ArrayOfCargoCompartment',
      'ArrayOfCrewCompartment' => '\\ArrayOfCrewCompartment',
      'DefaultWeightParameters' => '\\DefaultWeightParameters',
      'ReferencedMassAndBalanceConfiguration' => '\\ReferencedMassAndBalanceConfiguration',
      'UnspecifiedMassAndBalance' => '\\UnspecifiedMassAndBalance',
      'EmbeddedAircraftSpecification' => '\\EmbeddedAircraftSpecification',
      'Aircraft' => '\\Aircraft',
      'AircraftPerformanceData' => '\\AircraftPerformanceData',
      'ArrayOfAircraftPerformanceProfile' => '\\ArrayOfAircraftPerformanceProfile',
      'AircraftPerformanceProfile' => '\\AircraftPerformanceProfile',
      'BiasedClimbPerformanceProfile' => '\\BiasedClimbPerformanceProfile',
      'ClimbProfile' => '\\ClimbProfile',
      'DiscreteClimbProfile' => '\\DiscreteClimbProfile',
      'ArrayOfClimbDescendDataSeries' => '\\ArrayOfClimbDescendDataSeries',
      'ClimbDescendDataSeries' => '\\ClimbDescendDataSeries',
      'PerformanceDataSeries' => '\\PerformanceDataSeries',
      'HoldingDataSeries' => '\\HoldingDataSeries',
      'ArrayOfHoldingDataPoint' => '\\ArrayOfHoldingDataPoint',
      'HoldingDataPoint' => '\\HoldingDataPoint',
      'PerformanceDataPoint' => '\\PerformanceDataPoint',
      'ISAPoint' => '\\ISAPoint',
      'ClimbDescendDataPoint' => '\\ClimbDescendDataPoint',
      'ClimbDescendData' => '\\ClimbDescendData',
      'CruisePerformanceDataPoint' => '\\CruisePerformanceDataPoint',
      'AircraftCruisePerformanceData' => '\\AircraftCruisePerformanceData',
      'HoldingData' => '\\HoldingData',
      'CruisePerformanceDataSeries' => '\\CruisePerformanceDataSeries',
      'ArrayOfCruisePerformanceDataPoint' => '\\ArrayOfCruisePerformanceDataPoint',
      'ArrayOfClimbDescendDataPoint' => '\\ArrayOfClimbDescendDataPoint',
      'SimpleClimbProfile' => '\\SimpleClimbProfile',
      'Speed' => '\\Speed',
      'ByAltitudeClimbProfile' => '\\ByAltitudeClimbProfile',
      'ArrayOfByAltitudeFuelFlow' => '\\ArrayOfByAltitudeFuelFlow',
      'ByAltitudeFuelFlow' => '\\ByAltitudeFuelFlow',
      'ArrayOfByAltitudeClimbPerformance' => '\\ArrayOfByAltitudeClimbPerformance',
      'ByAltitudeClimbPerformance' => '\\ByAltitudeClimbPerformance',
      'BiasedCruisePerformanceProfile' => '\\BiasedCruisePerformanceProfile',
      'CruiseProfile' => '\\CruiseProfile',
      'DiscreteCruiseProfile' => '\\DiscreteCruiseProfile',
      'ArrayOfCruisePerformanceDataSeries' => '\\ArrayOfCruisePerformanceDataSeries',
      'ArrayOfWeightLimitation' => '\\ArrayOfWeightLimitation',
      'WeightLimitation' => '\\WeightLimitation',
      'ETOPSCruiseProfile' => '\\ETOPSCruiseProfile',
      'SimpleCruiseProfile' => '\\SimpleCruiseProfile',
      'Altitude' => '\\Altitude',
      'ByAltitudeCruiseProfile' => '\\ByAltitudeCruiseProfile',
      'ArrayOfByAltitudeCruisePerformance' => '\\ArrayOfByAltitudeCruisePerformance',
      'ByAltitudeCruisePerformance' => '\\ByAltitudeCruisePerformance',
      'BiasedDescentProfile' => '\\BiasedDescentProfile',
      'DescentProfile' => '\\DescentProfile',
      'DiscreteDescentProfile' => '\\DiscreteDescentProfile',
      'SimpleDescentProfile' => '\\SimpleDescentProfile',
      'ByAltitudeDescentProfile' => '\\ByAltitudeDescentProfile',
      'ArrayOfByAltitudeDescentPerformance' => '\\ArrayOfByAltitudeDescentPerformance',
      'ByAltitudeDescentPerformance' => '\\ByAltitudeDescentPerformance',
      'BiasedHoldingPerformanceProfile' => '\\BiasedHoldingPerformanceProfile',
      'HoldingProfile' => '\\HoldingProfile',
      'DiscreteHoldingProfile' => '\\DiscreteHoldingProfile',
      'ArrayOfHoldingDataSeries' => '\\ArrayOfHoldingDataSeries',
      'ReferencedPerformanceProfile' => '\\ReferencedPerformanceProfile',
      'AircraftStructure' => '\\AircraftStructure',
      'ArrayOfMassAndBalanceConfiguration' => '\\ArrayOfMassAndBalanceConfiguration',
      'AircraftEnvelope' => '\\AircraftEnvelope',
      'ArrayOfAircraftEnvelopePoint' => '\\ArrayOfAircraftEnvelopePoint',
      'AircraftEnvelopePoint' => '\\AircraftEnvelopePoint',
      'AircraftEngineSpecification' => '\\AircraftEngineSpecification',
      'AircraftType' => '\\AircraftType',
      'EngineDetails' => '\\EngineDetails',
      'ArrayOfAircraftCertification' => '\\ArrayOfAircraftCertification',
      'AircraftCertification' => '\\AircraftCertification',
      'ETOPSCertification' => '\\ETOPSCertification',
      'AircraftTypeConfigurationSpecification' => '\\AircraftTypeConfigurationSpecification',
      'SimplifiedAircraftSpecification' => '\\SimplifiedAircraftSpecification',
      'UUIDAircraftSpecification' => '\\UUIDAircraftSpecification',
      'ByAltitudeAircraftSpecification' => '\\ByAltitudeAircraftSpecification',
      'ArrayOfByAltitudeClimbProfile' => '\\ArrayOfByAltitudeClimbProfile',
      'ArrayOfByAltitudeCruiseProfile' => '\\ArrayOfByAltitudeCruiseProfile',
      'ArrayOfByAltitudeDescentProfile' => '\\ArrayOfByAltitudeDescentProfile',
      'Airport' => '\\Airport',
      'ArrayOfAirportFrequencyARINC424' => '\\ArrayOfAirportFrequencyARINC424',
      'AirportFrequencyARINC424' => '\\AirportFrequencyARINC424',
      'FIR' => '\\FIR',
      'Airspace' => '\\Airspace',
      'NationalAirspace' => '\\NationalAirspace',
      'SpecialActivityAirspace' => '\\SpecialActivityAirspace',
      'TemporaryFlightRestrictedAirspace' => '\\TemporaryFlightRestrictedAirspace',
      'SphereicPoint' => '\\SphereicPoint',
      'PathPoint' => '\\PathPoint',
      'ArrayOfRunway' => '\\ArrayOfRunway',
      'Runway' => '\\Runway',
      'FlightCalculationOptions' => '\\FlightCalculationOptions',
      'ContingencyStrategy' => '\\ContingencyStrategy',
      'DefaultContingenyStrategy' => '\\DefaultContingenyStrategy',
      'ERAContingencyStrategy' => '\\ERAContingencyStrategy',
      'ReducedContingencyFuelStrategy' => '\\ReducedContingencyFuelStrategy',
      'Route' => '\\Route',
      'WaypointData' => '\\WaypointData',
      'ArrayOfRouteNode' => '\\ArrayOfRouteNode',
      'RouteNode' => '\\RouteNode',
      'FlightData' => '\\FlightData',
      'RouteNodeIdentifier' => '\\RouteNodeIdentifier',
      'ParserInformation' => '\\ParserInformation',
      'RouteProfile' => '\\RouteProfile',
      'RouteReport' => '\\RouteReport',
      'ArrayOfAirspace' => '\\ArrayOfAirspace',
      'ArrayOfInformationItem' => '\\ArrayOfInformationItem',
      'InformationItem' => '\\InformationItem',
      'ArrayOfInformationData' => '\\ArrayOfInformationData',
      'InformationData' => '\\InformationData',
      'ArrayOfRouteInfo' => '\\ArrayOfRouteInfo',
      'RouteInfo' => '\\RouteInfo',
      'ArrayOfWarning' => '\\ArrayOfWarning',
      'Warning' => '\\Warning',
      'AirportProcedure' => '\\AirportProcedure',
      'ArrayOfAirportEnrouteTransitionProcedure' => '\\ArrayOfAirportEnrouteTransitionProcedure',
      'AirportEnrouteTransitionProcedure' => '\\AirportEnrouteTransitionProcedure',
      'ArrayOfWaypointData' => '\\ArrayOfWaypointData',
      'AirportProcedureIdentifier' => '\\AirportProcedureIdentifier',
      'FlightLevelOptimizationOptions' => '\\FlightLevelOptimizationOptions',
      'PerformanceProfileSpecification' => '\\PerformanceProfileSpecification',
      'SimpleftPerformanceProfileSpecification' => '\\SimpleftPerformanceProfileSpecification',
      'InterpolatedPerformanceProfileSpecification' => '\\InterpolatedPerformanceProfileSpecification',
      'ArrayOfAltitude' => '\\ArrayOfAltitude',
      'OptimumFlightlevelOptionSpecification' => '\\OptimumFlightlevelOptionSpecification',
      'FuelPolicy' => '\\FuelPolicy',
      'RelativeFuelPolicy' => '\\RelativeFuelPolicy',
      'ValueFuelPolicy' => '\\ValueFuelPolicy',
      'ActualFuelPolicy' => '\\ActualFuelPolicy',
      'ExtraFuelPolicy' => '\\ExtraFuelPolicy',
      'LandingFuelPolicy' => '\\LandingFuelPolicy',
      'TakeOffMassPolicy' => '\\TakeOffMassPolicy',
      'LandingMassPolicy' => '\\LandingMassPolicy',
      'MinimumRequiredFuelPolicy' => '\\MinimumRequiredFuelPolicy',
      'MaximumFuelPolicy' => '\\MaximumFuelPolicy',
      'GainLossCalculationInfo' => '\\GainLossCalculationInfo',
      'InlineOptionSpecification' => '\\InlineOptionSpecification',
      'RAIMPredictionOptions' => '\\RAIMPredictionOptions',
      'FlightCalculationSIDSTAROptions' => '\\FlightCalculationSIDSTAROptions',
      'TaxiFuelSpecification' => '\\TaxiFuelSpecification',
      'MassBasedTaxiFuelSpecification' => '\\MassBasedTaxiFuelSpecification',
      'TimeBasedTaxiFuelSpecification' => '\\TimeBasedTaxiFuelSpecification',
      'TaxiTimeSpecification' => '\\TaxiTimeSpecification',
      'ManuelTaxiTime' => '\\ManuelTaxiTime',
      'DefaultTaxiTime' => '\\DefaultTaxiTime',
      'CargoLoadSpecification' => '\\CargoLoadSpecification',
      'SimpleCargoLoadSpecification' => '\\SimpleCargoLoadSpecification',
      'SpecificCargoLoadSpecification' => '\\SpecificCargoLoadSpecification',
      'ArrayOfCargoSectionLoad' => '\\ArrayOfCargoSectionLoad',
      'CargoSectionLoad' => '\\CargoSectionLoad',
      'RelativeCargoLoadSpecification' => '\\RelativeCargoLoadSpecification',
      'MaxCargoLoadSpecification' => '\\MaxCargoLoadSpecification',
      'CrewLoadSpecification' => '\\CrewLoadSpecification',
      'SimpleCrewSpecification' => '\\SimpleCrewSpecification',
      'SpecificCrewLoadSpecification' => '\\SpecificCrewLoadSpecification',
      'ETPSettings' => '\\ETPSettings',
      'ArrayOfAirport' => '\\ArrayOfAirport',
      'ArrayOfETOPSSettingSpecification' => '\\ArrayOfETOPSSettingSpecification',
      'ETOPSSettingSpecification' => '\\ETOPSSettingSpecification',
      'FlightLevelSpecification' => '\\FlightLevelSpecification',
      'MaximumFlightLevelSpecification' => '\\MaximumFlightLevelSpecification',
      'ExactFlightLevelSpecification' => '\\ExactFlightLevelSpecification',
      'OptimumFlightLevelDefinition' => '\\OptimumFlightLevelDefinition',
      'FlightFuelRules' => '\\FlightFuelRules',
      'FAAOpsRules' => '\\FAAOpsRules',
      'EUOpsRules' => '\\EUOpsRules',
      'CustomFuelRule' => '\\CustomFuelRule',
      'PaxLoadSpecification' => '\\PaxLoadSpecification',
      'SimplePaxLoadSpecification' => '\\SimplePaxLoadSpecification',
      'PaxLoad' => '\\PaxLoad',
      'UnspecifiedPaxTypeLoad' => '\\UnspecifiedPaxTypeLoad',
      'SpecifiedPaxTypeLoad' => '\\SpecifiedPaxTypeLoad',
      'SectionBasedPaxLoadSpecification' => '\\SectionBasedPaxLoadSpecification',
      'ArrayOfPaxSectionLoad' => '\\ArrayOfPaxSectionLoad',
      'PaxSectionLoad' => '\\PaxSectionLoad',
      'FlightLevelAdjustmentSpecification' => '\\FlightLevelAdjustmentSpecification',
      'ArrayOfAltitudeNode' => '\\ArrayOfAltitudeNode',
      'AltitudeNode' => '\\AltitudeNode',
      'ArrayOfCruiseChangeNode' => '\\ArrayOfCruiseChangeNode',
      'CruiseChangeNode' => '\\CruiseChangeNode',
      'Winds' => '\\Winds',
      'ForecastWinds' => '\\ForecastWinds',
      'DenseWindModel' => '\\DenseWindModel',
      'HistoricalWinds' => '\\HistoricalWinds',
      'FixedWinds' => '\\FixedWinds',
      'CalculateFlightResponse' => '\\CalculateFlightResponse',
      'FlightLog' => '\\FlightLog',
      'ATCFPLMessage' => '\\ATCFPLMessage',
      'ATCFlightMessage' => '\\ATCFlightMessage',
      'ATCMessageBase' => '\\ATCMessageBase',
      'ATCRejectedMessage' => '\\ATCRejectedMessage',
      'ATCRequestFlightPlanMessage' => '\\ATCRequestFlightPlanMessage',
      'ATCLongAcknowledgeMessage' => '\\ATCLongAcknowledgeMessage',
      'ATCAcknowledgeMessage' => '\\ATCAcknowledgeMessage',
      'ATCDivertedArrivialMessage' => '\\ATCDivertedArrivialMessage',
      'ATCDepartureMessage' => '\\ATCDepartureMessage',
      'ATCDelayMessage' => '\\ATCDelayMessage',
      'ATCChangeMessage' => '\\ATCChangeMessage',
      'ATCCancelMessage' => '\\ATCCancelMessage',
      'ATCArrivalMessage' => '\\ATCArrivalMessage',
      'AircraftATCInfoCodes' => '\\AircraftATCInfoCodes',
      'ArrayOfAFTNAddress' => '\\ArrayOfAFTNAddress',
      'AFTNAddress' => '\\AFTNAddress',
      'FLightATCEET' => '\\FLightATCEET',
      'ArrayOfFlightATCEETElement' => '\\ArrayOfFlightATCEETElement',
      'FlightATCEETElement' => '\\FlightATCEETElement',
      'ATCOtherInformation' => '\\ATCOtherInformation',
      'ATCOtherInformationCode' => '\\ATCOtherInformationCode',
      'ATCOtherInformationContents' => '\\ATCOtherInformationContents',
      'CalculatedAircraftWeights' => '\\CalculatedAircraftWeights',
      'ArrayOfIntersectionPoint' => '\\ArrayOfIntersectionPoint',
      'IntersectionPoint' => '\\IntersectionPoint',
      'StartEndPoint' => '\\StartEndPoint',
      'BorderIntersectionPoint' => '\\BorderIntersectionPoint',
      'InputPoint' => '\\InputPoint',
      'CalculationInformation' => '\\CalculationInformation',
      'CalculatedETPInformation' => '\\CalculatedETPInformation',
      'ArrayOfAirportTimeWindow' => '\\ArrayOfAirportTimeWindow',
      'AirportTimeWindow' => '\\AirportTimeWindow',
      'ArrayOfETPPoint' => '\\ArrayOfETPPoint',
      'ETPPoint' => '\\ETPPoint',
      'ETPData' => '\\ETPData',
      'ArrayOfETPScenario' => '\\ArrayOfETPScenario',
      'ETPScenario' => '\\ETPScenario',
      'OverflightCalculationInformation' => '\\OverflightCalculationInformation',
      'ArrayOfCountryInformation' => '\\ArrayOfCountryInformation',
      'CountryInformation' => '\\CountryInformation',
      'ACCurrency' => '\\ACCurrency',
      'ArrayOfFIR' => '\\ArrayOfFIR',
      'FlightAnalysis' => '\\FlightAnalysis',
      'RiskAnalysis' => '\\RiskAnalysis',
      'ArrayOfEnrouteDiversionAirport' => '\\ArrayOfEnrouteDiversionAirport',
      'EnrouteDiversionAirport' => '\\EnrouteDiversionAirport',
      'ArrayOfSphereicPoint' => '\\ArrayOfSphereicPoint',
      'ArrayOfFlightSegment' => '\\ArrayOfFlightSegment',
      'FlightSegment' => '\\FlightSegment',
      'FlightDistance' => '\\FlightDistance',
      'FlightFuelValues' => '\\FlightFuelValues',
      'FlightMetaData' => '\\FlightMetaData',
      'FlightTimes' => '\\FlightTimes',
      'FlightMetrologicalData' => '\\FlightMetrologicalData',
      'IcingPoint' => '\\IcingPoint',
      'TurbulencePoint' => '\\TurbulencePoint',
      'FlightRAIMData' => '\\FlightRAIMData',
      'ArrayOfOverflightCosts' => '\\ArrayOfOverflightCosts',
      'OverflightCosts' => '\\OverflightCosts',
      'PathwayData' => '\\PathwayData',
      'ArrayOfDataPoint' => '\\ArrayOfDataPoint',
      'DataPoint' => '\\DataPoint',
      'AirwayData' => '\\AirwayData',
      'FuelData' => '\\FuelData',
      'MeteorologicalData' => '\\MeteorologicalData',
      'ArrayOfWeatherDataRecord' => '\\ArrayOfWeatherDataRecord',
      'WeatherDataRecord' => '\\WeatherDataRecord',
      'ArrayOfIcingPoint' => '\\ArrayOfIcingPoint',
      'ArrayOfTurbulencePoint' => '\\ArrayOfTurbulencePoint',
      'ArrayOfFlightValidationError' => '\\ArrayOfFlightValidationError',
      'FlightValidationError' => '\\FlightValidationError',
      'Account' => '\\Account',
      'CalculateFlightPlan' => '\\CalculateFlightPlan',
      'CalculateFlightPlanResponse' => '\\CalculateFlightPlanResponse',
      'FlightCalculationResponse' => '\\FlightCalculationResponse',
      'CrewBriefingIntegrationDetails' => '\\CrewBriefingIntegrationDetails',
      'CalculateTrip' => '\\CalculateTrip',
      'TripCalculationRequest' => '\\TripCalculationRequest',
      'TripPackageRequest' => '\\TripPackageRequest',
      'FuelTankeringDocumentSpecification' => '\\FuelTankeringDocumentSpecification',
      'DocumentSpecification' => '\\DocumentSpecification',
      'SignifantWeatherDocumentSpecification' => '\\SignifantWeatherDocumentSpecification',
      'WindChartDocumentSpecification' => '\\WindChartDocumentSpecification',
      'CrossSectionDocumentSpecification' => '\\CrossSectionDocumentSpecification',
      'NotamDocumentSpecification' => '\\NotamDocumentSpecification',
      'WxNotamDocumentSpecification' => '\\WxNotamDocumentSpecification',
      'WxDocumentSpecification' => '\\WxDocumentSpecification',
      'PDFFlightlogUnits' => '\\PDFFlightlogUnits',
      'ICAOFlightPlanDocumentSpecification' => '\\ICAOFlightPlanDocumentSpecification',
      'EnrouteChargeDocumentSpecification' => '\\EnrouteChargeDocumentSpecification',
      'ETPWindChartDocumentSpecification' => '\\ETPWindChartDocumentSpecification',
      'NatTrackDocumentSpecification' => '\\NatTrackDocumentSpecification',
      'SingleEngineRiskChartDocumentSpecification' => '\\SingleEngineRiskChartDocumentSpecification',
      'TripSpecification' => '\\TripSpecification',
      'ArrayOfFlightSpecification' => '\\ArrayOfFlightSpecification',
      'CalculateTripResponse' => '\\CalculateTripResponse',
      'TripCalculationResponse' => '\\TripCalculationResponse',
      'BriefingPackageResponse' => '\\BriefingPackageResponse',
      'ArrayOfBriefingDocument' => '\\ArrayOfBriefingDocument',
      'BriefingDocument' => '\\BriefingDocument',
      'ArrayOfBriefingDocumentMetaData' => '\\ArrayOfBriefingDocumentMetaData',
      'BriefingDocumentMetaData' => '\\BriefingDocumentMetaData',
      'ArrayOfFlightLog' => '\\ArrayOfFlightLog',
      'TankeringData' => '\\TankeringData',
      'ArrayOfArrayOfTankeringSummary' => '\\ArrayOfArrayOfTankeringSummary',
      'ArrayOfTankeringSummary' => '\\ArrayOfTankeringSummary',
      'TankeringSummary' => '\\TankeringSummary',
      'ValidateFlight' => '\\ValidateFlight',
      'FlightValidationRequest' => '\\FlightValidationRequest',
      'ValidateFlightResponse' => '\\ValidateFlightResponse',
      'CFMUValidationResult' => '\\CFMUValidationResult',
      'ArrayOfCFMUValidationError' => '\\ArrayOfCFMUValidationError',
      'CFMUValidationError' => '\\CFMUValidationError',
      'ArrayOfCFMUValidationWarning' => '\\ArrayOfCFMUValidationWarning',
      'CFMUValidationWarning' => '\\CFMUValidationWarning',
      'GenerateBriefingPackage' => '\\GenerateBriefingPackage',
      'BriefingPackageRequest' => '\\BriefingPackageRequest',
      'AdditionalBriefingData' => '\\AdditionalBriefingData',
      'GenerateBriefingPackageResponse' => '\\GenerateBriefingPackageResponse',
      'CalculateQoute' => '\\CalculateQoute',
      'QouteSpecification' => '\\QouteSpecification',
      'CostFunctionSpecification' => '\\CostFunctionSpecification',
      'CostFunction' => '\\CostFunction',
      'DistanceCostFunction' => '\\DistanceCostFunction',
      'ESADCostFunction' => '\\ESADCostFunction',
      'CostMinimizingCostFunction' => '\\CostMinimizingCostFunction',
      'TimeCostFunction' => '\\TimeCostFunction',
      'FuelCostFunction' => '\\FuelCostFunction',
      'ProcedureSpecification' => '\\ProcedureSpecification',
      'PathOptions' => '\\PathOptions',
      'ArrayOfArea' => '\\ArrayOfArea',
      'Area' => '\\Area',
      'ArrayOfAirspaceIdentifier' => '\\ArrayOfAirspaceIdentifier',
      'AirspaceIdentifier' => '\\AirspaceIdentifier',
      'ETOPSSpecification' => '\\ETOPSSpecification',
      'ArrayOfRouteNodeIdentifier' => '\\ArrayOfRouteNodeIdentifier',
      'ProcedureOptions' => '\\ProcedureOptions',
      'ArrayOfProcedureIdentifier' => '\\ArrayOfProcedureIdentifier',
      'ProcedureIdentifier' => '\\ProcedureIdentifier',
      'SIDIdentifier' => '\\SIDIdentifier',
      'STARIdentifier' => '\\STARIdentifier',
      'TransitionPointSpecification' => '\\TransitionPointSpecification',
      'TrackIdentifier' => '\\TrackIdentifier',
      'NATIdentifier' => '\\NATIdentifier',
      'PACIdentifier' => '\\PACIdentifier',
      'AUSIdentifier' => '\\AUSIdentifier',
      'AircraftTraficLoadDefinition' => '\\AircraftTraficLoadDefinition',
      'AbsoluteTraficLoadDefinition' => '\\AbsoluteTraficLoadDefinition',
      'RelativeTraficLoadDefinition' => '\\RelativeTraficLoadDefinition',
      'CalculateQouteResponse' => '\\CalculateQouteResponse',
      'FlightQoute' => '\\FlightQoute',
      'ValidateAircraftSpecificatin' => '\\ValidateAircraftSpecificatin',
      'ValidateAircraftRequest' => '\\ValidateAircraftRequest',
      'ValidateAircraftSpecificatinResponse' => '\\ValidateAircraftSpecificatinResponse',
      'AircraftEquipmentValidationResponse' => '\\AircraftEquipmentValidationResponse',
      'ArrayOfAircraftValidationError' => '\\ArrayOfAircraftValidationError',
      'AircraftValidationError' => '\\AircraftValidationError',
      'TypeReferencedMassAndBalanceConfiguration' => '\\TypeReferencedMassAndBalanceConfiguration',
      'ArrayOfAltitudeChangeInformation' => '\\ArrayOfAltitudeChangeInformation',
      'AltitudeChangeInformation' => '\\AltitudeChangeInformation',
      'ArrayOfstring' => '\\ArrayOfstring',
      'FuelCheckOptionsSpecification' => '\\FuelCheckOptionsSpecification',
      'OutputUnitSpecification' => '\\OutputUnitSpecification',
      'PDFFlightlogGenerationOptions' => '\\PDFFlightlogGenerationOptions',
      'NavLogDocumentSpecification' => '\\NavLogDocumentSpecification',
      'FlightLevelOptimizationData' => '\\FlightLevelOptimizationData',
      'ArrayOfFlightLevelOptimizationDataPoint' => '\\ArrayOfFlightLevelOptimizationDataPoint',
      'FlightLevelOptimizationDataPoint' => '\\FlightLevelOptimizationDataPoint',
      'FuelBurnApproximation' => '\\FuelBurnApproximation',
      'FlightCalculationFault' => '\\FlightCalculationFault',
      'InvalidFlightSpecificationFault' => '\\InvalidFlightSpecificationFault',
      'UnidentifiableAirportFault' => '\\UnidentifiableAirportFault',
      'UnidentifiableAircraftFault' => '\\UnidentifiableAircraftFault',
      'AircraftGenericDataDeviationFault' => '\\AircraftGenericDataDeviationFault',
      'AircraftMaxWeightFault' => '\\AircraftMaxWeightFault',
      'InvalidRouteSpecificationFault' => '\\InvalidRouteSpecificationFault',
      'ValidationFault' => '\\ValidationFault',
      'ArrayOfValidationDetail' => '\\ArrayOfValidationDetail',
      'ValidationDetail' => '\\ValidationDetail',
      'ArrayOfAircraftGenericDataMaxDeviatioNExceedError' => '\\ArrayOfAircraftGenericDataMaxDeviatioNExceedError',
      'AircraftGenericDataMaxDeviatioNExceedError' => '\\AircraftGenericDataMaxDeviatioNExceedError',
      'ArrayOfAircraftMaxWeightExceededError' => '\\ArrayOfAircraftMaxWeightExceededError',
      'AircraftMaxWeightExceededError' => '\\AircraftMaxWeightExceededError',
      'QouteCalculationRequest' => '\\QouteCalculationRequest',
      'QouteCalculationResponse' => '\\QouteCalculationResponse',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'wsdl/FlightGenerationService.wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     *
     *     <strong>Deprecated, please use calculate flight plan method instead</strong>
     *
     *     Calculates flight data based on the given flight specification. The result of a flight calculation is a flightlog containing all calculated parameters.
     *     To perform a flight calculation an account ID must be provided such that the service can fetch the necessary aircraft performance data using the specified tailnumber.
     *
     *     A calculation request describing the flight. The flight specification will be validated for errors before calculations are initialized. Furthermore the service will make sure the specified aircraft tailnumber is registered with the services and all specified airports can be uniquely identified
     *
     *     A flightlog object containing calculated flight data such as flight fuel and flight times
     *
     *
     * @param CalculateFlight $parameters
     * @return CalculateFlightResponse
     */
    public function CalculateFlight(CalculateFlight $parameters)
    {
      return $this->__soapCall('CalculateFlight', array($parameters));
    }

    /**
     *
     *
     *     Calculates flight data based on the given flight specification. The result of a flight calculation is a flightlog containing all calculated parameters.
     *     To perform a flight calculation an account ID must be provided such that the service can fetch the necessary aircraft performance data using the specified tailnumber.
     *
     *     A calculation request describing the flight. The flight specification will be validated for errors before calculations are initialized. Furthermore the service will make sure the specified aircraft tailnumber is registered with the services and all specified airports can be uniquely identified
     *
     *     A flightlog object containing calculated flight data such as flight fuel and flight times
     *
     *
     * @param CalculateFlightPlan $parameters
     * @return CalculateFlightPlanResponse
     */
    public function CalculateFlightPlan(CalculateFlightPlan $parameters)
    {
      return $this->__soapCall('CalculateFlightPlan', array($parameters));
    }

    /**
     *
     *
     *     Calculates flight data based on the given flight specification. The result of a flight calculation is a flightlog containing all calculated parameters.
     *     To perform a flight calculation an account ID must be provided such that the service can fetch the necessary aircraft performance data using the specified tailnumber.
     *
     *     A calculation request describing the flight. The flight specification will be validated for errors before calculations are initialized. Furthermore the service will make sure the specified aircraft tailnumber is registered with the services and all specified airports can be uniquely identified
     *
     *     A flightlog object containing calculated flight data such as flight fuel and flight times
     *
     *
     * @param CalculateTrip $parameters
     * @return CalculateTripResponse
     */
    public function CalculateTrip(CalculateTrip $parameters)
    {
      return $this->__soapCall('CalculateTrip', array($parameters));
    }

    /**
     *
     *     Validates the flight using CFMU IFPS flight plan assistance services.
     *     To validate the flight either a flightlog or a flight specification can be provided.
     *     To achieve the most accurate validation results, a flightlog (e.g. calculated) flight should be provided.
     *
     *     Request containing flight for validation. Account ID must be set to retrieve the relevant aircraft
     *     A CFMU validation result containing a list of validation errors
     *
     * @param ValidateFlight $parameters
     * @return ValidateFlightResponse
     */
    public function ValidateFlight(ValidateFlight $parameters)
    {
      return $this->__soapCall('ValidateFlight', array($parameters));
    }

    /**
     *
     *     Generates briefing documents based on a flight specification object.
     *     The services will internally do a flight calculation based on the given flight specification and provide you with briefing documents based on these data.
     *
     *     Request containing the data needed for briefing package generation. Account ID must be set to retrieve relevant data from account repository
     *     A briefing package response containing the documents (as byte arrays) and document meta data
     *
     * @param GenerateBriefingPackage $parameters
     * @return GenerateBriefingPackageResponse
     */
    public function GenerateBriefingPackage(GenerateBriefingPackage $parameters)
    {
      return $this->__soapCall('GenerateBriefingPackage', array($parameters));
    }

    /**
     *
     *     Calculates a flight quote
     *
     *     The quote calculation request which contains basic flight data and optimization method
     *
     *
     * @param CalculateQoute $parameters
     * @return CalculateQouteResponse
     */
    public function CalculateQoute(CalculateQoute $parameters)
    {
      return $this->__soapCall('CalculateQoute', array($parameters));
    }

    /**
     * @param ValidateAircraftSpecificatin $parameters
     * @return ValidateAircraftSpecificatinResponse
     */
    public function ValidateAircraftSpecificatin(ValidateAircraftSpecificatin $parameters)
    {
      return $this->__soapCall('ValidateAircraftSpecificatin', array($parameters));
    }

}
