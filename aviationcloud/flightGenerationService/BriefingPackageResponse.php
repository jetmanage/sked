<?php

class BriefingPackageResponse
{

    /**
     * @var ArrayOfBriefingDocument $Documents
     */
    protected $Documents = null;

    /**
     * @var boolean $HasPerformedFMSUpdate
     */
    protected $HasPerformedFMSUpdate = null;

    /**
     * @var boolean $IsFMSDataLinkSuccessful
     */
    protected $IsFMSDataLinkSuccessful = null;

    /**
     * @var string $RecallIdentifier
     */
    protected $RecallIdentifier = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfBriefingDocument
     */
    public function getDocuments()
    {
      return $this->Documents;
    }

    /**
     * @param ArrayOfBriefingDocument $Documents
     * @return BriefingPackageResponse
     */
    public function setDocuments($Documents)
    {
      $this->Documents = $Documents;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHasPerformedFMSUpdate()
    {
      return $this->HasPerformedFMSUpdate;
    }

    /**
     * @param boolean $HasPerformedFMSUpdate
     * @return BriefingPackageResponse
     */
    public function setHasPerformedFMSUpdate($HasPerformedFMSUpdate)
    {
      $this->HasPerformedFMSUpdate = $HasPerformedFMSUpdate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsFMSDataLinkSuccessful()
    {
      return $this->IsFMSDataLinkSuccessful;
    }

    /**
     * @param boolean $IsFMSDataLinkSuccessful
     * @return BriefingPackageResponse
     */
    public function setIsFMSDataLinkSuccessful($IsFMSDataLinkSuccessful)
    {
      $this->IsFMSDataLinkSuccessful = $IsFMSDataLinkSuccessful;
      return $this;
    }

    /**
     * @return string
     */
    public function getRecallIdentifier()
    {
      return $this->RecallIdentifier;
    }

    /**
     * @param string $RecallIdentifier
     * @return BriefingPackageResponse
     */
    public function setRecallIdentifier($RecallIdentifier)
    {
      $this->RecallIdentifier = $RecallIdentifier;
      return $this;
    }

}
