<?php

class CalculatedAircraftWeights
{

    /**
     * @var Weight $AircraftTrafficLoad
     */
    protected $AircraftTrafficLoad = null;

    /**
     * @var Weight $Alternate1LandingWeight
     */
    protected $Alternate1LandingWeight = null;

    /**
     * @var Weight $Alternate2LandingWeight
     */
    protected $Alternate2LandingWeight = null;

    /**
     * @var Weight $BasicMass
     */
    protected $BasicMass = null;

    /**
     * @var Weight $LandingWeight
     */
    protected $LandingWeight = null;

    /**
     * @var Weight $MaxLandingMass
     */
    protected $MaxLandingMass = null;

    /**
     * @var Weight $MaxRampMass
     */
    protected $MaxRampMass = null;

    /**
     * @var Weight $MaxTakeOffMass
     */
    protected $MaxTakeOffMass = null;

    /**
     * @var Weight $MaxZeroFuelMass
     */
    protected $MaxZeroFuelMass = null;

    /**
     * @var int $NumberOfEngines
     */
    protected $NumberOfEngines = null;

    /**
     * @var Weight $RampMass
     */
    protected $RampMass = null;

    /**
     * @var Weight $TakeOffMass
     */
    protected $TakeOffMass = null;

    /**
     * @var Weight $TakeoffAlternateLandingWeight
     */
    protected $TakeoffAlternateLandingWeight = null;

    /**
     * @var Weight $TotalPayload
     */
    protected $TotalPayload = null;

    /**
     * @var Weight $ZeroFuelMass
     */
    protected $ZeroFuelMass = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getAircraftTrafficLoad()
    {
      return $this->AircraftTrafficLoad;
    }

    /**
     * @param Weight $AircraftTrafficLoad
     * @return CalculatedAircraftWeights
     */
    public function setAircraftTrafficLoad($AircraftTrafficLoad)
    {
      $this->AircraftTrafficLoad = $AircraftTrafficLoad;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getAlternate1LandingWeight()
    {
      return $this->Alternate1LandingWeight;
    }

    /**
     * @param Weight $Alternate1LandingWeight
     * @return CalculatedAircraftWeights
     */
    public function setAlternate1LandingWeight($Alternate1LandingWeight)
    {
      $this->Alternate1LandingWeight = $Alternate1LandingWeight;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getAlternate2LandingWeight()
    {
      return $this->Alternate2LandingWeight;
    }

    /**
     * @param Weight $Alternate2LandingWeight
     * @return CalculatedAircraftWeights
     */
    public function setAlternate2LandingWeight($Alternate2LandingWeight)
    {
      $this->Alternate2LandingWeight = $Alternate2LandingWeight;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getBasicMass()
    {
      return $this->BasicMass;
    }

    /**
     * @param Weight $BasicMass
     * @return CalculatedAircraftWeights
     */
    public function setBasicMass($BasicMass)
    {
      $this->BasicMass = $BasicMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getLandingWeight()
    {
      return $this->LandingWeight;
    }

    /**
     * @param Weight $LandingWeight
     * @return CalculatedAircraftWeights
     */
    public function setLandingWeight($LandingWeight)
    {
      $this->LandingWeight = $LandingWeight;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxLandingMass()
    {
      return $this->MaxLandingMass;
    }

    /**
     * @param Weight $MaxLandingMass
     * @return CalculatedAircraftWeights
     */
    public function setMaxLandingMass($MaxLandingMass)
    {
      $this->MaxLandingMass = $MaxLandingMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxRampMass()
    {
      return $this->MaxRampMass;
    }

    /**
     * @param Weight $MaxRampMass
     * @return CalculatedAircraftWeights
     */
    public function setMaxRampMass($MaxRampMass)
    {
      $this->MaxRampMass = $MaxRampMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxTakeOffMass()
    {
      return $this->MaxTakeOffMass;
    }

    /**
     * @param Weight $MaxTakeOffMass
     * @return CalculatedAircraftWeights
     */
    public function setMaxTakeOffMass($MaxTakeOffMass)
    {
      $this->MaxTakeOffMass = $MaxTakeOffMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxZeroFuelMass()
    {
      return $this->MaxZeroFuelMass;
    }

    /**
     * @param Weight $MaxZeroFuelMass
     * @return CalculatedAircraftWeights
     */
    public function setMaxZeroFuelMass($MaxZeroFuelMass)
    {
      $this->MaxZeroFuelMass = $MaxZeroFuelMass;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfEngines()
    {
      return $this->NumberOfEngines;
    }

    /**
     * @param int $NumberOfEngines
     * @return CalculatedAircraftWeights
     */
    public function setNumberOfEngines($NumberOfEngines)
    {
      $this->NumberOfEngines = $NumberOfEngines;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getRampMass()
    {
      return $this->RampMass;
    }

    /**
     * @param Weight $RampMass
     * @return CalculatedAircraftWeights
     */
    public function setRampMass($RampMass)
    {
      $this->RampMass = $RampMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTakeOffMass()
    {
      return $this->TakeOffMass;
    }

    /**
     * @param Weight $TakeOffMass
     * @return CalculatedAircraftWeights
     */
    public function setTakeOffMass($TakeOffMass)
    {
      $this->TakeOffMass = $TakeOffMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTakeoffAlternateLandingWeight()
    {
      return $this->TakeoffAlternateLandingWeight;
    }

    /**
     * @param Weight $TakeoffAlternateLandingWeight
     * @return CalculatedAircraftWeights
     */
    public function setTakeoffAlternateLandingWeight($TakeoffAlternateLandingWeight)
    {
      $this->TakeoffAlternateLandingWeight = $TakeoffAlternateLandingWeight;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTotalPayload()
    {
      return $this->TotalPayload;
    }

    /**
     * @param Weight $TotalPayload
     * @return CalculatedAircraftWeights
     */
    public function setTotalPayload($TotalPayload)
    {
      $this->TotalPayload = $TotalPayload;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getZeroFuelMass()
    {
      return $this->ZeroFuelMass;
    }

    /**
     * @param Weight $ZeroFuelMass
     * @return CalculatedAircraftWeights
     */
    public function setZeroFuelMass($ZeroFuelMass)
    {
      $this->ZeroFuelMass = $ZeroFuelMass;
      return $this;
    }

}
