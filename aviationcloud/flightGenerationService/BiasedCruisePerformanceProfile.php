<?php

class BiasedCruisePerformanceProfile extends CruiseProfile
{

    /**
     * @var int $FuelDeviation
     */
    protected $FuelDeviation = null;

    /**
     * @var int $ReferencedProfile
     */
    protected $ReferencedProfile = null;

    /**
     * @var int $SpeedDeviation
     */
    protected $SpeedDeviation = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getFuelDeviation()
    {
      return $this->FuelDeviation;
    }

    /**
     * @param int $FuelDeviation
     * @return BiasedCruisePerformanceProfile
     */
    public function setFuelDeviation($FuelDeviation)
    {
      $this->FuelDeviation = $FuelDeviation;
      return $this;
    }

    /**
     * @return int
     */
    public function getReferencedProfile()
    {
      return $this->ReferencedProfile;
    }

    /**
     * @param int $ReferencedProfile
     * @return BiasedCruisePerformanceProfile
     */
    public function setReferencedProfile($ReferencedProfile)
    {
      $this->ReferencedProfile = $ReferencedProfile;
      return $this;
    }

    /**
     * @return int
     */
    public function getSpeedDeviation()
    {
      return $this->SpeedDeviation;
    }

    /**
     * @param int $SpeedDeviation
     * @return BiasedCruisePerformanceProfile
     */
    public function setSpeedDeviation($SpeedDeviation)
    {
      $this->SpeedDeviation = $SpeedDeviation;
      return $this;
    }

}
