<?php

class AircraftValidationError
{

    /**
     * @var int $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorDescription
     */
    protected $ErrorDescription = null;

    /**
     * @var string $ErrorKey
     */
    protected $ErrorKey = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param int $ErrorCode
     * @return AircraftValidationError
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorDescription()
    {
      return $this->ErrorDescription;
    }

    /**
     * @param string $ErrorDescription
     * @return AircraftValidationError
     */
    public function setErrorDescription($ErrorDescription)
    {
      $this->ErrorDescription = $ErrorDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorKey()
    {
      return $this->ErrorKey;
    }

    /**
     * @param string $ErrorKey
     * @return AircraftValidationError
     */
    public function setErrorKey($ErrorKey)
    {
      $this->ErrorKey = $ErrorKey;
      return $this;
    }

}
