<?php


 function autoload_731cf93c4370d04c90cf53d1cd92993f($class)
{
    $classes = array(
        'FlightPlanGenerationServices' => __DIR__ .'/FlightPlanGenerationServices.php',
        'CalculateFlight' => __DIR__ .'/CalculateFlight.php',
        'FlightCalculationRequest' => __DIR__ .'/FlightCalculationRequest.php',
        'RequestBase' => __DIR__ .'/RequestBase.php',
        'FlightSpecification' => __DIR__ .'/FlightSpecification.php',
        'ATCData' => __DIR__ .'/ATCData.php',
        'ATCFlightRule' => __DIR__ .'/ATCFlightRule.php',
        'ArrayOfATCOtherItemField' => __DIR__ .'/ArrayOfATCOtherItemField.php',
        'ATCOtherItemField' => __DIR__ .'/ATCOtherItemField.php',
        'Length' => __DIR__ .'/Length.php',
        'LengthUnit' => __DIR__ .'/LengthUnit.php',
        'ATCSupplementaryInfo' => __DIR__ .'/ATCSupplementaryInfo.php',
        'ATCFlightType' => __DIR__ .'/ATCFlightType.php',
        'Currency' => __DIR__ .'/Currency.php',
        'CurrencyUnit' => __DIR__ .'/CurrencyUnit.php',
        'AircraftSpecification' => __DIR__ .'/AircraftSpecification.php',
        'TailnumberAircraftSpecification' => __DIR__ .'/TailnumberAircraftSpecification.php',
        'ICAOIdentAircraftSpecification' => __DIR__ .'/ICAOIdentAircraftSpecification.php',
        'AircraftATCInfo' => __DIR__ .'/AircraftATCInfo.php',
        'AircraftEquipment' => __DIR__ .'/AircraftEquipment.php',
        'PerformanceBasedNavigationCapabilities' => __DIR__ .'/PerformanceBasedNavigationCapabilities.php',
        'RadioAndNavigationEquipment' => __DIR__ .'/RadioAndNavigationEquipment.php',
        'ATCACARSEquipment' => __DIR__ .'/ATCACARSEquipment.php',
        'CPDLCEquipment' => __DIR__ .'/CPDLCEquipment.php',
        'RTFEquipment' => __DIR__ .'/RTFEquipment.php',
        'SSREquipment' => __DIR__ .'/SSREquipment.php',
        'ADSCapabilities' => __DIR__ .'/ADSCapabilities.php',
        'ModeSCapabilities' => __DIR__ .'/ModeSCapabilities.php',
        'AircraftFrequencyAvailability' => __DIR__ .'/AircraftFrequencyAvailability.php',
        'AircraftLifeJacketEquipment' => __DIR__ .'/AircraftLifeJacketEquipment.php',
        'RNAVPreferentialRoutes' => __DIR__ .'/RNAVPreferentialRoutes.php',
        'AircraftSurvivalEquipment' => __DIR__ .'/AircraftSurvivalEquipment.php',
        'Weight' => __DIR__ .'/Weight.php',
        'WeightUnit' => __DIR__ .'/WeightUnit.php',
        'Time' => __DIR__ .'/Time.php',
        'TimeUNit' => __DIR__ .'/TimeUNit.php',
        'FuelTankDefinition' => __DIR__ .'/FuelTankDefinition.php',
        'SimpleFuelTankDefinition' => __DIR__ .'/SimpleFuelTankDefinition.php',
        'FuelTankListDefinition' => __DIR__ .'/FuelTankListDefinition.php',
        'ArrayOfFuelTank' => __DIR__ .'/ArrayOfFuelTank.php',
        'FuelTank' => __DIR__ .'/FuelTank.php',
        'AircraftStructureUnit' => __DIR__ .'/AircraftStructureUnit.php',
        'ArmDefinition' => __DIR__ .'/ArmDefinition.php',
        'StaticArmDefinition' => __DIR__ .'/StaticArmDefinition.php',
        'DynamicArmDefinition' => __DIR__ .'/DynamicArmDefinition.php',
        'ArrayOfWeightDependentArm' => __DIR__ .'/ArrayOfWeightDependentArm.php',
        'WeightDependentArm' => __DIR__ .'/WeightDependentArm.php',
        'PassengerCompartment' => __DIR__ .'/PassengerCompartment.php',
        'CargoCompartment' => __DIR__ .'/CargoCompartment.php',
        'CrewCompartment' => __DIR__ .'/CrewCompartment.php',
        'CrewType' => __DIR__ .'/CrewType.php',
        'MassAndBalanceConfigurationBase' => __DIR__ .'/MassAndBalanceConfigurationBase.php',
        'MassAndBalanceConfiguration' => __DIR__ .'/MassAndBalanceConfiguration.php',
        'SimpleMassAndBalanceConfiguration' => __DIR__ .'/SimpleMassAndBalanceConfiguration.php',
        'CompartmentBasedMassAndBalanceConfiguration' => __DIR__ .'/CompartmentBasedMassAndBalanceConfiguration.php',
        'ArrayOfPassengerCompartment' => __DIR__ .'/ArrayOfPassengerCompartment.php',
        'ArrayOfCargoCompartment' => __DIR__ .'/ArrayOfCargoCompartment.php',
        'ArrayOfCrewCompartment' => __DIR__ .'/ArrayOfCrewCompartment.php',
        'DefaultWeightParameters' => __DIR__ .'/DefaultWeightParameters.php',
        'ReferencedMassAndBalanceConfiguration' => __DIR__ .'/ReferencedMassAndBalanceConfiguration.php',
        'UnspecifiedMassAndBalance' => __DIR__ .'/UnspecifiedMassAndBalance.php',
        'EmbeddedAircraftSpecification' => __DIR__ .'/EmbeddedAircraftSpecification.php',
        'Aircraft' => __DIR__ .'/Aircraft.php',
        'AircraftPerformanceData' => __DIR__ .'/AircraftPerformanceData.php',
        'ArrayOfAircraftPerformanceProfile' => __DIR__ .'/ArrayOfAircraftPerformanceProfile.php',
        'AircraftPerformanceProfile' => __DIR__ .'/AircraftPerformanceProfile.php',
        'BiasedClimbPerformanceProfile' => __DIR__ .'/BiasedClimbPerformanceProfile.php',
        'ClimbProfile' => __DIR__ .'/ClimbProfile.php',
        'DiscreteClimbProfile' => __DIR__ .'/DiscreteClimbProfile.php',
        'ArrayOfClimbDescendDataSeries' => __DIR__ .'/ArrayOfClimbDescendDataSeries.php',
        'ClimbDescendDataSeries' => __DIR__ .'/ClimbDescendDataSeries.php',
        'PerformanceDataSeries' => __DIR__ .'/PerformanceDataSeries.php',
        'HoldingDataSeries' => __DIR__ .'/HoldingDataSeries.php',
        'ArrayOfHoldingDataPoint' => __DIR__ .'/ArrayOfHoldingDataPoint.php',
        'HoldingDataPoint' => __DIR__ .'/HoldingDataPoint.php',
        'PerformanceDataPoint' => __DIR__ .'/PerformanceDataPoint.php',
        'ISAPoint' => __DIR__ .'/ISAPoint.php',
        'ClimbDescendDataPoint' => __DIR__ .'/ClimbDescendDataPoint.php',
        'ClimbDescendData' => __DIR__ .'/ClimbDescendData.php',
        'CruisePerformanceDataPoint' => __DIR__ .'/CruisePerformanceDataPoint.php',
        'AircraftCruisePerformanceData' => __DIR__ .'/AircraftCruisePerformanceData.php',
        'HoldingData' => __DIR__ .'/HoldingData.php',
        'CruisePerformanceDataSeries' => __DIR__ .'/CruisePerformanceDataSeries.php',
        'ArrayOfCruisePerformanceDataPoint' => __DIR__ .'/ArrayOfCruisePerformanceDataPoint.php',
        'ArrayOfClimbDescendDataPoint' => __DIR__ .'/ArrayOfClimbDescendDataPoint.php',
        'SimpleClimbProfile' => __DIR__ .'/SimpleClimbProfile.php',
        'Speed' => __DIR__ .'/Speed.php',
        'SpeedUnit' => __DIR__ .'/SpeedUnit.php',
        'ByAltitudeClimbProfile' => __DIR__ .'/ByAltitudeClimbProfile.php',
        'ArrayOfByAltitudeFuelFlow' => __DIR__ .'/ArrayOfByAltitudeFuelFlow.php',
        'ByAltitudeFuelFlow' => __DIR__ .'/ByAltitudeFuelFlow.php',
        'ArrayOfByAltitudeClimbPerformance' => __DIR__ .'/ArrayOfByAltitudeClimbPerformance.php',
        'ByAltitudeClimbPerformance' => __DIR__ .'/ByAltitudeClimbPerformance.php',
        'BiasedCruisePerformanceProfile' => __DIR__ .'/BiasedCruisePerformanceProfile.php',
        'CruiseProfile' => __DIR__ .'/CruiseProfile.php',
        'DiscreteCruiseProfile' => __DIR__ .'/DiscreteCruiseProfile.php',
        'ArrayOfCruisePerformanceDataSeries' => __DIR__ .'/ArrayOfCruisePerformanceDataSeries.php',
        'ArrayOfWeightLimitation' => __DIR__ .'/ArrayOfWeightLimitation.php',
        'WeightLimitation' => __DIR__ .'/WeightLimitation.php',
        'ETOPSCruiseProfile' => __DIR__ .'/ETOPSCruiseProfile.php',
        'SimpleCruiseProfile' => __DIR__ .'/SimpleCruiseProfile.php',
        'Altitude' => __DIR__ .'/Altitude.php',
        'AltitudeType' => __DIR__ .'/AltitudeType.php',
        'ByAltitudeCruiseProfile' => __DIR__ .'/ByAltitudeCruiseProfile.php',
        'ArrayOfByAltitudeCruisePerformance' => __DIR__ .'/ArrayOfByAltitudeCruisePerformance.php',
        'ByAltitudeCruisePerformance' => __DIR__ .'/ByAltitudeCruisePerformance.php',
        'BiasedDescentProfile' => __DIR__ .'/BiasedDescentProfile.php',
        'DescentProfile' => __DIR__ .'/DescentProfile.php',
        'DiscreteDescentProfile' => __DIR__ .'/DiscreteDescentProfile.php',
        'SimpleDescentProfile' => __DIR__ .'/SimpleDescentProfile.php',
        'ByAltitudeDescentProfile' => __DIR__ .'/ByAltitudeDescentProfile.php',
        'ArrayOfByAltitudeDescentPerformance' => __DIR__ .'/ArrayOfByAltitudeDescentPerformance.php',
        'ByAltitudeDescentPerformance' => __DIR__ .'/ByAltitudeDescentPerformance.php',
        'BiasedHoldingPerformanceProfile' => __DIR__ .'/BiasedHoldingPerformanceProfile.php',
        'HoldingProfile' => __DIR__ .'/HoldingProfile.php',
        'DiscreteHoldingProfile' => __DIR__ .'/DiscreteHoldingProfile.php',
        'ArrayOfHoldingDataSeries' => __DIR__ .'/ArrayOfHoldingDataSeries.php',
        'ReferencedPerformanceProfile' => __DIR__ .'/ReferencedPerformanceProfile.php',
        'AircraftStructure' => __DIR__ .'/AircraftStructure.php',
        'ArrayOfMassAndBalanceConfiguration' => __DIR__ .'/ArrayOfMassAndBalanceConfiguration.php',
        'AircraftEnvelope' => __DIR__ .'/AircraftEnvelope.php',
        'ArrayOfAircraftEnvelopePoint' => __DIR__ .'/ArrayOfAircraftEnvelopePoint.php',
        'AircraftEnvelopePoint' => __DIR__ .'/AircraftEnvelopePoint.php',
        'AircraftEngineSpecification' => __DIR__ .'/AircraftEngineSpecification.php',
        'AircraftType' => __DIR__ .'/AircraftType.php',
        'ATCWakeTurbulanceCategory' => __DIR__ .'/ATCWakeTurbulanceCategory.php',
        'AircraftFuelType' => __DIR__ .'/AircraftFuelType.php',
        'EngineDetails' => __DIR__ .'/EngineDetails.php',
        'AircraftEngineType' => __DIR__ .'/AircraftEngineType.php',
        'ArrayOfAircraftCertification' => __DIR__ .'/ArrayOfAircraftCertification.php',
        'AircraftCertification' => __DIR__ .'/AircraftCertification.php',
        'ETOPSCertification' => __DIR__ .'/ETOPSCertification.php',
        'AircraftTypeConfigurationSpecification' => __DIR__ .'/AircraftTypeConfigurationSpecification.php',
        'SimplifiedAircraftSpecification' => __DIR__ .'/SimplifiedAircraftSpecification.php',
        'UUIDAircraftSpecification' => __DIR__ .'/UUIDAircraftSpecification.php',
        'ByAltitudeAircraftSpecification' => __DIR__ .'/ByAltitudeAircraftSpecification.php',
        'ArrayOfByAltitudeClimbProfile' => __DIR__ .'/ArrayOfByAltitudeClimbProfile.php',
        'ArrayOfByAltitudeCruiseProfile' => __DIR__ .'/ArrayOfByAltitudeCruiseProfile.php',
        'ArrayOfByAltitudeDescentProfile' => __DIR__ .'/ArrayOfByAltitudeDescentProfile.php',
        'Airport' => __DIR__ .'/Airport.php',
        'ArrayOfAirportFrequencyARINC424' => __DIR__ .'/ArrayOfAirportFrequencyARINC424.php',
        'AirportFrequencyARINC424' => __DIR__ .'/AirportFrequencyARINC424.php',
        'FrequencyType' => __DIR__ .'/FrequencyType.php',
        'FrequencyUnit' => __DIR__ .'/FrequencyUnit.php',
        'FIR' => __DIR__ .'/FIR.php',
        'Airspace' => __DIR__ .'/Airspace.php',
        'NationalAirspace' => __DIR__ .'/NationalAirspace.php',
        'SpecialActivityAirspace' => __DIR__ .'/SpecialActivityAirspace.php',
        'TemporaryFlightRestrictedAirspace' => __DIR__ .'/TemporaryFlightRestrictedAirspace.php',
        'SphereicPoint' => __DIR__ .'/SphereicPoint.php',
        'PathPoint' => __DIR__ .'/PathPoint.php',
        'ArrayOfRunway' => __DIR__ .'/ArrayOfRunway.php',
        'Runway' => __DIR__ .'/Runway.php',
        'AirportType' => __DIR__ .'/AirportType.php',
        'FlightCalculationOptions' => __DIR__ .'/FlightCalculationOptions.php',
        'ContingencyStrategy' => __DIR__ .'/ContingencyStrategy.php',
        'DefaultContingenyStrategy' => __DIR__ .'/DefaultContingenyStrategy.php',
        'ERAContingencyStrategy' => __DIR__ .'/ERAContingencyStrategy.php',
        'ReducedContingencyFuelStrategy' => __DIR__ .'/ReducedContingencyFuelStrategy.php',
        'Route' => __DIR__ .'/Route.php',
        'WaypointData' => __DIR__ .'/WaypointData.php',
        'ArrayOfRouteNode' => __DIR__ .'/ArrayOfRouteNode.php',
        'RouteNode' => __DIR__ .'/RouteNode.php',
        'FlightData' => __DIR__ .'/FlightData.php',
        'FlightState' => __DIR__ .'/FlightState.php',
        'RouteNodeIdentifier' => __DIR__ .'/RouteNodeIdentifier.php',
        'ParserInformation' => __DIR__ .'/ParserInformation.php',
        'RouteNodeType' => __DIR__ .'/RouteNodeType.php',
        'RouteProfile' => __DIR__ .'/RouteProfile.php',
        'RouteReport' => __DIR__ .'/RouteReport.php',
        'ArrayOfAirspace' => __DIR__ .'/ArrayOfAirspace.php',
        'ArrayOfInformationItem' => __DIR__ .'/ArrayOfInformationItem.php',
        'InformationItem' => __DIR__ .'/InformationItem.php',
        'ArrayOfInformationData' => __DIR__ .'/ArrayOfInformationData.php',
        'InformationData' => __DIR__ .'/InformationData.php',
        'ArrayOfRouteInfo' => __DIR__ .'/ArrayOfRouteInfo.php',
        'RouteInfo' => __DIR__ .'/RouteInfo.php',
        'ArrayOfWarning' => __DIR__ .'/ArrayOfWarning.php',
        'Warning' => __DIR__ .'/Warning.php',
        'AirportProcedure' => __DIR__ .'/AirportProcedure.php',
        'ArrayOfAirportEnrouteTransitionProcedure' => __DIR__ .'/ArrayOfAirportEnrouteTransitionProcedure.php',
        'AirportEnrouteTransitionProcedure' => __DIR__ .'/AirportEnrouteTransitionProcedure.php',
        'ArrayOfWaypointData' => __DIR__ .'/ArrayOfWaypointData.php',
        'AirportProcedureIdentifier' => __DIR__ .'/AirportProcedureIdentifier.php',
        'FlightLevelOptimizationOptions' => __DIR__ .'/FlightLevelOptimizationOptions.php',
        'PerformanceProfileSpecification' => __DIR__ .'/PerformanceProfileSpecification.php',
        'SimpleftPerformanceProfileSpecification' => __DIR__ .'/SimpleftPerformanceProfileSpecification.php',
        'InterpolatedPerformanceProfileSpecification' => __DIR__ .'/InterpolatedPerformanceProfileSpecification.php',
        'ArrayOfAltitude' => __DIR__ .'/ArrayOfAltitude.php',
        'OptimumFlightlevelOptionSpecification' => __DIR__ .'/OptimumFlightlevelOptionSpecification.php',
        'FuelPolicy' => __DIR__ .'/FuelPolicy.php',
        'RelativeFuelPolicy' => __DIR__ .'/RelativeFuelPolicy.php',
        'ValueFuelPolicy' => __DIR__ .'/ValueFuelPolicy.php',
        'ActualFuelPolicy' => __DIR__ .'/ActualFuelPolicy.php',
        'ExtraFuelPolicy' => __DIR__ .'/ExtraFuelPolicy.php',
        'LandingFuelPolicy' => __DIR__ .'/LandingFuelPolicy.php',
        'TakeOffMassPolicy' => __DIR__ .'/TakeOffMassPolicy.php',
        'LandingMassPolicy' => __DIR__ .'/LandingMassPolicy.php',
        'MinimumRequiredFuelPolicy' => __DIR__ .'/MinimumRequiredFuelPolicy.php',
        'MaximumFuelPolicy' => __DIR__ .'/MaximumFuelPolicy.php',
        'GainLossCalculationInfo' => __DIR__ .'/GainLossCalculationInfo.php',
        'InlineOptionSpecification' => __DIR__ .'/InlineOptionSpecification.php',
        'FuelUnit' => __DIR__ .'/FuelUnit.php',
        'RAIMPredictionOptions' => __DIR__ .'/RAIMPredictionOptions.php',
        'FlightCalculationSIDSTAROptions' => __DIR__ .'/FlightCalculationSIDSTAROptions.php',
        'TaxiFuelSpecification' => __DIR__ .'/TaxiFuelSpecification.php',
        'MassBasedTaxiFuelSpecification' => __DIR__ .'/MassBasedTaxiFuelSpecification.php',
        'TimeBasedTaxiFuelSpecification' => __DIR__ .'/TimeBasedTaxiFuelSpecification.php',
        'TaxiTimeSpecification' => __DIR__ .'/TaxiTimeSpecification.php',
        'ManuelTaxiTime' => __DIR__ .'/ManuelTaxiTime.php',
        'DefaultTaxiTime' => __DIR__ .'/DefaultTaxiTime.php',
        'CargoLoadSpecification' => __DIR__ .'/CargoLoadSpecification.php',
        'SimpleCargoLoadSpecification' => __DIR__ .'/SimpleCargoLoadSpecification.php',
        'SpecificCargoLoadSpecification' => __DIR__ .'/SpecificCargoLoadSpecification.php',
        'ArrayOfCargoSectionLoad' => __DIR__ .'/ArrayOfCargoSectionLoad.php',
        'CargoSectionLoad' => __DIR__ .'/CargoSectionLoad.php',
        'RelativeCargoLoadSpecification' => __DIR__ .'/RelativeCargoLoadSpecification.php',
        'MaxCargoLoadSpecification' => __DIR__ .'/MaxCargoLoadSpecification.php',
        'CrewLoadSpecification' => __DIR__ .'/CrewLoadSpecification.php',
        'SimpleCrewSpecification' => __DIR__ .'/SimpleCrewSpecification.php',
        'SpecificCrewLoadSpecification' => __DIR__ .'/SpecificCrewLoadSpecification.php',
        'ETPSettings' => __DIR__ .'/ETPSettings.php',
        'ArrayOfAirport' => __DIR__ .'/ArrayOfAirport.php',
        'DepressurisationScenario' => __DIR__ .'/DepressurisationScenario.php',
        'ArrayOfETOPSSettingSpecification' => __DIR__ .'/ArrayOfETOPSSettingSpecification.php',
        'ETOPSSettingSpecification' => __DIR__ .'/ETOPSSettingSpecification.php',
        'FlightLevelSpecification' => __DIR__ .'/FlightLevelSpecification.php',
        'MaximumFlightLevelSpecification' => __DIR__ .'/MaximumFlightLevelSpecification.php',
        'ExactFlightLevelSpecification' => __DIR__ .'/ExactFlightLevelSpecification.php',
        'OptimumFlightLevelDefinition' => __DIR__ .'/OptimumFlightLevelDefinition.php',
        'FlightFuelRules' => __DIR__ .'/FlightFuelRules.php',
        'FAAOpsRules' => __DIR__ .'/FAAOpsRules.php',
        'EUOpsRules' => __DIR__ .'/EUOpsRules.php',
        'CustomFuelRule' => __DIR__ .'/CustomFuelRule.php',
        'PaxLoadSpecification' => __DIR__ .'/PaxLoadSpecification.php',
        'SimplePaxLoadSpecification' => __DIR__ .'/SimplePaxLoadSpecification.php',
        'PaxLoad' => __DIR__ .'/PaxLoad.php',
        'UnspecifiedPaxTypeLoad' => __DIR__ .'/UnspecifiedPaxTypeLoad.php',
        'SpecifiedPaxTypeLoad' => __DIR__ .'/SpecifiedPaxTypeLoad.php',
        'SectionBasedPaxLoadSpecification' => __DIR__ .'/SectionBasedPaxLoadSpecification.php',
        'ArrayOfPaxSectionLoad' => __DIR__ .'/ArrayOfPaxSectionLoad.php',
        'PaxSectionLoad' => __DIR__ .'/PaxSectionLoad.php',
        'FlightLevelAdjustmentSpecification' => __DIR__ .'/FlightLevelAdjustmentSpecification.php',
        'ArrayOfAltitudeNode' => __DIR__ .'/ArrayOfAltitudeNode.php',
        'AltitudeNode' => __DIR__ .'/AltitudeNode.php',
        'ArrayOfCruiseChangeNode' => __DIR__ .'/ArrayOfCruiseChangeNode.php',
        'CruiseChangeNode' => __DIR__ .'/CruiseChangeNode.php',
        'Winds' => __DIR__ .'/Winds.php',
        'ForecastWinds' => __DIR__ .'/ForecastWinds.php',
        'DenseWindModel' => __DIR__ .'/DenseWindModel.php',
        'HistoricalWinds' => __DIR__ .'/HistoricalWinds.php',
        'FixedWinds' => __DIR__ .'/FixedWinds.php',
        'CalculateFlightResponse' => __DIR__ .'/CalculateFlightResponse.php',
        'FlightLog' => __DIR__ .'/FlightLog.php',
        'ATCFPLMessage' => __DIR__ .'/ATCFPLMessage.php',
        'ATCFlightMessage' => __DIR__ .'/ATCFlightMessage.php',
        'ATCMessageBase' => __DIR__ .'/ATCMessageBase.php',
        'ATCRejectedMessage' => __DIR__ .'/ATCRejectedMessage.php',
        'ATCRequestFlightPlanMessage' => __DIR__ .'/ATCRequestFlightPlanMessage.php',
        'ATCLongAcknowledgeMessage' => __DIR__ .'/ATCLongAcknowledgeMessage.php',
        'ATCAcknowledgeMessage' => __DIR__ .'/ATCAcknowledgeMessage.php',
        'ATCDivertedArrivialMessage' => __DIR__ .'/ATCDivertedArrivialMessage.php',
        'ATCDepartureMessage' => __DIR__ .'/ATCDepartureMessage.php',
        'ATCDelayMessage' => __DIR__ .'/ATCDelayMessage.php',
        'ATCChangeMessage' => __DIR__ .'/ATCChangeMessage.php',
        'ATCCancelMessage' => __DIR__ .'/ATCCancelMessage.php',
        'ATCArrivalMessage' => __DIR__ .'/ATCArrivalMessage.php',
        'AircraftATCInfoCodes' => __DIR__ .'/AircraftATCInfoCodes.php',
        'ArrayOfAFTNAddress' => __DIR__ .'/ArrayOfAFTNAddress.php',
        'AFTNAddress' => __DIR__ .'/AFTNAddress.php',
        'FLightATCEET' => __DIR__ .'/FLightATCEET.php',
        'ArrayOfFlightATCEETElement' => __DIR__ .'/ArrayOfFlightATCEETElement.php',
        'FlightATCEETElement' => __DIR__ .'/FlightATCEETElement.php',
        'ATCOtherInformation' => __DIR__ .'/ATCOtherInformation.php',
        'ATCOtherInformationCode' => __DIR__ .'/ATCOtherInformationCode.php',
        'ATCOtherInformationContents' => __DIR__ .'/ATCOtherInformationContents.php',
        'CalculatedAircraftWeights' => __DIR__ .'/CalculatedAircraftWeights.php',
        'ArrayOfIntersectionPoint' => __DIR__ .'/ArrayOfIntersectionPoint.php',
        'IntersectionPoint' => __DIR__ .'/IntersectionPoint.php',
        'StartEndPoint' => __DIR__ .'/StartEndPoint.php',
        'BorderIntersectionPoint' => __DIR__ .'/BorderIntersectionPoint.php',
        'InputPoint' => __DIR__ .'/InputPoint.php',
        'CalculationInformation' => __DIR__ .'/CalculationInformation.php',
        'CalculatedETPInformation' => __DIR__ .'/CalculatedETPInformation.php',
        'ArrayOfAirportTimeWindow' => __DIR__ .'/ArrayOfAirportTimeWindow.php',
        'AirportTimeWindow' => __DIR__ .'/AirportTimeWindow.php',
        'ArrayOfETPPoint' => __DIR__ .'/ArrayOfETPPoint.php',
        'ETPPoint' => __DIR__ .'/ETPPoint.php',
        'ETPData' => __DIR__ .'/ETPData.php',
        'ArrayOfETPScenario' => __DIR__ .'/ArrayOfETPScenario.php',
        'ETPScenario' => __DIR__ .'/ETPScenario.php',
        'OverflightCalculationInformation' => __DIR__ .'/OverflightCalculationInformation.php',
        'ArrayOfCountryInformation' => __DIR__ .'/ArrayOfCountryInformation.php',
        'CountryInformation' => __DIR__ .'/CountryInformation.php',
        'ACCurrency' => __DIR__ .'/ACCurrency.php',
        'ArrayOfFIR' => __DIR__ .'/ArrayOfFIR.php',
        'FlightAnalysis' => __DIR__ .'/FlightAnalysis.php',
        'RiskAnalysis' => __DIR__ .'/RiskAnalysis.php',
        'ArrayOfEnrouteDiversionAirport' => __DIR__ .'/ArrayOfEnrouteDiversionAirport.php',
        'EnrouteDiversionAirport' => __DIR__ .'/EnrouteDiversionAirport.php',
        'ArrayOfSphereicPoint' => __DIR__ .'/ArrayOfSphereicPoint.php',
        'ArrayOfFlightSegment' => __DIR__ .'/ArrayOfFlightSegment.php',
        'FlightSegment' => __DIR__ .'/FlightSegment.php',
        'FlightDistance' => __DIR__ .'/FlightDistance.php',
        'FlightFuelValues' => __DIR__ .'/FlightFuelValues.php',
        'FlightMetaData' => __DIR__ .'/FlightMetaData.php',
        'FlightTimes' => __DIR__ .'/FlightTimes.php',
        'FlightMetrologicalData' => __DIR__ .'/FlightMetrologicalData.php',
        'IcingPoint' => __DIR__ .'/IcingPoint.php',
        'IcingSeverity' => __DIR__ .'/IcingSeverity.php',
        'TurbulencePoint' => __DIR__ .'/TurbulencePoint.php',
        'FlightRAIMData' => __DIR__ .'/FlightRAIMData.php',
        'ArrayOfOverflightCosts' => __DIR__ .'/ArrayOfOverflightCosts.php',
        'OverflightCosts' => __DIR__ .'/OverflightCosts.php',
        'PathwayData' => __DIR__ .'/PathwayData.php',
        'ArrayOfDataPoint' => __DIR__ .'/ArrayOfDataPoint.php',
        'DataPoint' => __DIR__ .'/DataPoint.php',
        'AirwayData' => __DIR__ .'/AirwayData.php',
        'FuelData' => __DIR__ .'/FuelData.php',
        'MeteorologicalData' => __DIR__ .'/MeteorologicalData.php',
        'ArrayOfWeatherDataRecord' => __DIR__ .'/ArrayOfWeatherDataRecord.php',
        'WeatherDataRecord' => __DIR__ .'/WeatherDataRecord.php',
        'ArrayOfIcingPoint' => __DIR__ .'/ArrayOfIcingPoint.php',
        'ArrayOfTurbulencePoint' => __DIR__ .'/ArrayOfTurbulencePoint.php',
        'ArrayOfFlightValidationError' => __DIR__ .'/ArrayOfFlightValidationError.php',
        'FlightValidationError' => __DIR__ .'/FlightValidationError.php',
        'Account' => __DIR__ .'/Account.php',
        'CalculateFlightPlan' => __DIR__ .'/CalculateFlightPlan.php',
        'CalculateFlightPlanResponse' => __DIR__ .'/CalculateFlightPlanResponse.php',
        'FlightCalculationResponse' => __DIR__ .'/FlightCalculationResponse.php',
        'CrewBriefingIntegrationDetails' => __DIR__ .'/CrewBriefingIntegrationDetails.php',
        'CalculateTrip' => __DIR__ .'/CalculateTrip.php',
        'TripCalculationRequest' => __DIR__ .'/TripCalculationRequest.php',
        'TripPackageRequest' => __DIR__ .'/TripPackageRequest.php',
        'FuelTankeringDocumentSpecification' => __DIR__ .'/FuelTankeringDocumentSpecification.php',
        'DocumentSpecification' => __DIR__ .'/DocumentSpecification.php',
        'SignifantWeatherDocumentSpecification' => __DIR__ .'/SignifantWeatherDocumentSpecification.php',
        'WindChartDocumentSpecification' => __DIR__ .'/WindChartDocumentSpecification.php',
        'CrossSectionDocumentSpecification' => __DIR__ .'/CrossSectionDocumentSpecification.php',
        'NotamDocumentSpecification' => __DIR__ .'/NotamDocumentSpecification.php',
        'WxNotamDocumentSpecification' => __DIR__ .'/WxNotamDocumentSpecification.php',
        'WxDocumentSpecification' => __DIR__ .'/WxDocumentSpecification.php',
        'PDFFlightlogUnits' => __DIR__ .'/PDFFlightlogUnits.php',
        'ICAOFlightPlanDocumentSpecification' => __DIR__ .'/ICAOFlightPlanDocumentSpecification.php',
        'EnrouteChargeDocumentSpecification' => __DIR__ .'/EnrouteChargeDocumentSpecification.php',
        'ETPWindChartDocumentSpecification' => __DIR__ .'/ETPWindChartDocumentSpecification.php',
        'NatTrackDocumentSpecification' => __DIR__ .'/NatTrackDocumentSpecification.php',
        'SingleEngineRiskChartDocumentSpecification' => __DIR__ .'/SingleEngineRiskChartDocumentSpecification.php',
        'TripSpecification' => __DIR__ .'/TripSpecification.php',
        'ArrayOfFlightSpecification' => __DIR__ .'/ArrayOfFlightSpecification.php',
        'CalculateTripResponse' => __DIR__ .'/CalculateTripResponse.php',
        'TripCalculationResponse' => __DIR__ .'/TripCalculationResponse.php',
        'BriefingPackageResponse' => __DIR__ .'/BriefingPackageResponse.php',
        'ArrayOfBriefingDocument' => __DIR__ .'/ArrayOfBriefingDocument.php',
        'BriefingDocument' => __DIR__ .'/BriefingDocument.php',
        'ArrayOfBriefingDocumentMetaData' => __DIR__ .'/ArrayOfBriefingDocumentMetaData.php',
        'BriefingDocumentMetaData' => __DIR__ .'/BriefingDocumentMetaData.php',
        'ArrayOfFlightLog' => __DIR__ .'/ArrayOfFlightLog.php',
        'TankeringData' => __DIR__ .'/TankeringData.php',
        'ArrayOfArrayOfTankeringSummary' => __DIR__ .'/ArrayOfArrayOfTankeringSummary.php',
        'ArrayOfTankeringSummary' => __DIR__ .'/ArrayOfTankeringSummary.php',
        'TankeringSummary' => __DIR__ .'/TankeringSummary.php',
        'ValidateFlight' => __DIR__ .'/ValidateFlight.php',
        'FlightValidationRequest' => __DIR__ .'/FlightValidationRequest.php',
        'ValidateFlightResponse' => __DIR__ .'/ValidateFlightResponse.php',
        'CFMUValidationResult' => __DIR__ .'/CFMUValidationResult.php',
        'ArrayOfCFMUValidationError' => __DIR__ .'/ArrayOfCFMUValidationError.php',
        'CFMUValidationError' => __DIR__ .'/CFMUValidationError.php',
        'ArrayOfCFMUValidationWarning' => __DIR__ .'/ArrayOfCFMUValidationWarning.php',
        'CFMUValidationWarning' => __DIR__ .'/CFMUValidationWarning.php',
        'GenerateBriefingPackage' => __DIR__ .'/GenerateBriefingPackage.php',
        'BriefingPackageRequest' => __DIR__ .'/BriefingPackageRequest.php',
        'AdditionalBriefingData' => __DIR__ .'/AdditionalBriefingData.php',
        'GenerateBriefingPackageResponse' => __DIR__ .'/GenerateBriefingPackageResponse.php',
        'CalculateQoute' => __DIR__ .'/CalculateQoute.php',
        'QouteSpecification' => __DIR__ .'/QouteSpecification.php',
        'CostFunctionSpecification' => __DIR__ .'/CostFunctionSpecification.php',
        'CostFunction' => __DIR__ .'/CostFunction.php',
        'DistanceCostFunction' => __DIR__ .'/DistanceCostFunction.php',
        'ESADCostFunction' => __DIR__ .'/ESADCostFunction.php',
        'CostMinimizingCostFunction' => __DIR__ .'/CostMinimizingCostFunction.php',
        'TimeCostFunction' => __DIR__ .'/TimeCostFunction.php',
        'FuelCostFunction' => __DIR__ .'/FuelCostFunction.php',
        'ProcedureSpecification' => __DIR__ .'/ProcedureSpecification.php',
        'PathOptions' => __DIR__ .'/PathOptions.php',
        'ArrayOfArea' => __DIR__ .'/ArrayOfArea.php',
        'Area' => __DIR__ .'/Area.php',
        'ArrayOfAirspaceIdentifier' => __DIR__ .'/ArrayOfAirspaceIdentifier.php',
        'AirspaceIdentifier' => __DIR__ .'/AirspaceIdentifier.php',
        'ETOPSSpecification' => __DIR__ .'/ETOPSSpecification.php',
        'ArrayOfRouteNodeIdentifier' => __DIR__ .'/ArrayOfRouteNodeIdentifier.php',
        'ProcedureOptions' => __DIR__ .'/ProcedureOptions.php',
        'ArrayOfProcedureIdentifier' => __DIR__ .'/ArrayOfProcedureIdentifier.php',
        'ProcedureIdentifier' => __DIR__ .'/ProcedureIdentifier.php',
        'SIDIdentifier' => __DIR__ .'/SIDIdentifier.php',
        'STARIdentifier' => __DIR__ .'/STARIdentifier.php',
        'TransitionPointSpecification' => __DIR__ .'/TransitionPointSpecification.php',
        'TrackIdentifier' => __DIR__ .'/TrackIdentifier.php',
        'NATIdentifier' => __DIR__ .'/NATIdentifier.php',
        'PACIdentifier' => __DIR__ .'/PACIdentifier.php',
        'AUSIdentifier' => __DIR__ .'/AUSIdentifier.php',
        'AircraftTraficLoadDefinition' => __DIR__ .'/AircraftTraficLoadDefinition.php',
        'AbsoluteTraficLoadDefinition' => __DIR__ .'/AbsoluteTraficLoadDefinition.php',
        'RelativeTraficLoadDefinition' => __DIR__ .'/RelativeTraficLoadDefinition.php',
        'CalculateQouteResponse' => __DIR__ .'/CalculateQouteResponse.php',
        'FlightQoute' => __DIR__ .'/FlightQoute.php',
        'ValidateAircraftSpecificatin' => __DIR__ .'/ValidateAircraftSpecificatin.php',
        'ValidateAircraftRequest' => __DIR__ .'/ValidateAircraftRequest.php',
        'ValidateAircraftSpecificatinResponse' => __DIR__ .'/ValidateAircraftSpecificatinResponse.php',
        'AircraftEquipmentValidationResponse' => __DIR__ .'/AircraftEquipmentValidationResponse.php',
        'ArrayOfAircraftValidationError' => __DIR__ .'/ArrayOfAircraftValidationError.php',
        'AircraftValidationError' => __DIR__ .'/AircraftValidationError.php',
        'SpecialHandlingReason' => __DIR__ .'/SpecialHandlingReason.php',
        'TypeReferencedMassAndBalanceConfiguration' => __DIR__ .'/TypeReferencedMassAndBalanceConfiguration.php',
        'DataLevel' => __DIR__ .'/DataLevel.php',
        'AircrraftPerformanceProfileIndexType' => __DIR__ .'/AircrraftPerformanceProfileIndexType.php',
        'AircraftRuleSet' => __DIR__ .'/AircraftRuleSet.php',
        'ArrayOfAltitudeChangeInformation' => __DIR__ .'/ArrayOfAltitudeChangeInformation.php',
        'AltitudeChangeInformation' => __DIR__ .'/AltitudeChangeInformation.php',
        'ArrayOfstring' => __DIR__ .'/ArrayOfstring.php',
        'FlightlevelOptimizationCriteria' => __DIR__ .'/FlightlevelOptimizationCriteria.php',
        'FuelCheckOptionsSpecification' => __DIR__ .'/FuelCheckOptionsSpecification.php',
        'OutputUnitSpecification' => __DIR__ .'/OutputUnitSpecification.php',
        'PDFFlightlogGenerationOptions' => __DIR__ .'/PDFFlightlogGenerationOptions.php',
        'NavLogDocumentSpecification' => __DIR__ .'/NavLogDocumentSpecification.php',
        'IntersectionNodeType' => __DIR__ .'/IntersectionNodeType.php',
        'FlightLevelOptimizationData' => __DIR__ .'/FlightLevelOptimizationData.php',
        'ArrayOfFlightLevelOptimizationDataPoint' => __DIR__ .'/ArrayOfFlightLevelOptimizationDataPoint.php',
        'FlightLevelOptimizationDataPoint' => __DIR__ .'/FlightLevelOptimizationDataPoint.php',
        'FuelBurnApproximation' => __DIR__ .'/FuelBurnApproximation.php',
        'FlightCalculationFault' => __DIR__ .'/FlightCalculationFault.php',
        'InvalidFlightSpecificationFault' => __DIR__ .'/InvalidFlightSpecificationFault.php',
        'UnidentifiableAirportFault' => __DIR__ .'/UnidentifiableAirportFault.php',
        'UnidentifiableAircraftFault' => __DIR__ .'/UnidentifiableAircraftFault.php',
        'AircraftGenericDataDeviationFault' => __DIR__ .'/AircraftGenericDataDeviationFault.php',
        'AircraftMaxWeightFault' => __DIR__ .'/AircraftMaxWeightFault.php',
        'InvalidRouteSpecificationFault' => __DIR__ .'/InvalidRouteSpecificationFault.php',
        'ValidationFault' => __DIR__ .'/ValidationFault.php',
        'ArrayOfValidationDetail' => __DIR__ .'/ArrayOfValidationDetail.php',
        'ValidationDetail' => __DIR__ .'/ValidationDetail.php',
        'ArrayOfAircraftGenericDataMaxDeviatioNExceedError' => __DIR__ .'/ArrayOfAircraftGenericDataMaxDeviatioNExceedError.php',
        'AircraftGenericDataMaxDeviatioNExceedError' => __DIR__ .'/AircraftGenericDataMaxDeviatioNExceedError.php',
        'ArrayOfAircraftMaxWeightExceededError' => __DIR__ .'/ArrayOfAircraftMaxWeightExceededError.php',
        'AircraftMaxWeightExceededError' => __DIR__ .'/AircraftMaxWeightExceededError.php',
        'NatTrackDirection' => __DIR__ .'/NatTrackDirection.php',
        'DocumentFormat' => __DIR__ .'/DocumentFormat.php',
        'QouteCalculationRequest' => __DIR__ .'/QouteCalculationRequest.php',
        'QouteCalculationResponse' => __DIR__ .'/QouteCalculationResponse.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_731cf93c4370d04c90cf53d1cd92993f');


{
}
