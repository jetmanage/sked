<?php

class CalculateTripResponse
{

    /**
     * @var TripCalculationResponse $CalculateTripResult
     */
    protected $CalculateTripResult = null;

    /**
     * @param TripCalculationResponse $CalculateTripResult
     */
    public function __construct($CalculateTripResult)
    {
      $this->CalculateTripResult = $CalculateTripResult;
    }

    /**
     * @return TripCalculationResponse
     */
    public function getCalculateTripResult()
    {
      return $this->CalculateTripResult;
    }

    /**
     * @param TripCalculationResponse $CalculateTripResult
     * @return CalculateTripResponse
     */
    public function setCalculateTripResult($CalculateTripResult)
    {
      $this->CalculateTripResult = $CalculateTripResult;
      return $this;
    }

}
