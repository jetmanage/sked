<?php

class ExactFlightLevelSpecification extends FlightLevelSpecification
{

    /**
     * @var int $FlightLevel
     */
    protected $FlightLevel = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getFlightLevel()
    {
      return $this->FlightLevel;
    }

    /**
     * @param int $FlightLevel
     * @return ExactFlightLevelSpecification
     */
    public function setFlightLevel($FlightLevel)
    {
      $this->FlightLevel = $FlightLevel;
      return $this;
    }

}
