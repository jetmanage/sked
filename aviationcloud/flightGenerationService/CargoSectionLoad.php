<?php

class CargoSectionLoad
{

    /**
     * @var Weight $CargoLoad
     */
    protected $CargoLoad = null;

    /**
     * @var string $SectionIdentifier
     */
    protected $SectionIdentifier = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getCargoLoad()
    {
      return $this->CargoLoad;
    }

    /**
     * @param Weight $CargoLoad
     * @return CargoSectionLoad
     */
    public function setCargoLoad($CargoLoad)
    {
      $this->CargoLoad = $CargoLoad;
      return $this;
    }

    /**
     * @return string
     */
    public function getSectionIdentifier()
    {
      return $this->SectionIdentifier;
    }

    /**
     * @param string $SectionIdentifier
     * @return CargoSectionLoad
     */
    public function setSectionIdentifier($SectionIdentifier)
    {
      $this->SectionIdentifier = $SectionIdentifier;
      return $this;
    }

}
