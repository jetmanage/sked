<?php

class CountryInformation
{

    /**
     * @var ACCurrency $AccumulatedCost
     */
    protected $AccumulatedCost = null;

    /**
     * @var ACCurrency $Cost
     */
    protected $Cost = null;

    /**
     * @var string $CountryISO2Code
     */
    protected $CountryISO2Code = null;

    /**
     * @var string $CountryName
     */
    protected $CountryName = null;

    /**
     * @var Length $DistanceFlownIn
     */
    protected $DistanceFlownIn = null;

    /**
     * @var WaypointData $EntryPoint
     */
    protected $EntryPoint = null;

    /**
     * @var \DateTime $EntryTime
     */
    protected $EntryTime = null;

    /**
     * @var WaypointData $ExitPoint
     */
    protected $ExitPoint = null;

    /**
     * @var \DateTime $ExitTime
     */
    protected $ExitTime = null;

    /**
     * @var ArrayOfFIR $Firs
     */
    protected $Firs = null;

    /**
     * @var Length $GreatCircleDistance
     */
    protected $GreatCircleDistance = null;

    /**
     * @var boolean $IsOceanicFIRZone
     */
    protected $IsOceanicFIRZone = null;

    /**
     * @var boolean $NoDataAvailable
     */
    protected $NoDataAvailable = null;

    /**
     * @var duration $TimeInCountry
     */
    protected $TimeInCountry = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ACCurrency
     */
    public function getAccumulatedCost()
    {
      return $this->AccumulatedCost;
    }

    /**
     * @param ACCurrency $AccumulatedCost
     * @return CountryInformation
     */
    public function setAccumulatedCost($AccumulatedCost)
    {
      $this->AccumulatedCost = $AccumulatedCost;
      return $this;
    }

    /**
     * @return ACCurrency
     */
    public function getCost()
    {
      return $this->Cost;
    }

    /**
     * @param ACCurrency $Cost
     * @return CountryInformation
     */
    public function setCost($Cost)
    {
      $this->Cost = $Cost;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryISO2Code()
    {
      return $this->CountryISO2Code;
    }

    /**
     * @param string $CountryISO2Code
     * @return CountryInformation
     */
    public function setCountryISO2Code($CountryISO2Code)
    {
      $this->CountryISO2Code = $CountryISO2Code;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryName()
    {
      return $this->CountryName;
    }

    /**
     * @param string $CountryName
     * @return CountryInformation
     */
    public function setCountryName($CountryName)
    {
      $this->CountryName = $CountryName;
      return $this;
    }

    /**
     * @return Length
     */
    public function getDistanceFlownIn()
    {
      return $this->DistanceFlownIn;
    }

    /**
     * @param Length $DistanceFlownIn
     * @return CountryInformation
     */
    public function setDistanceFlownIn($DistanceFlownIn)
    {
      $this->DistanceFlownIn = $DistanceFlownIn;
      return $this;
    }

    /**
     * @return WaypointData
     */
    public function getEntryPoint()
    {
      return $this->EntryPoint;
    }

    /**
     * @param WaypointData $EntryPoint
     * @return CountryInformation
     */
    public function setEntryPoint($EntryPoint)
    {
      $this->EntryPoint = $EntryPoint;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEntryTime()
    {
      if ($this->EntryTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EntryTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EntryTime
     * @return CountryInformation
     */
    public function setEntryTime(\DateTime $EntryTime = null)
    {
      if ($EntryTime == null) {
       $this->EntryTime = null;
      } else {
        $this->EntryTime = $EntryTime->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return WaypointData
     */
    public function getExitPoint()
    {
      return $this->ExitPoint;
    }

    /**
     * @param WaypointData $ExitPoint
     * @return CountryInformation
     */
    public function setExitPoint($ExitPoint)
    {
      $this->ExitPoint = $ExitPoint;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExitTime()
    {
      if ($this->ExitTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ExitTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ExitTime
     * @return CountryInformation
     */
    public function setExitTime(\DateTime $ExitTime = null)
    {
      if ($ExitTime == null) {
       $this->ExitTime = null;
      } else {
        $this->ExitTime = $ExitTime->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return ArrayOfFIR
     */
    public function getFirs()
    {
      return $this->Firs;
    }

    /**
     * @param ArrayOfFIR $Firs
     * @return CountryInformation
     */
    public function setFirs($Firs)
    {
      $this->Firs = $Firs;
      return $this;
    }

    /**
     * @return Length
     */
    public function getGreatCircleDistance()
    {
      return $this->GreatCircleDistance;
    }

    /**
     * @param Length $GreatCircleDistance
     * @return CountryInformation
     */
    public function setGreatCircleDistance($GreatCircleDistance)
    {
      $this->GreatCircleDistance = $GreatCircleDistance;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsOceanicFIRZone()
    {
      return $this->IsOceanicFIRZone;
    }

    /**
     * @param boolean $IsOceanicFIRZone
     * @return CountryInformation
     */
    public function setIsOceanicFIRZone($IsOceanicFIRZone)
    {
      $this->IsOceanicFIRZone = $IsOceanicFIRZone;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNoDataAvailable()
    {
      return $this->NoDataAvailable;
    }

    /**
     * @param boolean $NoDataAvailable
     * @return CountryInformation
     */
    public function setNoDataAvailable($NoDataAvailable)
    {
      $this->NoDataAvailable = $NoDataAvailable;
      return $this;
    }

    /**
     * @return duration
     */
    public function getTimeInCountry()
    {
      return $this->TimeInCountry;
    }

    /**
     * @param duration $TimeInCountry
     * @return CountryInformation
     */
    public function setTimeInCountry($TimeInCountry)
    {
      $this->TimeInCountry = $TimeInCountry;
      return $this;
    }

}
