<?php

class CrewBriefingIntegrationDetails
{

    /**
     * @var int $FlightId
     */
    protected $FlightId = null;

    /**
     * @var string $UploadError
     */
    protected $UploadError = null;

    /**
     * @var boolean $UploadSucceeded
     */
    protected $UploadSucceeded = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getFlightId()
    {
      return $this->FlightId;
    }

    /**
     * @param int $FlightId
     * @return CrewBriefingIntegrationDetails
     */
    public function setFlightId($FlightId)
    {
      $this->FlightId = $FlightId;
      return $this;
    }

    /**
     * @return string
     */
    public function getUploadError()
    {
      return $this->UploadError;
    }

    /**
     * @param string $UploadError
     * @return CrewBriefingIntegrationDetails
     */
    public function setUploadError($UploadError)
    {
      $this->UploadError = $UploadError;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUploadSucceeded()
    {
      return $this->UploadSucceeded;
    }

    /**
     * @param boolean $UploadSucceeded
     * @return CrewBriefingIntegrationDetails
     */
    public function setUploadSucceeded($UploadSucceeded)
    {
      $this->UploadSucceeded = $UploadSucceeded;
      return $this;
    }

}
