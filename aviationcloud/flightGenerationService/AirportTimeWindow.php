<?php

class AirportTimeWindow
{

    /**
     * @var Airport $Airport
     */
    protected $Airport = null;

    /**
     * @var \DateTime $EarliestArrivalTime
     */
    protected $EarliestArrivalTime = null;

    /**
     * @var \DateTime $LatestArrivalTime
     */
    protected $LatestArrivalTime = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Airport
     */
    public function getAirport()
    {
      return $this->Airport;
    }

    /**
     * @param Airport $Airport
     * @return AirportTimeWindow
     */
    public function setAirport($Airport)
    {
      $this->Airport = $Airport;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEarliestArrivalTime()
    {
      if ($this->EarliestArrivalTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EarliestArrivalTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EarliestArrivalTime
     * @return AirportTimeWindow
     */
    public function setEarliestArrivalTime(\DateTime $EarliestArrivalTime = null)
    {
      if ($EarliestArrivalTime == null) {
       $this->EarliestArrivalTime = null;
      } else {
        $this->EarliestArrivalTime = $EarliestArrivalTime->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLatestArrivalTime()
    {
      if ($this->LatestArrivalTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->LatestArrivalTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $LatestArrivalTime
     * @return AirportTimeWindow
     */
    public function setLatestArrivalTime(\DateTime $LatestArrivalTime = null)
    {
      if ($LatestArrivalTime == null) {
       $this->LatestArrivalTime = null;
      } else {
        $this->LatestArrivalTime = $LatestArrivalTime->format(\DateTime::ATOM);
      }
      return $this;
    }

}
