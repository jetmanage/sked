<?php

class PathwayData
{

    /**
     * @var ArrayOfDataPoint $Alternate1Data
     */
    protected $Alternate1Data = null;

    /**
     * @var ArrayOfDataPoint $Alternate2Data
     */
    protected $Alternate2Data = null;

    /**
     * @var ArrayOfDataPoint $DestinationData
     */
    protected $DestinationData = null;

    /**
     * @var ArrayOfDataPoint $SecondDestinationAlternateData
     */
    protected $SecondDestinationAlternateData = null;

    /**
     * @var ArrayOfDataPoint $SecondDestinationData
     */
    protected $SecondDestinationData = null;

    /**
     * @var ArrayOfDataPoint $TakeOffAlternateData
     */
    protected $TakeOffAlternateData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfDataPoint
     */
    public function getAlternate1Data()
    {
      return $this->Alternate1Data;
    }

    /**
     * @param ArrayOfDataPoint $Alternate1Data
     * @return PathwayData
     */
    public function setAlternate1Data($Alternate1Data)
    {
      $this->Alternate1Data = $Alternate1Data;
      return $this;
    }

    /**
     * @return ArrayOfDataPoint
     */
    public function getAlternate2Data()
    {
      return $this->Alternate2Data;
    }

    /**
     * @param ArrayOfDataPoint $Alternate2Data
     * @return PathwayData
     */
    public function setAlternate2Data($Alternate2Data)
    {
      $this->Alternate2Data = $Alternate2Data;
      return $this;
    }

    /**
     * @return ArrayOfDataPoint
     */
    public function getDestinationData()
    {
      return $this->DestinationData;
    }

    /**
     * @param ArrayOfDataPoint $DestinationData
     * @return PathwayData
     */
    public function setDestinationData($DestinationData)
    {
      $this->DestinationData = $DestinationData;
      return $this;
    }

    /**
     * @return ArrayOfDataPoint
     */
    public function getSecondDestinationAlternateData()
    {
      return $this->SecondDestinationAlternateData;
    }

    /**
     * @param ArrayOfDataPoint $SecondDestinationAlternateData
     * @return PathwayData
     */
    public function setSecondDestinationAlternateData($SecondDestinationAlternateData)
    {
      $this->SecondDestinationAlternateData = $SecondDestinationAlternateData;
      return $this;
    }

    /**
     * @return ArrayOfDataPoint
     */
    public function getSecondDestinationData()
    {
      return $this->SecondDestinationData;
    }

    /**
     * @param ArrayOfDataPoint $SecondDestinationData
     * @return PathwayData
     */
    public function setSecondDestinationData($SecondDestinationData)
    {
      $this->SecondDestinationData = $SecondDestinationData;
      return $this;
    }

    /**
     * @return ArrayOfDataPoint
     */
    public function getTakeOffAlternateData()
    {
      return $this->TakeOffAlternateData;
    }

    /**
     * @param ArrayOfDataPoint $TakeOffAlternateData
     * @return PathwayData
     */
    public function setTakeOffAlternateData($TakeOffAlternateData)
    {
      $this->TakeOffAlternateData = $TakeOffAlternateData;
      return $this;
    }

}
