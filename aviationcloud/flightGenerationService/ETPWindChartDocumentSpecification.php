<?php

class ETPWindChartDocumentSpecification extends DocumentSpecification
{

    /**
     * @var DocumentFormat $Format
     */
    protected $Format = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return DocumentFormat
     */
    public function getFormat()
    {
      return $this->Format;
    }

    /**
     * @param DocumentFormat $Format
     * @return ETPWindChartDocumentSpecification
     */
    public function setFormat($Format)
    {
      $this->Format = $Format;
      return $this;
    }

}
