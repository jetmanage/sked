<?php

class OptimumFlightlevelOptionSpecification
{

    /**
     * @var float $DeltaDeviation
     */
    protected $DeltaDeviation = null;

    /**
     * @var float $DesiredFlightlevel
     */
    protected $DesiredFlightlevel = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getDeltaDeviation()
    {
      return $this->DeltaDeviation;
    }

    /**
     * @param float $DeltaDeviation
     * @return OptimumFlightlevelOptionSpecification
     */
    public function setDeltaDeviation($DeltaDeviation)
    {
      $this->DeltaDeviation = $DeltaDeviation;
      return $this;
    }

    /**
     * @return float
     */
    public function getDesiredFlightlevel()
    {
      return $this->DesiredFlightlevel;
    }

    /**
     * @param float $DesiredFlightlevel
     * @return OptimumFlightlevelOptionSpecification
     */
    public function setDesiredFlightlevel($DesiredFlightlevel)
    {
      $this->DesiredFlightlevel = $DesiredFlightlevel;
      return $this;
    }

}
