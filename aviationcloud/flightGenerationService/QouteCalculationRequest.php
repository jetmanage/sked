<?php

class QouteCalculationRequest extends RequestBase
{

    /**
     * @var QouteSpecification $Flight
     */
    protected $Flight = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return QouteSpecification
     */
    public function getFlight()
    {
      return $this->Flight;
    }

    /**
     * @param QouteSpecification $Flight
     * @return QouteCalculationRequest
     */
    public function setFlight($Flight)
    {
      $this->Flight = $Flight;
      return $this;
    }

}
