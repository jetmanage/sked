<?php

class SignifantWeatherDocumentSpecification extends DocumentSpecification
{

    /**
     * @var NatTrackDirection $Direction
     */
    protected $Direction = null;

    /**
     * @var DocumentFormat $Format
     */
    protected $Format = null;

    /**
     * @var boolean $UseV2Chart
     */
    protected $UseV2Chart = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return NatTrackDirection
     */
    public function getDirection()
    {
      return $this->Direction;
    }

    /**
     * @param NatTrackDirection $Direction
     * @return SignifantWeatherDocumentSpecification
     */
    public function setDirection($Direction)
    {
      $this->Direction = $Direction;
      return $this;
    }

    /**
     * @return DocumentFormat
     */
    public function getFormat()
    {
      return $this->Format;
    }

    /**
     * @param DocumentFormat $Format
     * @return SignifantWeatherDocumentSpecification
     */
    public function setFormat($Format)
    {
      $this->Format = $Format;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseV2Chart()
    {
      return $this->UseV2Chart;
    }

    /**
     * @param boolean $UseV2Chart
     * @return SignifantWeatherDocumentSpecification
     */
    public function setUseV2Chart($UseV2Chart)
    {
      $this->UseV2Chart = $UseV2Chart;
      return $this;
    }

}
