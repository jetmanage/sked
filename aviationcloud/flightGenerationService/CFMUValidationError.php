<?php

class CFMUValidationError
{

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorDescription
     */
    protected $ErrorDescription = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return CFMUValidationError
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorDescription()
    {
      return $this->ErrorDescription;
    }

    /**
     * @param string $ErrorDescription
     * @return CFMUValidationError
     */
    public function setErrorDescription($ErrorDescription)
    {
      $this->ErrorDescription = $ErrorDescription;
      return $this;
    }

}
