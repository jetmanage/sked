<?php

class OverflightCalculationInformation
{

    /**
     * @var ArrayOfCountryInformation $OverflownCountries
     */
    protected $OverflownCountries = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfCountryInformation
     */
    public function getOverflownCountries()
    {
      return $this->OverflownCountries;
    }

    /**
     * @param ArrayOfCountryInformation $OverflownCountries
     * @return OverflightCalculationInformation
     */
    public function setOverflownCountries($OverflownCountries)
    {
      $this->OverflownCountries = $OverflownCountries;
      return $this;
    }

}
