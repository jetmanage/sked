<?php

class FlightLevelOptimizationDataPoint
{

    /**
     * @var CalculationInformation $CalculationReport
     */
    protected $CalculationReport = null;

    /**
     * @var string $CruiseMode
     */
    protected $CruiseMode = null;

    /**
     * @var int $FlightLevel
     */
    protected $FlightLevel = null;

    /**
     * @var Time $FlightTime
     */
    protected $FlightTime = null;

    /**
     * @var Weight $Fuel
     */
    protected $Fuel = null;

    /**
     * @var Weight $LandingMass
     */
    protected $LandingMass = null;

    /**
     * @var Weight $TakeOffMass
     */
    protected $TakeOffMass = null;

    /**
     * @var float $WindCorrection
     */
    protected $WindCorrection = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CalculationInformation
     */
    public function getCalculationReport()
    {
      return $this->CalculationReport;
    }

    /**
     * @param CalculationInformation $CalculationReport
     * @return FlightLevelOptimizationDataPoint
     */
    public function setCalculationReport($CalculationReport)
    {
      $this->CalculationReport = $CalculationReport;
      return $this;
    }

    /**
     * @return string
     */
    public function getCruiseMode()
    {
      return $this->CruiseMode;
    }

    /**
     * @param string $CruiseMode
     * @return FlightLevelOptimizationDataPoint
     */
    public function setCruiseMode($CruiseMode)
    {
      $this->CruiseMode = $CruiseMode;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightLevel()
    {
      return $this->FlightLevel;
    }

    /**
     * @param int $FlightLevel
     * @return FlightLevelOptimizationDataPoint
     */
    public function setFlightLevel($FlightLevel)
    {
      $this->FlightLevel = $FlightLevel;
      return $this;
    }

    /**
     * @return Time
     */
    public function getFlightTime()
    {
      return $this->FlightTime;
    }

    /**
     * @param Time $FlightTime
     * @return FlightLevelOptimizationDataPoint
     */
    public function setFlightTime($FlightTime)
    {
      $this->FlightTime = $FlightTime;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuel()
    {
      return $this->Fuel;
    }

    /**
     * @param Weight $Fuel
     * @return FlightLevelOptimizationDataPoint
     */
    public function setFuel($Fuel)
    {
      $this->Fuel = $Fuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getLandingMass()
    {
      return $this->LandingMass;
    }

    /**
     * @param Weight $LandingMass
     * @return FlightLevelOptimizationDataPoint
     */
    public function setLandingMass($LandingMass)
    {
      $this->LandingMass = $LandingMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTakeOffMass()
    {
      return $this->TakeOffMass;
    }

    /**
     * @param Weight $TakeOffMass
     * @return FlightLevelOptimizationDataPoint
     */
    public function setTakeOffMass($TakeOffMass)
    {
      $this->TakeOffMass = $TakeOffMass;
      return $this;
    }

    /**
     * @return float
     */
    public function getWindCorrection()
    {
      return $this->WindCorrection;
    }

    /**
     * @param float $WindCorrection
     * @return FlightLevelOptimizationDataPoint
     */
    public function setWindCorrection($WindCorrection)
    {
      $this->WindCorrection = $WindCorrection;
      return $this;
    }

}
