<?php

class ICAOFlightPlanDocumentSpecification extends DocumentSpecification
{

    /**
     * @var boolean $DisablePDFFlatten
     */
    protected $DisablePDFFlatten = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return boolean
     */
    public function getDisablePDFFlatten()
    {
      return $this->DisablePDFFlatten;
    }

    /**
     * @param boolean $DisablePDFFlatten
     * @return ICAOFlightPlanDocumentSpecification
     */
    public function setDisablePDFFlatten($DisablePDFFlatten)
    {
      $this->DisablePDFFlatten = $DisablePDFFlatten;
      return $this;
    }

}
