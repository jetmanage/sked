<?php

class AircraftATCInfoCodes
{

    /**
     * @var string $AircraftColorAndMarkings
     */
    protected $AircraftColorAndMarkings = null;

    /**
     * @var int $DingiesCapacity
     */
    protected $DingiesCapacity = null;

    /**
     * @var string $DingiesColor
     */
    protected $DingiesColor = null;

    /**
     * @var boolean $DingiesCovered
     */
    protected $DingiesCovered = null;

    /**
     * @var string $FrequencyAvailability
     */
    protected $FrequencyAvailability = null;

    /**
     * @var string $LifeJacketEquipment
     */
    protected $LifeJacketEquipment = null;

    /**
     * @var int $NumberOfDingies
     */
    protected $NumberOfDingies = null;

    /**
     * @var string $RTCAAircraftAddressCode
     */
    protected $RTCAAircraftAddressCode = null;

    /**
     * @var string $RadioAndNavEquipment
     */
    protected $RadioAndNavEquipment = null;

    /**
     * @var string $SELCALCode
     */
    protected $SELCALCode = null;

    /**
     * @var string $SurveillanceEquipment
     */
    protected $SurveillanceEquipment = null;

    /**
     * @var string $SurvivalEquipment
     */
    protected $SurvivalEquipment = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAircraftColorAndMarkings()
    {
      return $this->AircraftColorAndMarkings;
    }

    /**
     * @param string $AircraftColorAndMarkings
     * @return AircraftATCInfoCodes
     */
    public function setAircraftColorAndMarkings($AircraftColorAndMarkings)
    {
      $this->AircraftColorAndMarkings = $AircraftColorAndMarkings;
      return $this;
    }

    /**
     * @return int
     */
    public function getDingiesCapacity()
    {
      return $this->DingiesCapacity;
    }

    /**
     * @param int $DingiesCapacity
     * @return AircraftATCInfoCodes
     */
    public function setDingiesCapacity($DingiesCapacity)
    {
      $this->DingiesCapacity = $DingiesCapacity;
      return $this;
    }

    /**
     * @return string
     */
    public function getDingiesColor()
    {
      return $this->DingiesColor;
    }

    /**
     * @param string $DingiesColor
     * @return AircraftATCInfoCodes
     */
    public function setDingiesColor($DingiesColor)
    {
      $this->DingiesColor = $DingiesColor;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDingiesCovered()
    {
      return $this->DingiesCovered;
    }

    /**
     * @param boolean $DingiesCovered
     * @return AircraftATCInfoCodes
     */
    public function setDingiesCovered($DingiesCovered)
    {
      $this->DingiesCovered = $DingiesCovered;
      return $this;
    }

    /**
     * @return string
     */
    public function getFrequencyAvailability()
    {
      return $this->FrequencyAvailability;
    }

    /**
     * @param string $FrequencyAvailability
     * @return AircraftATCInfoCodes
     */
    public function setFrequencyAvailability($FrequencyAvailability)
    {
      $this->FrequencyAvailability = $FrequencyAvailability;
      return $this;
    }

    /**
     * @return string
     */
    public function getLifeJacketEquipment()
    {
      return $this->LifeJacketEquipment;
    }

    /**
     * @param string $LifeJacketEquipment
     * @return AircraftATCInfoCodes
     */
    public function setLifeJacketEquipment($LifeJacketEquipment)
    {
      $this->LifeJacketEquipment = $LifeJacketEquipment;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfDingies()
    {
      return $this->NumberOfDingies;
    }

    /**
     * @param int $NumberOfDingies
     * @return AircraftATCInfoCodes
     */
    public function setNumberOfDingies($NumberOfDingies)
    {
      $this->NumberOfDingies = $NumberOfDingies;
      return $this;
    }

    /**
     * @return string
     */
    public function getRTCAAircraftAddressCode()
    {
      return $this->RTCAAircraftAddressCode;
    }

    /**
     * @param string $RTCAAircraftAddressCode
     * @return AircraftATCInfoCodes
     */
    public function setRTCAAircraftAddressCode($RTCAAircraftAddressCode)
    {
      $this->RTCAAircraftAddressCode = $RTCAAircraftAddressCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRadioAndNavEquipment()
    {
      return $this->RadioAndNavEquipment;
    }

    /**
     * @param string $RadioAndNavEquipment
     * @return AircraftATCInfoCodes
     */
    public function setRadioAndNavEquipment($RadioAndNavEquipment)
    {
      $this->RadioAndNavEquipment = $RadioAndNavEquipment;
      return $this;
    }

    /**
     * @return string
     */
    public function getSELCALCode()
    {
      return $this->SELCALCode;
    }

    /**
     * @param string $SELCALCode
     * @return AircraftATCInfoCodes
     */
    public function setSELCALCode($SELCALCode)
    {
      $this->SELCALCode = $SELCALCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSurveillanceEquipment()
    {
      return $this->SurveillanceEquipment;
    }

    /**
     * @param string $SurveillanceEquipment
     * @return AircraftATCInfoCodes
     */
    public function setSurveillanceEquipment($SurveillanceEquipment)
    {
      $this->SurveillanceEquipment = $SurveillanceEquipment;
      return $this;
    }

    /**
     * @return string
     */
    public function getSurvivalEquipment()
    {
      return $this->SurvivalEquipment;
    }

    /**
     * @param string $SurvivalEquipment
     * @return AircraftATCInfoCodes
     */
    public function setSurvivalEquipment($SurvivalEquipment)
    {
      $this->SurvivalEquipment = $SurvivalEquipment;
      return $this;
    }

}
