<?php

class BiasedDescentProfile extends DescentProfile
{

    /**
     * @var int $DistanceDeviation
     */
    protected $DistanceDeviation = null;

    /**
     * @var int $FuelDeviation
     */
    protected $FuelDeviation = null;

    /**
     * @var int $ReferencedProfile
     */
    protected $ReferencedProfile = null;

    /**
     * @var int $TimeDeviation
     */
    protected $TimeDeviation = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getDistanceDeviation()
    {
      return $this->DistanceDeviation;
    }

    /**
     * @param int $DistanceDeviation
     * @return BiasedDescentProfile
     */
    public function setDistanceDeviation($DistanceDeviation)
    {
      $this->DistanceDeviation = $DistanceDeviation;
      return $this;
    }

    /**
     * @return int
     */
    public function getFuelDeviation()
    {
      return $this->FuelDeviation;
    }

    /**
     * @param int $FuelDeviation
     * @return BiasedDescentProfile
     */
    public function setFuelDeviation($FuelDeviation)
    {
      $this->FuelDeviation = $FuelDeviation;
      return $this;
    }

    /**
     * @return int
     */
    public function getReferencedProfile()
    {
      return $this->ReferencedProfile;
    }

    /**
     * @param int $ReferencedProfile
     * @return BiasedDescentProfile
     */
    public function setReferencedProfile($ReferencedProfile)
    {
      $this->ReferencedProfile = $ReferencedProfile;
      return $this;
    }

    /**
     * @return int
     */
    public function getTimeDeviation()
    {
      return $this->TimeDeviation;
    }

    /**
     * @param int $TimeDeviation
     * @return BiasedDescentProfile
     */
    public function setTimeDeviation($TimeDeviation)
    {
      $this->TimeDeviation = $TimeDeviation;
      return $this;
    }

}
