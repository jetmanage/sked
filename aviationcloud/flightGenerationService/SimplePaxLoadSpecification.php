<?php

class SimplePaxLoadSpecification extends PaxLoadSpecification
{

    /**
     * @var PaxLoad $PaxLoad
     */
    protected $PaxLoad = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return PaxLoad
     */
    public function getPaxLoad()
    {
      return $this->PaxLoad;
    }

    /**
     * @param PaxLoad $PaxLoad
     * @return SimplePaxLoadSpecification
     */
    public function setPaxLoad($PaxLoad)
    {
      $this->PaxLoad = $PaxLoad;
      return $this;
    }

}
