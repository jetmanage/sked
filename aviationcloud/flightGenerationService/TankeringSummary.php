<?php

class TankeringSummary
{

    /**
     * @var string $Departure
     */
    protected $Departure = null;

    /**
     * @var string $Destination
     */
    protected $Destination = null;

    /**
     * @var Weight $FinalFuel
     */
    protected $FinalFuel = null;

    /**
     * @var Weight $FuelBurn
     */
    protected $FuelBurn = null;

    /**
     * @var float $FuelCost
     */
    protected $FuelCost = null;

    /**
     * @var string $FuelPriceCurrencyLabel
     */
    protected $FuelPriceCurrencyLabel = null;

    /**
     * @var float $FuelPricePrGallon
     */
    protected $FuelPricePrGallon = null;

    /**
     * @var Weight $FuelUplift
     */
    protected $FuelUplift = null;

    /**
     * @var boolean $FuelUpliftInsufficient
     */
    protected $FuelUpliftInsufficient = null;

    /**
     * @var Weight $InitialFuel
     */
    protected $InitialFuel = null;

    /**
     * @var Weight $LandingFuel
     */
    protected $LandingFuel = null;

    /**
     * @var float $RampFee
     */
    protected $RampFee = null;

    /**
     * @var \DateTime $ScheduledTimeOfDeparture
     */
    protected $ScheduledTimeOfDeparture = null;

    /**
     * @var float $TotalCost
     */
    protected $TotalCost = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDeparture()
    {
      return $this->Departure;
    }

    /**
     * @param string $Departure
     * @return TankeringSummary
     */
    public function setDeparture($Departure)
    {
      $this->Departure = $Departure;
      return $this;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
      return $this->Destination;
    }

    /**
     * @param string $Destination
     * @return TankeringSummary
     */
    public function setDestination($Destination)
    {
      $this->Destination = $Destination;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFinalFuel()
    {
      return $this->FinalFuel;
    }

    /**
     * @param Weight $FinalFuel
     * @return TankeringSummary
     */
    public function setFinalFuel($FinalFuel)
    {
      $this->FinalFuel = $FinalFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuelBurn()
    {
      return $this->FuelBurn;
    }

    /**
     * @param Weight $FuelBurn
     * @return TankeringSummary
     */
    public function setFuelBurn($FuelBurn)
    {
      $this->FuelBurn = $FuelBurn;
      return $this;
    }

    /**
     * @return float
     */
    public function getFuelCost()
    {
      return $this->FuelCost;
    }

    /**
     * @param float $FuelCost
     * @return TankeringSummary
     */
    public function setFuelCost($FuelCost)
    {
      $this->FuelCost = $FuelCost;
      return $this;
    }

    /**
     * @return string
     */
    public function getFuelPriceCurrencyLabel()
    {
      return $this->FuelPriceCurrencyLabel;
    }

    /**
     * @param string $FuelPriceCurrencyLabel
     * @return TankeringSummary
     */
    public function setFuelPriceCurrencyLabel($FuelPriceCurrencyLabel)
    {
      $this->FuelPriceCurrencyLabel = $FuelPriceCurrencyLabel;
      return $this;
    }

    /**
     * @return float
     */
    public function getFuelPricePrGallon()
    {
      return $this->FuelPricePrGallon;
    }

    /**
     * @param float $FuelPricePrGallon
     * @return TankeringSummary
     */
    public function setFuelPricePrGallon($FuelPricePrGallon)
    {
      $this->FuelPricePrGallon = $FuelPricePrGallon;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuelUplift()
    {
      return $this->FuelUplift;
    }

    /**
     * @param Weight $FuelUplift
     * @return TankeringSummary
     */
    public function setFuelUplift($FuelUplift)
    {
      $this->FuelUplift = $FuelUplift;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFuelUpliftInsufficient()
    {
      return $this->FuelUpliftInsufficient;
    }

    /**
     * @param boolean $FuelUpliftInsufficient
     * @return TankeringSummary
     */
    public function setFuelUpliftInsufficient($FuelUpliftInsufficient)
    {
      $this->FuelUpliftInsufficient = $FuelUpliftInsufficient;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getInitialFuel()
    {
      return $this->InitialFuel;
    }

    /**
     * @param Weight $InitialFuel
     * @return TankeringSummary
     */
    public function setInitialFuel($InitialFuel)
    {
      $this->InitialFuel = $InitialFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getLandingFuel()
    {
      return $this->LandingFuel;
    }

    /**
     * @param Weight $LandingFuel
     * @return TankeringSummary
     */
    public function setLandingFuel($LandingFuel)
    {
      $this->LandingFuel = $LandingFuel;
      return $this;
    }

    /**
     * @return float
     */
    public function getRampFee()
    {
      return $this->RampFee;
    }

    /**
     * @param float $RampFee
     * @return TankeringSummary
     */
    public function setRampFee($RampFee)
    {
      $this->RampFee = $RampFee;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getScheduledTimeOfDeparture()
    {
      if ($this->ScheduledTimeOfDeparture == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ScheduledTimeOfDeparture);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ScheduledTimeOfDeparture
     * @return TankeringSummary
     */
    public function setScheduledTimeOfDeparture(\DateTime $ScheduledTimeOfDeparture = null)
    {
      if ($ScheduledTimeOfDeparture == null) {
       $this->ScheduledTimeOfDeparture = null;
      } else {
        $this->ScheduledTimeOfDeparture = $ScheduledTimeOfDeparture->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return float
     */
    public function getTotalCost()
    {
      return $this->TotalCost;
    }

    /**
     * @param float $TotalCost
     * @return TankeringSummary
     */
    public function setTotalCost($TotalCost)
    {
      $this->TotalCost = $TotalCost;
      return $this;
    }

}
