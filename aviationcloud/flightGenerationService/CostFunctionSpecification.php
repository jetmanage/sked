<?php

class CostFunctionSpecification
{

    /**
     * @var boolean $AvoidAllTFRs
     */
    protected $AvoidAllTFRs = null;

    /**
     * @var boolean $AvoidLakes
     */
    protected $AvoidLakes = null;

    /**
     * @var boolean $AvoidMOAs
     */
    protected $AvoidMOAs = null;

    /**
     * @var CostFunction $CostFunction
     */
    protected $CostFunction = null;

    /**
     * @var boolean $EnableNRP
     */
    protected $EnableNRP = null;

    /**
     * @var float $HeuristicLevel
     */
    protected $HeuristicLevel = null;

    /**
     * @var boolean $IgnoreAirspaceElevation
     */
    protected $IgnoreAirspaceElevation = null;

    /**
     * @var boolean $IgnoreConstraints
     */
    protected $IgnoreConstraints = null;

    /**
     * @var boolean $MustUseOTS
     */
    protected $MustUseOTS = null;

    /**
     * @var boolean $PreferClearedAsFiled
     */
    protected $PreferClearedAsFiled = null;

    /**
     * @var float $PreferLand
     */
    protected $PreferLand = null;

    /**
     * @var float $PreferLandToLakes
     */
    protected $PreferLandToLakes = null;

    /**
     * @var ProcedureSpecification $ProcedureDefinition
     */
    protected $ProcedureDefinition = null;

    /**
     * @var int $SearchAggressivenessLevel
     */
    protected $SearchAggressivenessLevel = null;

    /**
     * @var boolean $ShadowOTS
     */
    protected $ShadowOTS = null;

    /**
     * @var boolean $StayOverLand
     */
    protected $StayOverLand = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getAvoidAllTFRs()
    {
      return $this->AvoidAllTFRs;
    }

    /**
     * @param boolean $AvoidAllTFRs
     * @return CostFunctionSpecification
     */
    public function setAvoidAllTFRs($AvoidAllTFRs)
    {
      $this->AvoidAllTFRs = $AvoidAllTFRs;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAvoidLakes()
    {
      return $this->AvoidLakes;
    }

    /**
     * @param boolean $AvoidLakes
     * @return CostFunctionSpecification
     */
    public function setAvoidLakes($AvoidLakes)
    {
      $this->AvoidLakes = $AvoidLakes;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAvoidMOAs()
    {
      return $this->AvoidMOAs;
    }

    /**
     * @param boolean $AvoidMOAs
     * @return CostFunctionSpecification
     */
    public function setAvoidMOAs($AvoidMOAs)
    {
      $this->AvoidMOAs = $AvoidMOAs;
      return $this;
    }

    /**
     * @return CostFunction
     */
    public function getCostFunction()
    {
      return $this->CostFunction;
    }

    /**
     * @param CostFunction $CostFunction
     * @return CostFunctionSpecification
     */
    public function setCostFunction($CostFunction)
    {
      $this->CostFunction = $CostFunction;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableNRP()
    {
      return $this->EnableNRP;
    }

    /**
     * @param boolean $EnableNRP
     * @return CostFunctionSpecification
     */
    public function setEnableNRP($EnableNRP)
    {
      $this->EnableNRP = $EnableNRP;
      return $this;
    }

    /**
     * @return float
     */
    public function getHeuristicLevel()
    {
      return $this->HeuristicLevel;
    }

    /**
     * @param float $HeuristicLevel
     * @return CostFunctionSpecification
     */
    public function setHeuristicLevel($HeuristicLevel)
    {
      $this->HeuristicLevel = $HeuristicLevel;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIgnoreAirspaceElevation()
    {
      return $this->IgnoreAirspaceElevation;
    }

    /**
     * @param boolean $IgnoreAirspaceElevation
     * @return CostFunctionSpecification
     */
    public function setIgnoreAirspaceElevation($IgnoreAirspaceElevation)
    {
      $this->IgnoreAirspaceElevation = $IgnoreAirspaceElevation;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIgnoreConstraints()
    {
      return $this->IgnoreConstraints;
    }

    /**
     * @param boolean $IgnoreConstraints
     * @return CostFunctionSpecification
     */
    public function setIgnoreConstraints($IgnoreConstraints)
    {
      $this->IgnoreConstraints = $IgnoreConstraints;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMustUseOTS()
    {
      return $this->MustUseOTS;
    }

    /**
     * @param boolean $MustUseOTS
     * @return CostFunctionSpecification
     */
    public function setMustUseOTS($MustUseOTS)
    {
      $this->MustUseOTS = $MustUseOTS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPreferClearedAsFiled()
    {
      return $this->PreferClearedAsFiled;
    }

    /**
     * @param boolean $PreferClearedAsFiled
     * @return CostFunctionSpecification
     */
    public function setPreferClearedAsFiled($PreferClearedAsFiled)
    {
      $this->PreferClearedAsFiled = $PreferClearedAsFiled;
      return $this;
    }

    /**
     * @return float
     */
    public function getPreferLand()
    {
      return $this->PreferLand;
    }

    /**
     * @param float $PreferLand
     * @return CostFunctionSpecification
     */
    public function setPreferLand($PreferLand)
    {
      $this->PreferLand = $PreferLand;
      return $this;
    }

    /**
     * @return float
     */
    public function getPreferLandToLakes()
    {
      return $this->PreferLandToLakes;
    }

    /**
     * @param float $PreferLandToLakes
     * @return CostFunctionSpecification
     */
    public function setPreferLandToLakes($PreferLandToLakes)
    {
      $this->PreferLandToLakes = $PreferLandToLakes;
      return $this;
    }

    /**
     * @return ProcedureSpecification
     */
    public function getProcedureDefinition()
    {
      return $this->ProcedureDefinition;
    }

    /**
     * @param ProcedureSpecification $ProcedureDefinition
     * @return CostFunctionSpecification
     */
    public function setProcedureDefinition($ProcedureDefinition)
    {
      $this->ProcedureDefinition = $ProcedureDefinition;
      return $this;
    }

    /**
     * @return int
     */
    public function getSearchAggressivenessLevel()
    {
      return $this->SearchAggressivenessLevel;
    }

    /**
     * @param int $SearchAggressivenessLevel
     * @return CostFunctionSpecification
     */
    public function setSearchAggressivenessLevel($SearchAggressivenessLevel)
    {
      $this->SearchAggressivenessLevel = $SearchAggressivenessLevel;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getShadowOTS()
    {
      return $this->ShadowOTS;
    }

    /**
     * @param boolean $ShadowOTS
     * @return CostFunctionSpecification
     */
    public function setShadowOTS($ShadowOTS)
    {
      $this->ShadowOTS = $ShadowOTS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getStayOverLand()
    {
      return $this->StayOverLand;
    }

    /**
     * @param boolean $StayOverLand
     * @return CostFunctionSpecification
     */
    public function setStayOverLand($StayOverLand)
    {
      $this->StayOverLand = $StayOverLand;
      return $this;
    }

}
