<?php

class GenerateBriefingPackageResponse
{

    /**
     * @var BriefingPackageResponse $GenerateBriefingPackageResult
     */
    protected $GenerateBriefingPackageResult = null;

    /**
     * @param BriefingPackageResponse $GenerateBriefingPackageResult
     */
    public function __construct($GenerateBriefingPackageResult)
    {
      $this->GenerateBriefingPackageResult = $GenerateBriefingPackageResult;
    }

    /**
     * @return BriefingPackageResponse
     */
    public function getGenerateBriefingPackageResult()
    {
      return $this->GenerateBriefingPackageResult;
    }

    /**
     * @param BriefingPackageResponse $GenerateBriefingPackageResult
     * @return GenerateBriefingPackageResponse
     */
    public function setGenerateBriefingPackageResult($GenerateBriefingPackageResult)
    {
      $this->GenerateBriefingPackageResult = $GenerateBriefingPackageResult;
      return $this;
    }

}
