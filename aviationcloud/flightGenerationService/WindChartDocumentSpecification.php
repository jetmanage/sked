<?php

class WindChartDocumentSpecification extends DocumentSpecification
{

    /**
     * @var DocumentFormat $Format
     */
    protected $Format = null;

    /**
     * @var boolean $GenerateForActualFlightLevel
     */
    protected $GenerateForActualFlightLevel = null;

    /**
     * @var boolean $GenerateForHigherFlightLevel
     */
    protected $GenerateForHigherFlightLevel = null;

    /**
     * @var boolean $GenerateForLowerFlightLevel
     */
    protected $GenerateForLowerFlightLevel = null;

    /**
     * @var boolean $UseV2Chart
     */
    protected $UseV2Chart = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return DocumentFormat
     */
    public function getFormat()
    {
      return $this->Format;
    }

    /**
     * @param DocumentFormat $Format
     * @return WindChartDocumentSpecification
     */
    public function setFormat($Format)
    {
      $this->Format = $Format;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getGenerateForActualFlightLevel()
    {
      return $this->GenerateForActualFlightLevel;
    }

    /**
     * @param boolean $GenerateForActualFlightLevel
     * @return WindChartDocumentSpecification
     */
    public function setGenerateForActualFlightLevel($GenerateForActualFlightLevel)
    {
      $this->GenerateForActualFlightLevel = $GenerateForActualFlightLevel;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getGenerateForHigherFlightLevel()
    {
      return $this->GenerateForHigherFlightLevel;
    }

    /**
     * @param boolean $GenerateForHigherFlightLevel
     * @return WindChartDocumentSpecification
     */
    public function setGenerateForHigherFlightLevel($GenerateForHigherFlightLevel)
    {
      $this->GenerateForHigherFlightLevel = $GenerateForHigherFlightLevel;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getGenerateForLowerFlightLevel()
    {
      return $this->GenerateForLowerFlightLevel;
    }

    /**
     * @param boolean $GenerateForLowerFlightLevel
     * @return WindChartDocumentSpecification
     */
    public function setGenerateForLowerFlightLevel($GenerateForLowerFlightLevel)
    {
      $this->GenerateForLowerFlightLevel = $GenerateForLowerFlightLevel;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseV2Chart()
    {
      return $this->UseV2Chart;
    }

    /**
     * @param boolean $UseV2Chart
     * @return WindChartDocumentSpecification
     */
    public function setUseV2Chart($UseV2Chart)
    {
      $this->UseV2Chart = $UseV2Chart;
      return $this;
    }

}
