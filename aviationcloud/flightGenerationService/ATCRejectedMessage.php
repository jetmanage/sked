<?php

class ATCRejectedMessage extends ATCMessageBase
{

    /**
     * @var ATCMessageBase $OriginalMessage
     */
    protected $OriginalMessage = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ATCMessageBase
     */
    public function getOriginalMessage()
    {
      return $this->OriginalMessage;
    }

    /**
     * @param ATCMessageBase $OriginalMessage
     * @return ATCRejectedMessage
     */
    public function setOriginalMessage($OriginalMessage)
    {
      $this->OriginalMessage = $OriginalMessage;
      return $this;
    }

}
