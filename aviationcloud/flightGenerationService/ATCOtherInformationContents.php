<?php

class ATCOtherInformationContents extends ATCOtherInformation
{

    /**
     * @var string $AircraftPerformanceCategory
     */
    protected $AircraftPerformanceCategory = null;

    /**
     * @var ArrayOfATCOtherItemField $OtherItemFields
     */
    protected $OtherItemFields = null;

    /**
     * @var PerformanceBasedNavigationCapabilities $PBNCapabilities
     */
    protected $PBNCapabilities = null;

    /**
     * @var RNAVPreferentialRoutes $RNAVPreferentialRoutes
     */
    protected $RNAVPreferentialRoutes = null;

    /**
     * @var SpecialHandlingReason $ReasonForSpecialHandling
     */
    protected $ReasonForSpecialHandling = null;

    /**
     * @var Length $RunwayVisualRange
     */
    protected $RunwayVisualRange = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAircraftPerformanceCategory()
    {
      return $this->AircraftPerformanceCategory;
    }

    /**
     * @param string $AircraftPerformanceCategory
     * @return ATCOtherInformationContents
     */
    public function setAircraftPerformanceCategory($AircraftPerformanceCategory)
    {
      $this->AircraftPerformanceCategory = $AircraftPerformanceCategory;
      return $this;
    }

    /**
     * @return ArrayOfATCOtherItemField
     */
    public function getOtherItemFields()
    {
      return $this->OtherItemFields;
    }

    /**
     * @param ArrayOfATCOtherItemField $OtherItemFields
     * @return ATCOtherInformationContents
     */
    public function setOtherItemFields($OtherItemFields)
    {
      $this->OtherItemFields = $OtherItemFields;
      return $this;
    }

    /**
     * @return PerformanceBasedNavigationCapabilities
     */
    public function getPBNCapabilities()
    {
      return $this->PBNCapabilities;
    }

    /**
     * @param PerformanceBasedNavigationCapabilities $PBNCapabilities
     * @return ATCOtherInformationContents
     */
    public function setPBNCapabilities($PBNCapabilities)
    {
      $this->PBNCapabilities = $PBNCapabilities;
      return $this;
    }

    /**
     * @return RNAVPreferentialRoutes
     */
    public function getRNAVPreferentialRoutes()
    {
      return $this->RNAVPreferentialRoutes;
    }

    /**
     * @param RNAVPreferentialRoutes $RNAVPreferentialRoutes
     * @return ATCOtherInformationContents
     */
    public function setRNAVPreferentialRoutes($RNAVPreferentialRoutes)
    {
      $this->RNAVPreferentialRoutes = $RNAVPreferentialRoutes;
      return $this;
    }

    /**
     * @return SpecialHandlingReason
     */
    public function getReasonForSpecialHandling()
    {
      return $this->ReasonForSpecialHandling;
    }

    /**
     * @param SpecialHandlingReason $ReasonForSpecialHandling
     * @return ATCOtherInformationContents
     */
    public function setReasonForSpecialHandling($ReasonForSpecialHandling)
    {
      $this->ReasonForSpecialHandling = $ReasonForSpecialHandling;
      return $this;
    }

    /**
     * @return Length
     */
    public function getRunwayVisualRange()
    {
      return $this->RunwayVisualRange;
    }

    /**
     * @param Length $RunwayVisualRange
     * @return ATCOtherInformationContents
     */
    public function setRunwayVisualRange($RunwayVisualRange)
    {
      $this->RunwayVisualRange = $RunwayVisualRange;
      return $this;
    }

}
