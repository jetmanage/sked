<?php

class PDFFlightlogUnits
{

    /**
     * @var FuelUnit $FuelOutputUnit
     */
    protected $FuelOutputUnit = null;

    /**
     * @var WeightUnit $WeightOutputUnit
     */
    protected $WeightOutputUnit = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FuelUnit
     */
    public function getFuelOutputUnit()
    {
      return $this->FuelOutputUnit;
    }

    /**
     * @param FuelUnit $FuelOutputUnit
     * @return PDFFlightlogUnits
     */
    public function setFuelOutputUnit($FuelOutputUnit)
    {
      $this->FuelOutputUnit = $FuelOutputUnit;
      return $this;
    }

    /**
     * @return WeightUnit
     */
    public function getWeightOutputUnit()
    {
      return $this->WeightOutputUnit;
    }

    /**
     * @param WeightUnit $WeightOutputUnit
     * @return PDFFlightlogUnits
     */
    public function setWeightOutputUnit($WeightOutputUnit)
    {
      $this->WeightOutputUnit = $WeightOutputUnit;
      return $this;
    }

}
