<?php

class ValidateAircraftSpecificatinResponse
{

    /**
     * @var AircraftEquipmentValidationResponse $ValidateAircraftSpecificatinResult
     */
    protected $ValidateAircraftSpecificatinResult = null;

    /**
     * @param AircraftEquipmentValidationResponse $ValidateAircraftSpecificatinResult
     */
    public function __construct($ValidateAircraftSpecificatinResult)
    {
      $this->ValidateAircraftSpecificatinResult = $ValidateAircraftSpecificatinResult;
    }

    /**
     * @return AircraftEquipmentValidationResponse
     */
    public function getValidateAircraftSpecificatinResult()
    {
      return $this->ValidateAircraftSpecificatinResult;
    }

    /**
     * @param AircraftEquipmentValidationResponse $ValidateAircraftSpecificatinResult
     * @return ValidateAircraftSpecificatinResponse
     */
    public function setValidateAircraftSpecificatinResult($ValidateAircraftSpecificatinResult)
    {
      $this->ValidateAircraftSpecificatinResult = $ValidateAircraftSpecificatinResult;
      return $this;
    }

}
