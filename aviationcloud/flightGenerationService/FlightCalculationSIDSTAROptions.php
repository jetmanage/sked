<?php

class FlightCalculationSIDSTAROptions
{

    /**
     * @var boolean $DisableAutomaticalProceduresConnections
     */
    protected $DisableAutomaticalProceduresConnections = null;

    /**
     * @var boolean $ForceUseOfProceduresIfAvailable
     */
    protected $ForceUseOfProceduresIfAvailable = null;

    /**
     * @var boolean $FullSIDSTARCalculation
     */
    protected $FullSIDSTARCalculation = null;

    /**
     * @var boolean $IgnoreExplicitSIDSTARsInATCString
     */
    protected $IgnoreExplicitSIDSTARsInATCString = null;

    /**
     * @var Length $SIDDistance
     */
    protected $SIDDistance = null;

    /**
     * @var Length $STARDistance
     */
    protected $STARDistance = null;

    /**
     * @var boolean $UseLongestSIDSTAR
     */
    protected $UseLongestSIDSTAR = null;

    /**
     * @var boolean $UseShortestSIDSTAR
     */
    protected $UseShortestSIDSTAR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getDisableAutomaticalProceduresConnections()
    {
      return $this->DisableAutomaticalProceduresConnections;
    }

    /**
     * @param boolean $DisableAutomaticalProceduresConnections
     * @return FlightCalculationSIDSTAROptions
     */
    public function setDisableAutomaticalProceduresConnections($DisableAutomaticalProceduresConnections)
    {
      $this->DisableAutomaticalProceduresConnections = $DisableAutomaticalProceduresConnections;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getForceUseOfProceduresIfAvailable()
    {
      return $this->ForceUseOfProceduresIfAvailable;
    }

    /**
     * @param boolean $ForceUseOfProceduresIfAvailable
     * @return FlightCalculationSIDSTAROptions
     */
    public function setForceUseOfProceduresIfAvailable($ForceUseOfProceduresIfAvailable)
    {
      $this->ForceUseOfProceduresIfAvailable = $ForceUseOfProceduresIfAvailable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFullSIDSTARCalculation()
    {
      return $this->FullSIDSTARCalculation;
    }

    /**
     * @param boolean $FullSIDSTARCalculation
     * @return FlightCalculationSIDSTAROptions
     */
    public function setFullSIDSTARCalculation($FullSIDSTARCalculation)
    {
      $this->FullSIDSTARCalculation = $FullSIDSTARCalculation;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIgnoreExplicitSIDSTARsInATCString()
    {
      return $this->IgnoreExplicitSIDSTARsInATCString;
    }

    /**
     * @param boolean $IgnoreExplicitSIDSTARsInATCString
     * @return FlightCalculationSIDSTAROptions
     */
    public function setIgnoreExplicitSIDSTARsInATCString($IgnoreExplicitSIDSTARsInATCString)
    {
      $this->IgnoreExplicitSIDSTARsInATCString = $IgnoreExplicitSIDSTARsInATCString;
      return $this;
    }

    /**
     * @return Length
     */
    public function getSIDDistance()
    {
      return $this->SIDDistance;
    }

    /**
     * @param Length $SIDDistance
     * @return FlightCalculationSIDSTAROptions
     */
    public function setSIDDistance($SIDDistance)
    {
      $this->SIDDistance = $SIDDistance;
      return $this;
    }

    /**
     * @return Length
     */
    public function getSTARDistance()
    {
      return $this->STARDistance;
    }

    /**
     * @param Length $STARDistance
     * @return FlightCalculationSIDSTAROptions
     */
    public function setSTARDistance($STARDistance)
    {
      $this->STARDistance = $STARDistance;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseLongestSIDSTAR()
    {
      return $this->UseLongestSIDSTAR;
    }

    /**
     * @param boolean $UseLongestSIDSTAR
     * @return FlightCalculationSIDSTAROptions
     */
    public function setUseLongestSIDSTAR($UseLongestSIDSTAR)
    {
      $this->UseLongestSIDSTAR = $UseLongestSIDSTAR;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseShortestSIDSTAR()
    {
      return $this->UseShortestSIDSTAR;
    }

    /**
     * @param boolean $UseShortestSIDSTAR
     * @return FlightCalculationSIDSTAROptions
     */
    public function setUseShortestSIDSTAR($UseShortestSIDSTAR)
    {
      $this->UseShortestSIDSTAR = $UseShortestSIDSTAR;
      return $this;
    }

}
