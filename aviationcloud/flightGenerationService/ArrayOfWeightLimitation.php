<?php

class ArrayOfWeightLimitation implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var WeightLimitation[] $WeightLimitation
     */
    protected $WeightLimitation = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return WeightLimitation[]
     */
    public function getWeightLimitation()
    {
      return $this->WeightLimitation;
    }

    /**
     * @param WeightLimitation[] $WeightLimitation
     * @return ArrayOfWeightLimitation
     */
    public function setWeightLimitation(array $WeightLimitation = null)
    {
      $this->WeightLimitation = $WeightLimitation;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->WeightLimitation[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return WeightLimitation
     */
    public function offsetGet($offset)
    {
      return $this->WeightLimitation[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param WeightLimitation $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->WeightLimitation[] = $value;
      } else {
        $this->WeightLimitation[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->WeightLimitation[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return WeightLimitation Return the current element
     */
    public function current()
    {
      return current($this->WeightLimitation);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->WeightLimitation);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->WeightLimitation);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->WeightLimitation);
    }

    /**
     * Countable implementation
     *
     * @return WeightLimitation Return count of elements
     */
    public function count()
    {
      return count($this->WeightLimitation);
    }

}
