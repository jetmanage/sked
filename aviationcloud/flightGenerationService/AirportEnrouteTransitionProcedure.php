<?php

class AirportEnrouteTransitionProcedure
{

    /**
     * @var string $Identifier
     */
    protected $Identifier = null;

    /**
     * @var boolean $IsArrival
     */
    protected $IsArrival = null;

    /**
     * @var boolean $IsDeparture
     */
    protected $IsDeparture = null;

    /**
     * @var ArrayOfWaypointData $Path
     */
    protected $Path = null;

    /**
     * @var string $PathCompassDirection
     */
    protected $PathCompassDirection = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
      return $this->Identifier;
    }

    /**
     * @param string $Identifier
     * @return AirportEnrouteTransitionProcedure
     */
    public function setIdentifier($Identifier)
    {
      $this->Identifier = $Identifier;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsArrival()
    {
      return $this->IsArrival;
    }

    /**
     * @param boolean $IsArrival
     * @return AirportEnrouteTransitionProcedure
     */
    public function setIsArrival($IsArrival)
    {
      $this->IsArrival = $IsArrival;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDeparture()
    {
      return $this->IsDeparture;
    }

    /**
     * @param boolean $IsDeparture
     * @return AirportEnrouteTransitionProcedure
     */
    public function setIsDeparture($IsDeparture)
    {
      $this->IsDeparture = $IsDeparture;
      return $this;
    }

    /**
     * @return ArrayOfWaypointData
     */
    public function getPath()
    {
      return $this->Path;
    }

    /**
     * @param ArrayOfWaypointData $Path
     * @return AirportEnrouteTransitionProcedure
     */
    public function setPath($Path)
    {
      $this->Path = $Path;
      return $this;
    }

    /**
     * @return string
     */
    public function getPathCompassDirection()
    {
      return $this->PathCompassDirection;
    }

    /**
     * @param string $PathCompassDirection
     * @return AirportEnrouteTransitionProcedure
     */
    public function setPathCompassDirection($PathCompassDirection)
    {
      $this->PathCompassDirection = $PathCompassDirection;
      return $this;
    }

}
