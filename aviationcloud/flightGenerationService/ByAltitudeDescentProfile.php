<?php

class ByAltitudeDescentProfile extends DescentProfile
{

    /**
     * @var ArrayOfByAltitudeFuelFlow $FuelFlow
     */
    protected $FuelFlow = null;

    /**
     * @var string $ModelName
     */
    protected $ModelName = null;

    /**
     * @var ArrayOfByAltitudeDescentPerformance $Performance
     */
    protected $Performance = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ArrayOfByAltitudeFuelFlow
     */
    public function getFuelFlow()
    {
      return $this->FuelFlow;
    }

    /**
     * @param ArrayOfByAltitudeFuelFlow $FuelFlow
     * @return ByAltitudeDescentProfile
     */
    public function setFuelFlow($FuelFlow)
    {
      $this->FuelFlow = $FuelFlow;
      return $this;
    }

    /**
     * @return string
     */
    public function getModelName()
    {
      return $this->ModelName;
    }

    /**
     * @param string $ModelName
     * @return ByAltitudeDescentProfile
     */
    public function setModelName($ModelName)
    {
      $this->ModelName = $ModelName;
      return $this;
    }

    /**
     * @return ArrayOfByAltitudeDescentPerformance
     */
    public function getPerformance()
    {
      return $this->Performance;
    }

    /**
     * @param ArrayOfByAltitudeDescentPerformance $Performance
     * @return ByAltitudeDescentProfile
     */
    public function setPerformance($Performance)
    {
      $this->Performance = $Performance;
      return $this;
    }

}
