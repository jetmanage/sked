<?php

class ArrayOfAltitudeNode implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AltitudeNode[] $AltitudeNode
     */
    protected $AltitudeNode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AltitudeNode[]
     */
    public function getAltitudeNode()
    {
      return $this->AltitudeNode;
    }

    /**
     * @param AltitudeNode[] $AltitudeNode
     * @return ArrayOfAltitudeNode
     */
    public function setAltitudeNode(array $AltitudeNode = null)
    {
      $this->AltitudeNode = $AltitudeNode;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AltitudeNode[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AltitudeNode
     */
    public function offsetGet($offset)
    {
      return $this->AltitudeNode[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AltitudeNode $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AltitudeNode[] = $value;
      } else {
        $this->AltitudeNode[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AltitudeNode[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AltitudeNode Return the current element
     */
    public function current()
    {
      return current($this->AltitudeNode);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AltitudeNode);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AltitudeNode);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AltitudeNode);
    }

    /**
     * Countable implementation
     *
     * @return AltitudeNode Return count of elements
     */
    public function count()
    {
      return count($this->AltitudeNode);
    }

}
