<?php

class Altitude
{

    /**
     * @var AltitudeType $Unit
     */
    protected $Unit = null;

    /**
     * @var float $Value
     */
    protected $Value = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AltitudeType
     */
    public function getUnit()
    {
      return $this->Unit;
    }

    /**
     * @param AltitudeType $Unit
     * @return Altitude
     */
    public function setUnit($Unit)
    {
      $this->Unit = $Unit;
      return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param float $Value
     * @return Altitude
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

}
