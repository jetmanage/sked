<?php

class RelativeCargoLoadSpecification extends CargoLoadSpecification
{

    /**
     * @var float $PercentLoaded
     */
    protected $PercentLoaded = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return float
     */
    public function getPercentLoaded()
    {
      return $this->PercentLoaded;
    }

    /**
     * @param float $PercentLoaded
     * @return RelativeCargoLoadSpecification
     */
    public function setPercentLoaded($PercentLoaded)
    {
      $this->PercentLoaded = $PercentLoaded;
      return $this;
    }

}
