<?php

class EmbeddedAircraftSpecification extends AircraftSpecification
{

    /**
     * @var Aircraft $Airraft
     */
    protected $Airraft = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Aircraft
     */
    public function getAirraft()
    {
      return $this->Airraft;
    }

    /**
     * @param Aircraft $Airraft
     * @return EmbeddedAircraftSpecification
     */
    public function setAirraft($Airraft)
    {
      $this->Airraft = $Airraft;
      return $this;
    }

}
