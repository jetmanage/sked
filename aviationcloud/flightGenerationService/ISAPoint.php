<?php

class ISAPoint
{

    /**
     * @var float $FlightLevel
     */
    protected $FlightLevel = null;

    /**
     * @var int $IsaDeviation
     */
    protected $IsaDeviation = null;

    /**
     * @var int $WindComponent
     */
    protected $WindComponent = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFlightLevel()
    {
      return $this->FlightLevel;
    }

    /**
     * @param float $FlightLevel
     * @return ISAPoint
     */
    public function setFlightLevel($FlightLevel)
    {
      $this->FlightLevel = $FlightLevel;
      return $this;
    }

    /**
     * @return int
     */
    public function getIsaDeviation()
    {
      return $this->IsaDeviation;
    }

    /**
     * @param int $IsaDeviation
     * @return ISAPoint
     */
    public function setIsaDeviation($IsaDeviation)
    {
      $this->IsaDeviation = $IsaDeviation;
      return $this;
    }

    /**
     * @return int
     */
    public function getWindComponent()
    {
      return $this->WindComponent;
    }

    /**
     * @param int $WindComponent
     * @return ISAPoint
     */
    public function setWindComponent($WindComponent)
    {
      $this->WindComponent = $WindComponent;
      return $this;
    }

}
