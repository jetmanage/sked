<?php

class OptimumFlightLevelDefinition extends FlightLevelSpecification
{

    /**
     * @var int $LowerBound
     */
    protected $LowerBound = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getLowerBound()
    {
      return $this->LowerBound;
    }

    /**
     * @param int $LowerBound
     * @return OptimumFlightLevelDefinition
     */
    public function setLowerBound($LowerBound)
    {
      $this->LowerBound = $LowerBound;
      return $this;
    }

}
