<?php

class NavLogDocumentSpecification extends DocumentSpecification
{

    /**
     * @var SpeedUnit $AirspeedOutputUnit
     */
    protected $AirspeedOutputUnit = null;

    /**
     * @var boolean $EnableRAIMPrediction
     */
    protected $EnableRAIMPrediction = null;

    /**
     * @var ArrayOfstring $FBOFrequencies
     */
    protected $FBOFrequencies = null;

    /**
     * @var string $FBOName
     */
    protected $FBOName = null;

    /**
     * @var ArrayOfstring $FBOPhoneNumbers
     */
    protected $FBOPhoneNumbers = null;

    /**
     * @var DocumentFormat $Format
     */
    protected $Format = null;

    /**
     * @var FuelUnit $FuelOutputUnit
     */
    protected $FuelOutputUnit = null;

    /**
     * @var boolean $Use12Hour
     */
    protected $Use12Hour = null;

    /**
     * @var boolean $UseLocalTime
     */
    protected $UseLocalTime = null;

    /**
     * @var boolean $UseRAIMTestSystem
     */
    protected $UseRAIMTestSystem = null;

    /**
     * @var WeightUnit $WeightOutputUnit
     */
    protected $WeightOutputUnit = null;

    /**
     * @var SpeedUnit $WindspeedOutputUnit
     */
    protected $WindspeedOutputUnit = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return SpeedUnit
     */
    public function getAirspeedOutputUnit()
    {
      return $this->AirspeedOutputUnit;
    }

    /**
     * @param SpeedUnit $AirspeedOutputUnit
     * @return NavLogDocumentSpecification
     */
    public function setAirspeedOutputUnit($AirspeedOutputUnit)
    {
      $this->AirspeedOutputUnit = $AirspeedOutputUnit;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableRAIMPrediction()
    {
      return $this->EnableRAIMPrediction;
    }

    /**
     * @param boolean $EnableRAIMPrediction
     * @return NavLogDocumentSpecification
     */
    public function setEnableRAIMPrediction($EnableRAIMPrediction)
    {
      $this->EnableRAIMPrediction = $EnableRAIMPrediction;
      return $this;
    }

    /**
     * @return ArrayOfstring
     */
    public function getFBOFrequencies()
    {
      return $this->FBOFrequencies;
    }

    /**
     * @param ArrayOfstring $FBOFrequencies
     * @return NavLogDocumentSpecification
     */
    public function setFBOFrequencies($FBOFrequencies)
    {
      $this->FBOFrequencies = $FBOFrequencies;
      return $this;
    }

    /**
     * @return string
     */
    public function getFBOName()
    {
      return $this->FBOName;
    }

    /**
     * @param string $FBOName
     * @return NavLogDocumentSpecification
     */
    public function setFBOName($FBOName)
    {
      $this->FBOName = $FBOName;
      return $this;
    }

    /**
     * @return ArrayOfstring
     */
    public function getFBOPhoneNumbers()
    {
      return $this->FBOPhoneNumbers;
    }

    /**
     * @param ArrayOfstring $FBOPhoneNumbers
     * @return NavLogDocumentSpecification
     */
    public function setFBOPhoneNumbers($FBOPhoneNumbers)
    {
      $this->FBOPhoneNumbers = $FBOPhoneNumbers;
      return $this;
    }

    /**
     * @return DocumentFormat
     */
    public function getFormat()
    {
      return $this->Format;
    }

    /**
     * @param DocumentFormat $Format
     * @return NavLogDocumentSpecification
     */
    public function setFormat($Format)
    {
      $this->Format = $Format;
      return $this;
    }

    /**
     * @return FuelUnit
     */
    public function getFuelOutputUnit()
    {
      return $this->FuelOutputUnit;
    }

    /**
     * @param FuelUnit $FuelOutputUnit
     * @return NavLogDocumentSpecification
     */
    public function setFuelOutputUnit($FuelOutputUnit)
    {
      $this->FuelOutputUnit = $FuelOutputUnit;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUse12Hour()
    {
      return $this->Use12Hour;
    }

    /**
     * @param boolean $Use12Hour
     * @return NavLogDocumentSpecification
     */
    public function setUse12Hour($Use12Hour)
    {
      $this->Use12Hour = $Use12Hour;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseLocalTime()
    {
      return $this->UseLocalTime;
    }

    /**
     * @param boolean $UseLocalTime
     * @return NavLogDocumentSpecification
     */
    public function setUseLocalTime($UseLocalTime)
    {
      $this->UseLocalTime = $UseLocalTime;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseRAIMTestSystem()
    {
      return $this->UseRAIMTestSystem;
    }

    /**
     * @param boolean $UseRAIMTestSystem
     * @return NavLogDocumentSpecification
     */
    public function setUseRAIMTestSystem($UseRAIMTestSystem)
    {
      $this->UseRAIMTestSystem = $UseRAIMTestSystem;
      return $this;
    }

    /**
     * @return WeightUnit
     */
    public function getWeightOutputUnit()
    {
      return $this->WeightOutputUnit;
    }

    /**
     * @param WeightUnit $WeightOutputUnit
     * @return NavLogDocumentSpecification
     */
    public function setWeightOutputUnit($WeightOutputUnit)
    {
      $this->WeightOutputUnit = $WeightOutputUnit;
      return $this;
    }

    /**
     * @return SpeedUnit
     */
    public function getWindspeedOutputUnit()
    {
      return $this->WindspeedOutputUnit;
    }

    /**
     * @param SpeedUnit $WindspeedOutputUnit
     * @return NavLogDocumentSpecification
     */
    public function setWindspeedOutputUnit($WindspeedOutputUnit)
    {
      $this->WindspeedOutputUnit = $WindspeedOutputUnit;
      return $this;
    }

}
