<?php

class StartEndPoint extends IntersectionPoint
{

    /**
     * @var string $FIR
     */
    protected $FIR = null;

    /**
     * @var Airspace $InAirspace
     */
    protected $InAirspace = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getFIR()
    {
      return $this->FIR;
    }

    /**
     * @param string $FIR
     * @return StartEndPoint
     */
    public function setFIR($FIR)
    {
      $this->FIR = $FIR;
      return $this;
    }

    /**
     * @return Airspace
     */
    public function getInAirspace()
    {
      return $this->InAirspace;
    }

    /**
     * @param Airspace $InAirspace
     * @return StartEndPoint
     */
    public function setInAirspace($InAirspace)
    {
      $this->InAirspace = $InAirspace;
      return $this;
    }

}
