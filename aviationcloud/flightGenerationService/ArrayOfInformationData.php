<?php

class ArrayOfInformationData implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var InformationData[] $InformationData
     */
    protected $InformationData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return InformationData[]
     */
    public function getInformationData()
    {
      return $this->InformationData;
    }

    /**
     * @param InformationData[] $InformationData
     * @return ArrayOfInformationData
     */
    public function setInformationData(array $InformationData = null)
    {
      $this->InformationData = $InformationData;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->InformationData[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return InformationData
     */
    public function offsetGet($offset)
    {
      return $this->InformationData[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param InformationData $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->InformationData[] = $value;
      } else {
        $this->InformationData[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->InformationData[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return InformationData Return the current element
     */
    public function current()
    {
      return current($this->InformationData);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->InformationData);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->InformationData);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->InformationData);
    }

    /**
     * Countable implementation
     *
     * @return InformationData Return count of elements
     */
    public function count()
    {
      return count($this->InformationData);
    }

}
