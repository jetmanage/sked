<?php

class AircraftFrequencyAvailability
{

    /**
     * @var boolean $ELT
     */
    protected $ELT = null;

    /**
     * @var boolean $UHF
     */
    protected $UHF = null;

    /**
     * @var boolean $VHF
     */
    protected $VHF = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getELT()
    {
      return $this->ELT;
    }

    /**
     * @param boolean $ELT
     * @return AircraftFrequencyAvailability
     */
    public function setELT($ELT)
    {
      $this->ELT = $ELT;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUHF()
    {
      return $this->UHF;
    }

    /**
     * @param boolean $UHF
     * @return AircraftFrequencyAvailability
     */
    public function setUHF($UHF)
    {
      $this->UHF = $UHF;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getVHF()
    {
      return $this->VHF;
    }

    /**
     * @param boolean $VHF
     * @return AircraftFrequencyAvailability
     */
    public function setVHF($VHF)
    {
      $this->VHF = $VHF;
      return $this;
    }

}
