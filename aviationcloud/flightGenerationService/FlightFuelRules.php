<?php

class FlightFuelRules
{

    /**
     * @var boolean $UseIsolatedAerodromeProcedure
     */
    protected $UseIsolatedAerodromeProcedure = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getUseIsolatedAerodromeProcedure()
    {
      return $this->UseIsolatedAerodromeProcedure;
    }

    /**
     * @param boolean $UseIsolatedAerodromeProcedure
     * @return FlightFuelRules
     */
    public function setUseIsolatedAerodromeProcedure($UseIsolatedAerodromeProcedure)
    {
      $this->UseIsolatedAerodromeProcedure = $UseIsolatedAerodromeProcedure;
      return $this;
    }

}
