<?php

class FlightLevelOptimizationData
{

    /**
     * @var ArrayOfFlightLevelOptimizationDataPoint $AtLowerWeight
     */
    protected $AtLowerWeight = null;

    /**
     * @var FuelBurnApproximation $FuelBurnApproximation
     */
    protected $FuelBurnApproximation = null;

    /**
     * @var ArrayOfFlightLevelOptimizationDataPoint $MainData
     */
    protected $MainData = null;

    /**
     * @var ArrayOfFlightLevelOptimizationDataPoint $UsingAlternativeCruiseProfile
     */
    protected $UsingAlternativeCruiseProfile = null;

    /**
     * @var ArrayOfFlightLevelOptimizationDataPoint $UsingSecondAlternativeCruiseProfile
     */
    protected $UsingSecondAlternativeCruiseProfile = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfFlightLevelOptimizationDataPoint
     */
    public function getAtLowerWeight()
    {
      return $this->AtLowerWeight;
    }

    /**
     * @param ArrayOfFlightLevelOptimizationDataPoint $AtLowerWeight
     * @return FlightLevelOptimizationData
     */
    public function setAtLowerWeight($AtLowerWeight)
    {
      $this->AtLowerWeight = $AtLowerWeight;
      return $this;
    }

    /**
     * @return FuelBurnApproximation
     */
    public function getFuelBurnApproximation()
    {
      return $this->FuelBurnApproximation;
    }

    /**
     * @param FuelBurnApproximation $FuelBurnApproximation
     * @return FlightLevelOptimizationData
     */
    public function setFuelBurnApproximation($FuelBurnApproximation)
    {
      $this->FuelBurnApproximation = $FuelBurnApproximation;
      return $this;
    }

    /**
     * @return ArrayOfFlightLevelOptimizationDataPoint
     */
    public function getMainData()
    {
      return $this->MainData;
    }

    /**
     * @param ArrayOfFlightLevelOptimizationDataPoint $MainData
     * @return FlightLevelOptimizationData
     */
    public function setMainData($MainData)
    {
      $this->MainData = $MainData;
      return $this;
    }

    /**
     * @return ArrayOfFlightLevelOptimizationDataPoint
     */
    public function getUsingAlternativeCruiseProfile()
    {
      return $this->UsingAlternativeCruiseProfile;
    }

    /**
     * @param ArrayOfFlightLevelOptimizationDataPoint $UsingAlternativeCruiseProfile
     * @return FlightLevelOptimizationData
     */
    public function setUsingAlternativeCruiseProfile($UsingAlternativeCruiseProfile)
    {
      $this->UsingAlternativeCruiseProfile = $UsingAlternativeCruiseProfile;
      return $this;
    }

    /**
     * @return ArrayOfFlightLevelOptimizationDataPoint
     */
    public function getUsingSecondAlternativeCruiseProfile()
    {
      return $this->UsingSecondAlternativeCruiseProfile;
    }

    /**
     * @param ArrayOfFlightLevelOptimizationDataPoint $UsingSecondAlternativeCruiseProfile
     * @return FlightLevelOptimizationData
     */
    public function setUsingSecondAlternativeCruiseProfile($UsingSecondAlternativeCruiseProfile)
    {
      $this->UsingSecondAlternativeCruiseProfile = $UsingSecondAlternativeCruiseProfile;
      return $this;
    }

}
