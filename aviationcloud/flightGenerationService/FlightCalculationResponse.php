<?php

class FlightCalculationResponse
{

    /**
     * @var CrewBriefingIntegrationDetails $CrewbriefingUploadData
     */
    protected $CrewbriefingUploadData = null;

    /**
     * @var FlightLog $FlightData
     */
    protected $FlightData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CrewBriefingIntegrationDetails
     */
    public function getCrewbriefingUploadData()
    {
      return $this->CrewbriefingUploadData;
    }

    /**
     * @param CrewBriefingIntegrationDetails $CrewbriefingUploadData
     * @return FlightCalculationResponse
     */
    public function setCrewbriefingUploadData($CrewbriefingUploadData)
    {
      $this->CrewbriefingUploadData = $CrewbriefingUploadData;
      return $this;
    }

    /**
     * @return FlightLog
     */
    public function getFlightData()
    {
      return $this->FlightData;
    }

    /**
     * @param FlightLog $FlightData
     * @return FlightCalculationResponse
     */
    public function setFlightData($FlightData)
    {
      $this->FlightData = $FlightData;
      return $this;
    }

}
