<?php

class FLightATCEET
{

    /**
     * @var string $ATCEETString
     */
    protected $ATCEETString = null;

    /**
     * @var ArrayOfFlightATCEETElement $OverflownFIRs
     */
    protected $OverflownFIRs = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getATCEETString()
    {
      return $this->ATCEETString;
    }

    /**
     * @param string $ATCEETString
     * @return FLightATCEET
     */
    public function setATCEETString($ATCEETString)
    {
      $this->ATCEETString = $ATCEETString;
      return $this;
    }

    /**
     * @return ArrayOfFlightATCEETElement
     */
    public function getOverflownFIRs()
    {
      return $this->OverflownFIRs;
    }

    /**
     * @param ArrayOfFlightATCEETElement $OverflownFIRs
     * @return FLightATCEET
     */
    public function setOverflownFIRs($OverflownFIRs)
    {
      $this->OverflownFIRs = $OverflownFIRs;
      return $this;
    }

}
