<?php

class CalculationInformation
{

    /**
     * @var ArrayOfInformationItem $Report
     */
    protected $Report = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfInformationItem
     */
    public function getReport()
    {
      return $this->Report;
    }

    /**
     * @param ArrayOfInformationItem $Report
     * @return CalculationInformation
     */
    public function setReport($Report)
    {
      $this->Report = $Report;
      return $this;
    }

}
