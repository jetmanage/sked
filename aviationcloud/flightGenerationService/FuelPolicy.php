<?php

class FuelPolicy
{

    /**
     * @var Weight $CompanyFuel
     */
    protected $CompanyFuel = null;

    /**
     * @var float $MinimumGain
     */
    protected $MinimumGain = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getCompanyFuel()
    {
      return $this->CompanyFuel;
    }

    /**
     * @param Weight $CompanyFuel
     * @return FuelPolicy
     */
    public function setCompanyFuel($CompanyFuel)
    {
      $this->CompanyFuel = $CompanyFuel;
      return $this;
    }

    /**
     * @return float
     */
    public function getMinimumGain()
    {
      return $this->MinimumGain;
    }

    /**
     * @param float $MinimumGain
     * @return FuelPolicy
     */
    public function setMinimumGain($MinimumGain)
    {
      $this->MinimumGain = $MinimumGain;
      return $this;
    }

}
