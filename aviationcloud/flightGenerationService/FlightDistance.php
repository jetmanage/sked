<?php

class FlightDistance
{

    /**
     * @var float $Alternate1Course
     */
    protected $Alternate1Course = null;

    /**
     * @var int $Alternate1GridMora
     */
    protected $Alternate1GridMora = null;

    /**
     * @var float $Alternate2Course
     */
    protected $Alternate2Course = null;

    /**
     * @var int $Alternate2GridMora
     */
    protected $Alternate2GridMora = null;

    /**
     * @var Length $ClimbDistance
     */
    protected $ClimbDistance = null;

    /**
     * @var Length $CruiseDistance
     */
    protected $CruiseDistance = null;

    /**
     * @var Length $DescentDistance
     */
    protected $DescentDistance = null;

    /**
     * @var Length $DistanceToAlternate
     */
    protected $DistanceToAlternate = null;

    /**
     * @var Length $DistanceToAlternate1
     */
    protected $DistanceToAlternate1 = null;

    /**
     * @var Length $DistanceToAlternate2
     */
    protected $DistanceToAlternate2 = null;

    /**
     * @var Length $DistanceToDestination
     */
    protected $DistanceToDestination = null;

    /**
     * @var Length $DistanceToTakeoffAlternate
     */
    protected $DistanceToTakeoffAlternate = null;

    /**
     * @var Length $ESADToDestination
     */
    protected $ESADToDestination = null;

    /**
     * @var Length $GCDTo2ndDest
     */
    protected $GCDTo2ndDest = null;

    /**
     * @var Length $GCDTo2ndDestAlt
     */
    protected $GCDTo2ndDestAlt = null;

    /**
     * @var Length $GCDToAlt1
     */
    protected $GCDToAlt1 = null;

    /**
     * @var Length $GCDToAlt2
     */
    protected $GCDToAlt2 = null;

    /**
     * @var Length $GCDToDestination
     */
    protected $GCDToDestination = null;

    /**
     * @var Length $GCDToTakeoffAlternate
     */
    protected $GCDToTakeoffAlternate = null;

    /**
     * @var float $TakeoffAlternateCourse
     */
    protected $TakeoffAlternateCourse = null;

    /**
     * @var int $TakeoffAlternateGridMora
     */
    protected $TakeoffAlternateGridMora = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getAlternate1Course()
    {
      return $this->Alternate1Course;
    }

    /**
     * @param float $Alternate1Course
     * @return FlightDistance
     */
    public function setAlternate1Course($Alternate1Course)
    {
      $this->Alternate1Course = $Alternate1Course;
      return $this;
    }

    /**
     * @return int
     */
    public function getAlternate1GridMora()
    {
      return $this->Alternate1GridMora;
    }

    /**
     * @param int $Alternate1GridMora
     * @return FlightDistance
     */
    public function setAlternate1GridMora($Alternate1GridMora)
    {
      $this->Alternate1GridMora = $Alternate1GridMora;
      return $this;
    }

    /**
     * @return float
     */
    public function getAlternate2Course()
    {
      return $this->Alternate2Course;
    }

    /**
     * @param float $Alternate2Course
     * @return FlightDistance
     */
    public function setAlternate2Course($Alternate2Course)
    {
      $this->Alternate2Course = $Alternate2Course;
      return $this;
    }

    /**
     * @return int
     */
    public function getAlternate2GridMora()
    {
      return $this->Alternate2GridMora;
    }

    /**
     * @param int $Alternate2GridMora
     * @return FlightDistance
     */
    public function setAlternate2GridMora($Alternate2GridMora)
    {
      $this->Alternate2GridMora = $Alternate2GridMora;
      return $this;
    }

    /**
     * @return Length
     */
    public function getClimbDistance()
    {
      return $this->ClimbDistance;
    }

    /**
     * @param Length $ClimbDistance
     * @return FlightDistance
     */
    public function setClimbDistance($ClimbDistance)
    {
      $this->ClimbDistance = $ClimbDistance;
      return $this;
    }

    /**
     * @return Length
     */
    public function getCruiseDistance()
    {
      return $this->CruiseDistance;
    }

    /**
     * @param Length $CruiseDistance
     * @return FlightDistance
     */
    public function setCruiseDistance($CruiseDistance)
    {
      $this->CruiseDistance = $CruiseDistance;
      return $this;
    }

    /**
     * @return Length
     */
    public function getDescentDistance()
    {
      return $this->DescentDistance;
    }

    /**
     * @param Length $DescentDistance
     * @return FlightDistance
     */
    public function setDescentDistance($DescentDistance)
    {
      $this->DescentDistance = $DescentDistance;
      return $this;
    }

    /**
     * @return Length
     */
    public function getDistanceToAlternate()
    {
      return $this->DistanceToAlternate;
    }

    /**
     * @param Length $DistanceToAlternate
     * @return FlightDistance
     */
    public function setDistanceToAlternate($DistanceToAlternate)
    {
      $this->DistanceToAlternate = $DistanceToAlternate;
      return $this;
    }

    /**
     * @return Length
     */
    public function getDistanceToAlternate1()
    {
      return $this->DistanceToAlternate1;
    }

    /**
     * @param Length $DistanceToAlternate1
     * @return FlightDistance
     */
    public function setDistanceToAlternate1($DistanceToAlternate1)
    {
      $this->DistanceToAlternate1 = $DistanceToAlternate1;
      return $this;
    }

    /**
     * @return Length
     */
    public function getDistanceToAlternate2()
    {
      return $this->DistanceToAlternate2;
    }

    /**
     * @param Length $DistanceToAlternate2
     * @return FlightDistance
     */
    public function setDistanceToAlternate2($DistanceToAlternate2)
    {
      $this->DistanceToAlternate2 = $DistanceToAlternate2;
      return $this;
    }

    /**
     * @return Length
     */
    public function getDistanceToDestination()
    {
      return $this->DistanceToDestination;
    }

    /**
     * @param Length $DistanceToDestination
     * @return FlightDistance
     */
    public function setDistanceToDestination($DistanceToDestination)
    {
      $this->DistanceToDestination = $DistanceToDestination;
      return $this;
    }

    /**
     * @return Length
     */
    public function getDistanceToTakeoffAlternate()
    {
      return $this->DistanceToTakeoffAlternate;
    }

    /**
     * @param Length $DistanceToTakeoffAlternate
     * @return FlightDistance
     */
    public function setDistanceToTakeoffAlternate($DistanceToTakeoffAlternate)
    {
      $this->DistanceToTakeoffAlternate = $DistanceToTakeoffAlternate;
      return $this;
    }

    /**
     * @return Length
     */
    public function getESADToDestination()
    {
      return $this->ESADToDestination;
    }

    /**
     * @param Length $ESADToDestination
     * @return FlightDistance
     */
    public function setESADToDestination($ESADToDestination)
    {
      $this->ESADToDestination = $ESADToDestination;
      return $this;
    }

    /**
     * @return Length
     */
    public function getGCDTo2ndDest()
    {
      return $this->GCDTo2ndDest;
    }

    /**
     * @param Length $GCDTo2ndDest
     * @return FlightDistance
     */
    public function setGCDTo2ndDest($GCDTo2ndDest)
    {
      $this->GCDTo2ndDest = $GCDTo2ndDest;
      return $this;
    }

    /**
     * @return Length
     */
    public function getGCDTo2ndDestAlt()
    {
      return $this->GCDTo2ndDestAlt;
    }

    /**
     * @param Length $GCDTo2ndDestAlt
     * @return FlightDistance
     */
    public function setGCDTo2ndDestAlt($GCDTo2ndDestAlt)
    {
      $this->GCDTo2ndDestAlt = $GCDTo2ndDestAlt;
      return $this;
    }

    /**
     * @return Length
     */
    public function getGCDToAlt1()
    {
      return $this->GCDToAlt1;
    }

    /**
     * @param Length $GCDToAlt1
     * @return FlightDistance
     */
    public function setGCDToAlt1($GCDToAlt1)
    {
      $this->GCDToAlt1 = $GCDToAlt1;
      return $this;
    }

    /**
     * @return Length
     */
    public function getGCDToAlt2()
    {
      return $this->GCDToAlt2;
    }

    /**
     * @param Length $GCDToAlt2
     * @return FlightDistance
     */
    public function setGCDToAlt2($GCDToAlt2)
    {
      $this->GCDToAlt2 = $GCDToAlt2;
      return $this;
    }

    /**
     * @return Length
     */
    public function getGCDToDestination()
    {
      return $this->GCDToDestination;
    }

    /**
     * @param Length $GCDToDestination
     * @return FlightDistance
     */
    public function setGCDToDestination($GCDToDestination)
    {
      $this->GCDToDestination = $GCDToDestination;
      return $this;
    }

    /**
     * @return Length
     */
    public function getGCDToTakeoffAlternate()
    {
      return $this->GCDToTakeoffAlternate;
    }

    /**
     * @param Length $GCDToTakeoffAlternate
     * @return FlightDistance
     */
    public function setGCDToTakeoffAlternate($GCDToTakeoffAlternate)
    {
      $this->GCDToTakeoffAlternate = $GCDToTakeoffAlternate;
      return $this;
    }

    /**
     * @return float
     */
    public function getTakeoffAlternateCourse()
    {
      return $this->TakeoffAlternateCourse;
    }

    /**
     * @param float $TakeoffAlternateCourse
     * @return FlightDistance
     */
    public function setTakeoffAlternateCourse($TakeoffAlternateCourse)
    {
      $this->TakeoffAlternateCourse = $TakeoffAlternateCourse;
      return $this;
    }

    /**
     * @return int
     */
    public function getTakeoffAlternateGridMora()
    {
      return $this->TakeoffAlternateGridMora;
    }

    /**
     * @param int $TakeoffAlternateGridMora
     * @return FlightDistance
     */
    public function setTakeoffAlternateGridMora($TakeoffAlternateGridMora)
    {
      $this->TakeoffAlternateGridMora = $TakeoffAlternateGridMora;
      return $this;
    }

}
