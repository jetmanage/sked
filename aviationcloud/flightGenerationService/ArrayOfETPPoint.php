<?php

class ArrayOfETPPoint implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ETPPoint[] $ETPPoint
     */
    protected $ETPPoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ETPPoint[]
     */
    public function getETPPoint()
    {
      return $this->ETPPoint;
    }

    /**
     * @param ETPPoint[] $ETPPoint
     * @return ArrayOfETPPoint
     */
    public function setETPPoint(array $ETPPoint = null)
    {
      $this->ETPPoint = $ETPPoint;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ETPPoint[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ETPPoint
     */
    public function offsetGet($offset)
    {
      return $this->ETPPoint[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ETPPoint $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ETPPoint[] = $value;
      } else {
        $this->ETPPoint[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ETPPoint[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ETPPoint Return the current element
     */
    public function current()
    {
      return current($this->ETPPoint);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ETPPoint);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ETPPoint);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ETPPoint);
    }

    /**
     * Countable implementation
     *
     * @return ETPPoint Return count of elements
     */
    public function count()
    {
      return count($this->ETPPoint);
    }

}
