<?php

class CalculateFlightPlanResponse
{

    /**
     * @var FlightCalculationResponse $CalculateFlightPlanResult
     */
    protected $CalculateFlightPlanResult = null;

    /**
     * @param FlightCalculationResponse $CalculateFlightPlanResult
     */
    public function __construct($CalculateFlightPlanResult)
    {
      $this->CalculateFlightPlanResult = $CalculateFlightPlanResult;
    }

    /**
     * @return FlightCalculationResponse
     */
    public function getCalculateFlightPlanResult()
    {
      return $this->CalculateFlightPlanResult;
    }

    /**
     * @param FlightCalculationResponse $CalculateFlightPlanResult
     * @return CalculateFlightPlanResponse
     */
    public function setCalculateFlightPlanResult($CalculateFlightPlanResult)
    {
      $this->CalculateFlightPlanResult = $CalculateFlightPlanResult;
      return $this;
    }

}
