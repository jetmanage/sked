<?php

class FlightQoute
{

    /**
     * @var Time $FlightTime
     */
    protected $FlightTime = null;

    /**
     * @var Weight $Fuel
     */
    protected $Fuel = null;

    /**
     * @var ArrayOfOverflightCosts $OverflightCosts
     */
    protected $OverflightCosts = null;

    /**
     * @var ArrayOfFIR $OverflownFIRs
     */
    protected $OverflownFIRs = null;

    /**
     * @var ArrayOfSphereicPoint $RouteTrajectory
     */
    protected $RouteTrajectory = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Time
     */
    public function getFlightTime()
    {
      return $this->FlightTime;
    }

    /**
     * @param Time $FlightTime
     * @return FlightQoute
     */
    public function setFlightTime($FlightTime)
    {
      $this->FlightTime = $FlightTime;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuel()
    {
      return $this->Fuel;
    }

    /**
     * @param Weight $Fuel
     * @return FlightQoute
     */
    public function setFuel($Fuel)
    {
      $this->Fuel = $Fuel;
      return $this;
    }

    /**
     * @return ArrayOfOverflightCosts
     */
    public function getOverflightCosts()
    {
      return $this->OverflightCosts;
    }

    /**
     * @param ArrayOfOverflightCosts $OverflightCosts
     * @return FlightQoute
     */
    public function setOverflightCosts($OverflightCosts)
    {
      $this->OverflightCosts = $OverflightCosts;
      return $this;
    }

    /**
     * @return ArrayOfFIR
     */
    public function getOverflownFIRs()
    {
      return $this->OverflownFIRs;
    }

    /**
     * @param ArrayOfFIR $OverflownFIRs
     * @return FlightQoute
     */
    public function setOverflownFIRs($OverflownFIRs)
    {
      $this->OverflownFIRs = $OverflownFIRs;
      return $this;
    }

    /**
     * @return ArrayOfSphereicPoint
     */
    public function getRouteTrajectory()
    {
      return $this->RouteTrajectory;
    }

    /**
     * @param ArrayOfSphereicPoint $RouteTrajectory
     * @return FlightQoute
     */
    public function setRouteTrajectory($RouteTrajectory)
    {
      $this->RouteTrajectory = $RouteTrajectory;
      return $this;
    }

}
