<?php

class CalculateQoute
{

    /**
     * @var QouteCalculationRequest $request
     */
    protected $request = null;

    /**
     * @param QouteCalculationRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return QouteCalculationRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param QouteCalculationRequest $request
     * @return CalculateQoute
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
