<?php

class AircraftATCInfo
{

    /**
     * @var string $AircraftColorAndMarkings
     */
    protected $AircraftColorAndMarkings = null;

    /**
     * @var string $AircraftPerformanceCategory
     */
    protected $AircraftPerformanceCategory = null;

    /**
     * @var int $DingiesCapacity
     */
    protected $DingiesCapacity = null;

    /**
     * @var string $DingiesColor
     */
    protected $DingiesColor = null;

    /**
     * @var boolean $DingiesCovered
     */
    protected $DingiesCovered = null;

    /**
     * @var AircraftEquipment $Equipment
     */
    protected $Equipment = null;

    /**
     * @var AircraftFrequencyAvailability $FrequencyAvailability
     */
    protected $FrequencyAvailability = null;

    /**
     * @var AircraftLifeJacketEquipment $LifeJacketEquipment
     */
    protected $LifeJacketEquipment = null;

    /**
     * @var int $NumberOfDingies
     */
    protected $NumberOfDingies = null;

    /**
     * @var RNAVPreferentialRoutes $RNAVPreferentialRoutes
     */
    protected $RNAVPreferentialRoutes = null;

    /**
     * @var AircraftSurvivalEquipment $SurvivalEquipment
     */
    protected $SurvivalEquipment = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAircraftColorAndMarkings()
    {
      return $this->AircraftColorAndMarkings;
    }

    /**
     * @param string $AircraftColorAndMarkings
     * @return AircraftATCInfo
     */
    public function setAircraftColorAndMarkings($AircraftColorAndMarkings)
    {
      $this->AircraftColorAndMarkings = $AircraftColorAndMarkings;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftPerformanceCategory()
    {
      return $this->AircraftPerformanceCategory;
    }

    /**
     * @param string $AircraftPerformanceCategory
     * @return AircraftATCInfo
     */
    public function setAircraftPerformanceCategory($AircraftPerformanceCategory)
    {
      $this->AircraftPerformanceCategory = $AircraftPerformanceCategory;
      return $this;
    }

    /**
     * @return int
     */
    public function getDingiesCapacity()
    {
      return $this->DingiesCapacity;
    }

    /**
     * @param int $DingiesCapacity
     * @return AircraftATCInfo
     */
    public function setDingiesCapacity($DingiesCapacity)
    {
      $this->DingiesCapacity = $DingiesCapacity;
      return $this;
    }

    /**
     * @return string
     */
    public function getDingiesColor()
    {
      return $this->DingiesColor;
    }

    /**
     * @param string $DingiesColor
     * @return AircraftATCInfo
     */
    public function setDingiesColor($DingiesColor)
    {
      $this->DingiesColor = $DingiesColor;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDingiesCovered()
    {
      return $this->DingiesCovered;
    }

    /**
     * @param boolean $DingiesCovered
     * @return AircraftATCInfo
     */
    public function setDingiesCovered($DingiesCovered)
    {
      $this->DingiesCovered = $DingiesCovered;
      return $this;
    }

    /**
     * @return AircraftEquipment
     */
    public function getEquipment()
    {
      return $this->Equipment;
    }

    /**
     * @param AircraftEquipment $Equipment
     * @return AircraftATCInfo
     */
    public function setEquipment($Equipment)
    {
      $this->Equipment = $Equipment;
      return $this;
    }

    /**
     * @return AircraftFrequencyAvailability
     */
    public function getFrequencyAvailability()
    {
      return $this->FrequencyAvailability;
    }

    /**
     * @param AircraftFrequencyAvailability $FrequencyAvailability
     * @return AircraftATCInfo
     */
    public function setFrequencyAvailability($FrequencyAvailability)
    {
      $this->FrequencyAvailability = $FrequencyAvailability;
      return $this;
    }

    /**
     * @return AircraftLifeJacketEquipment
     */
    public function getLifeJacketEquipment()
    {
      return $this->LifeJacketEquipment;
    }

    /**
     * @param AircraftLifeJacketEquipment $LifeJacketEquipment
     * @return AircraftATCInfo
     */
    public function setLifeJacketEquipment($LifeJacketEquipment)
    {
      $this->LifeJacketEquipment = $LifeJacketEquipment;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfDingies()
    {
      return $this->NumberOfDingies;
    }

    /**
     * @param int $NumberOfDingies
     * @return AircraftATCInfo
     */
    public function setNumberOfDingies($NumberOfDingies)
    {
      $this->NumberOfDingies = $NumberOfDingies;
      return $this;
    }

    /**
     * @return RNAVPreferentialRoutes
     */
    public function getRNAVPreferentialRoutes()
    {
      return $this->RNAVPreferentialRoutes;
    }

    /**
     * @param RNAVPreferentialRoutes $RNAVPreferentialRoutes
     * @return AircraftATCInfo
     */
    public function setRNAVPreferentialRoutes($RNAVPreferentialRoutes)
    {
      $this->RNAVPreferentialRoutes = $RNAVPreferentialRoutes;
      return $this;
    }

    /**
     * @return AircraftSurvivalEquipment
     */
    public function getSurvivalEquipment()
    {
      return $this->SurvivalEquipment;
    }

    /**
     * @param AircraftSurvivalEquipment $SurvivalEquipment
     * @return AircraftATCInfo
     */
    public function setSurvivalEquipment($SurvivalEquipment)
    {
      $this->SurvivalEquipment = $SurvivalEquipment;
      return $this;
    }

}
