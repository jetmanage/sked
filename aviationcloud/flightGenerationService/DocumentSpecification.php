<?php

class DocumentSpecification
{

    /**
     * @var boolean $Generate
     */
    protected $Generate = null;

    /**
     * @var int $Order
     */
    protected $Order = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getGenerate()
    {
      return $this->Generate;
    }

    /**
     * @param boolean $Generate
     * @return DocumentSpecification
     */
    public function setGenerate($Generate)
    {
      $this->Generate = $Generate;
      return $this;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
      return $this->Order;
    }

    /**
     * @param int $Order
     * @return DocumentSpecification
     */
    public function setOrder($Order)
    {
      $this->Order = $Order;
      return $this;
    }

}
