<?php

class ArrayOfArrayOfTankeringSummary implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ArrayOfTankeringSummary[] $ArrayOfTankeringSummary
     */
    protected $ArrayOfTankeringSummary = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfTankeringSummary[]
     */
    public function getArrayOfTankeringSummary()
    {
      return $this->ArrayOfTankeringSummary;
    }

    /**
     * @param ArrayOfTankeringSummary[] $ArrayOfTankeringSummary
     * @return ArrayOfArrayOfTankeringSummary
     */
    public function setArrayOfTankeringSummary(array $ArrayOfTankeringSummary = null)
    {
      $this->ArrayOfTankeringSummary = $ArrayOfTankeringSummary;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ArrayOfTankeringSummary[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ArrayOfTankeringSummary
     */
    public function offsetGet($offset)
    {
      return $this->ArrayOfTankeringSummary[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ArrayOfTankeringSummary $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ArrayOfTankeringSummary[] = $value;
      } else {
        $this->ArrayOfTankeringSummary[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ArrayOfTankeringSummary[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ArrayOfTankeringSummary Return the current element
     */
    public function current()
    {
      return current($this->ArrayOfTankeringSummary);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ArrayOfTankeringSummary);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ArrayOfTankeringSummary);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ArrayOfTankeringSummary);
    }

    /**
     * Countable implementation
     *
     * @return ArrayOfTankeringSummary Return count of elements
     */
    public function count()
    {
      return count($this->ArrayOfTankeringSummary);
    }

}
