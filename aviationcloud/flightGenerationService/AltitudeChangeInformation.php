<?php

class AltitudeChangeInformation
{

    /**
     * @var Altitude $Altitude
     */
    protected $Altitude = null;

    /**
     * @var WaypointData $RouteNode
     */
    protected $RouteNode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Altitude
     */
    public function getAltitude()
    {
      return $this->Altitude;
    }

    /**
     * @param Altitude $Altitude
     * @return AltitudeChangeInformation
     */
    public function setAltitude($Altitude)
    {
      $this->Altitude = $Altitude;
      return $this;
    }

    /**
     * @return WaypointData
     */
    public function getRouteNode()
    {
      return $this->RouteNode;
    }

    /**
     * @param WaypointData $RouteNode
     * @return AltitudeChangeInformation
     */
    public function setRouteNode($RouteNode)
    {
      $this->RouteNode = $RouteNode;
      return $this;
    }

}
