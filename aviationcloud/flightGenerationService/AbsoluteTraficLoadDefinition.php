<?php

class AbsoluteTraficLoadDefinition extends AircraftTraficLoadDefinition
{

    /**
     * @var Weight $TraficLoad
     */
    protected $TraficLoad = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getTraficLoad()
    {
      return $this->TraficLoad;
    }

    /**
     * @param Weight $TraficLoad
     * @return AbsoluteTraficLoadDefinition
     */
    public function setTraficLoad($TraficLoad)
    {
      $this->TraficLoad = $TraficLoad;
      return $this;
    }

}
