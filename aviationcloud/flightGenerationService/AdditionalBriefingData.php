<?php

class AdditionalBriefingData
{

    /**
     * @var boolean $EnableProductionSystemForFMSUpload
     */
    protected $EnableProductionSystemForFMSUpload = null;

    /**
     * @var string $FMSDatalinkServiceProvider
     */
    protected $FMSDatalinkServiceProvider = null;

    /**
     * @var string $RecallIdentifier
     */
    protected $RecallIdentifier = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getEnableProductionSystemForFMSUpload()
    {
      return $this->EnableProductionSystemForFMSUpload;
    }

    /**
     * @param boolean $EnableProductionSystemForFMSUpload
     * @return AdditionalBriefingData
     */
    public function setEnableProductionSystemForFMSUpload($EnableProductionSystemForFMSUpload)
    {
      $this->EnableProductionSystemForFMSUpload = $EnableProductionSystemForFMSUpload;
      return $this;
    }

    /**
     * @return string
     */
    public function getFMSDatalinkServiceProvider()
    {
      return $this->FMSDatalinkServiceProvider;
    }

    /**
     * @param string $FMSDatalinkServiceProvider
     * @return AdditionalBriefingData
     */
    public function setFMSDatalinkServiceProvider($FMSDatalinkServiceProvider)
    {
      $this->FMSDatalinkServiceProvider = $FMSDatalinkServiceProvider;
      return $this;
    }

    /**
     * @return string
     */
    public function getRecallIdentifier()
    {
      return $this->RecallIdentifier;
    }

    /**
     * @param string $RecallIdentifier
     * @return AdditionalBriefingData
     */
    public function setRecallIdentifier($RecallIdentifier)
    {
      $this->RecallIdentifier = $RecallIdentifier;
      return $this;
    }

}
