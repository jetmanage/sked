<?php

class AirportProcedure
{

    /**
     * @var ArrayOfAirportEnrouteTransitionProcedure $AvailableTransitions
     */
    protected $AvailableTransitions = null;

    /**
     * @var WaypointData $EnrouteConnectingPoint
     */
    protected $EnrouteConnectingPoint = null;

    /**
     * @var AirportProcedureIdentifier $Id
     */
    protected $Id = null;

    /**
     * @var boolean $IsArrivalProcedure
     */
    protected $IsArrivalProcedure = null;

    /**
     * @var boolean $IsDepartureProcedure
     */
    protected $IsDepartureProcedure = null;

    /**
     * @var boolean $IsRNAV
     */
    protected $IsRNAV = null;

    /**
     * @var ArrayOfWaypointData $MainPath
     */
    protected $MainPath = null;

    /**
     * @var string $MainPathCompassDirection
     */
    protected $MainPathCompassDirection = null;

    /**
     * @var Runway $Runway
     */
    protected $Runway = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAirportEnrouteTransitionProcedure
     */
    public function getAvailableTransitions()
    {
      return $this->AvailableTransitions;
    }

    /**
     * @param ArrayOfAirportEnrouteTransitionProcedure $AvailableTransitions
     * @return AirportProcedure
     */
    public function setAvailableTransitions($AvailableTransitions)
    {
      $this->AvailableTransitions = $AvailableTransitions;
      return $this;
    }

    /**
     * @return WaypointData
     */
    public function getEnrouteConnectingPoint()
    {
      return $this->EnrouteConnectingPoint;
    }

    /**
     * @param WaypointData $EnrouteConnectingPoint
     * @return AirportProcedure
     */
    public function setEnrouteConnectingPoint($EnrouteConnectingPoint)
    {
      $this->EnrouteConnectingPoint = $EnrouteConnectingPoint;
      return $this;
    }

    /**
     * @return AirportProcedureIdentifier
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param AirportProcedureIdentifier $Id
     * @return AirportProcedure
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsArrivalProcedure()
    {
      return $this->IsArrivalProcedure;
    }

    /**
     * @param boolean $IsArrivalProcedure
     * @return AirportProcedure
     */
    public function setIsArrivalProcedure($IsArrivalProcedure)
    {
      $this->IsArrivalProcedure = $IsArrivalProcedure;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDepartureProcedure()
    {
      return $this->IsDepartureProcedure;
    }

    /**
     * @param boolean $IsDepartureProcedure
     * @return AirportProcedure
     */
    public function setIsDepartureProcedure($IsDepartureProcedure)
    {
      $this->IsDepartureProcedure = $IsDepartureProcedure;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsRNAV()
    {
      return $this->IsRNAV;
    }

    /**
     * @param boolean $IsRNAV
     * @return AirportProcedure
     */
    public function setIsRNAV($IsRNAV)
    {
      $this->IsRNAV = $IsRNAV;
      return $this;
    }

    /**
     * @return ArrayOfWaypointData
     */
    public function getMainPath()
    {
      return $this->MainPath;
    }

    /**
     * @param ArrayOfWaypointData $MainPath
     * @return AirportProcedure
     */
    public function setMainPath($MainPath)
    {
      $this->MainPath = $MainPath;
      return $this;
    }

    /**
     * @return string
     */
    public function getMainPathCompassDirection()
    {
      return $this->MainPathCompassDirection;
    }

    /**
     * @param string $MainPathCompassDirection
     * @return AirportProcedure
     */
    public function setMainPathCompassDirection($MainPathCompassDirection)
    {
      $this->MainPathCompassDirection = $MainPathCompassDirection;
      return $this;
    }

    /**
     * @return Runway
     */
    public function getRunway()
    {
      return $this->Runway;
    }

    /**
     * @param Runway $Runway
     * @return AirportProcedure
     */
    public function setRunway($Runway)
    {
      $this->Runway = $Runway;
      return $this;
    }

}
