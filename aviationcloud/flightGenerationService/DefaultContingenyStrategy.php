<?php

class DefaultContingenyStrategy extends ContingencyStrategy
{

    /**
     * @var int $ContigencyPercentage
     */
    protected $ContigencyPercentage = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getContigencyPercentage()
    {
      return $this->ContigencyPercentage;
    }

    /**
     * @param int $ContigencyPercentage
     * @return DefaultContingenyStrategy
     */
    public function setContigencyPercentage($ContigencyPercentage)
    {
      $this->ContigencyPercentage = $ContigencyPercentage;
      return $this;
    }

}
