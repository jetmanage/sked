<?php

class ArrayOfAircraftGenericDataMaxDeviatioNExceedError implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AircraftGenericDataMaxDeviatioNExceedError[] $AircraftGenericDataMaxDeviatioNExceedError
     */
    protected $AircraftGenericDataMaxDeviatioNExceedError = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftGenericDataMaxDeviatioNExceedError[]
     */
    public function getAircraftGenericDataMaxDeviatioNExceedError()
    {
      return $this->AircraftGenericDataMaxDeviatioNExceedError;
    }

    /**
     * @param AircraftGenericDataMaxDeviatioNExceedError[] $AircraftGenericDataMaxDeviatioNExceedError
     * @return ArrayOfAircraftGenericDataMaxDeviatioNExceedError
     */
    public function setAircraftGenericDataMaxDeviatioNExceedError(array $AircraftGenericDataMaxDeviatioNExceedError = null)
    {
      $this->AircraftGenericDataMaxDeviatioNExceedError = $AircraftGenericDataMaxDeviatioNExceedError;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AircraftGenericDataMaxDeviatioNExceedError[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AircraftGenericDataMaxDeviatioNExceedError
     */
    public function offsetGet($offset)
    {
      return $this->AircraftGenericDataMaxDeviatioNExceedError[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AircraftGenericDataMaxDeviatioNExceedError $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AircraftGenericDataMaxDeviatioNExceedError[] = $value;
      } else {
        $this->AircraftGenericDataMaxDeviatioNExceedError[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AircraftGenericDataMaxDeviatioNExceedError[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AircraftGenericDataMaxDeviatioNExceedError Return the current element
     */
    public function current()
    {
      return current($this->AircraftGenericDataMaxDeviatioNExceedError);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AircraftGenericDataMaxDeviatioNExceedError);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AircraftGenericDataMaxDeviatioNExceedError);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AircraftGenericDataMaxDeviatioNExceedError);
    }

    /**
     * Countable implementation
     *
     * @return AircraftGenericDataMaxDeviatioNExceedError Return count of elements
     */
    public function count()
    {
      return count($this->AircraftGenericDataMaxDeviatioNExceedError);
    }

}
