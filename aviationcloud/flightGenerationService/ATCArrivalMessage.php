<?php

class ATCArrivalMessage extends ATCFlightMessage
{

    /**
     * @var \DateTime $TimeOfArrivial
     */
    protected $TimeOfArrivial = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return \DateTime
     */
    public function getTimeOfArrivial()
    {
      if ($this->TimeOfArrivial == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->TimeOfArrivial);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $TimeOfArrivial
     * @return ATCArrivalMessage
     */
    public function setTimeOfArrivial(\DateTime $TimeOfArrivial = null)
    {
      if ($TimeOfArrivial == null) {
       $this->TimeOfArrivial = null;
      } else {
        $this->TimeOfArrivial = $TimeOfArrivial->format(\DateTime::ATOM);
      }
      return $this;
    }

}
