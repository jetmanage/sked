<?php

class ATCACARSEquipment
{

    /**
     * @var boolean $DFIS
     */
    protected $DFIS = null;

    /**
     * @var boolean $FMCWPRACARS
     */
    protected $FMCWPRACARS = null;

    /**
     * @var boolean $PDC
     */
    protected $PDC = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getDFIS()
    {
      return $this->DFIS;
    }

    /**
     * @param boolean $DFIS
     * @return ATCACARSEquipment
     */
    public function setDFIS($DFIS)
    {
      $this->DFIS = $DFIS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFMCWPRACARS()
    {
      return $this->FMCWPRACARS;
    }

    /**
     * @param boolean $FMCWPRACARS
     * @return ATCACARSEquipment
     */
    public function setFMCWPRACARS($FMCWPRACARS)
    {
      $this->FMCWPRACARS = $FMCWPRACARS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPDC()
    {
      return $this->PDC;
    }

    /**
     * @param boolean $PDC
     * @return ATCACARSEquipment
     */
    public function setPDC($PDC)
    {
      $this->PDC = $PDC;
      return $this;
    }

}
