<?php

class QouteCalculationResponse
{

    /**
     * @var FlightQoute $Qoute
     */
    protected $Qoute = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FlightQoute
     */
    public function getQoute()
    {
      return $this->Qoute;
    }

    /**
     * @param FlightQoute $Qoute
     * @return QouteCalculationResponse
     */
    public function setQoute($Qoute)
    {
      $this->Qoute = $Qoute;
      return $this;
    }

}
