<?php

class DiscreteDescentProfile extends DescentProfile
{

    /**
     * @var ArrayOfClimbDescendDataSeries $DescendPerformanceDataSet
     */
    protected $DescendPerformanceDataSet = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ArrayOfClimbDescendDataSeries
     */
    public function getDescendPerformanceDataSet()
    {
      return $this->DescendPerformanceDataSet;
    }

    /**
     * @param ArrayOfClimbDescendDataSeries $DescendPerformanceDataSet
     * @return DiscreteDescentProfile
     */
    public function setDescendPerformanceDataSet($DescendPerformanceDataSet)
    {
      $this->DescendPerformanceDataSet = $DescendPerformanceDataSet;
      return $this;
    }

}
