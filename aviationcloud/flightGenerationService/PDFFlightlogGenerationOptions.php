<?php

class PDFFlightlogGenerationOptions extends DocumentSpecification
{

    /**
     * @var DataLevel $DataLevel
     */
    protected $DataLevel = null;

    /**
     * @var int $LayoutNumber
     */
    protected $LayoutNumber = null;

    /**
     * @var PDFFlightlogUnits $Units
     */
    protected $Units = null;

    /**
     * @var boolean $Use12Hour
     */
    protected $Use12Hour = null;

    /**
     * @var boolean $UseLocalTime
     */
    protected $UseLocalTime = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return DataLevel
     */
    public function getDataLevel()
    {
      return $this->DataLevel;
    }

    /**
     * @param DataLevel $DataLevel
     * @return PDFFlightlogGenerationOptions
     */
    public function setDataLevel($DataLevel)
    {
      $this->DataLevel = $DataLevel;
      return $this;
    }

    /**
     * @return int
     */
    public function getLayoutNumber()
    {
      return $this->LayoutNumber;
    }

    /**
     * @param int $LayoutNumber
     * @return PDFFlightlogGenerationOptions
     */
    public function setLayoutNumber($LayoutNumber)
    {
      $this->LayoutNumber = $LayoutNumber;
      return $this;
    }

    /**
     * @return PDFFlightlogUnits
     */
    public function getUnits()
    {
      return $this->Units;
    }

    /**
     * @param PDFFlightlogUnits $Units
     * @return PDFFlightlogGenerationOptions
     */
    public function setUnits($Units)
    {
      $this->Units = $Units;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUse12Hour()
    {
      return $this->Use12Hour;
    }

    /**
     * @param boolean $Use12Hour
     * @return PDFFlightlogGenerationOptions
     */
    public function setUse12Hour($Use12Hour)
    {
      $this->Use12Hour = $Use12Hour;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseLocalTime()
    {
      return $this->UseLocalTime;
    }

    /**
     * @param boolean $UseLocalTime
     * @return PDFFlightlogGenerationOptions
     */
    public function setUseLocalTime($UseLocalTime)
    {
      $this->UseLocalTime = $UseLocalTime;
      return $this;
    }

}
