<?php

class FlightCalculationRequest extends RequestBase
{

    /**
     * @var FlightSpecification $FlightSpecification
     */
    protected $FlightSpecification = null;

    /**
     * @var boolean $UseV2Engine
     */
    protected $UseV2Engine = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return FlightSpecification
     */
    public function getFlightSpecification()
    {
      return $this->FlightSpecification;
    }

    /**
     * @param FlightSpecification $FlightSpecification
     * @return FlightCalculationRequest
     */
    public function setFlightSpecification($FlightSpecification)
    {
      $this->FlightSpecification = $FlightSpecification;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseV2Engine()
    {
      return $this->UseV2Engine;
    }

    /**
     * @param boolean $UseV2Engine
     * @return FlightCalculationRequest
     */
    public function setUseV2Engine($UseV2Engine)
    {
      $this->UseV2Engine = $UseV2Engine;
      return $this;
    }

}
