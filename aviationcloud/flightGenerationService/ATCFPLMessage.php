<?php

class ATCFPLMessage extends ATCFlightMessage
{

    /**
     * @var AircraftATCInfoCodes $ATCAircraftInfoCodes
     */
    protected $ATCAircraftInfoCodes = null;

    /**
     * @var string $ATCItem19Remarks
     */
    protected $ATCItem19Remarks = null;

    /**
     * @var string $AircraftType
     */
    protected $AircraftType = null;

    /**
     * @var Airport $AlternateAirport
     */
    protected $AlternateAirport = null;

    /**
     * @var Airport $AlternateAirport2
     */
    protected $AlternateAirport2 = null;

    /**
     * @var RouteNodeIdentifier $DeparturePoint
     */
    protected $DeparturePoint = null;

    /**
     * @var RouteNodeIdentifier $DestinationPoint
     */
    protected $DestinationPoint = null;

    /**
     * @var Time $Endurance
     */
    protected $Endurance = null;

    /**
     * @var Time $EstimatedEnrouteTime
     */
    protected $EstimatedEnrouteTime = null;

    /**
     * @var ArrayOfAFTNAddress $FileToAddresses
     */
    protected $FileToAddresses = null;

    /**
     * @var FLightATCEET $FlightEETData
     */
    protected $FlightEETData = null;

    /**
     * @var string $FlightNumber
     */
    protected $FlightNumber = null;

    /**
     * @var ATCFlightRule $FlightRule
     */
    protected $FlightRule = null;

    /**
     * @var boolean $IsTBAPersonsOnBoard
     */
    protected $IsTBAPersonsOnBoard = null;

    /**
     * @var int $NumberOfAircrafts
     */
    protected $NumberOfAircrafts = null;

    /**
     * @var ATCOtherInformation $OtherInformation
     */
    protected $OtherInformation = null;

    /**
     * @var int $PersonsOnBoard
     */
    protected $PersonsOnBoard = null;

    /**
     * @var string $PilotInCommand
     */
    protected $PilotInCommand = null;

    /**
     * @var Route $Route
     */
    protected $Route = null;

    /**
     * @var string $Tailnumber
     */
    protected $Tailnumber = null;

    /**
     * @var ATCFlightType $TypeOfFlight
     */
    protected $TypeOfFlight = null;

    /**
     * @var ATCWakeTurbulanceCategory $WakeTurbulanceCategory
     */
    protected $WakeTurbulanceCategory = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return AircraftATCInfoCodes
     */
    public function getATCAircraftInfoCodes()
    {
      return $this->ATCAircraftInfoCodes;
    }

    /**
     * @param AircraftATCInfoCodes $ATCAircraftInfoCodes
     * @return ATCFPLMessage
     */
    public function setATCAircraftInfoCodes($ATCAircraftInfoCodes)
    {
      $this->ATCAircraftInfoCodes = $ATCAircraftInfoCodes;
      return $this;
    }

    /**
     * @return string
     */
    public function getATCItem19Remarks()
    {
      return $this->ATCItem19Remarks;
    }

    /**
     * @param string $ATCItem19Remarks
     * @return ATCFPLMessage
     */
    public function setATCItem19Remarks($ATCItem19Remarks)
    {
      $this->ATCItem19Remarks = $ATCItem19Remarks;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftType()
    {
      return $this->AircraftType;
    }

    /**
     * @param string $AircraftType
     * @return ATCFPLMessage
     */
    public function setAircraftType($AircraftType)
    {
      $this->AircraftType = $AircraftType;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getAlternateAirport()
    {
      return $this->AlternateAirport;
    }

    /**
     * @param Airport $AlternateAirport
     * @return ATCFPLMessage
     */
    public function setAlternateAirport($AlternateAirport)
    {
      $this->AlternateAirport = $AlternateAirport;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getAlternateAirport2()
    {
      return $this->AlternateAirport2;
    }

    /**
     * @param Airport $AlternateAirport2
     * @return ATCFPLMessage
     */
    public function setAlternateAirport2($AlternateAirport2)
    {
      $this->AlternateAirport2 = $AlternateAirport2;
      return $this;
    }

    /**
     * @return RouteNodeIdentifier
     */
    public function getDeparturePoint()
    {
      return $this->DeparturePoint;
    }

    /**
     * @param RouteNodeIdentifier $DeparturePoint
     * @return ATCFPLMessage
     */
    public function setDeparturePoint($DeparturePoint)
    {
      $this->DeparturePoint = $DeparturePoint;
      return $this;
    }

    /**
     * @return RouteNodeIdentifier
     */
    public function getDestinationPoint()
    {
      return $this->DestinationPoint;
    }

    /**
     * @param RouteNodeIdentifier $DestinationPoint
     * @return ATCFPLMessage
     */
    public function setDestinationPoint($DestinationPoint)
    {
      $this->DestinationPoint = $DestinationPoint;
      return $this;
    }

    /**
     * @return Time
     */
    public function getEndurance()
    {
      return $this->Endurance;
    }

    /**
     * @param Time $Endurance
     * @return ATCFPLMessage
     */
    public function setEndurance($Endurance)
    {
      $this->Endurance = $Endurance;
      return $this;
    }

    /**
     * @return Time
     */
    public function getEstimatedEnrouteTime()
    {
      return $this->EstimatedEnrouteTime;
    }

    /**
     * @param Time $EstimatedEnrouteTime
     * @return ATCFPLMessage
     */
    public function setEstimatedEnrouteTime($EstimatedEnrouteTime)
    {
      $this->EstimatedEnrouteTime = $EstimatedEnrouteTime;
      return $this;
    }

    /**
     * @return ArrayOfAFTNAddress
     */
    public function getFileToAddresses()
    {
      return $this->FileToAddresses;
    }

    /**
     * @param ArrayOfAFTNAddress $FileToAddresses
     * @return ATCFPLMessage
     */
    public function setFileToAddresses($FileToAddresses)
    {
      $this->FileToAddresses = $FileToAddresses;
      return $this;
    }

    /**
     * @return FLightATCEET
     */
    public function getFlightEETData()
    {
      return $this->FlightEETData;
    }

    /**
     * @param FLightATCEET $FlightEETData
     * @return ATCFPLMessage
     */
    public function setFlightEETData($FlightEETData)
    {
      $this->FlightEETData = $FlightEETData;
      return $this;
    }

    /**
     * @return string
     */
    public function getFlightNumber()
    {
      return $this->FlightNumber;
    }

    /**
     * @param string $FlightNumber
     * @return ATCFPLMessage
     */
    public function setFlightNumber($FlightNumber)
    {
      $this->FlightNumber = $FlightNumber;
      return $this;
    }

    /**
     * @return ATCFlightRule
     */
    public function getFlightRule()
    {
      return $this->FlightRule;
    }

    /**
     * @param ATCFlightRule $FlightRule
     * @return ATCFPLMessage
     */
    public function setFlightRule($FlightRule)
    {
      $this->FlightRule = $FlightRule;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsTBAPersonsOnBoard()
    {
      return $this->IsTBAPersonsOnBoard;
    }

    /**
     * @param boolean $IsTBAPersonsOnBoard
     * @return ATCFPLMessage
     */
    public function setIsTBAPersonsOnBoard($IsTBAPersonsOnBoard)
    {
      $this->IsTBAPersonsOnBoard = $IsTBAPersonsOnBoard;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfAircrafts()
    {
      return $this->NumberOfAircrafts;
    }

    /**
     * @param int $NumberOfAircrafts
     * @return ATCFPLMessage
     */
    public function setNumberOfAircrafts($NumberOfAircrafts)
    {
      $this->NumberOfAircrafts = $NumberOfAircrafts;
      return $this;
    }

    /**
     * @return ATCOtherInformation
     */
    public function getOtherInformation()
    {
      return $this->OtherInformation;
    }

    /**
     * @param ATCOtherInformation $OtherInformation
     * @return ATCFPLMessage
     */
    public function setOtherInformation($OtherInformation)
    {
      $this->OtherInformation = $OtherInformation;
      return $this;
    }

    /**
     * @return int
     */
    public function getPersonsOnBoard()
    {
      return $this->PersonsOnBoard;
    }

    /**
     * @param int $PersonsOnBoard
     * @return ATCFPLMessage
     */
    public function setPersonsOnBoard($PersonsOnBoard)
    {
      $this->PersonsOnBoard = $PersonsOnBoard;
      return $this;
    }

    /**
     * @return string
     */
    public function getPilotInCommand()
    {
      return $this->PilotInCommand;
    }

    /**
     * @param string $PilotInCommand
     * @return ATCFPLMessage
     */
    public function setPilotInCommand($PilotInCommand)
    {
      $this->PilotInCommand = $PilotInCommand;
      return $this;
    }

    /**
     * @return Route
     */
    public function getRoute()
    {
      return $this->Route;
    }

    /**
     * @param Route $Route
     * @return ATCFPLMessage
     */
    public function setRoute($Route)
    {
      $this->Route = $Route;
      return $this;
    }

    /**
     * @return string
     */
    public function getTailnumber()
    {
      return $this->Tailnumber;
    }

    /**
     * @param string $Tailnumber
     * @return ATCFPLMessage
     */
    public function setTailnumber($Tailnumber)
    {
      $this->Tailnumber = $Tailnumber;
      return $this;
    }

    /**
     * @return ATCFlightType
     */
    public function getTypeOfFlight()
    {
      return $this->TypeOfFlight;
    }

    /**
     * @param ATCFlightType $TypeOfFlight
     * @return ATCFPLMessage
     */
    public function setTypeOfFlight($TypeOfFlight)
    {
      $this->TypeOfFlight = $TypeOfFlight;
      return $this;
    }

    /**
     * @return ATCWakeTurbulanceCategory
     */
    public function getWakeTurbulanceCategory()
    {
      return $this->WakeTurbulanceCategory;
    }

    /**
     * @param ATCWakeTurbulanceCategory $WakeTurbulanceCategory
     * @return ATCFPLMessage
     */
    public function setWakeTurbulanceCategory($WakeTurbulanceCategory)
    {
      $this->WakeTurbulanceCategory = $WakeTurbulanceCategory;
      return $this;
    }

}
