<?php

class ArrayOfBriefingDocument implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var BriefingDocument[] $BriefingDocument
     */
    protected $BriefingDocument = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return BriefingDocument[]
     */
    public function getBriefingDocument()
    {
      return $this->BriefingDocument;
    }

    /**
     * @param BriefingDocument[] $BriefingDocument
     * @return ArrayOfBriefingDocument
     */
    public function setBriefingDocument(array $BriefingDocument = null)
    {
      $this->BriefingDocument = $BriefingDocument;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->BriefingDocument[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return BriefingDocument
     */
    public function offsetGet($offset)
    {
      return $this->BriefingDocument[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param BriefingDocument $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->BriefingDocument[] = $value;
      } else {
        $this->BriefingDocument[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->BriefingDocument[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return BriefingDocument Return the current element
     */
    public function current()
    {
      return current($this->BriefingDocument);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->BriefingDocument);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->BriefingDocument);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->BriefingDocument);
    }

    /**
     * Countable implementation
     *
     * @return BriefingDocument Return count of elements
     */
    public function count()
    {
      return count($this->BriefingDocument);
    }

}
