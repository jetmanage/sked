<?php

class ArrayOfAirportFrequencyARINC424 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AirportFrequencyARINC424[] $AirportFrequencyARINC424
     */
    protected $AirportFrequencyARINC424 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AirportFrequencyARINC424[]
     */
    public function getAirportFrequencyARINC424()
    {
      return $this->AirportFrequencyARINC424;
    }

    /**
     * @param AirportFrequencyARINC424[] $AirportFrequencyARINC424
     * @return ArrayOfAirportFrequencyARINC424
     */
    public function setAirportFrequencyARINC424(array $AirportFrequencyARINC424 = null)
    {
      $this->AirportFrequencyARINC424 = $AirportFrequencyARINC424;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AirportFrequencyARINC424[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AirportFrequencyARINC424
     */
    public function offsetGet($offset)
    {
      return $this->AirportFrequencyARINC424[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AirportFrequencyARINC424 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AirportFrequencyARINC424[] = $value;
      } else {
        $this->AirportFrequencyARINC424[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AirportFrequencyARINC424[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AirportFrequencyARINC424 Return the current element
     */
    public function current()
    {
      return current($this->AirportFrequencyARINC424);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AirportFrequencyARINC424);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AirportFrequencyARINC424);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AirportFrequencyARINC424);
    }

    /**
     * Countable implementation
     *
     * @return AirportFrequencyARINC424 Return count of elements
     */
    public function count()
    {
      return count($this->AirportFrequencyARINC424);
    }

}
