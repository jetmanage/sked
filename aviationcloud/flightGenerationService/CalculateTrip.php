<?php

class CalculateTrip
{

    /**
     * @var TripCalculationRequest $request
     */
    protected $request = null;

    /**
     * @param TripCalculationRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return TripCalculationRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param TripCalculationRequest $request
     * @return CalculateTrip
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
