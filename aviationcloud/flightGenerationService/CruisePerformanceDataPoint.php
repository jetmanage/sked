<?php

class CruisePerformanceDataPoint extends PerformanceDataPoint
{

    /**
     * @var AircraftCruisePerformanceData $CruisePerformance
     */
    protected $CruisePerformance = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return AircraftCruisePerformanceData
     */
    public function getCruisePerformance()
    {
      return $this->CruisePerformance;
    }

    /**
     * @param AircraftCruisePerformanceData $CruisePerformance
     * @return CruisePerformanceDataPoint
     */
    public function setCruisePerformance($CruisePerformance)
    {
      $this->CruisePerformance = $CruisePerformance;
      return $this;
    }

}
