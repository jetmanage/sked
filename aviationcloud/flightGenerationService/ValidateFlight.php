<?php

class ValidateFlight
{

    /**
     * @var FlightValidationRequest $request
     */
    protected $request = null;

    /**
     * @param FlightValidationRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return FlightValidationRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param FlightValidationRequest $request
     * @return ValidateFlight
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
