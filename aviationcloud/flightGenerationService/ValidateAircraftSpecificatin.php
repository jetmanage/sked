<?php

class ValidateAircraftSpecificatin
{

    /**
     * @var ValidateAircraftRequest $request
     */
    protected $request = null;

    /**
     * @param ValidateAircraftRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return ValidateAircraftRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param ValidateAircraftRequest $request
     * @return ValidateAircraftSpecificatin
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
