<?php

class ETPScenario
{

    /**
     * @var ArrayOfETPPoint $ETPData
     */
    protected $ETPData = null;

    /**
     * @var string $ScenarioName
     */
    protected $ScenarioName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfETPPoint
     */
    public function getETPData()
    {
      return $this->ETPData;
    }

    /**
     * @param ArrayOfETPPoint $ETPData
     * @return ETPScenario
     */
    public function setETPData($ETPData)
    {
      $this->ETPData = $ETPData;
      return $this;
    }

    /**
     * @return string
     */
    public function getScenarioName()
    {
      return $this->ScenarioName;
    }

    /**
     * @param string $ScenarioName
     * @return ETPScenario
     */
    public function setScenarioName($ScenarioName)
    {
      $this->ScenarioName = $ScenarioName;
      return $this;
    }

}
