<?php

class ArrayOfByAltitudeDescentProfile implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ByAltitudeDescentProfile[] $ByAltitudeDescentProfile
     */
    protected $ByAltitudeDescentProfile = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ByAltitudeDescentProfile[]
     */
    public function getByAltitudeDescentProfile()
    {
      return $this->ByAltitudeDescentProfile;
    }

    /**
     * @param ByAltitudeDescentProfile[] $ByAltitudeDescentProfile
     * @return ArrayOfByAltitudeDescentProfile
     */
    public function setByAltitudeDescentProfile(array $ByAltitudeDescentProfile = null)
    {
      $this->ByAltitudeDescentProfile = $ByAltitudeDescentProfile;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ByAltitudeDescentProfile[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ByAltitudeDescentProfile
     */
    public function offsetGet($offset)
    {
      return $this->ByAltitudeDescentProfile[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ByAltitudeDescentProfile $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ByAltitudeDescentProfile[] = $value;
      } else {
        $this->ByAltitudeDescentProfile[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ByAltitudeDescentProfile[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ByAltitudeDescentProfile Return the current element
     */
    public function current()
    {
      return current($this->ByAltitudeDescentProfile);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ByAltitudeDescentProfile);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ByAltitudeDescentProfile);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ByAltitudeDescentProfile);
    }

    /**
     * Countable implementation
     *
     * @return ByAltitudeDescentProfile Return count of elements
     */
    public function count()
    {
      return count($this->ByAltitudeDescentProfile);
    }

}
