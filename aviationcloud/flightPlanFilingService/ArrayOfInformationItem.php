<?php

class ArrayOfInformationItem implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var InformationItem[] $InformationItem
     */
    protected $InformationItem = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return InformationItem[]
     */
    public function getInformationItem()
    {
      return $this->InformationItem;
    }

    /**
     * @param InformationItem[] $InformationItem
     * @return ArrayOfInformationItem
     */
    public function setInformationItem(array $InformationItem = null)
    {
      $this->InformationItem = $InformationItem;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->InformationItem[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return InformationItem
     */
    public function offsetGet($offset)
    {
      return $this->InformationItem[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param InformationItem $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->InformationItem[] = $value;
      } else {
        $this->InformationItem[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->InformationItem[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return InformationItem Return the current element
     */
    public function current()
    {
      return current($this->InformationItem);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->InformationItem);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->InformationItem);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->InformationItem);
    }

    /**
     * Countable implementation
     *
     * @return InformationItem Return count of elements
     */
    public function count()
    {
      return count($this->InformationItem);
    }

}
