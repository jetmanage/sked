<?php

class ACCurrency
{

    /**
     * @var string $CurrencyUnit
     */
    protected $CurrencyUnit = null;

    /**
     * @var float $Value
     */
    protected $Value = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCurrencyUnit()
    {
      return $this->CurrencyUnit;
    }

    /**
     * @param string $CurrencyUnit
     * @return ACCurrency
     */
    public function setCurrencyUnit($CurrencyUnit)
    {
      $this->CurrencyUnit = $CurrencyUnit;
      return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param float $Value
     * @return ACCurrency
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

}
