<?php

class CancelFlight
{

    /**
     * @var CancelFlightRequest $request
     */
    protected $request = null;

    /**
     * @param CancelFlightRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return CancelFlightRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param CancelFlightRequest $request
     * @return CancelFlight
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
