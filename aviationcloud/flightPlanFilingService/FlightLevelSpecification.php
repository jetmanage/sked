<?php

class FlightLevelSpecification
{

    /**
     * @var int $UpperBound
     */
    protected $UpperBound = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getUpperBound()
    {
      return $this->UpperBound;
    }

    /**
     * @param int $UpperBound
     * @return FlightLevelSpecification
     */
    public function setUpperBound($UpperBound)
    {
      $this->UpperBound = $UpperBound;
      return $this;
    }

}
