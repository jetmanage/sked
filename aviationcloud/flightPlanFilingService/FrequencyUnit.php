<?php

class FrequencyUnit
{
    const __default = 'LowFrequency';
    const LowFrequency = 'LowFrequency';
    const MediumFrequency = 'MediumFrequency';
    const HighFrequency = 'HighFrequency';
    const VeryHighFrequency100KhzSparsing = 'VeryHighFrequency100KhzSparsing';
    const VeryHighFreuqency50KhzSparsing = 'VeryHighFreuqency50KhzSparsing';
    const VeryHighFrequency25KhzSparsing = 'VeryHighFrequency25KhzSparsing';
    const VeryHighFrequencyNonStandardSparsing = 'VeryHighFrequencyNonStandardSparsing';
    const UltraHighFrequency = 'UltraHighFrequency';
    const VeryHighFrequency833Sparsing = 'VeryHighFrequency833Sparsing';
    const DigitalService = 'DigitalService';


}
