<?php

class DelayFlightPlan
{

    /**
     * @var DelayFlightPlanRequest $request
     */
    protected $request = null;

    /**
     * @param DelayFlightPlanRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return DelayFlightPlanRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param DelayFlightPlanRequest $request
     * @return DelayFlightPlan
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
