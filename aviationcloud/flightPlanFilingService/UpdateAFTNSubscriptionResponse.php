<?php

class UpdateAFTNSubscriptionResponse
{

    /**
     * @var AFTNSubscriptionUpdateResponse $UpdateAFTNSubscriptionResult
     */
    protected $UpdateAFTNSubscriptionResult = null;

    /**
     * @param AFTNSubscriptionUpdateResponse $UpdateAFTNSubscriptionResult
     */
    public function __construct($UpdateAFTNSubscriptionResult)
    {
      $this->UpdateAFTNSubscriptionResult = $UpdateAFTNSubscriptionResult;
    }

    /**
     * @return AFTNSubscriptionUpdateResponse
     */
    public function getUpdateAFTNSubscriptionResult()
    {
      return $this->UpdateAFTNSubscriptionResult;
    }

    /**
     * @param AFTNSubscriptionUpdateResponse $UpdateAFTNSubscriptionResult
     * @return UpdateAFTNSubscriptionResponse
     */
    public function setUpdateAFTNSubscriptionResult($UpdateAFTNSubscriptionResult)
    {
      $this->UpdateAFTNSubscriptionResult = $UpdateAFTNSubscriptionResult;
      return $this;
    }

}
