<?php

class RouteProfile
{

    /**
     * @var string $Eurocontrol4DProfileInfo
     */
    protected $Eurocontrol4DProfileInfo = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getEurocontrol4DProfileInfo()
    {
      return $this->Eurocontrol4DProfileInfo;
    }

    /**
     * @param string $Eurocontrol4DProfileInfo
     * @return RouteProfile
     */
    public function setEurocontrol4DProfileInfo($Eurocontrol4DProfileInfo)
    {
      $this->Eurocontrol4DProfileInfo = $Eurocontrol4DProfileInfo;
      return $this;
    }

}
