<?php

class UpdateFlightPlanResponse
{

    /**
     * @var FlightPlanUpdateResponse $UpdateFlightPlanResult
     */
    protected $UpdateFlightPlanResult = null;

    /**
     * @param FlightPlanUpdateResponse $UpdateFlightPlanResult
     */
    public function __construct($UpdateFlightPlanResult)
    {
      $this->UpdateFlightPlanResult = $UpdateFlightPlanResult;
    }

    /**
     * @return FlightPlanUpdateResponse
     */
    public function getUpdateFlightPlanResult()
    {
      return $this->UpdateFlightPlanResult;
    }

    /**
     * @param FlightPlanUpdateResponse $UpdateFlightPlanResult
     * @return UpdateFlightPlanResponse
     */
    public function setUpdateFlightPlanResult($UpdateFlightPlanResult)
    {
      $this->UpdateFlightPlanResult = $UpdateFlightPlanResult;
      return $this;
    }

}
