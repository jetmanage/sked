<?php

class EnrouteDiversionAirport
{

    /**
     * @var Airport $Airport
     */
    protected $Airport = null;

    /**
     * @var float $GlideDistance
     */
    protected $GlideDistance = null;

    /**
     * @var ArrayOfSphereicPoint $Polygon
     */
    protected $Polygon = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Airport
     */
    public function getAirport()
    {
      return $this->Airport;
    }

    /**
     * @param Airport $Airport
     * @return EnrouteDiversionAirport
     */
    public function setAirport($Airport)
    {
      $this->Airport = $Airport;
      return $this;
    }

    /**
     * @return float
     */
    public function getGlideDistance()
    {
      return $this->GlideDistance;
    }

    /**
     * @param float $GlideDistance
     * @return EnrouteDiversionAirport
     */
    public function setGlideDistance($GlideDistance)
    {
      $this->GlideDistance = $GlideDistance;
      return $this;
    }

    /**
     * @return ArrayOfSphereicPoint
     */
    public function getPolygon()
    {
      return $this->Polygon;
    }

    /**
     * @param ArrayOfSphereicPoint $Polygon
     * @return EnrouteDiversionAirport
     */
    public function setPolygon($Polygon)
    {
      $this->Polygon = $Polygon;
      return $this;
    }

}
