<?php

class ETPData
{

    /**
     * @var float $AverageISA
     */
    protected $AverageISA = null;

    /**
     * @var float $AverageTemperature
     */
    protected $AverageTemperature = null;

    /**
     * @var int $AverageWindComponent
     */
    protected $AverageWindComponent = null;

    /**
     * @var float $AverageWindDirection
     */
    protected $AverageWindDirection = null;

    /**
     * @var float $AverageWindVelocity
     */
    protected $AverageWindVelocity = null;

    /**
     * @var Weight $CruiseFuel
     */
    protected $CruiseFuel = null;

    /**
     * @var float $DriftDownFlightlevel
     */
    protected $DriftDownFlightlevel = null;

    /**
     * @var Weight $EmergencyDescentFuel
     */
    protected $EmergencyDescentFuel = null;

    /**
     * @var Weight $FinalDescentFuel
     */
    protected $FinalDescentFuel = null;

    /**
     * @var Time $FlightTime
     */
    protected $FlightTime = null;

    /**
     * @var Weight $FuelDumpRequired
     */
    protected $FuelDumpRequired = null;

    /**
     * @var Weight $FuelRemaining
     */
    protected $FuelRemaining = null;

    /**
     * @var Length $GreatCircleDistance
     */
    protected $GreatCircleDistance = null;

    /**
     * @var float $GrossWeight
     */
    protected $GrossWeight = null;

    /**
     * @var float $GroundSpeed
     */
    protected $GroundSpeed = null;

    /**
     * @var int $HighestGRIDMORA
     */
    protected $HighestGRIDMORA = null;

    /**
     * @var Weight $HoldingFuel
     */
    protected $HoldingFuel = null;

    /**
     * @var Weight $TotalEnrouteFuel
     */
    protected $TotalEnrouteFuel = null;

    /**
     * @var Weight $TotalFuelConsumption
     */
    protected $TotalFuelConsumption = null;

    /**
     * @var Weight $TotalFuelRequiredFromDeparture
     */
    protected $TotalFuelRequiredFromDeparture = null;

    /**
     * @var float $Track
     */
    protected $Track = null;

    /**
     * @var float $TrueAirSpeed
     */
    protected $TrueAirSpeed = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getAverageISA()
    {
      return $this->AverageISA;
    }

    /**
     * @param float $AverageISA
     * @return ETPData
     */
    public function setAverageISA($AverageISA)
    {
      $this->AverageISA = $AverageISA;
      return $this;
    }

    /**
     * @return float
     */
    public function getAverageTemperature()
    {
      return $this->AverageTemperature;
    }

    /**
     * @param float $AverageTemperature
     * @return ETPData
     */
    public function setAverageTemperature($AverageTemperature)
    {
      $this->AverageTemperature = $AverageTemperature;
      return $this;
    }

    /**
     * @return int
     */
    public function getAverageWindComponent()
    {
      return $this->AverageWindComponent;
    }

    /**
     * @param int $AverageWindComponent
     * @return ETPData
     */
    public function setAverageWindComponent($AverageWindComponent)
    {
      $this->AverageWindComponent = $AverageWindComponent;
      return $this;
    }

    /**
     * @return float
     */
    public function getAverageWindDirection()
    {
      return $this->AverageWindDirection;
    }

    /**
     * @param float $AverageWindDirection
     * @return ETPData
     */
    public function setAverageWindDirection($AverageWindDirection)
    {
      $this->AverageWindDirection = $AverageWindDirection;
      return $this;
    }

    /**
     * @return float
     */
    public function getAverageWindVelocity()
    {
      return $this->AverageWindVelocity;
    }

    /**
     * @param float $AverageWindVelocity
     * @return ETPData
     */
    public function setAverageWindVelocity($AverageWindVelocity)
    {
      $this->AverageWindVelocity = $AverageWindVelocity;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getCruiseFuel()
    {
      return $this->CruiseFuel;
    }

    /**
     * @param Weight $CruiseFuel
     * @return ETPData
     */
    public function setCruiseFuel($CruiseFuel)
    {
      $this->CruiseFuel = $CruiseFuel;
      return $this;
    }

    /**
     * @return float
     */
    public function getDriftDownFlightlevel()
    {
      return $this->DriftDownFlightlevel;
    }

    /**
     * @param float $DriftDownFlightlevel
     * @return ETPData
     */
    public function setDriftDownFlightlevel($DriftDownFlightlevel)
    {
      $this->DriftDownFlightlevel = $DriftDownFlightlevel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getEmergencyDescentFuel()
    {
      return $this->EmergencyDescentFuel;
    }

    /**
     * @param Weight $EmergencyDescentFuel
     * @return ETPData
     */
    public function setEmergencyDescentFuel($EmergencyDescentFuel)
    {
      $this->EmergencyDescentFuel = $EmergencyDescentFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFinalDescentFuel()
    {
      return $this->FinalDescentFuel;
    }

    /**
     * @param Weight $FinalDescentFuel
     * @return ETPData
     */
    public function setFinalDescentFuel($FinalDescentFuel)
    {
      $this->FinalDescentFuel = $FinalDescentFuel;
      return $this;
    }

    /**
     * @return Time
     */
    public function getFlightTime()
    {
      return $this->FlightTime;
    }

    /**
     * @param Time $FlightTime
     * @return ETPData
     */
    public function setFlightTime($FlightTime)
    {
      $this->FlightTime = $FlightTime;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuelDumpRequired()
    {
      return $this->FuelDumpRequired;
    }

    /**
     * @param Weight $FuelDumpRequired
     * @return ETPData
     */
    public function setFuelDumpRequired($FuelDumpRequired)
    {
      $this->FuelDumpRequired = $FuelDumpRequired;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuelRemaining()
    {
      return $this->FuelRemaining;
    }

    /**
     * @param Weight $FuelRemaining
     * @return ETPData
     */
    public function setFuelRemaining($FuelRemaining)
    {
      $this->FuelRemaining = $FuelRemaining;
      return $this;
    }

    /**
     * @return Length
     */
    public function getGreatCircleDistance()
    {
      return $this->GreatCircleDistance;
    }

    /**
     * @param Length $GreatCircleDistance
     * @return ETPData
     */
    public function setGreatCircleDistance($GreatCircleDistance)
    {
      $this->GreatCircleDistance = $GreatCircleDistance;
      return $this;
    }

    /**
     * @return float
     */
    public function getGrossWeight()
    {
      return $this->GrossWeight;
    }

    /**
     * @param float $GrossWeight
     * @return ETPData
     */
    public function setGrossWeight($GrossWeight)
    {
      $this->GrossWeight = $GrossWeight;
      return $this;
    }

    /**
     * @return float
     */
    public function getGroundSpeed()
    {
      return $this->GroundSpeed;
    }

    /**
     * @param float $GroundSpeed
     * @return ETPData
     */
    public function setGroundSpeed($GroundSpeed)
    {
      $this->GroundSpeed = $GroundSpeed;
      return $this;
    }

    /**
     * @return int
     */
    public function getHighestGRIDMORA()
    {
      return $this->HighestGRIDMORA;
    }

    /**
     * @param int $HighestGRIDMORA
     * @return ETPData
     */
    public function setHighestGRIDMORA($HighestGRIDMORA)
    {
      $this->HighestGRIDMORA = $HighestGRIDMORA;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getHoldingFuel()
    {
      return $this->HoldingFuel;
    }

    /**
     * @param Weight $HoldingFuel
     * @return ETPData
     */
    public function setHoldingFuel($HoldingFuel)
    {
      $this->HoldingFuel = $HoldingFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTotalEnrouteFuel()
    {
      return $this->TotalEnrouteFuel;
    }

    /**
     * @param Weight $TotalEnrouteFuel
     * @return ETPData
     */
    public function setTotalEnrouteFuel($TotalEnrouteFuel)
    {
      $this->TotalEnrouteFuel = $TotalEnrouteFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTotalFuelConsumption()
    {
      return $this->TotalFuelConsumption;
    }

    /**
     * @param Weight $TotalFuelConsumption
     * @return ETPData
     */
    public function setTotalFuelConsumption($TotalFuelConsumption)
    {
      $this->TotalFuelConsumption = $TotalFuelConsumption;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTotalFuelRequiredFromDeparture()
    {
      return $this->TotalFuelRequiredFromDeparture;
    }

    /**
     * @param Weight $TotalFuelRequiredFromDeparture
     * @return ETPData
     */
    public function setTotalFuelRequiredFromDeparture($TotalFuelRequiredFromDeparture)
    {
      $this->TotalFuelRequiredFromDeparture = $TotalFuelRequiredFromDeparture;
      return $this;
    }

    /**
     * @return float
     */
    public function getTrack()
    {
      return $this->Track;
    }

    /**
     * @param float $Track
     * @return ETPData
     */
    public function setTrack($Track)
    {
      $this->Track = $Track;
      return $this;
    }

    /**
     * @return float
     */
    public function getTrueAirSpeed()
    {
      return $this->TrueAirSpeed;
    }

    /**
     * @param float $TrueAirSpeed
     * @return ETPData
     */
    public function setTrueAirSpeed($TrueAirSpeed)
    {
      $this->TrueAirSpeed = $TrueAirSpeed;
      return $this;
    }

}
