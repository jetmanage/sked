<?php

class ArrayOfFlightIdentifier implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var FlightIdentifier[] $FlightIdentifier
     */
    protected $FlightIdentifier = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FlightIdentifier[]
     */
    public function getFlightIdentifier()
    {
      return $this->FlightIdentifier;
    }

    /**
     * @param FlightIdentifier[] $FlightIdentifier
     * @return ArrayOfFlightIdentifier
     */
    public function setFlightIdentifier(array $FlightIdentifier = null)
    {
      $this->FlightIdentifier = $FlightIdentifier;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->FlightIdentifier[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return FlightIdentifier
     */
    public function offsetGet($offset)
    {
      return $this->FlightIdentifier[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param FlightIdentifier $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->FlightIdentifier[] = $value;
      } else {
        $this->FlightIdentifier[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->FlightIdentifier[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return FlightIdentifier Return the current element
     */
    public function current()
    {
      return current($this->FlightIdentifier);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->FlightIdentifier);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->FlightIdentifier);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->FlightIdentifier);
    }

    /**
     * Countable implementation
     *
     * @return FlightIdentifier Return count of elements
     */
    public function count()
    {
      return count($this->FlightIdentifier);
    }

}
