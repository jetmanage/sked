<?php

class AFTNAddressesResolveResponse
{

    /**
     * @var ArrayOfAFTNAddress $AFTNAdresses
     */
    protected $AFTNAdresses = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAFTNAddress
     */
    public function getAFTNAdresses()
    {
      return $this->AFTNAdresses;
    }

    /**
     * @param ArrayOfAFTNAddress $AFTNAdresses
     * @return AFTNAddressesResolveResponse
     */
    public function setAFTNAdresses($AFTNAdresses)
    {
      $this->AFTNAdresses = $AFTNAdresses;
      return $this;
    }

}
