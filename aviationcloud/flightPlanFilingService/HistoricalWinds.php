<?php

class HistoricalWinds extends Winds
{

    /**
     * @var boolean $BestCaseConfidence
     */
    protected $BestCaseConfidence = null;

    /**
     * @var float $ConfidenceInterval
     */
    protected $ConfidenceInterval = null;

    /**
     * @var int $Month
     */
    protected $Month = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getBestCaseConfidence()
    {
      return $this->BestCaseConfidence;
    }

    /**
     * @param boolean $BestCaseConfidence
     * @return HistoricalWinds
     */
    public function setBestCaseConfidence($BestCaseConfidence)
    {
      $this->BestCaseConfidence = $BestCaseConfidence;
      return $this;
    }

    /**
     * @return float
     */
    public function getConfidenceInterval()
    {
      return $this->ConfidenceInterval;
    }

    /**
     * @param float $ConfidenceInterval
     * @return HistoricalWinds
     */
    public function setConfidenceInterval($ConfidenceInterval)
    {
      $this->ConfidenceInterval = $ConfidenceInterval;
      return $this;
    }

    /**
     * @return int
     */
    public function getMonth()
    {
      return $this->Month;
    }

    /**
     * @param int $Month
     * @return HistoricalWinds
     */
    public function setMonth($Month)
    {
      $this->Month = $Month;
      return $this;
    }

}
