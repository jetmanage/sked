<?php

class FlightPlanUpdateResponse
{

    /**
     * @var string $Action
     */
    protected $Action = null;

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    /**
     * @var int $FlightplanId
     */
    protected $FlightplanId = null;

    /**
     * @var string $GeneratedMessage
     */
    protected $GeneratedMessage = null;

    /**
     * @var boolean $HasUpdatedItem19
     */
    protected $HasUpdatedItem19 = null;

    /**
     * @var boolean $IsDeferredFiling
     */
    protected $IsDeferredFiling = null;

    /**
     * @var string $MetaFPLString
     */
    protected $MetaFPLString = null;

    /**
     * @var AFTNUpdateAction $UpdateAction
     */
    protected $UpdateAction = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param string $Action
     * @return FlightPlanUpdateResponse
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return FlightPlanUpdateResponse
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightplanId()
    {
      return $this->FlightplanId;
    }

    /**
     * @param int $FlightplanId
     * @return FlightPlanUpdateResponse
     */
    public function setFlightplanId($FlightplanId)
    {
      $this->FlightplanId = $FlightplanId;
      return $this;
    }

    /**
     * @return string
     */
    public function getGeneratedMessage()
    {
      return $this->GeneratedMessage;
    }

    /**
     * @param string $GeneratedMessage
     * @return FlightPlanUpdateResponse
     */
    public function setGeneratedMessage($GeneratedMessage)
    {
      $this->GeneratedMessage = $GeneratedMessage;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHasUpdatedItem19()
    {
      return $this->HasUpdatedItem19;
    }

    /**
     * @param boolean $HasUpdatedItem19
     * @return FlightPlanUpdateResponse
     */
    public function setHasUpdatedItem19($HasUpdatedItem19)
    {
      $this->HasUpdatedItem19 = $HasUpdatedItem19;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDeferredFiling()
    {
      return $this->IsDeferredFiling;
    }

    /**
     * @param boolean $IsDeferredFiling
     * @return FlightPlanUpdateResponse
     */
    public function setIsDeferredFiling($IsDeferredFiling)
    {
      $this->IsDeferredFiling = $IsDeferredFiling;
      return $this;
    }

    /**
     * @return string
     */
    public function getMetaFPLString()
    {
      return $this->MetaFPLString;
    }

    /**
     * @param string $MetaFPLString
     * @return FlightPlanUpdateResponse
     */
    public function setMetaFPLString($MetaFPLString)
    {
      $this->MetaFPLString = $MetaFPLString;
      return $this;
    }

    /**
     * @return AFTNUpdateAction
     */
    public function getUpdateAction()
    {
      return $this->UpdateAction;
    }

    /**
     * @param AFTNUpdateAction $UpdateAction
     * @return FlightPlanUpdateResponse
     */
    public function setUpdateAction($UpdateAction)
    {
      $this->UpdateAction = $UpdateAction;
      return $this;
    }

}
