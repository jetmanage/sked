<?php

class FlightplanFilingService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'FileFlightPlan' => '\\FileFlightPlan',
      'FileFlightPlanResponse' => '\\FileFlightPlanResponse',
      'RetrieveFiledFlights' => '\\RetrieveFiledFlights',
      'RetrieveFiledFlightsResponse' => '\\RetrieveFiledFlightsResponse',
      'RetrieveMessages' => '\\RetrieveMessages',
      'RetrieveMessagesResponse' => '\\RetrieveMessagesResponse',
      'CancelFlight' => '\\CancelFlight',
      'CancelFlightResponse' => '\\CancelFlightResponse',
      'CancelFlightPlan' => '\\CancelFlightPlan',
      'CancelFlightPlanResponse' => '\\CancelFlightPlanResponse',
      'DelayFlightPlan' => '\\DelayFlightPlan',
      'DelayFlightPlanResponse' => '\\DelayFlightPlanResponse',
      'ResolveAFTNAdresses' => '\\ResolveAFTNAdresses',
      'ResolveAFTNAdressesResponse' => '\\ResolveAFTNAdressesResponse',
      'SubscribeAFTNMessages' => '\\SubscribeAFTNMessages',
      'SubscribeAFTNMessagesResponse' => '\\SubscribeAFTNMessagesResponse',
      'UnsubscribeAFTNMessages' => '\\UnsubscribeAFTNMessages',
      'UnsubscribeAFTNMessagesResponse' => '\\UnsubscribeAFTNMessagesResponse',
      'FileFlightPlanAsynchronously' => '\\FileFlightPlanAsynchronously',
      'FileFlightPlanAsynchronouslyResponse' => '\\FileFlightPlanAsynchronouslyResponse',
      'GetExistingAFTNSubscriptions' => '\\GetExistingAFTNSubscriptions',
      'GetExistingAFTNSubscriptionsResponse' => '\\GetExistingAFTNSubscriptionsResponse',
      'UpdateFlightPlan' => '\\UpdateFlightPlan',
      'UpdateFlightPlanResponse' => '\\UpdateFlightPlanResponse',
      'GenerateFPLString' => '\\GenerateFPLString',
      'GenerateFPLStringResponse' => '\\GenerateFPLStringResponse',
      'GetFPLString' => '\\GetFPLString',
      'GetFPLStringResponse' => '\\GetFPLStringResponse',
      'UpdateAFTNSubscription' => '\\UpdateAFTNSubscription',
      'UpdateAFTNSubscriptionResponse' => '\\UpdateAFTNSubscriptionResponse',
      'FMSUploadFlightPlan' => '\\FMSUploadFlightPlan',
      'FMSUploadFlightPlanResponse' => '\\FMSUploadFlightPlanResponse',
      'FMSUpdateFlightPlan' => '\\FMSUpdateFlightPlan',
      'FMSUpdateFlightPlanResponse' => '\\FMSUpdateFlightPlanResponse',
      'FileFlightplanRequest' => '\\FileFlightplanRequest',
      'RequestBase' => '\\RequestBase',
      'DeferredFilingOptions' => '\\DeferredFilingOptions',
      'EmulatorResponseOptions' => '\\EmulatorResponseOptions',
      'ArrayOfEmulatorAFTNResponse' => '\\ArrayOfEmulatorAFTNResponse',
      'EmulatorAFTNResponse' => '\\EmulatorAFTNResponse',
      'ATCFPLMessage' => '\\ATCFPLMessage',
      'ATCFlightMessage' => '\\ATCFlightMessage',
      'ATCMessageBase' => '\\ATCMessageBase',
      'ATCRejectedMessage' => '\\ATCRejectedMessage',
      'ATCRequestFlightPlanMessage' => '\\ATCRequestFlightPlanMessage',
      'ATCLongAcknowledgeMessage' => '\\ATCLongAcknowledgeMessage',
      'ATCAcknowledgeMessage' => '\\ATCAcknowledgeMessage',
      'ATCDivertedArrivialMessage' => '\\ATCDivertedArrivialMessage',
      'Airport' => '\\Airport',
      'ArrayOfAirportFrequencyARINC424' => '\\ArrayOfAirportFrequencyARINC424',
      'AirportFrequencyARINC424' => '\\AirportFrequencyARINC424',
      'Length' => '\\Length',
      'FIR' => '\\FIR',
      'Airspace' => '\\Airspace',
      'NationalAirspace' => '\\NationalAirspace',
      'SpecialActivityAirspace' => '\\SpecialActivityAirspace',
      'TemporaryFlightRestrictedAirspace' => '\\TemporaryFlightRestrictedAirspace',
      'SphereicPoint' => '\\SphereicPoint',
      'PathPoint' => '\\PathPoint',
      'ArrayOfRunway' => '\\ArrayOfRunway',
      'Runway' => '\\Runway',
      'Altitude' => '\\Altitude',
      'ATCDepartureMessage' => '\\ATCDepartureMessage',
      'ATCDelayMessage' => '\\ATCDelayMessage',
      'ATCChangeMessage' => '\\ATCChangeMessage',
      'AircraftEquipment' => '\\AircraftEquipment',
      'PerformanceBasedNavigationCapabilities' => '\\PerformanceBasedNavigationCapabilities',
      'RadioAndNavigationEquipment' => '\\RadioAndNavigationEquipment',
      'ATCACARSEquipment' => '\\ATCACARSEquipment',
      'CPDLCEquipment' => '\\CPDLCEquipment',
      'RTFEquipment' => '\\RTFEquipment',
      'SSREquipment' => '\\SSREquipment',
      'ADSCapabilities' => '\\ADSCapabilities',
      'ModeSCapabilities' => '\\ModeSCapabilities',
      'Route' => '\\Route',
      'WaypointData' => '\\WaypointData',
      'Time' => '\\Time',
      'Speed' => '\\Speed',
      'ArrayOfRouteNode' => '\\ArrayOfRouteNode',
      'RouteNode' => '\\RouteNode',
      'FlightData' => '\\FlightData',
      'RouteNodeIdentifier' => '\\RouteNodeIdentifier',
      'ParserInformation' => '\\ParserInformation',
      'RouteProfile' => '\\RouteProfile',
      'RouteReport' => '\\RouteReport',
      'ArrayOfAirspace' => '\\ArrayOfAirspace',
      'ArrayOfInformationItem' => '\\ArrayOfInformationItem',
      'InformationItem' => '\\InformationItem',
      'ArrayOfInformationData' => '\\ArrayOfInformationData',
      'InformationData' => '\\InformationData',
      'ArrayOfRouteInfo' => '\\ArrayOfRouteInfo',
      'RouteInfo' => '\\RouteInfo',
      'ArrayOfWarning' => '\\ArrayOfWarning',
      'Warning' => '\\Warning',
      'AirportProcedure' => '\\AirportProcedure',
      'ArrayOfAirportEnrouteTransitionProcedure' => '\\ArrayOfAirportEnrouteTransitionProcedure',
      'AirportEnrouteTransitionProcedure' => '\\AirportEnrouteTransitionProcedure',
      'ArrayOfWaypointData' => '\\ArrayOfWaypointData',
      'AirportProcedureIdentifier' => '\\AirportProcedureIdentifier',
      'ATCCancelMessage' => '\\ATCCancelMessage',
      'ATCArrivalMessage' => '\\ATCArrivalMessage',
      'AircraftATCInfoCodes' => '\\AircraftATCInfoCodes',
      'ArrayOfAFTNAddress' => '\\ArrayOfAFTNAddress',
      'AFTNAddress' => '\\AFTNAddress',
      'FLightATCEET' => '\\FLightATCEET',
      'ArrayOfFlightATCEETElement' => '\\ArrayOfFlightATCEETElement',
      'FlightATCEETElement' => '\\FlightATCEETElement',
      'ATCOtherInformation' => '\\ATCOtherInformation',
      'ATCOtherInformationCode' => '\\ATCOtherInformationCode',
      'ATCOtherInformationContents' => '\\ATCOtherInformationContents',
      'ArrayOfATCOtherItemField' => '\\ArrayOfATCOtherItemField',
      'ATCOtherItemField' => '\\ATCOtherItemField',
      'RNAVPreferentialRoutes' => '\\RNAVPreferentialRoutes',
      'FlightLog' => '\\FlightLog',
      'ArrayOfAirport' => '\\ArrayOfAirport',
      'Currency' => '\\Currency',
      'CalculatedAircraftWeights' => '\\CalculatedAircraftWeights',
      'Weight' => '\\Weight',
      'ArrayOfIntersectionPoint' => '\\ArrayOfIntersectionPoint',
      'IntersectionPoint' => '\\IntersectionPoint',
      'StartEndPoint' => '\\StartEndPoint',
      'BorderIntersectionPoint' => '\\BorderIntersectionPoint',
      'InputPoint' => '\\InputPoint',
      'CalculationInformation' => '\\CalculationInformation',
      'CalculatedETPInformation' => '\\CalculatedETPInformation',
      'ArrayOfAirportTimeWindow' => '\\ArrayOfAirportTimeWindow',
      'AirportTimeWindow' => '\\AirportTimeWindow',
      'ArrayOfETOPSSettingSpecification' => '\\ArrayOfETOPSSettingSpecification',
      'ETOPSSettingSpecification' => '\\ETOPSSettingSpecification',
      'ArrayOfETPPoint' => '\\ArrayOfETPPoint',
      'ETPPoint' => '\\ETPPoint',
      'ETPData' => '\\ETPData',
      'ArrayOfETPScenario' => '\\ArrayOfETPScenario',
      'ETPScenario' => '\\ETPScenario',
      'OverflightCalculationInformation' => '\\OverflightCalculationInformation',
      'ArrayOfCountryInformation' => '\\ArrayOfCountryInformation',
      'CountryInformation' => '\\CountryInformation',
      'ACCurrency' => '\\ACCurrency',
      'ArrayOfFIR' => '\\ArrayOfFIR',
      'FlightAnalysis' => '\\FlightAnalysis',
      'RiskAnalysis' => '\\RiskAnalysis',
      'ArrayOfEnrouteDiversionAirport' => '\\ArrayOfEnrouteDiversionAirport',
      'EnrouteDiversionAirport' => '\\EnrouteDiversionAirport',
      'ArrayOfSphereicPoint' => '\\ArrayOfSphereicPoint',
      'ArrayOfFlightSegment' => '\\ArrayOfFlightSegment',
      'FlightSegment' => '\\FlightSegment',
      'FlightDistance' => '\\FlightDistance',
      'FlightFuelValues' => '\\FlightFuelValues',
      'FlightMetaData' => '\\FlightMetaData',
      'FlightSpecification' => '\\FlightSpecification',
      'ATCData' => '\\ATCData',
      'ATCSupplementaryInfo' => '\\ATCSupplementaryInfo',
      'AircraftSpecification' => '\\AircraftSpecification',
      'TailnumberAircraftSpecification' => '\\TailnumberAircraftSpecification',
      'ICAOIdentAircraftSpecification' => '\\ICAOIdentAircraftSpecification',
      'AircraftATCInfo' => '\\AircraftATCInfo',
      'AircraftFrequencyAvailability' => '\\AircraftFrequencyAvailability',
      'AircraftLifeJacketEquipment' => '\\AircraftLifeJacketEquipment',
      'AircraftSurvivalEquipment' => '\\AircraftSurvivalEquipment',
      'FuelTankDefinition' => '\\FuelTankDefinition',
      'SimpleFuelTankDefinition' => '\\SimpleFuelTankDefinition',
      'FuelTankListDefinition' => '\\FuelTankListDefinition',
      'ArrayOfFuelTank' => '\\ArrayOfFuelTank',
      'FuelTank' => '\\FuelTank',
      'AircraftStructureUnit' => '\\AircraftStructureUnit',
      'ArmDefinition' => '\\ArmDefinition',
      'StaticArmDefinition' => '\\StaticArmDefinition',
      'DynamicArmDefinition' => '\\DynamicArmDefinition',
      'ArrayOfWeightDependentArm' => '\\ArrayOfWeightDependentArm',
      'WeightDependentArm' => '\\WeightDependentArm',
      'PassengerCompartment' => '\\PassengerCompartment',
      'CargoCompartment' => '\\CargoCompartment',
      'CrewCompartment' => '\\CrewCompartment',
      'MassAndBalanceConfigurationBase' => '\\MassAndBalanceConfigurationBase',
      'MassAndBalanceConfiguration' => '\\MassAndBalanceConfiguration',
      'SimpleMassAndBalanceConfiguration' => '\\SimpleMassAndBalanceConfiguration',
      'CompartmentBasedMassAndBalanceConfiguration' => '\\CompartmentBasedMassAndBalanceConfiguration',
      'ArrayOfPassengerCompartment' => '\\ArrayOfPassengerCompartment',
      'ArrayOfCargoCompartment' => '\\ArrayOfCargoCompartment',
      'ArrayOfCrewCompartment' => '\\ArrayOfCrewCompartment',
      'DefaultWeightParameters' => '\\DefaultWeightParameters',
      'ReferencedMassAndBalanceConfiguration' => '\\ReferencedMassAndBalanceConfiguration',
      'UnspecifiedMassAndBalance' => '\\UnspecifiedMassAndBalance',
      'EmbeddedAircraftSpecification' => '\\EmbeddedAircraftSpecification',
      'Aircraft' => '\\Aircraft',
      'AircraftPerformanceData' => '\\AircraftPerformanceData',
      'ArrayOfAircraftPerformanceProfile' => '\\ArrayOfAircraftPerformanceProfile',
      'AircraftPerformanceProfile' => '\\AircraftPerformanceProfile',
      'BiasedClimbPerformanceProfile' => '\\BiasedClimbPerformanceProfile',
      'ClimbProfile' => '\\ClimbProfile',
      'DiscreteClimbProfile' => '\\DiscreteClimbProfile',
      'ArrayOfClimbDescendDataSeries' => '\\ArrayOfClimbDescendDataSeries',
      'ClimbDescendDataSeries' => '\\ClimbDescendDataSeries',
      'PerformanceDataSeries' => '\\PerformanceDataSeries',
      'HoldingDataSeries' => '\\HoldingDataSeries',
      'ArrayOfHoldingDataPoint' => '\\ArrayOfHoldingDataPoint',
      'HoldingDataPoint' => '\\HoldingDataPoint',
      'PerformanceDataPoint' => '\\PerformanceDataPoint',
      'ISAPoint' => '\\ISAPoint',
      'ClimbDescendDataPoint' => '\\ClimbDescendDataPoint',
      'ClimbDescendData' => '\\ClimbDescendData',
      'CruisePerformanceDataPoint' => '\\CruisePerformanceDataPoint',
      'AircraftCruisePerformanceData' => '\\AircraftCruisePerformanceData',
      'HoldingData' => '\\HoldingData',
      'CruisePerformanceDataSeries' => '\\CruisePerformanceDataSeries',
      'ArrayOfCruisePerformanceDataPoint' => '\\ArrayOfCruisePerformanceDataPoint',
      'ArrayOfClimbDescendDataPoint' => '\\ArrayOfClimbDescendDataPoint',
      'SimpleClimbProfile' => '\\SimpleClimbProfile',
      'ByAltitudeClimbProfile' => '\\ByAltitudeClimbProfile',
      'ArrayOfByAltitudeFuelFlow' => '\\ArrayOfByAltitudeFuelFlow',
      'ByAltitudeFuelFlow' => '\\ByAltitudeFuelFlow',
      'ArrayOfByAltitudeClimbPerformance' => '\\ArrayOfByAltitudeClimbPerformance',
      'ByAltitudeClimbPerformance' => '\\ByAltitudeClimbPerformance',
      'BiasedCruisePerformanceProfile' => '\\BiasedCruisePerformanceProfile',
      'CruiseProfile' => '\\CruiseProfile',
      'DiscreteCruiseProfile' => '\\DiscreteCruiseProfile',
      'ArrayOfCruisePerformanceDataSeries' => '\\ArrayOfCruisePerformanceDataSeries',
      'ArrayOfWeightLimitation' => '\\ArrayOfWeightLimitation',
      'WeightLimitation' => '\\WeightLimitation',
      'ETOPSCruiseProfile' => '\\ETOPSCruiseProfile',
      'SimpleCruiseProfile' => '\\SimpleCruiseProfile',
      'ByAltitudeCruiseProfile' => '\\ByAltitudeCruiseProfile',
      'ArrayOfByAltitudeCruisePerformance' => '\\ArrayOfByAltitudeCruisePerformance',
      'ByAltitudeCruisePerformance' => '\\ByAltitudeCruisePerformance',
      'BiasedDescentProfile' => '\\BiasedDescentProfile',
      'DescentProfile' => '\\DescentProfile',
      'DiscreteDescentProfile' => '\\DiscreteDescentProfile',
      'SimpleDescentProfile' => '\\SimpleDescentProfile',
      'ByAltitudeDescentProfile' => '\\ByAltitudeDescentProfile',
      'ArrayOfByAltitudeDescentPerformance' => '\\ArrayOfByAltitudeDescentPerformance',
      'ByAltitudeDescentPerformance' => '\\ByAltitudeDescentPerformance',
      'BiasedHoldingPerformanceProfile' => '\\BiasedHoldingPerformanceProfile',
      'HoldingProfile' => '\\HoldingProfile',
      'DiscreteHoldingProfile' => '\\DiscreteHoldingProfile',
      'ArrayOfHoldingDataSeries' => '\\ArrayOfHoldingDataSeries',
      'ReferencedPerformanceProfile' => '\\ReferencedPerformanceProfile',
      'AircraftStructure' => '\\AircraftStructure',
      'ArrayOfMassAndBalanceConfiguration' => '\\ArrayOfMassAndBalanceConfiguration',
      'AircraftEnvelope' => '\\AircraftEnvelope',
      'ArrayOfAircraftEnvelopePoint' => '\\ArrayOfAircraftEnvelopePoint',
      'AircraftEnvelopePoint' => '\\AircraftEnvelopePoint',
      'AircraftEngineSpecification' => '\\AircraftEngineSpecification',
      'AircraftType' => '\\AircraftType',
      'EngineDetails' => '\\EngineDetails',
      'ArrayOfAircraftCertification' => '\\ArrayOfAircraftCertification',
      'AircraftCertification' => '\\AircraftCertification',
      'ETOPSCertification' => '\\ETOPSCertification',
      'AircraftTypeConfigurationSpecification' => '\\AircraftTypeConfigurationSpecification',
      'SimplifiedAircraftSpecification' => '\\SimplifiedAircraftSpecification',
      'UUIDAircraftSpecification' => '\\UUIDAircraftSpecification',
      'ByAltitudeAircraftSpecification' => '\\ByAltitudeAircraftSpecification',
      'ArrayOfByAltitudeClimbProfile' => '\\ArrayOfByAltitudeClimbProfile',
      'ArrayOfByAltitudeCruiseProfile' => '\\ArrayOfByAltitudeCruiseProfile',
      'ArrayOfByAltitudeDescentProfile' => '\\ArrayOfByAltitudeDescentProfile',
      'FlightCalculationOptions' => '\\FlightCalculationOptions',
      'ContingencyStrategy' => '\\ContingencyStrategy',
      'DefaultContingenyStrategy' => '\\DefaultContingenyStrategy',
      'ERAContingencyStrategy' => '\\ERAContingencyStrategy',
      'ReducedContingencyFuelStrategy' => '\\ReducedContingencyFuelStrategy',
      'FlightLevelOptimizationOptions' => '\\FlightLevelOptimizationOptions',
      'PerformanceProfileSpecification' => '\\PerformanceProfileSpecification',
      'SimpleftPerformanceProfileSpecification' => '\\SimpleftPerformanceProfileSpecification',
      'InterpolatedPerformanceProfileSpecification' => '\\InterpolatedPerformanceProfileSpecification',
      'ArrayOfAltitude' => '\\ArrayOfAltitude',
      'OptimumFlightlevelOptionSpecification' => '\\OptimumFlightlevelOptionSpecification',
      'FuelPolicy' => '\\FuelPolicy',
      'RelativeFuelPolicy' => '\\RelativeFuelPolicy',
      'ValueFuelPolicy' => '\\ValueFuelPolicy',
      'ActualFuelPolicy' => '\\ActualFuelPolicy',
      'ExtraFuelPolicy' => '\\ExtraFuelPolicy',
      'LandingFuelPolicy' => '\\LandingFuelPolicy',
      'TakeOffMassPolicy' => '\\TakeOffMassPolicy',
      'LandingMassPolicy' => '\\LandingMassPolicy',
      'MinimumRequiredFuelPolicy' => '\\MinimumRequiredFuelPolicy',
      'MaximumFuelPolicy' => '\\MaximumFuelPolicy',
      'GainLossCalculationInfo' => '\\GainLossCalculationInfo',
      'InlineOptionSpecification' => '\\InlineOptionSpecification',
      'RAIMPredictionOptions' => '\\RAIMPredictionOptions',
      'FlightCalculationSIDSTAROptions' => '\\FlightCalculationSIDSTAROptions',
      'TaxiFuelSpecification' => '\\TaxiFuelSpecification',
      'MassBasedTaxiFuelSpecification' => '\\MassBasedTaxiFuelSpecification',
      'TimeBasedTaxiFuelSpecification' => '\\TimeBasedTaxiFuelSpecification',
      'TaxiTimeSpecification' => '\\TaxiTimeSpecification',
      'ManuelTaxiTime' => '\\ManuelTaxiTime',
      'DefaultTaxiTime' => '\\DefaultTaxiTime',
      'CargoLoadSpecification' => '\\CargoLoadSpecification',
      'SimpleCargoLoadSpecification' => '\\SimpleCargoLoadSpecification',
      'SpecificCargoLoadSpecification' => '\\SpecificCargoLoadSpecification',
      'ArrayOfCargoSectionLoad' => '\\ArrayOfCargoSectionLoad',
      'CargoSectionLoad' => '\\CargoSectionLoad',
      'RelativeCargoLoadSpecification' => '\\RelativeCargoLoadSpecification',
      'MaxCargoLoadSpecification' => '\\MaxCargoLoadSpecification',
      'CrewLoadSpecification' => '\\CrewLoadSpecification',
      'SimpleCrewSpecification' => '\\SimpleCrewSpecification',
      'SpecificCrewLoadSpecification' => '\\SpecificCrewLoadSpecification',
      'ETPSettings' => '\\ETPSettings',
      'FlightLevelSpecification' => '\\FlightLevelSpecification',
      'MaximumFlightLevelSpecification' => '\\MaximumFlightLevelSpecification',
      'ExactFlightLevelSpecification' => '\\ExactFlightLevelSpecification',
      'OptimumFlightLevelDefinition' => '\\OptimumFlightLevelDefinition',
      'FlightFuelRules' => '\\FlightFuelRules',
      'FAAOpsRules' => '\\FAAOpsRules',
      'EUOpsRules' => '\\EUOpsRules',
      'CustomFuelRule' => '\\CustomFuelRule',
      'PaxLoadSpecification' => '\\PaxLoadSpecification',
      'SimplePaxLoadSpecification' => '\\SimplePaxLoadSpecification',
      'PaxLoad' => '\\PaxLoad',
      'UnspecifiedPaxTypeLoad' => '\\UnspecifiedPaxTypeLoad',
      'SpecifiedPaxTypeLoad' => '\\SpecifiedPaxTypeLoad',
      'SectionBasedPaxLoadSpecification' => '\\SectionBasedPaxLoadSpecification',
      'ArrayOfPaxSectionLoad' => '\\ArrayOfPaxSectionLoad',
      'PaxSectionLoad' => '\\PaxSectionLoad',
      'FlightLevelAdjustmentSpecification' => '\\FlightLevelAdjustmentSpecification',
      'ArrayOfAltitudeNode' => '\\ArrayOfAltitudeNode',
      'AltitudeNode' => '\\AltitudeNode',
      'ArrayOfCruiseChangeNode' => '\\ArrayOfCruiseChangeNode',
      'CruiseChangeNode' => '\\CruiseChangeNode',
      'Winds' => '\\Winds',
      'ForecastWinds' => '\\ForecastWinds',
      'DenseWindModel' => '\\DenseWindModel',
      'HistoricalWinds' => '\\HistoricalWinds',
      'FixedWinds' => '\\FixedWinds',
      'FlightTimes' => '\\FlightTimes',
      'FlightMetrologicalData' => '\\FlightMetrologicalData',
      'IcingPoint' => '\\IcingPoint',
      'TurbulencePoint' => '\\TurbulencePoint',
      'FlightRAIMData' => '\\FlightRAIMData',
      'ArrayOfOverflightCosts' => '\\ArrayOfOverflightCosts',
      'OverflightCosts' => '\\OverflightCosts',
      'PathwayData' => '\\PathwayData',
      'ArrayOfDataPoint' => '\\ArrayOfDataPoint',
      'DataPoint' => '\\DataPoint',
      'AirwayData' => '\\AirwayData',
      'FuelData' => '\\FuelData',
      'MeteorologicalData' => '\\MeteorologicalData',
      'ArrayOfWeatherDataRecord' => '\\ArrayOfWeatherDataRecord',
      'WeatherDataRecord' => '\\WeatherDataRecord',
      'ArrayOfIcingPoint' => '\\ArrayOfIcingPoint',
      'ArrayOfTurbulencePoint' => '\\ArrayOfTurbulencePoint',
      'FlightplanFilingResponse' => '\\FlightplanFilingResponse',
      'Account' => '\\Account',
      'ArrayOfFlightValidationError' => '\\ArrayOfFlightValidationError',
      'FlightValidationError' => '\\FlightValidationError',
      'RetrieveFiledFlightsRequest' => '\\RetrieveFiledFlightsRequest',
      'FiledFlightsRetrievalResponse' => '\\FiledFlightsRetrievalResponse',
      'ArrayOfFlightIdentifier' => '\\ArrayOfFlightIdentifier',
      'FlightIdentifier' => '\\FlightIdentifier',
      'RetrieveFlightMessagesRequest' => '\\RetrieveFlightMessagesRequest',
      'FiledFlightMessagesResponse' => '\\FiledFlightMessagesResponse',
      'ArrayOfATCMessageBase' => '\\ArrayOfATCMessageBase',
      'CancelFlightRequest' => '\\CancelFlightRequest',
      'CancelFlightResposne' => '\\CancelFlightResposne',
      'CancelFlightPlanRequest' => '\\CancelFlightPlanRequest',
      'DelayFlightPlanRequest' => '\\DelayFlightPlanRequest',
      'AFTNResolveRequest' => '\\AFTNResolveRequest',
      'AFTNAddressesResolveResponse' => '\\AFTNAddressesResolveResponse',
      'AFTNMessageSubscriptionRequest' => '\\AFTNMessageSubscriptionRequest',
      'AFTNMessageSubscription' => '\\AFTNMessageSubscription',
      'AwsSQSContext' => '\\AwsSQSContext',
      'BasicHttpAuthCredentials' => '\\BasicHttpAuthCredentials',
      'AFTNMessageUnsubscribeRequest' => '\\AFTNMessageUnsubscribeRequest',
      'ArrayOfAFTNMessageSubscription' => '\\ArrayOfAFTNMessageSubscription',
      'FlightPlanUpdateRequest' => '\\FlightPlanUpdateRequest',
      'FlightPlanUpdateResponse' => '\\FlightPlanUpdateResponse',
      'GenerateFPLStringRequest' => '\\GenerateFPLStringRequest',
      'FPLStringRequest' => '\\FPLStringRequest',
      'FPLStringResponse' => '\\FPLStringResponse',
      'AFTNSubscriptionUpdateRequest' => '\\AFTNSubscriptionUpdateRequest',
      'AFTNSubscriptionUpdateResponse' => '\\AFTNSubscriptionUpdateResponse',
      'FMSUploadRequest' => '\\FMSUploadRequest',
      'FMSUploadResponse' => '\\FMSUploadResponse',
      'FMSUpdateRequest' => '\\FMSUpdateRequest',
      'FMSUpdateResponse' => '\\FMSUpdateResponse',
      'ArrayOfAltitudeChangeInformation' => '\\ArrayOfAltitudeChangeInformation',
      'AltitudeChangeInformation' => '\\AltitudeChangeInformation',
      'ArrayOfstring' => '\\ArrayOfstring',
      'FlightLevelOptimizationData' => '\\FlightLevelOptimizationData',
      'ArrayOfFlightLevelOptimizationDataPoint' => '\\ArrayOfFlightLevelOptimizationDataPoint',
      'FlightLevelOptimizationDataPoint' => '\\FlightLevelOptimizationDataPoint',
      'FuelBurnApproximation' => '\\FuelBurnApproximation',
      'TypeReferencedMassAndBalanceConfiguration' => '\\TypeReferencedMassAndBalanceConfiguration',
      'FuelCheckOptionsSpecification' => '\\FuelCheckOptionsSpecification',
      'OutputUnitSpecification' => '\\OutputUnitSpecification',
      'ValidationFault' => '\\ValidationFault',
      'ArrayOfValidationDetail' => '\\ArrayOfValidationDetail',
      'ValidationDetail' => '\\ValidationDetail',
      'UnidentifiableAirportFault' => '\\UnidentifiableAirportFault',
      'UnidentifiableAircraftFault' => '\\UnidentifiableAircraftFault',
      'UnparseableRouteFault' => '\\UnparseableRouteFault',
      'LiveFilingDisabledFault' => '\\LiveFilingDisabledFault',
      'UnsupportedFilingAircraftFault' => '\\UnsupportedFilingAircraftFault',
      'FlightPlanFilingFault' => '\\FlightPlanFilingFault',
      'InvalidRouteSpecificationFault' => '\\InvalidRouteSpecificationFault',
      'FlightPlanNotFoundFault' => '\\FlightPlanNotFoundFault',
      'FlightPlanStateFault' => '\\FlightPlanStateFault',
      'FlightDelayFault' => '\\FlightDelayFault',
      'InvalidDelayInputFault' => '\\InvalidDelayInputFault',
      'FlightPlanCollisionFault' => '\\FlightPlanCollisionFault',
      'AFTNSubscriptionDuplicateFault' => '\\AFTNSubscriptionDuplicateFault',
      'AFTNSubscriptionNotFoundFault' => '\\AFTNSubscriptionNotFoundFault',
      'InvalidUpdateOperationFault' => '\\InvalidUpdateOperationFault',
      'CancelRefileFault' => '\\CancelRefileFault',
      'UnchangedFlightPlanFault' => '\\UnchangedFlightPlanFault',
      'FPLStringGenerationFault' => '\\FPLStringGenerationFault',
      'FlightCalculationFault' => '\\FlightCalculationFault',
      'InvalidFlightSpecificationFault' => '\\InvalidFlightSpecificationFault',
      'AircraftGenericDataDeviationFault' => '\\AircraftGenericDataDeviationFault',
      'AircraftMaxWeightFault' => '\\AircraftMaxWeightFault',
      'FMSFlightNotFoundFault' => '\\FMSFlightNotFoundFault',
      'ArrayOfAircraftGenericDataMaxDeviatioNExceedError' => '\\ArrayOfAircraftGenericDataMaxDeviatioNExceedError',
      'AircraftGenericDataMaxDeviatioNExceedError' => '\\AircraftGenericDataMaxDeviatioNExceedError',
      'ArrayOfAircraftMaxWeightExceededError' => '\\ArrayOfAircraftMaxWeightExceededError',
      'AircraftMaxWeightExceededError' => '\\AircraftMaxWeightExceededError',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'https://testservices.aviationcloud.aero/FlightPlanFilingService.svc?singleWsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     *
     * File a flight plan either be using a flightlog object from the flightplan generation web service or by a self-created FPL message
     *
     * Contains either a flightlog or a self-constructed ATCFPL message to be filed through the Aviation Cloud services
     * The response contains the ID of the uploaded flight plan, which can be used for updating the flight plan.
     *
     * @param FileFlightPlan $parameters
     * @return FileFlightPlanResponse
     */
    public function FileFlightPlan(FileFlightPlan $parameters)
    {
      return $this->__soapCall('FileFlightPlan', array($parameters));
    }

    /**
     *
     * Retrieve a list of all filed flights
     *
     * Specifies the account to retrieve flights for
     *
     *
     * @param RetrieveFiledFlights $parameters
     * @return RetrieveFiledFlightsResponse
     */
    public function RetrieveFiledFlights(RetrieveFiledFlights $parameters)
    {
      return $this->__soapCall('RetrieveFiledFlights', array($parameters));
    }

    /**
     *
     * Retrieve all messages (ACK, REJ etc.) messages for a flight
     *
     * Specifies the account and flight to retrieve messages for
     *
     *
     * @param RetrieveMessages $parameters
     * @return RetrieveMessagesResponse
     */
    public function RetrieveMessages(RetrieveMessages $parameters)
    {
      return $this->__soapCall('RetrieveMessages', array($parameters));
    }

    /**
     *
     * Sends a cancel message for a flight
     *
     * Contains a flight identifier which identifies the flight to cancel
     *
     *
     * @param CancelFlight $parameters
     * @return CancelFlightResponse
     */
    public function CancelFlight(CancelFlight $parameters)
    {
      return $this->__soapCall('CancelFlight', array($parameters));
    }

    /**
     *
     * Sends a cancel message for a registered flight plan, having the specified flight identifier.
     *
     * The flight cancellation request, encapsulating the flight plan identifier.
     *
     * @param CancelFlightPlan $parameters
     * @return CancelFlightPlanResponse
     */
    public function CancelFlightPlan(CancelFlightPlan $parameters)
    {
      return $this->__soapCall('CancelFlightPlan', array($parameters));
    }

    /**
     *
     * Sends a delay message with the updated time of departure for a registered flight plan, having the specified flight identifier.
     *
     * The request to delay a flight plan, specifying the flight identifier and the updated time of departure.
     *
     * @param DelayFlightPlan $parameters
     * @return DelayFlightPlanResponse
     */
    public function DelayFlightPlan(DelayFlightPlan $parameters)
    {
      return $this->__soapCall('DelayFlightPlan', array($parameters));
    }

    /**
     *
     * Resolves AFTN addresses for a flight plan. This service should be invoked if the flight was not calculated with the FlightPlanGeneratin service, in that case, the flightlog address will contain AFTn addresses.
     *
     * A request containing an FPL message for which to resolve AFTN addresses
     * A list of AFTN addresses that the flight plan should be filed to
     *
     * @param ResolveAFTNAdresses $parameters
     * @return ResolveAFTNAdressesResponse
     */
    public function ResolveAFTNAdresses(ResolveAFTNAdresses $parameters)
    {
      return $this->__soapCall('ResolveAFTNAdresses', array($parameters));
    }

    /**
     *
     * Subscribes to AFTN messages, delivering these asynchronously to the specified address as AFTN messages are generated, using the desired communication method.
     *
     * A request containing the configuration for subscription (delivery method, data format, destination address etc.)
     * An identifier for the newly created subscription.
     *
     * @param SubscribeAFTNMessages $parameters
     * @return SubscribeAFTNMessagesResponse
     */
    public function SubscribeAFTNMessages(SubscribeAFTNMessages $parameters)
    {
      return $this->__soapCall('SubscribeAFTNMessages', array($parameters));
    }

    /**
     *
     * Removes an existing AFTN message subscription.
     *
     * A request containing the ID of the AFTN message subscription to unregister.
     *
     * @param UnsubscribeAFTNMessages $parameters
     * @return UnsubscribeAFTNMessagesResponse
     */
    public function UnsubscribeAFTNMessages(UnsubscribeAFTNMessages $parameters)
    {
      return $this->__soapCall('UnsubscribeAFTNMessages', array($parameters));
    }

    /**
     *
     * Files a flight plan, delivering the resulting AFTN messages asynchronously if subscriptions exist for this account.
     *
     * Contains either a flightlog or a self-constructed ATCFPL message to be filed through the Aviation Cloud services
     * The response contains the ID of the uploaded flight plan, which can be used for updating the flight plan.
     *
     * @param FileFlightPlanAsynchronously $parameters
     * @return FileFlightPlanAsynchronouslyResponse
     */
    public function FileFlightPlanAsynchronously(FileFlightPlanAsynchronously $parameters)
    {
      return $this->__soapCall('FileFlightPlanAsynchronously', array($parameters));
    }

    /**
     *
     * Retrieves a list of existing AFTN message subscriptions for this account. If an AFTN message subscription is associated with
     * HTTP basic authentication, an empty 'BasicHttpAuthCredentials' will be set.
     *
     * A default request encapsulating the account for which AFTN subscriptions will be retrieved.
     * A list of existing AFTN message subscriptions for this account.
     *
     * @param GetExistingAFTNSubscriptions $parameters
     * @return GetExistingAFTNSubscriptionsResponse
     */
    public function GetExistingAFTNSubscriptions(GetExistingAFTNSubscriptions $parameters)
    {
      return $this->__soapCall('GetExistingAFTNSubscriptions', array($parameters));
    }

    /**
     *
     * Updates an existing flight plan that has been filed via the FileFlightPlan service.
     * The necessary AFTN actions will be performed depending on the updated parameters.
     *
     * The request containing an identifier referencing the flight plan to be updated in addition to the updated ATCFPLMessage object.
     * An encapsulation of the updated flight plan identifier and the AFTN action applied.
     *
     * @param UpdateFlightPlan $parameters
     * @return UpdateFlightPlanResponse
     */
    public function UpdateFlightPlan(UpdateFlightPlan $parameters)
    {
      return $this->__soapCall('UpdateFlightPlan', array($parameters));
    }

    /**
     *
     * DEPRECATED
     *
     *
     *
     *
     * @param GenerateFPLString $parameters
     * @return GenerateFPLStringResponse
     */
    public function GenerateFPLString(GenerateFPLString $parameters)
    {
      return $this->__soapCall('GenerateFPLString', array($parameters));
    }

    /**
     *
     * Gets an ICAO string representation of the given flight plan.
     *
     * The request containing the flight plan from which the ICAO FPL string should be generated.
     * Response containing the generated ICAO FPL string.
     *
     * @param GetFPLString $parameters
     * @return GetFPLStringResponse
     */
    public function GetFPLString(GetFPLString $parameters)
    {
      return $this->__soapCall('GetFPLString', array($parameters));
    }

    /**
     *
     * Updates an existing AFTN message subscription.
     *
     * The request containing the updated AFTN message subscription.
     * Response indicating if the update was successful.
     *
     * @param UpdateAFTNSubscription $parameters
     * @return UpdateAFTNSubscriptionResponse
     */
    public function UpdateAFTNSubscription(UpdateAFTNSubscription $parameters)
    {
      return $this->__soapCall('UpdateAFTNSubscription', array($parameters));
    }

    /**
     *
     * Uploads a flight plan to an FMS data link provider.
     *
     * The request containing the flight plan to be uploaded.
     * Response including the recall identifier for the uploaded flight plan.
     *
     * @param FMSUploadFlightPlan $parameters
     * @return FMSUploadFlightPlanResponse
     */
    public function FMSUploadFlightPlan(FMSUploadFlightPlan $parameters)
    {
      return $this->__soapCall('FMSUploadFlightPlan', array($parameters));
    }

    /**
     *
     * Updates a flight plan that has previously been uploaded to the FMS data link provider.
     *
     * The request containing the new flight plan and the recall identifier for the existing flight plan.
     * Response containing the status of the update request.
     *
     * @param FMSUpdateFlightPlan $parameters
     * @return FMSUpdateFlightPlanResponse
     */
    public function FMSUpdateFlightPlan(FMSUpdateFlightPlan $parameters)
    {
      return $this->__soapCall('FMSUpdateFlightPlan', array($parameters));
    }

}
