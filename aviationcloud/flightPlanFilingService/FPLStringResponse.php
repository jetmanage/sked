<?php

class FPLStringResponse
{

    /**
     * @var string $FPLString
     */
    protected $FPLString = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getFPLString()
    {
      return $this->FPLString;
    }

    /**
     * @param string $FPLString
     * @return FPLStringResponse
     */
    public function setFPLString($FPLString)
    {
      $this->FPLString = $FPLString;
      return $this;
    }

}
