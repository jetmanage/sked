<?php

class PaxSectionLoad
{

    /**
     * @var PaxLoad $PaxLoad
     */
    protected $PaxLoad = null;

    /**
     * @var string $SectionIdentifier
     */
    protected $SectionIdentifier = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return PaxLoad
     */
    public function getPaxLoad()
    {
      return $this->PaxLoad;
    }

    /**
     * @param PaxLoad $PaxLoad
     * @return PaxSectionLoad
     */
    public function setPaxLoad($PaxLoad)
    {
      $this->PaxLoad = $PaxLoad;
      return $this;
    }

    /**
     * @return string
     */
    public function getSectionIdentifier()
    {
      return $this->SectionIdentifier;
    }

    /**
     * @param string $SectionIdentifier
     * @return PaxSectionLoad
     */
    public function setSectionIdentifier($SectionIdentifier)
    {
      $this->SectionIdentifier = $SectionIdentifier;
      return $this;
    }

}
