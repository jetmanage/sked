<?php

class Runway
{

    /**
     * @var float $Bearing
     */
    protected $Bearing = null;

    /**
     * @var string $Description
     */
    protected $Description = null;

    /**
     * @var string $Identifier
     */
    protected $Identifier = null;

    /**
     * @var Length $Length
     */
    protected $Length = null;

    /**
     * @var SphereicPoint $ReferencePosition
     */
    protected $ReferencePosition = null;

    /**
     * @var Length $Width
     */
    protected $Width = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getBearing()
    {
      return $this->Bearing;
    }

    /**
     * @param float $Bearing
     * @return Runway
     */
    public function setBearing($Bearing)
    {
      $this->Bearing = $Bearing;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Runway
     */
    public function setDescription($Description)
    {
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
      return $this->Identifier;
    }

    /**
     * @param string $Identifier
     * @return Runway
     */
    public function setIdentifier($Identifier)
    {
      $this->Identifier = $Identifier;
      return $this;
    }

    /**
     * @return Length
     */
    public function getLength()
    {
      return $this->Length;
    }

    /**
     * @param Length $Length
     * @return Runway
     */
    public function setLength($Length)
    {
      $this->Length = $Length;
      return $this;
    }

    /**
     * @return SphereicPoint
     */
    public function getReferencePosition()
    {
      return $this->ReferencePosition;
    }

    /**
     * @param SphereicPoint $ReferencePosition
     * @return Runway
     */
    public function setReferencePosition($ReferencePosition)
    {
      $this->ReferencePosition = $ReferencePosition;
      return $this;
    }

    /**
     * @return Length
     */
    public function getWidth()
    {
      return $this->Width;
    }

    /**
     * @param Length $Width
     * @return Runway
     */
    public function setWidth($Width)
    {
      $this->Width = $Width;
      return $this;
    }

}
