<?php

class SimpleCrewSpecification extends CrewLoadSpecification
{

    /**
     * @var string $NameOfCaptain
     */
    protected $NameOfCaptain = null;

    /**
     * @var Weight $TotalCrewWeight
     */
    protected $TotalCrewWeight = null;

    /**
     * @var int $TotalNumberOfCrew
     */
    protected $TotalNumberOfCrew = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getNameOfCaptain()
    {
      return $this->NameOfCaptain;
    }

    /**
     * @param string $NameOfCaptain
     * @return SimpleCrewSpecification
     */
    public function setNameOfCaptain($NameOfCaptain)
    {
      $this->NameOfCaptain = $NameOfCaptain;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTotalCrewWeight()
    {
      return $this->TotalCrewWeight;
    }

    /**
     * @param Weight $TotalCrewWeight
     * @return SimpleCrewSpecification
     */
    public function setTotalCrewWeight($TotalCrewWeight)
    {
      $this->TotalCrewWeight = $TotalCrewWeight;
      return $this;
    }

    /**
     * @return int
     */
    public function getTotalNumberOfCrew()
    {
      return $this->TotalNumberOfCrew;
    }

    /**
     * @param int $TotalNumberOfCrew
     * @return SimpleCrewSpecification
     */
    public function setTotalNumberOfCrew($TotalNumberOfCrew)
    {
      $this->TotalNumberOfCrew = $TotalNumberOfCrew;
      return $this;
    }

}
