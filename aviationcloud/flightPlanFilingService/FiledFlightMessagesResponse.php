<?php

class FiledFlightMessagesResponse
{

    /**
     * @var ArrayOfATCMessageBase $Messages
     */
    protected $Messages = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfATCMessageBase
     */
    public function getMessages()
    {
      return $this->Messages;
    }

    /**
     * @param ArrayOfATCMessageBase $Messages
     * @return FiledFlightMessagesResponse
     */
    public function setMessages($Messages)
    {
      $this->Messages = $Messages;
      return $this;
    }

}
