<?php

class Weight
{

    /**
     * @var WeightUnit $Unit
     */
    protected $Unit = null;

    /**
     * @var float $Value
     */
    protected $Value = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return WeightUnit
     */
    public function getUnit()
    {
      return $this->Unit;
    }

    /**
     * @param WeightUnit $Unit
     * @return Weight
     */
    public function setUnit($Unit)
    {
      $this->Unit = $Unit;
      return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param float $Value
     * @return Weight
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

}
