<?php

class HoldingDataPoint extends PerformanceDataPoint
{

    /**
     * @var HoldingData $HoldingPerformance
     */
    protected $HoldingPerformance = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return HoldingData
     */
    public function getHoldingPerformance()
    {
      return $this->HoldingPerformance;
    }

    /**
     * @param HoldingData $HoldingPerformance
     * @return HoldingDataPoint
     */
    public function setHoldingPerformance($HoldingPerformance)
    {
      $this->HoldingPerformance = $HoldingPerformance;
      return $this;
    }

}
