<?php

class FlightPlanStateFault
{

    /**
     * @var string $Action
     */
    protected $Action = null;

    /**
     * @var string $ContactInfo
     */
    protected $ContactInfo = null;

    /**
     * @var string $CurrentState
     */
    protected $CurrentState = null;

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    /**
     * @var int $FlightPlanId
     */
    protected $FlightPlanId = null;

    /**
     * @var boolean $IsProduction
     */
    protected $IsProduction = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param string $Action
     * @return FlightPlanStateFault
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getContactInfo()
    {
      return $this->ContactInfo;
    }

    /**
     * @param string $ContactInfo
     * @return FlightPlanStateFault
     */
    public function setContactInfo($ContactInfo)
    {
      $this->ContactInfo = $ContactInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrentState()
    {
      return $this->CurrentState;
    }

    /**
     * @param string $CurrentState
     * @return FlightPlanStateFault
     */
    public function setCurrentState($CurrentState)
    {
      $this->CurrentState = $CurrentState;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return FlightPlanStateFault
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return FlightPlanStateFault
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightPlanId()
    {
      return $this->FlightPlanId;
    }

    /**
     * @param int $FlightPlanId
     * @return FlightPlanStateFault
     */
    public function setFlightPlanId($FlightPlanId)
    {
      $this->FlightPlanId = $FlightPlanId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsProduction()
    {
      return $this->IsProduction;
    }

    /**
     * @param boolean $IsProduction
     * @return FlightPlanStateFault
     */
    public function setIsProduction($IsProduction)
    {
      $this->IsProduction = $IsProduction;
      return $this;
    }

}
