<?php

class CruisePerformanceDataSeries extends PerformanceDataSeries
{

    /**
     * @var ArrayOfCruisePerformanceDataPoint $PerformanceDataPoints
     */
    protected $PerformanceDataPoints = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ArrayOfCruisePerformanceDataPoint
     */
    public function getPerformanceDataPoints()
    {
      return $this->PerformanceDataPoints;
    }

    /**
     * @param ArrayOfCruisePerformanceDataPoint $PerformanceDataPoints
     * @return CruisePerformanceDataSeries
     */
    public function setPerformanceDataPoints($PerformanceDataPoints)
    {
      $this->PerformanceDataPoints = $PerformanceDataPoints;
      return $this;
    }

}
