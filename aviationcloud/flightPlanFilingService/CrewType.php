<?php

class CrewType
{
    const __default = 'Pilot';
    const Pilot = 'Pilot';
    const FirstOfficer = 'FirstOfficer';
    const CabbinAttendant = 'CabbinAttendant';
    const AdditionalCrewMember = 'AdditionalCrewMember';


}
