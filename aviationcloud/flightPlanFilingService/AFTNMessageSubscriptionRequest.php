<?php

class AFTNMessageSubscriptionRequest extends RequestBase
{

    /**
     * @var AFTNMessageSubscription $MessageSubscription
     */
    protected $MessageSubscription = null;

    /**
     * @var boolean $UpdateDeliveryAddress
     */
    protected $UpdateDeliveryAddress = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return AFTNMessageSubscription
     */
    public function getMessageSubscription()
    {
      return $this->MessageSubscription;
    }

    /**
     * @param AFTNMessageSubscription $MessageSubscription
     * @return AFTNMessageSubscriptionRequest
     */
    public function setMessageSubscription($MessageSubscription)
    {
      $this->MessageSubscription = $MessageSubscription;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUpdateDeliveryAddress()
    {
      return $this->UpdateDeliveryAddress;
    }

    /**
     * @param boolean $UpdateDeliveryAddress
     * @return AFTNMessageSubscriptionRequest
     */
    public function setUpdateDeliveryAddress($UpdateDeliveryAddress)
    {
      $this->UpdateDeliveryAddress = $UpdateDeliveryAddress;
      return $this;
    }

}
