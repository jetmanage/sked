<?php

class ArrayOfTurbulencePoint implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var TurbulencePoint[] $TurbulencePoint
     */
    protected $TurbulencePoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return TurbulencePoint[]
     */
    public function getTurbulencePoint()
    {
      return $this->TurbulencePoint;
    }

    /**
     * @param TurbulencePoint[] $TurbulencePoint
     * @return ArrayOfTurbulencePoint
     */
    public function setTurbulencePoint(array $TurbulencePoint = null)
    {
      $this->TurbulencePoint = $TurbulencePoint;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->TurbulencePoint[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return TurbulencePoint
     */
    public function offsetGet($offset)
    {
      return $this->TurbulencePoint[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param TurbulencePoint $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->TurbulencePoint[] = $value;
      } else {
        $this->TurbulencePoint[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->TurbulencePoint[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return TurbulencePoint Return the current element
     */
    public function current()
    {
      return current($this->TurbulencePoint);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->TurbulencePoint);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->TurbulencePoint);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->TurbulencePoint);
    }

    /**
     * Countable implementation
     *
     * @return TurbulencePoint Return count of elements
     */
    public function count()
    {
      return count($this->TurbulencePoint);
    }

}
