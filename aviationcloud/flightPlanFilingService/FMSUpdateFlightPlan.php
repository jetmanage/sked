<?php

class FMSUpdateFlightPlan
{

    /**
     * @var FMSUpdateRequest $request
     */
    protected $request = null;

    /**
     * @param FMSUpdateRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return FMSUpdateRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param FMSUpdateRequest $request
     * @return FMSUpdateFlightPlan
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
