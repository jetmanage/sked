<?php

class ETOPSCruiseProfile extends DiscreteCruiseProfile
{

    /**
     * @var int $NumberOfEngines
     */
    protected $NumberOfEngines = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getNumberOfEngines()
    {
      return $this->NumberOfEngines;
    }

    /**
     * @param int $NumberOfEngines
     * @return ETOPSCruiseProfile
     */
    public function setNumberOfEngines($NumberOfEngines)
    {
      $this->NumberOfEngines = $NumberOfEngines;
      return $this;
    }

}
