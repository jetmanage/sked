<?php

class FileFlightPlanResponse
{

    /**
     * @var FlightplanFilingResponse $FileFlightPlanResult
     */
    protected $FileFlightPlanResult = null;

    /**
     * @param FlightplanFilingResponse $FileFlightPlanResult
     */
    public function __construct($FileFlightPlanResult)
    {
      $this->FileFlightPlanResult = $FileFlightPlanResult;
    }

    /**
     * @return FlightplanFilingResponse
     */
    public function getFileFlightPlanResult()
    {
      return $this->FileFlightPlanResult;
    }

    /**
     * @param FlightplanFilingResponse $FileFlightPlanResult
     * @return FileFlightPlanResponse
     */
    public function setFileFlightPlanResult($FileFlightPlanResult)
    {
      $this->FileFlightPlanResult = $FileFlightPlanResult;
      return $this;
    }

}
