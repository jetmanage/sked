<?php

class CancelFlightPlan
{

    /**
     * @var CancelFlightPlanRequest $request
     */
    protected $request = null;

    /**
     * @param CancelFlightPlanRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return CancelFlightPlanRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param CancelFlightPlanRequest $request
     * @return CancelFlightPlan
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
