<?php

class ManuelTaxiTime extends TaxiTimeSpecification
{

    /**
     * @var Time $TaxiInTime
     */
    protected $TaxiInTime = null;

    /**
     * @var Time $TaxiOutTime
     */
    protected $TaxiOutTime = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Time
     */
    public function getTaxiInTime()
    {
      return $this->TaxiInTime;
    }

    /**
     * @param Time $TaxiInTime
     * @return ManuelTaxiTime
     */
    public function setTaxiInTime($TaxiInTime)
    {
      $this->TaxiInTime = $TaxiInTime;
      return $this;
    }

    /**
     * @return Time
     */
    public function getTaxiOutTime()
    {
      return $this->TaxiOutTime;
    }

    /**
     * @param Time $TaxiOutTime
     * @return ManuelTaxiTime
     */
    public function setTaxiOutTime($TaxiOutTime)
    {
      $this->TaxiOutTime = $TaxiOutTime;
      return $this;
    }

}
