<?php

class ATCChangeMessage extends ATCFlightMessage
{

    /**
     * @var AircraftEquipment $AircraftEquipment
     */
    protected $AircraftEquipment = null;

    /**
     * @var string $AircraftType
     */
    protected $AircraftType = null;

    /**
     * @var Airport $Alternate1Airport
     */
    protected $Alternate1Airport = null;

    /**
     * @var Airport $Alternate2Airport
     */
    protected $Alternate2Airport = null;

    /**
     * @var RadioAndNavigationEquipment $Equiipment
     */
    protected $Equiipment = null;

    /**
     * @var duration $EstimatedEnrouteTime
     */
    protected $EstimatedEnrouteTime = null;

    /**
     * @var ATCFlightRule $FlightRule
     */
    protected $FlightRule = null;

    /**
     * @var int $NumberOfAircrafts
     */
    protected $NumberOfAircrafts = null;

    /**
     * @var string $OtherInformation
     */
    protected $OtherInformation = null;

    /**
     * @var Route $Route
     */
    protected $Route = null;

    /**
     * @var ATCFlightType $TypeOfFlight
     */
    protected $TypeOfFlight = null;

    /**
     * @var ATCWakeTurbulanceCategory $WakeTurbulanceCategory
     */
    protected $WakeTurbulanceCategory = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return AircraftEquipment
     */
    public function getAircraftEquipment()
    {
      return $this->AircraftEquipment;
    }

    /**
     * @param AircraftEquipment $AircraftEquipment
     * @return ATCChangeMessage
     */
    public function setAircraftEquipment($AircraftEquipment)
    {
      $this->AircraftEquipment = $AircraftEquipment;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftType()
    {
      return $this->AircraftType;
    }

    /**
     * @param string $AircraftType
     * @return ATCChangeMessage
     */
    public function setAircraftType($AircraftType)
    {
      $this->AircraftType = $AircraftType;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getAlternate1Airport()
    {
      return $this->Alternate1Airport;
    }

    /**
     * @param Airport $Alternate1Airport
     * @return ATCChangeMessage
     */
    public function setAlternate1Airport($Alternate1Airport)
    {
      $this->Alternate1Airport = $Alternate1Airport;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getAlternate2Airport()
    {
      return $this->Alternate2Airport;
    }

    /**
     * @param Airport $Alternate2Airport
     * @return ATCChangeMessage
     */
    public function setAlternate2Airport($Alternate2Airport)
    {
      $this->Alternate2Airport = $Alternate2Airport;
      return $this;
    }

    /**
     * @return RadioAndNavigationEquipment
     */
    public function getEquiipment()
    {
      return $this->Equiipment;
    }

    /**
     * @param RadioAndNavigationEquipment $Equiipment
     * @return ATCChangeMessage
     */
    public function setEquiipment($Equiipment)
    {
      $this->Equiipment = $Equiipment;
      return $this;
    }

    /**
     * @return duration
     */
    public function getEstimatedEnrouteTime()
    {
      return $this->EstimatedEnrouteTime;
    }

    /**
     * @param duration $EstimatedEnrouteTime
     * @return ATCChangeMessage
     */
    public function setEstimatedEnrouteTime($EstimatedEnrouteTime)
    {
      $this->EstimatedEnrouteTime = $EstimatedEnrouteTime;
      return $this;
    }

    /**
     * @return ATCFlightRule
     */
    public function getFlightRule()
    {
      return $this->FlightRule;
    }

    /**
     * @param ATCFlightRule $FlightRule
     * @return ATCChangeMessage
     */
    public function setFlightRule($FlightRule)
    {
      $this->FlightRule = $FlightRule;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfAircrafts()
    {
      return $this->NumberOfAircrafts;
    }

    /**
     * @param int $NumberOfAircrafts
     * @return ATCChangeMessage
     */
    public function setNumberOfAircrafts($NumberOfAircrafts)
    {
      $this->NumberOfAircrafts = $NumberOfAircrafts;
      return $this;
    }

    /**
     * @return string
     */
    public function getOtherInformation()
    {
      return $this->OtherInformation;
    }

    /**
     * @param string $OtherInformation
     * @return ATCChangeMessage
     */
    public function setOtherInformation($OtherInformation)
    {
      $this->OtherInformation = $OtherInformation;
      return $this;
    }

    /**
     * @return Route
     */
    public function getRoute()
    {
      return $this->Route;
    }

    /**
     * @param Route $Route
     * @return ATCChangeMessage
     */
    public function setRoute($Route)
    {
      $this->Route = $Route;
      return $this;
    }

    /**
     * @return ATCFlightType
     */
    public function getTypeOfFlight()
    {
      return $this->TypeOfFlight;
    }

    /**
     * @param ATCFlightType $TypeOfFlight
     * @return ATCChangeMessage
     */
    public function setTypeOfFlight($TypeOfFlight)
    {
      $this->TypeOfFlight = $TypeOfFlight;
      return $this;
    }

    /**
     * @return ATCWakeTurbulanceCategory
     */
    public function getWakeTurbulanceCategory()
    {
      return $this->WakeTurbulanceCategory;
    }

    /**
     * @param ATCWakeTurbulanceCategory $WakeTurbulanceCategory
     * @return ATCChangeMessage
     */
    public function setWakeTurbulanceCategory($WakeTurbulanceCategory)
    {
      $this->WakeTurbulanceCategory = $WakeTurbulanceCategory;
      return $this;
    }

}
