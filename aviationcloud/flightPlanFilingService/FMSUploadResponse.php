<?php

class FMSUploadResponse
{

    /**
     * @var string $DatalinkServiceProvider
     */
    protected $DatalinkServiceProvider = null;

    /**
     * @var string $DepartureIdent
     */
    protected $DepartureIdent = null;

    /**
     * @var string $DestinationIdent
     */
    protected $DestinationIdent = null;

    /**
     * @var boolean $IsSuccessful
     */
    protected $IsSuccessful = null;

    /**
     * @var string $RecallIdent
     */
    protected $RecallIdent = null;

    /**
     * @var FlightLog $UploadedFlightLog
     */
    protected $UploadedFlightLog = null;

    /**
     * @var string $UploadedTailnumber
     */
    protected $UploadedTailnumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDatalinkServiceProvider()
    {
      return $this->DatalinkServiceProvider;
    }

    /**
     * @param string $DatalinkServiceProvider
     * @return FMSUploadResponse
     */
    public function setDatalinkServiceProvider($DatalinkServiceProvider)
    {
      $this->DatalinkServiceProvider = $DatalinkServiceProvider;
      return $this;
    }

    /**
     * @return string
     */
    public function getDepartureIdent()
    {
      return $this->DepartureIdent;
    }

    /**
     * @param string $DepartureIdent
     * @return FMSUploadResponse
     */
    public function setDepartureIdent($DepartureIdent)
    {
      $this->DepartureIdent = $DepartureIdent;
      return $this;
    }

    /**
     * @return string
     */
    public function getDestinationIdent()
    {
      return $this->DestinationIdent;
    }

    /**
     * @param string $DestinationIdent
     * @return FMSUploadResponse
     */
    public function setDestinationIdent($DestinationIdent)
    {
      $this->DestinationIdent = $DestinationIdent;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSuccessful()
    {
      return $this->IsSuccessful;
    }

    /**
     * @param boolean $IsSuccessful
     * @return FMSUploadResponse
     */
    public function setIsSuccessful($IsSuccessful)
    {
      $this->IsSuccessful = $IsSuccessful;
      return $this;
    }

    /**
     * @return string
     */
    public function getRecallIdent()
    {
      return $this->RecallIdent;
    }

    /**
     * @param string $RecallIdent
     * @return FMSUploadResponse
     */
    public function setRecallIdent($RecallIdent)
    {
      $this->RecallIdent = $RecallIdent;
      return $this;
    }

    /**
     * @return FlightLog
     */
    public function getUploadedFlightLog()
    {
      return $this->UploadedFlightLog;
    }

    /**
     * @param FlightLog $UploadedFlightLog
     * @return FMSUploadResponse
     */
    public function setUploadedFlightLog($UploadedFlightLog)
    {
      $this->UploadedFlightLog = $UploadedFlightLog;
      return $this;
    }

    /**
     * @return string
     */
    public function getUploadedTailnumber()
    {
      return $this->UploadedTailnumber;
    }

    /**
     * @param string $UploadedTailnumber
     * @return FMSUploadResponse
     */
    public function setUploadedTailnumber($UploadedTailnumber)
    {
      $this->UploadedTailnumber = $UploadedTailnumber;
      return $this;
    }

}
