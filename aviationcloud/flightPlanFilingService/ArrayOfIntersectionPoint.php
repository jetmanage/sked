<?php

class ArrayOfIntersectionPoint implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var IntersectionPoint[] $IntersectionPoint
     */
    protected $IntersectionPoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return IntersectionPoint[]
     */
    public function getIntersectionPoint()
    {
      return $this->IntersectionPoint;
    }

    /**
     * @param IntersectionPoint[] $IntersectionPoint
     * @return ArrayOfIntersectionPoint
     */
    public function setIntersectionPoint(array $IntersectionPoint = null)
    {
      $this->IntersectionPoint = $IntersectionPoint;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->IntersectionPoint[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return IntersectionPoint
     */
    public function offsetGet($offset)
    {
      return $this->IntersectionPoint[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param IntersectionPoint $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->IntersectionPoint[] = $value;
      } else {
        $this->IntersectionPoint[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->IntersectionPoint[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return IntersectionPoint Return the current element
     */
    public function current()
    {
      return current($this->IntersectionPoint);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->IntersectionPoint);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->IntersectionPoint);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->IntersectionPoint);
    }

    /**
     * Countable implementation
     *
     * @return IntersectionPoint Return count of elements
     */
    public function count()
    {
      return count($this->IntersectionPoint);
    }

}
