<?php

class SubscribeAFTNMessagesResponse
{

    /**
     * @var int $SubscribeAFTNMessagesResult
     */
    protected $SubscribeAFTNMessagesResult = null;

    /**
     * @param int $SubscribeAFTNMessagesResult
     */
    public function __construct($SubscribeAFTNMessagesResult)
    {
      $this->SubscribeAFTNMessagesResult = $SubscribeAFTNMessagesResult;
    }

    /**
     * @return int
     */
    public function getSubscribeAFTNMessagesResult()
    {
      return $this->SubscribeAFTNMessagesResult;
    }

    /**
     * @param int $SubscribeAFTNMessagesResult
     * @return SubscribeAFTNMessagesResponse
     */
    public function setSubscribeAFTNMessagesResult($SubscribeAFTNMessagesResult)
    {
      $this->SubscribeAFTNMessagesResult = $SubscribeAFTNMessagesResult;
      return $this;
    }

}
