<?php

class AFTNUpdateAction
{
    const __default = 'CancelRefile';
    const CancelRefile = 'CancelRefile';
    const CancelRefileFailed = 'CancelRefileFailed';
    const Change = 'Change';
    const Delay = 'Delay';
    const NewFlightPlanRequired = 'NewFlightPlanRequired';
    const None = 'None';


}
