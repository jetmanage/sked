<?php

class FMSUploadRequest extends RequestBase
{

    /**
     * @var string $DatalinkServiceProvider
     */
    protected $DatalinkServiceProvider = null;

    /**
     * @var boolean $EnableProductionSystem
     */
    protected $EnableProductionSystem = null;

    /**
     * @var FlightSpecification $FlightSpecification
     */
    protected $FlightSpecification = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getDatalinkServiceProvider()
    {
      return $this->DatalinkServiceProvider;
    }

    /**
     * @param string $DatalinkServiceProvider
     * @return FMSUploadRequest
     */
    public function setDatalinkServiceProvider($DatalinkServiceProvider)
    {
      $this->DatalinkServiceProvider = $DatalinkServiceProvider;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableProductionSystem()
    {
      return $this->EnableProductionSystem;
    }

    /**
     * @param boolean $EnableProductionSystem
     * @return FMSUploadRequest
     */
    public function setEnableProductionSystem($EnableProductionSystem)
    {
      $this->EnableProductionSystem = $EnableProductionSystem;
      return $this;
    }

    /**
     * @return FlightSpecification
     */
    public function getFlightSpecification()
    {
      return $this->FlightSpecification;
    }

    /**
     * @param FlightSpecification $FlightSpecification
     * @return FMSUploadRequest
     */
    public function setFlightSpecification($FlightSpecification)
    {
      $this->FlightSpecification = $FlightSpecification;
      return $this;
    }

}
