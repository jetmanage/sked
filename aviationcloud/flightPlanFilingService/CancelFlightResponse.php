<?php

class CancelFlightResponse
{

    /**
     * @var CancelFlightResposne $CancelFlightResult
     */
    protected $CancelFlightResult = null;

    /**
     * @param CancelFlightResposne $CancelFlightResult
     */
    public function __construct($CancelFlightResult)
    {
      $this->CancelFlightResult = $CancelFlightResult;
    }

    /**
     * @return CancelFlightResposne
     */
    public function getCancelFlightResult()
    {
      return $this->CancelFlightResult;
    }

    /**
     * @param CancelFlightResposne $CancelFlightResult
     * @return CancelFlightResponse
     */
    public function setCancelFlightResult($CancelFlightResult)
    {
      $this->CancelFlightResult = $CancelFlightResult;
      return $this;
    }

}
