<?php

class SimpleClimbProfile extends ClimbProfile
{

    /**
     * @var int $ClimbFeetPrMinute
     */
    protected $ClimbFeetPrMinute = null;

    /**
     * @var Weight $ClimbFuelPrHour
     */
    protected $ClimbFuelPrHour = null;

    /**
     * @var Speed $ClimbTrueAirspeed
     */
    protected $ClimbTrueAirspeed = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getClimbFeetPrMinute()
    {
      return $this->ClimbFeetPrMinute;
    }

    /**
     * @param int $ClimbFeetPrMinute
     * @return SimpleClimbProfile
     */
    public function setClimbFeetPrMinute($ClimbFeetPrMinute)
    {
      $this->ClimbFeetPrMinute = $ClimbFeetPrMinute;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getClimbFuelPrHour()
    {
      return $this->ClimbFuelPrHour;
    }

    /**
     * @param Weight $ClimbFuelPrHour
     * @return SimpleClimbProfile
     */
    public function setClimbFuelPrHour($ClimbFuelPrHour)
    {
      $this->ClimbFuelPrHour = $ClimbFuelPrHour;
      return $this;
    }

    /**
     * @return Speed
     */
    public function getClimbTrueAirspeed()
    {
      return $this->ClimbTrueAirspeed;
    }

    /**
     * @param Speed $ClimbTrueAirspeed
     * @return SimpleClimbProfile
     */
    public function setClimbTrueAirspeed($ClimbTrueAirspeed)
    {
      $this->ClimbTrueAirspeed = $ClimbTrueAirspeed;
      return $this;
    }

}
