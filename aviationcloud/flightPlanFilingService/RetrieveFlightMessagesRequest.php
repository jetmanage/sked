<?php

class RetrieveFlightMessagesRequest extends RequestBase
{

    /**
     * @var FlightIdentifier $Flight
     */
    protected $Flight = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return FlightIdentifier
     */
    public function getFlight()
    {
      return $this->Flight;
    }

    /**
     * @param FlightIdentifier $Flight
     * @return RetrieveFlightMessagesRequest
     */
    public function setFlight($Flight)
    {
      $this->Flight = $Flight;
      return $this;
    }

}
