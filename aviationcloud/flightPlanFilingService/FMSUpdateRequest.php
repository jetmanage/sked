<?php

class FMSUpdateRequest extends RequestBase
{

    /**
     * @var string $DatalinkServiceProvider
     */
    protected $DatalinkServiceProvider = null;

    /**
     * @var boolean $EnableProductionSystem
     */
    protected $EnableProductionSystem = null;

    /**
     * @var FlightSpecification $FlightSpecification
     */
    protected $FlightSpecification = null;

    /**
     * @var string $RecallIdent
     */
    protected $RecallIdent = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getDatalinkServiceProvider()
    {
      return $this->DatalinkServiceProvider;
    }

    /**
     * @param string $DatalinkServiceProvider
     * @return FMSUpdateRequest
     */
    public function setDatalinkServiceProvider($DatalinkServiceProvider)
    {
      $this->DatalinkServiceProvider = $DatalinkServiceProvider;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableProductionSystem()
    {
      return $this->EnableProductionSystem;
    }

    /**
     * @param boolean $EnableProductionSystem
     * @return FMSUpdateRequest
     */
    public function setEnableProductionSystem($EnableProductionSystem)
    {
      $this->EnableProductionSystem = $EnableProductionSystem;
      return $this;
    }

    /**
     * @return FlightSpecification
     */
    public function getFlightSpecification()
    {
      return $this->FlightSpecification;
    }

    /**
     * @param FlightSpecification $FlightSpecification
     * @return FMSUpdateRequest
     */
    public function setFlightSpecification($FlightSpecification)
    {
      $this->FlightSpecification = $FlightSpecification;
      return $this;
    }

    /**
     * @return string
     */
    public function getRecallIdent()
    {
      return $this->RecallIdent;
    }

    /**
     * @param string $RecallIdent
     * @return FMSUpdateRequest
     */
    public function setRecallIdent($RecallIdent)
    {
      $this->RecallIdent = $RecallIdent;
      return $this;
    }

}
