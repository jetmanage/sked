<?php

class FlightIdentifier
{

    /**
     * @var ArrayOfstring $ATCAddresses
     */
    protected $ATCAddresses = null;

    /**
     * @var Airport $DepartureAirport
     */
    protected $DepartureAirport = null;

    /**
     * @var Airport $DestinationAirport
     */
    protected $DestinationAirport = null;

    /**
     * @var Time $EstiamtedEnrouteTime
     */
    protected $EstiamtedEnrouteTime = null;

    /**
     * @var int $FlightId
     */
    protected $FlightId = null;

    /**
     * @var string $FlightNumber
     */
    protected $FlightNumber = null;

    /**
     * @var boolean $HasBeenCancelled
     */
    protected $HasBeenCancelled = null;

    /**
     * @var boolean $IsEmulationFiled
     */
    protected $IsEmulationFiled = null;

    /**
     * @var string $Status
     */
    protected $Status = null;

    /**
     * @var \DateTime $TimeOfDeparture
     */
    protected $TimeOfDeparture = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfstring
     */
    public function getATCAddresses()
    {
      return $this->ATCAddresses;
    }

    /**
     * @param ArrayOfstring $ATCAddresses
     * @return FlightIdentifier
     */
    public function setATCAddresses($ATCAddresses)
    {
      $this->ATCAddresses = $ATCAddresses;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDepartureAirport()
    {
      return $this->DepartureAirport;
    }

    /**
     * @param Airport $DepartureAirport
     * @return FlightIdentifier
     */
    public function setDepartureAirport($DepartureAirport)
    {
      $this->DepartureAirport = $DepartureAirport;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDestinationAirport()
    {
      return $this->DestinationAirport;
    }

    /**
     * @param Airport $DestinationAirport
     * @return FlightIdentifier
     */
    public function setDestinationAirport($DestinationAirport)
    {
      $this->DestinationAirport = $DestinationAirport;
      return $this;
    }

    /**
     * @return Time
     */
    public function getEstiamtedEnrouteTime()
    {
      return $this->EstiamtedEnrouteTime;
    }

    /**
     * @param Time $EstiamtedEnrouteTime
     * @return FlightIdentifier
     */
    public function setEstiamtedEnrouteTime($EstiamtedEnrouteTime)
    {
      $this->EstiamtedEnrouteTime = $EstiamtedEnrouteTime;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightId()
    {
      return $this->FlightId;
    }

    /**
     * @param int $FlightId
     * @return FlightIdentifier
     */
    public function setFlightId($FlightId)
    {
      $this->FlightId = $FlightId;
      return $this;
    }

    /**
     * @return string
     */
    public function getFlightNumber()
    {
      return $this->FlightNumber;
    }

    /**
     * @param string $FlightNumber
     * @return FlightIdentifier
     */
    public function setFlightNumber($FlightNumber)
    {
      $this->FlightNumber = $FlightNumber;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHasBeenCancelled()
    {
      return $this->HasBeenCancelled;
    }

    /**
     * @param boolean $HasBeenCancelled
     * @return FlightIdentifier
     */
    public function setHasBeenCancelled($HasBeenCancelled)
    {
      $this->HasBeenCancelled = $HasBeenCancelled;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsEmulationFiled()
    {
      return $this->IsEmulationFiled;
    }

    /**
     * @param boolean $IsEmulationFiled
     * @return FlightIdentifier
     */
    public function setIsEmulationFiled($IsEmulationFiled)
    {
      $this->IsEmulationFiled = $IsEmulationFiled;
      return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
      return $this->Status;
    }

    /**
     * @param string $Status
     * @return FlightIdentifier
     */
    public function setStatus($Status)
    {
      $this->Status = $Status;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimeOfDeparture()
    {
      if ($this->TimeOfDeparture == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->TimeOfDeparture);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $TimeOfDeparture
     * @return FlightIdentifier
     */
    public function setTimeOfDeparture(\DateTime $TimeOfDeparture = null)
    {
      if ($TimeOfDeparture == null) {
       $this->TimeOfDeparture = null;
      } else {
        $this->TimeOfDeparture = $TimeOfDeparture->format(\DateTime::ATOM);
      }
      return $this;
    }

}
