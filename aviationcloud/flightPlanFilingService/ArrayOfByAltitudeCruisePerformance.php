<?php

class ArrayOfByAltitudeCruisePerformance implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ByAltitudeCruisePerformance[] $ByAltitudeCruisePerformance
     */
    protected $ByAltitudeCruisePerformance = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ByAltitudeCruisePerformance[]
     */
    public function getByAltitudeCruisePerformance()
    {
      return $this->ByAltitudeCruisePerformance;
    }

    /**
     * @param ByAltitudeCruisePerformance[] $ByAltitudeCruisePerformance
     * @return ArrayOfByAltitudeCruisePerformance
     */
    public function setByAltitudeCruisePerformance(array $ByAltitudeCruisePerformance = null)
    {
      $this->ByAltitudeCruisePerformance = $ByAltitudeCruisePerformance;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ByAltitudeCruisePerformance[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ByAltitudeCruisePerformance
     */
    public function offsetGet($offset)
    {
      return $this->ByAltitudeCruisePerformance[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ByAltitudeCruisePerformance $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ByAltitudeCruisePerformance[] = $value;
      } else {
        $this->ByAltitudeCruisePerformance[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ByAltitudeCruisePerformance[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ByAltitudeCruisePerformance Return the current element
     */
    public function current()
    {
      return current($this->ByAltitudeCruisePerformance);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ByAltitudeCruisePerformance);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ByAltitudeCruisePerformance);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ByAltitudeCruisePerformance);
    }

    /**
     * Countable implementation
     *
     * @return ByAltitudeCruisePerformance Return count of elements
     */
    public function count()
    {
      return count($this->ByAltitudeCruisePerformance);
    }

}
