<?php

class GetExistingAFTNSubscriptionsResponse
{

    /**
     * @var ArrayOfAFTNMessageSubscription $GetExistingAFTNSubscriptionsResult
     */
    protected $GetExistingAFTNSubscriptionsResult = null;

    /**
     * @param ArrayOfAFTNMessageSubscription $GetExistingAFTNSubscriptionsResult
     */
    public function __construct($GetExistingAFTNSubscriptionsResult)
    {
      $this->GetExistingAFTNSubscriptionsResult = $GetExistingAFTNSubscriptionsResult;
    }

    /**
     * @return ArrayOfAFTNMessageSubscription
     */
    public function getGetExistingAFTNSubscriptionsResult()
    {
      return $this->GetExistingAFTNSubscriptionsResult;
    }

    /**
     * @param ArrayOfAFTNMessageSubscription $GetExistingAFTNSubscriptionsResult
     * @return GetExistingAFTNSubscriptionsResponse
     */
    public function setGetExistingAFTNSubscriptionsResult($GetExistingAFTNSubscriptionsResult)
    {
      $this->GetExistingAFTNSubscriptionsResult = $GetExistingAFTNSubscriptionsResult;
      return $this;
    }

}
