<?php

class SphereicPoint
{

    /**
     * @var Length $Elevation
     */
    protected $Elevation = null;

    /**
     * @var float $Latitude
     */
    protected $Latitude = null;

    /**
     * @var float $Longitude
     */
    protected $Longitude = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Length
     */
    public function getElevation()
    {
      return $this->Elevation;
    }

    /**
     * @param Length $Elevation
     * @return SphereicPoint
     */
    public function setElevation($Elevation)
    {
      $this->Elevation = $Elevation;
      return $this;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
      return $this->Latitude;
    }

    /**
     * @param float $Latitude
     * @return SphereicPoint
     */
    public function setLatitude($Latitude)
    {
      $this->Latitude = $Latitude;
      return $this;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
      return $this->Longitude;
    }

    /**
     * @param float $Longitude
     * @return SphereicPoint
     */
    public function setLongitude($Longitude)
    {
      $this->Longitude = $Longitude;
      return $this;
    }

}
