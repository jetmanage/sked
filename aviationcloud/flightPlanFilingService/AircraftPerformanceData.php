<?php

class AircraftPerformanceData
{

    /**
     * @var ArrayOfAircraftPerformanceProfile $PerformanceProfiles
     */
    protected $PerformanceProfiles = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAircraftPerformanceProfile
     */
    public function getPerformanceProfiles()
    {
      return $this->PerformanceProfiles;
    }

    /**
     * @param ArrayOfAircraftPerformanceProfile $PerformanceProfiles
     * @return AircraftPerformanceData
     */
    public function setPerformanceProfiles($PerformanceProfiles)
    {
      $this->PerformanceProfiles = $PerformanceProfiles;
      return $this;
    }

}
