<?php

class GetFPLStringResponse
{

    /**
     * @var FPLStringResponse $GetFPLStringResult
     */
    protected $GetFPLStringResult = null;

    /**
     * @param FPLStringResponse $GetFPLStringResult
     */
    public function __construct($GetFPLStringResult)
    {
      $this->GetFPLStringResult = $GetFPLStringResult;
    }

    /**
     * @return FPLStringResponse
     */
    public function getGetFPLStringResult()
    {
      return $this->GetFPLStringResult;
    }

    /**
     * @param FPLStringResponse $GetFPLStringResult
     * @return GetFPLStringResponse
     */
    public function setGetFPLStringResult($GetFPLStringResult)
    {
      $this->GetFPLStringResult = $GetFPLStringResult;
      return $this;
    }

}
