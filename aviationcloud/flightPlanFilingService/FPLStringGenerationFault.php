<?php

class FPLStringGenerationFault
{

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    /**
     * @var ATCFPLMessage $FPLMessage
     */
    protected $FPLMessage = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return FPLStringGenerationFault
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return FPLStringGenerationFault
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

    /**
     * @return ATCFPLMessage
     */
    public function getFPLMessage()
    {
      return $this->FPLMessage;
    }

    /**
     * @param ATCFPLMessage $FPLMessage
     * @return FPLStringGenerationFault
     */
    public function setFPLMessage($FPLMessage)
    {
      $this->FPLMessage = $FPLMessage;
      return $this;
    }

}
