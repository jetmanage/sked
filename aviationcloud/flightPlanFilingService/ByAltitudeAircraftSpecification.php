<?php

class ByAltitudeAircraftSpecification extends AircraftSpecification
{

    /**
     * @var string $AircraftICAOId
     */
    protected $AircraftICAOId = null;

    /**
     * @var string $AircraftMake
     */
    protected $AircraftMake = null;

    /**
     * @var string $AircraftModel
     */
    protected $AircraftModel = null;

    /**
     * @var string $AircraftVariant
     */
    protected $AircraftVariant = null;

    /**
     * @var ArrayOfByAltitudeClimbProfile $ClimbProfiles
     */
    protected $ClimbProfiles = null;

    /**
     * @var ArrayOfByAltitudeCruiseProfile $CruiseProfiles
     */
    protected $CruiseProfiles = null;

    /**
     * @var ArrayOfByAltitudeDescentProfile $DescentProfiles
     */
    protected $DescentProfiles = null;

    /**
     * @var float $EmptyWeight
     */
    protected $EmptyWeight = null;

    /**
     * @var AircraftEngineType $EngineType
     */
    protected $EngineType = null;

    /**
     * @var AircraftATCInfo $Equipment
     */
    protected $Equipment = null;

    /**
     * @var AircraftFuelType $FuelType
     */
    protected $FuelType = null;

    /**
     * @var float $MaxAltitudeFeet
     */
    protected $MaxAltitudeFeet = null;

    /**
     * @var float $MaxLanding
     */
    protected $MaxLanding = null;

    /**
     * @var float $MaxRampMass
     */
    protected $MaxRampMass = null;

    /**
     * @var float $MaxTakeoff
     */
    protected $MaxTakeoff = null;

    /**
     * @var float $MaxZeroFuel
     */
    protected $MaxZeroFuel = null;

    /**
     * @var int $NumberOfEngines
     */
    protected $NumberOfEngines = null;

    /**
     * @var float $UsableFuel
     */
    protected $UsableFuel = null;

    /**
     * @var ATCWakeTurbulanceCategory $WakeTurbulanceCategory
     */
    protected $WakeTurbulanceCategory = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAircraftICAOId()
    {
      return $this->AircraftICAOId;
    }

    /**
     * @param string $AircraftICAOId
     * @return ByAltitudeAircraftSpecification
     */
    public function setAircraftICAOId($AircraftICAOId)
    {
      $this->AircraftICAOId = $AircraftICAOId;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftMake()
    {
      return $this->AircraftMake;
    }

    /**
     * @param string $AircraftMake
     * @return ByAltitudeAircraftSpecification
     */
    public function setAircraftMake($AircraftMake)
    {
      $this->AircraftMake = $AircraftMake;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftModel()
    {
      return $this->AircraftModel;
    }

    /**
     * @param string $AircraftModel
     * @return ByAltitudeAircraftSpecification
     */
    public function setAircraftModel($AircraftModel)
    {
      $this->AircraftModel = $AircraftModel;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftVariant()
    {
      return $this->AircraftVariant;
    }

    /**
     * @param string $AircraftVariant
     * @return ByAltitudeAircraftSpecification
     */
    public function setAircraftVariant($AircraftVariant)
    {
      $this->AircraftVariant = $AircraftVariant;
      return $this;
    }

    /**
     * @return ArrayOfByAltitudeClimbProfile
     */
    public function getClimbProfiles()
    {
      return $this->ClimbProfiles;
    }

    /**
     * @param ArrayOfByAltitudeClimbProfile $ClimbProfiles
     * @return ByAltitudeAircraftSpecification
     */
    public function setClimbProfiles($ClimbProfiles)
    {
      $this->ClimbProfiles = $ClimbProfiles;
      return $this;
    }

    /**
     * @return ArrayOfByAltitudeCruiseProfile
     */
    public function getCruiseProfiles()
    {
      return $this->CruiseProfiles;
    }

    /**
     * @param ArrayOfByAltitudeCruiseProfile $CruiseProfiles
     * @return ByAltitudeAircraftSpecification
     */
    public function setCruiseProfiles($CruiseProfiles)
    {
      $this->CruiseProfiles = $CruiseProfiles;
      return $this;
    }

    /**
     * @return ArrayOfByAltitudeDescentProfile
     */
    public function getDescentProfiles()
    {
      return $this->DescentProfiles;
    }

    /**
     * @param ArrayOfByAltitudeDescentProfile $DescentProfiles
     * @return ByAltitudeAircraftSpecification
     */
    public function setDescentProfiles($DescentProfiles)
    {
      $this->DescentProfiles = $DescentProfiles;
      return $this;
    }

    /**
     * @return float
     */
    public function getEmptyWeight()
    {
      return $this->EmptyWeight;
    }

    /**
     * @param float $EmptyWeight
     * @return ByAltitudeAircraftSpecification
     */
    public function setEmptyWeight($EmptyWeight)
    {
      $this->EmptyWeight = $EmptyWeight;
      return $this;
    }

    /**
     * @return AircraftEngineType
     */
    public function getEngineType()
    {
      return $this->EngineType;
    }

    /**
     * @param AircraftEngineType $EngineType
     * @return ByAltitudeAircraftSpecification
     */
    public function setEngineType($EngineType)
    {
      $this->EngineType = $EngineType;
      return $this;
    }

    /**
     * @return AircraftATCInfo
     */
    public function getEquipment()
    {
      return $this->Equipment;
    }

    /**
     * @param AircraftATCInfo $Equipment
     * @return ByAltitudeAircraftSpecification
     */
    public function setEquipment($Equipment)
    {
      $this->Equipment = $Equipment;
      return $this;
    }

    /**
     * @return AircraftFuelType
     */
    public function getFuelType()
    {
      return $this->FuelType;
    }

    /**
     * @param AircraftFuelType $FuelType
     * @return ByAltitudeAircraftSpecification
     */
    public function setFuelType($FuelType)
    {
      $this->FuelType = $FuelType;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxAltitudeFeet()
    {
      return $this->MaxAltitudeFeet;
    }

    /**
     * @param float $MaxAltitudeFeet
     * @return ByAltitudeAircraftSpecification
     */
    public function setMaxAltitudeFeet($MaxAltitudeFeet)
    {
      $this->MaxAltitudeFeet = $MaxAltitudeFeet;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxLanding()
    {
      return $this->MaxLanding;
    }

    /**
     * @param float $MaxLanding
     * @return ByAltitudeAircraftSpecification
     */
    public function setMaxLanding($MaxLanding)
    {
      $this->MaxLanding = $MaxLanding;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxRampMass()
    {
      return $this->MaxRampMass;
    }

    /**
     * @param float $MaxRampMass
     * @return ByAltitudeAircraftSpecification
     */
    public function setMaxRampMass($MaxRampMass)
    {
      $this->MaxRampMass = $MaxRampMass;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxTakeoff()
    {
      return $this->MaxTakeoff;
    }

    /**
     * @param float $MaxTakeoff
     * @return ByAltitudeAircraftSpecification
     */
    public function setMaxTakeoff($MaxTakeoff)
    {
      $this->MaxTakeoff = $MaxTakeoff;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxZeroFuel()
    {
      return $this->MaxZeroFuel;
    }

    /**
     * @param float $MaxZeroFuel
     * @return ByAltitudeAircraftSpecification
     */
    public function setMaxZeroFuel($MaxZeroFuel)
    {
      $this->MaxZeroFuel = $MaxZeroFuel;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfEngines()
    {
      return $this->NumberOfEngines;
    }

    /**
     * @param int $NumberOfEngines
     * @return ByAltitudeAircraftSpecification
     */
    public function setNumberOfEngines($NumberOfEngines)
    {
      $this->NumberOfEngines = $NumberOfEngines;
      return $this;
    }

    /**
     * @return float
     */
    public function getUsableFuel()
    {
      return $this->UsableFuel;
    }

    /**
     * @param float $UsableFuel
     * @return ByAltitudeAircraftSpecification
     */
    public function setUsableFuel($UsableFuel)
    {
      $this->UsableFuel = $UsableFuel;
      return $this;
    }

    /**
     * @return ATCWakeTurbulanceCategory
     */
    public function getWakeTurbulanceCategory()
    {
      return $this->WakeTurbulanceCategory;
    }

    /**
     * @param ATCWakeTurbulanceCategory $WakeTurbulanceCategory
     * @return ByAltitudeAircraftSpecification
     */
    public function setWakeTurbulanceCategory($WakeTurbulanceCategory)
    {
      $this->WakeTurbulanceCategory = $WakeTurbulanceCategory;
      return $this;
    }

}
