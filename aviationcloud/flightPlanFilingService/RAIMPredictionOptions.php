<?php

class RAIMPredictionOptions
{

    /**
     * @var boolean $BaroAiding
     */
    protected $BaroAiding = null;

    /**
     * @var float $MaskAngle
     */
    protected $MaskAngle = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getBaroAiding()
    {
      return $this->BaroAiding;
    }

    /**
     * @param boolean $BaroAiding
     * @return RAIMPredictionOptions
     */
    public function setBaroAiding($BaroAiding)
    {
      $this->BaroAiding = $BaroAiding;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaskAngle()
    {
      return $this->MaskAngle;
    }

    /**
     * @param float $MaskAngle
     * @return RAIMPredictionOptions
     */
    public function setMaskAngle($MaskAngle)
    {
      $this->MaskAngle = $MaskAngle;
      return $this;
    }

}
