<?php

class RetrieveFiledFlightsResponse
{

    /**
     * @var FiledFlightsRetrievalResponse $RetrieveFiledFlightsResult
     */
    protected $RetrieveFiledFlightsResult = null;

    /**
     * @param FiledFlightsRetrievalResponse $RetrieveFiledFlightsResult
     */
    public function __construct($RetrieveFiledFlightsResult)
    {
      $this->RetrieveFiledFlightsResult = $RetrieveFiledFlightsResult;
    }

    /**
     * @return FiledFlightsRetrievalResponse
     */
    public function getRetrieveFiledFlightsResult()
    {
      return $this->RetrieveFiledFlightsResult;
    }

    /**
     * @param FiledFlightsRetrievalResponse $RetrieveFiledFlightsResult
     * @return RetrieveFiledFlightsResponse
     */
    public function setRetrieveFiledFlightsResult($RetrieveFiledFlightsResult)
    {
      $this->RetrieveFiledFlightsResult = $RetrieveFiledFlightsResult;
      return $this;
    }

}
