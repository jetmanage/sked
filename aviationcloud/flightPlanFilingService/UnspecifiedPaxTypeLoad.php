<?php

class UnspecifiedPaxTypeLoad extends PaxLoad
{

    /**
     * @var int $NumberOfPax
     */
    protected $NumberOfPax = null;

    /**
     * @var Weight $TotalWeight
     */
    protected $TotalWeight = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getNumberOfPax()
    {
      return $this->NumberOfPax;
    }

    /**
     * @param int $NumberOfPax
     * @return UnspecifiedPaxTypeLoad
     */
    public function setNumberOfPax($NumberOfPax)
    {
      $this->NumberOfPax = $NumberOfPax;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTotalWeight()
    {
      return $this->TotalWeight;
    }

    /**
     * @param Weight $TotalWeight
     * @return UnspecifiedPaxTypeLoad
     */
    public function setTotalWeight($TotalWeight)
    {
      $this->TotalWeight = $TotalWeight;
      return $this;
    }

}
