<?php

class FlightFuelValues
{

    /**
     * @var Weight $ActualFuel
     */
    protected $ActualFuel = null;

    /**
     * @var Weight $AdditionalFuel
     */
    protected $AdditionalFuel = null;

    /**
     * @var AircraftFuelType $AircraftFuelType
     */
    protected $AircraftFuelType = null;

    /**
     * @var Weight $Alternate1LandingFuel
     */
    protected $Alternate1LandingFuel = null;

    /**
     * @var Weight $Alternate2LandingFuel
     */
    protected $Alternate2LandingFuel = null;

    /**
     * @var Weight $AlternateFuel
     */
    protected $AlternateFuel = null;

    /**
     * @var Weight $ClimbFuel
     */
    protected $ClimbFuel = null;

    /**
     * @var Weight $ContingencyDestinationFuel
     */
    protected $ContingencyDestinationFuel = null;

    /**
     * @var Weight $ContingencyFuel
     */
    protected $ContingencyFuel = null;

    /**
     * @var Weight $ContingencySecondDestinationFuel
     */
    protected $ContingencySecondDestinationFuel = null;

    /**
     * @var Weight $CruiseFuel
     */
    protected $CruiseFuel = null;

    /**
     * @var Weight $DescentFuel
     */
    protected $DescentFuel = null;

    /**
     * @var Weight $ExtraFuel
     */
    protected $ExtraFuel = null;

    /**
     * @var Weight $FinalReserveFuel
     */
    protected $FinalReserveFuel = null;

    /**
     * @var SphereicPoint $FuelExhaustionPoint
     */
    protected $FuelExhaustionPoint = null;

    /**
     * @var Weight $FuelToAlternate1
     */
    protected $FuelToAlternate1 = null;

    /**
     * @var Weight $FuelToAlternate2
     */
    protected $FuelToAlternate2 = null;

    /**
     * @var Weight $FuelToTakeoffAlternate
     */
    protected $FuelToTakeoffAlternate = null;

    /**
     * @var Weight $HoldingFUel
     */
    protected $HoldingFUel = null;

    /**
     * @var Weight $LandingFuel
     */
    protected $LandingFuel = null;

    /**
     * @var Weight $MaxFuelCapacity
     */
    protected $MaxFuelCapacity = null;

    /**
     * @var Weight $MaximumAllowedFuel
     */
    protected $MaximumAllowedFuel = null;

    /**
     * @var Weight $MinimumPossibleActualFuel
     */
    protected $MinimumPossibleActualFuel = null;

    /**
     * @var Weight $MinimumPossibleTripFuel
     */
    protected $MinimumPossibleTripFuel = null;

    /**
     * @var Weight $MinimumRequired
     */
    protected $MinimumRequired = null;

    /**
     * @var Weight $MinimumRequiredAtDecisionPointToDestination
     */
    protected $MinimumRequiredAtDecisionPointToDestination = null;

    /**
     * @var Weight $MinimumRequiredAtDecisionPointToSecondDestination
     */
    protected $MinimumRequiredAtDecisionPointToSecondDestination = null;

    /**
     * @var Airport $PrimaryAlternate
     */
    protected $PrimaryAlternate = null;

    /**
     * @var Weight $RampInFuel
     */
    protected $RampInFuel = null;

    /**
     * @var Weight $RampOutFuel
     */
    protected $RampOutFuel = null;

    /**
     * @var SphereicPoint $ReserveFuelEntryPoint
     */
    protected $ReserveFuelEntryPoint = null;

    /**
     * @var Weight $TakeOffFuel
     */
    protected $TakeOffFuel = null;

    /**
     * @var Weight $TakeoffAlternateLandingFuel
     */
    protected $TakeoffAlternateLandingFuel = null;

    /**
     * @var Weight $TaxiFuel
     */
    protected $TaxiFuel = null;

    /**
     * @var Weight $TaxiInFuel
     */
    protected $TaxiInFuel = null;

    /**
     * @var Weight $TripFuel
     */
    protected $TripFuel = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getActualFuel()
    {
      return $this->ActualFuel;
    }

    /**
     * @param Weight $ActualFuel
     * @return FlightFuelValues
     */
    public function setActualFuel($ActualFuel)
    {
      $this->ActualFuel = $ActualFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getAdditionalFuel()
    {
      return $this->AdditionalFuel;
    }

    /**
     * @param Weight $AdditionalFuel
     * @return FlightFuelValues
     */
    public function setAdditionalFuel($AdditionalFuel)
    {
      $this->AdditionalFuel = $AdditionalFuel;
      return $this;
    }

    /**
     * @return AircraftFuelType
     */
    public function getAircraftFuelType()
    {
      return $this->AircraftFuelType;
    }

    /**
     * @param AircraftFuelType $AircraftFuelType
     * @return FlightFuelValues
     */
    public function setAircraftFuelType($AircraftFuelType)
    {
      $this->AircraftFuelType = $AircraftFuelType;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getAlternate1LandingFuel()
    {
      return $this->Alternate1LandingFuel;
    }

    /**
     * @param Weight $Alternate1LandingFuel
     * @return FlightFuelValues
     */
    public function setAlternate1LandingFuel($Alternate1LandingFuel)
    {
      $this->Alternate1LandingFuel = $Alternate1LandingFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getAlternate2LandingFuel()
    {
      return $this->Alternate2LandingFuel;
    }

    /**
     * @param Weight $Alternate2LandingFuel
     * @return FlightFuelValues
     */
    public function setAlternate2LandingFuel($Alternate2LandingFuel)
    {
      $this->Alternate2LandingFuel = $Alternate2LandingFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getAlternateFuel()
    {
      return $this->AlternateFuel;
    }

    /**
     * @param Weight $AlternateFuel
     * @return FlightFuelValues
     */
    public function setAlternateFuel($AlternateFuel)
    {
      $this->AlternateFuel = $AlternateFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getClimbFuel()
    {
      return $this->ClimbFuel;
    }

    /**
     * @param Weight $ClimbFuel
     * @return FlightFuelValues
     */
    public function setClimbFuel($ClimbFuel)
    {
      $this->ClimbFuel = $ClimbFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getContingencyDestinationFuel()
    {
      return $this->ContingencyDestinationFuel;
    }

    /**
     * @param Weight $ContingencyDestinationFuel
     * @return FlightFuelValues
     */
    public function setContingencyDestinationFuel($ContingencyDestinationFuel)
    {
      $this->ContingencyDestinationFuel = $ContingencyDestinationFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getContingencyFuel()
    {
      return $this->ContingencyFuel;
    }

    /**
     * @param Weight $ContingencyFuel
     * @return FlightFuelValues
     */
    public function setContingencyFuel($ContingencyFuel)
    {
      $this->ContingencyFuel = $ContingencyFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getContingencySecondDestinationFuel()
    {
      return $this->ContingencySecondDestinationFuel;
    }

    /**
     * @param Weight $ContingencySecondDestinationFuel
     * @return FlightFuelValues
     */
    public function setContingencySecondDestinationFuel($ContingencySecondDestinationFuel)
    {
      $this->ContingencySecondDestinationFuel = $ContingencySecondDestinationFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getCruiseFuel()
    {
      return $this->CruiseFuel;
    }

    /**
     * @param Weight $CruiseFuel
     * @return FlightFuelValues
     */
    public function setCruiseFuel($CruiseFuel)
    {
      $this->CruiseFuel = $CruiseFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getDescentFuel()
    {
      return $this->DescentFuel;
    }

    /**
     * @param Weight $DescentFuel
     * @return FlightFuelValues
     */
    public function setDescentFuel($DescentFuel)
    {
      $this->DescentFuel = $DescentFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getExtraFuel()
    {
      return $this->ExtraFuel;
    }

    /**
     * @param Weight $ExtraFuel
     * @return FlightFuelValues
     */
    public function setExtraFuel($ExtraFuel)
    {
      $this->ExtraFuel = $ExtraFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFinalReserveFuel()
    {
      return $this->FinalReserveFuel;
    }

    /**
     * @param Weight $FinalReserveFuel
     * @return FlightFuelValues
     */
    public function setFinalReserveFuel($FinalReserveFuel)
    {
      $this->FinalReserveFuel = $FinalReserveFuel;
      return $this;
    }

    /**
     * @return SphereicPoint
     */
    public function getFuelExhaustionPoint()
    {
      return $this->FuelExhaustionPoint;
    }

    /**
     * @param SphereicPoint $FuelExhaustionPoint
     * @return FlightFuelValues
     */
    public function setFuelExhaustionPoint($FuelExhaustionPoint)
    {
      $this->FuelExhaustionPoint = $FuelExhaustionPoint;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuelToAlternate1()
    {
      return $this->FuelToAlternate1;
    }

    /**
     * @param Weight $FuelToAlternate1
     * @return FlightFuelValues
     */
    public function setFuelToAlternate1($FuelToAlternate1)
    {
      $this->FuelToAlternate1 = $FuelToAlternate1;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuelToAlternate2()
    {
      return $this->FuelToAlternate2;
    }

    /**
     * @param Weight $FuelToAlternate2
     * @return FlightFuelValues
     */
    public function setFuelToAlternate2($FuelToAlternate2)
    {
      $this->FuelToAlternate2 = $FuelToAlternate2;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuelToTakeoffAlternate()
    {
      return $this->FuelToTakeoffAlternate;
    }

    /**
     * @param Weight $FuelToTakeoffAlternate
     * @return FlightFuelValues
     */
    public function setFuelToTakeoffAlternate($FuelToTakeoffAlternate)
    {
      $this->FuelToTakeoffAlternate = $FuelToTakeoffAlternate;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getHoldingFUel()
    {
      return $this->HoldingFUel;
    }

    /**
     * @param Weight $HoldingFUel
     * @return FlightFuelValues
     */
    public function setHoldingFUel($HoldingFUel)
    {
      $this->HoldingFUel = $HoldingFUel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getLandingFuel()
    {
      return $this->LandingFuel;
    }

    /**
     * @param Weight $LandingFuel
     * @return FlightFuelValues
     */
    public function setLandingFuel($LandingFuel)
    {
      $this->LandingFuel = $LandingFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxFuelCapacity()
    {
      return $this->MaxFuelCapacity;
    }

    /**
     * @param Weight $MaxFuelCapacity
     * @return FlightFuelValues
     */
    public function setMaxFuelCapacity($MaxFuelCapacity)
    {
      $this->MaxFuelCapacity = $MaxFuelCapacity;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaximumAllowedFuel()
    {
      return $this->MaximumAllowedFuel;
    }

    /**
     * @param Weight $MaximumAllowedFuel
     * @return FlightFuelValues
     */
    public function setMaximumAllowedFuel($MaximumAllowedFuel)
    {
      $this->MaximumAllowedFuel = $MaximumAllowedFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMinimumPossibleActualFuel()
    {
      return $this->MinimumPossibleActualFuel;
    }

    /**
     * @param Weight $MinimumPossibleActualFuel
     * @return FlightFuelValues
     */
    public function setMinimumPossibleActualFuel($MinimumPossibleActualFuel)
    {
      $this->MinimumPossibleActualFuel = $MinimumPossibleActualFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMinimumPossibleTripFuel()
    {
      return $this->MinimumPossibleTripFuel;
    }

    /**
     * @param Weight $MinimumPossibleTripFuel
     * @return FlightFuelValues
     */
    public function setMinimumPossibleTripFuel($MinimumPossibleTripFuel)
    {
      $this->MinimumPossibleTripFuel = $MinimumPossibleTripFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMinimumRequired()
    {
      return $this->MinimumRequired;
    }

    /**
     * @param Weight $MinimumRequired
     * @return FlightFuelValues
     */
    public function setMinimumRequired($MinimumRequired)
    {
      $this->MinimumRequired = $MinimumRequired;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMinimumRequiredAtDecisionPointToDestination()
    {
      return $this->MinimumRequiredAtDecisionPointToDestination;
    }

    /**
     * @param Weight $MinimumRequiredAtDecisionPointToDestination
     * @return FlightFuelValues
     */
    public function setMinimumRequiredAtDecisionPointToDestination($MinimumRequiredAtDecisionPointToDestination)
    {
      $this->MinimumRequiredAtDecisionPointToDestination = $MinimumRequiredAtDecisionPointToDestination;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMinimumRequiredAtDecisionPointToSecondDestination()
    {
      return $this->MinimumRequiredAtDecisionPointToSecondDestination;
    }

    /**
     * @param Weight $MinimumRequiredAtDecisionPointToSecondDestination
     * @return FlightFuelValues
     */
    public function setMinimumRequiredAtDecisionPointToSecondDestination($MinimumRequiredAtDecisionPointToSecondDestination)
    {
      $this->MinimumRequiredAtDecisionPointToSecondDestination = $MinimumRequiredAtDecisionPointToSecondDestination;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getPrimaryAlternate()
    {
      return $this->PrimaryAlternate;
    }

    /**
     * @param Airport $PrimaryAlternate
     * @return FlightFuelValues
     */
    public function setPrimaryAlternate($PrimaryAlternate)
    {
      $this->PrimaryAlternate = $PrimaryAlternate;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getRampInFuel()
    {
      return $this->RampInFuel;
    }

    /**
     * @param Weight $RampInFuel
     * @return FlightFuelValues
     */
    public function setRampInFuel($RampInFuel)
    {
      $this->RampInFuel = $RampInFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getRampOutFuel()
    {
      return $this->RampOutFuel;
    }

    /**
     * @param Weight $RampOutFuel
     * @return FlightFuelValues
     */
    public function setRampOutFuel($RampOutFuel)
    {
      $this->RampOutFuel = $RampOutFuel;
      return $this;
    }

    /**
     * @return SphereicPoint
     */
    public function getReserveFuelEntryPoint()
    {
      return $this->ReserveFuelEntryPoint;
    }

    /**
     * @param SphereicPoint $ReserveFuelEntryPoint
     * @return FlightFuelValues
     */
    public function setReserveFuelEntryPoint($ReserveFuelEntryPoint)
    {
      $this->ReserveFuelEntryPoint = $ReserveFuelEntryPoint;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTakeOffFuel()
    {
      return $this->TakeOffFuel;
    }

    /**
     * @param Weight $TakeOffFuel
     * @return FlightFuelValues
     */
    public function setTakeOffFuel($TakeOffFuel)
    {
      $this->TakeOffFuel = $TakeOffFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTakeoffAlternateLandingFuel()
    {
      return $this->TakeoffAlternateLandingFuel;
    }

    /**
     * @param Weight $TakeoffAlternateLandingFuel
     * @return FlightFuelValues
     */
    public function setTakeoffAlternateLandingFuel($TakeoffAlternateLandingFuel)
    {
      $this->TakeoffAlternateLandingFuel = $TakeoffAlternateLandingFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTaxiFuel()
    {
      return $this->TaxiFuel;
    }

    /**
     * @param Weight $TaxiFuel
     * @return FlightFuelValues
     */
    public function setTaxiFuel($TaxiFuel)
    {
      $this->TaxiFuel = $TaxiFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTaxiInFuel()
    {
      return $this->TaxiInFuel;
    }

    /**
     * @param Weight $TaxiInFuel
     * @return FlightFuelValues
     */
    public function setTaxiInFuel($TaxiInFuel)
    {
      $this->TaxiInFuel = $TaxiInFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTripFuel()
    {
      return $this->TripFuel;
    }

    /**
     * @param Weight $TripFuel
     * @return FlightFuelValues
     */
    public function setTripFuel($TripFuel)
    {
      $this->TripFuel = $TripFuel;
      return $this;
    }

}
