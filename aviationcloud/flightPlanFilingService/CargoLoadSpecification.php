<?php

class CargoLoadSpecification
{

    /**
     * @var boolean $AllowCargoOffLoad
     */
    protected $AllowCargoOffLoad = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getAllowCargoOffLoad()
    {
      return $this->AllowCargoOffLoad;
    }

    /**
     * @param boolean $AllowCargoOffLoad
     * @return CargoLoadSpecification
     */
    public function setAllowCargoOffLoad($AllowCargoOffLoad)
    {
      $this->AllowCargoOffLoad = $AllowCargoOffLoad;
      return $this;
    }

}
