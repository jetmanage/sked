<?php

class RNAVPreferentialRoutes
{

    /**
     * @var int $Enroute
     */
    protected $Enroute = null;

    /**
     * @var int $SID
     */
    protected $SID = null;

    /**
     * @var int $STAR
     */
    protected $STAR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getEnroute()
    {
      return $this->Enroute;
    }

    /**
     * @param int $Enroute
     * @return RNAVPreferentialRoutes
     */
    public function setEnroute($Enroute)
    {
      $this->Enroute = $Enroute;
      return $this;
    }

    /**
     * @return int
     */
    public function getSID()
    {
      return $this->SID;
    }

    /**
     * @param int $SID
     * @return RNAVPreferentialRoutes
     */
    public function setSID($SID)
    {
      $this->SID = $SID;
      return $this;
    }

    /**
     * @return int
     */
    public function getSTAR()
    {
      return $this->STAR;
    }

    /**
     * @param int $STAR
     * @return RNAVPreferentialRoutes
     */
    public function setSTAR($STAR)
    {
      $this->STAR = $STAR;
      return $this;
    }

}
