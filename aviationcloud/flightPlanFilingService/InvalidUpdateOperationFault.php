<?php

class InvalidUpdateOperationFault
{

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    /**
     * @var int $FlightPlanId
     */
    protected $FlightPlanId = null;

    /**
     * @var boolean $IsProduction
     */
    protected $IsProduction = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return InvalidUpdateOperationFault
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return InvalidUpdateOperationFault
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightPlanId()
    {
      return $this->FlightPlanId;
    }

    /**
     * @param int $FlightPlanId
     * @return InvalidUpdateOperationFault
     */
    public function setFlightPlanId($FlightPlanId)
    {
      $this->FlightPlanId = $FlightPlanId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsProduction()
    {
      return $this->IsProduction;
    }

    /**
     * @param boolean $IsProduction
     * @return InvalidUpdateOperationFault
     */
    public function setIsProduction($IsProduction)
    {
      $this->IsProduction = $IsProduction;
      return $this;
    }

}
