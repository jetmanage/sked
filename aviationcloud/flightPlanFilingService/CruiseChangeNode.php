<?php

class CruiseChangeNode
{

    /**
     * @var PerformanceProfileSpecification $Profile
     */
    protected $Profile = null;

    /**
     * @var RouteNodeIdentifier $Waypoint
     */
    protected $Waypoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return PerformanceProfileSpecification
     */
    public function getProfile()
    {
      return $this->Profile;
    }

    /**
     * @param PerformanceProfileSpecification $Profile
     * @return CruiseChangeNode
     */
    public function setProfile($Profile)
    {
      $this->Profile = $Profile;
      return $this;
    }

    /**
     * @return RouteNodeIdentifier
     */
    public function getWaypoint()
    {
      return $this->Waypoint;
    }

    /**
     * @param RouteNodeIdentifier $Waypoint
     * @return CruiseChangeNode
     */
    public function setWaypoint($Waypoint)
    {
      $this->Waypoint = $Waypoint;
      return $this;
    }

}
