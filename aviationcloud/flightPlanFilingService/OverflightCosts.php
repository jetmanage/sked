<?php

class OverflightCosts
{

    /**
     * @var Currency $Cost
     */
    protected $Cost = null;

    /**
     * @var Length $DistanceTraveledInFir
     */
    protected $DistanceTraveledInFir = null;

    /**
     * @var FIR $FIR
     */
    protected $FIR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Currency
     */
    public function getCost()
    {
      return $this->Cost;
    }

    /**
     * @param Currency $Cost
     * @return OverflightCosts
     */
    public function setCost($Cost)
    {
      $this->Cost = $Cost;
      return $this;
    }

    /**
     * @return Length
     */
    public function getDistanceTraveledInFir()
    {
      return $this->DistanceTraveledInFir;
    }

    /**
     * @param Length $DistanceTraveledInFir
     * @return OverflightCosts
     */
    public function setDistanceTraveledInFir($DistanceTraveledInFir)
    {
      $this->DistanceTraveledInFir = $DistanceTraveledInFir;
      return $this;
    }

    /**
     * @return FIR
     */
    public function getFIR()
    {
      return $this->FIR;
    }

    /**
     * @param FIR $FIR
     * @return OverflightCosts
     */
    public function setFIR($FIR)
    {
      $this->FIR = $FIR;
      return $this;
    }

}
