<?php

class ArrayOfWarning implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Warning[] $Warning
     */
    protected $Warning = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Warning[]
     */
    public function getWarning()
    {
      return $this->Warning;
    }

    /**
     * @param Warning[] $Warning
     * @return ArrayOfWarning
     */
    public function setWarning(array $Warning = null)
    {
      $this->Warning = $Warning;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Warning[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Warning
     */
    public function offsetGet($offset)
    {
      return $this->Warning[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Warning $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->Warning[] = $value;
      } else {
        $this->Warning[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Warning[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Warning Return the current element
     */
    public function current()
    {
      return current($this->Warning);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Warning);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Warning);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Warning);
    }

    /**
     * Countable implementation
     *
     * @return Warning Return count of elements
     */
    public function count()
    {
      return count($this->Warning);
    }

}
