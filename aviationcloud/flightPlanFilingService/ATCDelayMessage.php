<?php

class ATCDelayMessage extends ATCFlightMessage
{

    /**
     * @var \DateTime $NewTimeOfDeparture
     */
    protected $NewTimeOfDeparture = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return \DateTime
     */
    public function getNewTimeOfDeparture()
    {
      if ($this->NewTimeOfDeparture == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->NewTimeOfDeparture);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $NewTimeOfDeparture
     * @return ATCDelayMessage
     */
    public function setNewTimeOfDeparture(\DateTime $NewTimeOfDeparture = null)
    {
      if ($NewTimeOfDeparture == null) {
       $this->NewTimeOfDeparture = null;
      } else {
        $this->NewTimeOfDeparture = $NewTimeOfDeparture->format(\DateTime::ATOM);
      }
      return $this;
    }

}
