<?php

class ArrayOfEnrouteDiversionAirport implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var EnrouteDiversionAirport[] $EnrouteDiversionAirport
     */
    protected $EnrouteDiversionAirport = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return EnrouteDiversionAirport[]
     */
    public function getEnrouteDiversionAirport()
    {
      return $this->EnrouteDiversionAirport;
    }

    /**
     * @param EnrouteDiversionAirport[] $EnrouteDiversionAirport
     * @return ArrayOfEnrouteDiversionAirport
     */
    public function setEnrouteDiversionAirport(array $EnrouteDiversionAirport = null)
    {
      $this->EnrouteDiversionAirport = $EnrouteDiversionAirport;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->EnrouteDiversionAirport[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return EnrouteDiversionAirport
     */
    public function offsetGet($offset)
    {
      return $this->EnrouteDiversionAirport[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param EnrouteDiversionAirport $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->EnrouteDiversionAirport[] = $value;
      } else {
        $this->EnrouteDiversionAirport[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->EnrouteDiversionAirport[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return EnrouteDiversionAirport Return the current element
     */
    public function current()
    {
      return current($this->EnrouteDiversionAirport);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->EnrouteDiversionAirport);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->EnrouteDiversionAirport);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->EnrouteDiversionAirport);
    }

    /**
     * Countable implementation
     *
     * @return EnrouteDiversionAirport Return count of elements
     */
    public function count()
    {
      return count($this->EnrouteDiversionAirport);
    }

}
