<?php

class LengthUnit
{
    const __default = 'Feet';
    const Feet = 'Feet';
    const Meter = 'Meter';
    const Kilometer = 'Kilometer';
    const NauticalMile = 'NauticalMile';


}
