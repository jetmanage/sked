<?php

class ArrayOfValidationDetail implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ValidationDetail[] $ValidationDetail
     */
    protected $ValidationDetail = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ValidationDetail[]
     */
    public function getValidationDetail()
    {
      return $this->ValidationDetail;
    }

    /**
     * @param ValidationDetail[] $ValidationDetail
     * @return ArrayOfValidationDetail
     */
    public function setValidationDetail(array $ValidationDetail = null)
    {
      $this->ValidationDetail = $ValidationDetail;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ValidationDetail[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ValidationDetail
     */
    public function offsetGet($offset)
    {
      return $this->ValidationDetail[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ValidationDetail $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ValidationDetail[] = $value;
      } else {
        $this->ValidationDetail[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ValidationDetail[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ValidationDetail Return the current element
     */
    public function current()
    {
      return current($this->ValidationDetail);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ValidationDetail);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ValidationDetail);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ValidationDetail);
    }

    /**
     * Countable implementation
     *
     * @return ValidationDetail Return count of elements
     */
    public function count()
    {
      return count($this->ValidationDetail);
    }

}
