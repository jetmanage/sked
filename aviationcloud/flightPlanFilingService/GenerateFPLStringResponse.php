<?php

class GenerateFPLStringResponse
{

    /**
     * @var string $FPLString
     */
    protected $FPLString = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getFPLString()
    {
      return $this->FPLString;
    }

    /**
     * @param string $FPLString
     * @return GenerateFPLStringResponse
     */
    public function setFPLString($FPLString)
    {
      $this->FPLString = $FPLString;
      return $this;
    }

}
