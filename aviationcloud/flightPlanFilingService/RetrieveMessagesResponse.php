<?php

class RetrieveMessagesResponse
{

    /**
     * @var FiledFlightMessagesResponse $RetrieveMessagesResult
     */
    protected $RetrieveMessagesResult = null;

    /**
     * @param FiledFlightMessagesResponse $RetrieveMessagesResult
     */
    public function __construct($RetrieveMessagesResult)
    {
      $this->RetrieveMessagesResult = $RetrieveMessagesResult;
    }

    /**
     * @return FiledFlightMessagesResponse
     */
    public function getRetrieveMessagesResult()
    {
      return $this->RetrieveMessagesResult;
    }

    /**
     * @param FiledFlightMessagesResponse $RetrieveMessagesResult
     * @return RetrieveMessagesResponse
     */
    public function setRetrieveMessagesResult($RetrieveMessagesResult)
    {
      $this->RetrieveMessagesResult = $RetrieveMessagesResult;
      return $this;
    }

}
