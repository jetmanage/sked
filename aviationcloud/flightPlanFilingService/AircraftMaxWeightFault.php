<?php

class AircraftMaxWeightFault
{

    /**
     * @var ArrayOfAircraftMaxWeightExceededError $MaxWeightExceededErrors
     */
    protected $MaxWeightExceededErrors = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAircraftMaxWeightExceededError
     */
    public function getMaxWeightExceededErrors()
    {
      return $this->MaxWeightExceededErrors;
    }

    /**
     * @param ArrayOfAircraftMaxWeightExceededError $MaxWeightExceededErrors
     * @return AircraftMaxWeightFault
     */
    public function setMaxWeightExceededErrors($MaxWeightExceededErrors)
    {
      $this->MaxWeightExceededErrors = $MaxWeightExceededErrors;
      return $this;
    }

}
