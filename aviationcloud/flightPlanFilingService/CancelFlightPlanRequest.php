<?php

class CancelFlightPlanRequest extends RequestBase
{

    /**
     * @var EmulatorResponseOptions $EmulatorResponseOptions
     */
    protected $EmulatorResponseOptions = null;

    /**
     * @var boolean $EnableProductionCancellation
     */
    protected $EnableProductionCancellation = null;

    /**
     * @var int $FlightId
     */
    protected $FlightId = null;

    /**
     * @var boolean $ForceCancellation
     */
    protected $ForceCancellation = null;

    /**
     * @var string $PreferredTransportProvider
     */
    protected $PreferredTransportProvider = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return EmulatorResponseOptions
     */
    public function getEmulatorResponseOptions()
    {
      return $this->EmulatorResponseOptions;
    }

    /**
     * @param EmulatorResponseOptions $EmulatorResponseOptions
     * @return CancelFlightPlanRequest
     */
    public function setEmulatorResponseOptions($EmulatorResponseOptions)
    {
      $this->EmulatorResponseOptions = $EmulatorResponseOptions;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableProductionCancellation()
    {
      return $this->EnableProductionCancellation;
    }

    /**
     * @param boolean $EnableProductionCancellation
     * @return CancelFlightPlanRequest
     */
    public function setEnableProductionCancellation($EnableProductionCancellation)
    {
      $this->EnableProductionCancellation = $EnableProductionCancellation;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightId()
    {
      return $this->FlightId;
    }

    /**
     * @param int $FlightId
     * @return CancelFlightPlanRequest
     */
    public function setFlightId($FlightId)
    {
      $this->FlightId = $FlightId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getForceCancellation()
    {
      return $this->ForceCancellation;
    }

    /**
     * @param boolean $ForceCancellation
     * @return CancelFlightPlanRequest
     */
    public function setForceCancellation($ForceCancellation)
    {
      $this->ForceCancellation = $ForceCancellation;
      return $this;
    }

    /**
     * @return string
     */
    public function getPreferredTransportProvider()
    {
      return $this->PreferredTransportProvider;
    }

    /**
     * @param string $PreferredTransportProvider
     * @return CancelFlightPlanRequest
     */
    public function setPreferredTransportProvider($PreferredTransportProvider)
    {
      $this->PreferredTransportProvider = $PreferredTransportProvider;
      return $this;
    }

}
