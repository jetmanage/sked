<?php

class FiledFlightsRetrievalResponse
{

    /**
     * @var ArrayOfFlightIdentifier $Flights
     */
    protected $Flights = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfFlightIdentifier
     */
    public function getFlights()
    {
      return $this->Flights;
    }

    /**
     * @param ArrayOfFlightIdentifier $Flights
     * @return FiledFlightsRetrievalResponse
     */
    public function setFlights($Flights)
    {
      $this->Flights = $Flights;
      return $this;
    }

}
