<?php

class RetrieveMessages
{

    /**
     * @var RetrieveFlightMessagesRequest $request
     */
    protected $request = null;

    /**
     * @param RetrieveFlightMessagesRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return RetrieveFlightMessagesRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param RetrieveFlightMessagesRequest $request
     * @return RetrieveMessages
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
