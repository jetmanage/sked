<?php

class ResolveAFTNAdressesResponse
{

    /**
     * @var AFTNAddressesResolveResponse $ResolveAFTNAdressesResult
     */
    protected $ResolveAFTNAdressesResult = null;

    /**
     * @param AFTNAddressesResolveResponse $ResolveAFTNAdressesResult
     */
    public function __construct($ResolveAFTNAdressesResult)
    {
      $this->ResolveAFTNAdressesResult = $ResolveAFTNAdressesResult;
    }

    /**
     * @return AFTNAddressesResolveResponse
     */
    public function getResolveAFTNAdressesResult()
    {
      return $this->ResolveAFTNAdressesResult;
    }

    /**
     * @param AFTNAddressesResolveResponse $ResolveAFTNAdressesResult
     * @return ResolveAFTNAdressesResponse
     */
    public function setResolveAFTNAdressesResult($ResolveAFTNAdressesResult)
    {
      $this->ResolveAFTNAdressesResult = $ResolveAFTNAdressesResult;
      return $this;
    }

}
