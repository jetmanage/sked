<?php

class ArrayOfClimbDescendDataPoint implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ClimbDescendDataPoint[] $ClimbDescendDataPoint
     */
    protected $ClimbDescendDataPoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ClimbDescendDataPoint[]
     */
    public function getClimbDescendDataPoint()
    {
      return $this->ClimbDescendDataPoint;
    }

    /**
     * @param ClimbDescendDataPoint[] $ClimbDescendDataPoint
     * @return ArrayOfClimbDescendDataPoint
     */
    public function setClimbDescendDataPoint(array $ClimbDescendDataPoint = null)
    {
      $this->ClimbDescendDataPoint = $ClimbDescendDataPoint;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ClimbDescendDataPoint[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ClimbDescendDataPoint
     */
    public function offsetGet($offset)
    {
      return $this->ClimbDescendDataPoint[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ClimbDescendDataPoint $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ClimbDescendDataPoint[] = $value;
      } else {
        $this->ClimbDescendDataPoint[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ClimbDescendDataPoint[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ClimbDescendDataPoint Return the current element
     */
    public function current()
    {
      return current($this->ClimbDescendDataPoint);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ClimbDescendDataPoint);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ClimbDescendDataPoint);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ClimbDescendDataPoint);
    }

    /**
     * Countable implementation
     *
     * @return ClimbDescendDataPoint Return count of elements
     */
    public function count()
    {
      return count($this->ClimbDescendDataPoint);
    }

}
