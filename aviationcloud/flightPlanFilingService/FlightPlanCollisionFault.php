<?php

class FlightPlanCollisionFault
{

    /**
     * @var string $Action
     */
    protected $Action = null;

    /**
     * @var string $CollidingAircraftIdent
     */
    protected $CollidingAircraftIdent = null;

    /**
     * @var string $CollidingDepartureICAO
     */
    protected $CollidingDepartureICAO = null;

    /**
     * @var string $CollidingDestinationICAO
     */
    protected $CollidingDestinationICAO = null;

    /**
     * @var \DateTime $CollidingEOBT
     */
    protected $CollidingEOBT = null;

    /**
     * @var int $CollidingFlightPlanId
     */
    protected $CollidingFlightPlanId = null;

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    /**
     * @var int $FlightPlanId
     */
    protected $FlightPlanId = null;

    /**
     * @var boolean $IsProduction
     */
    protected $IsProduction = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAction()
    {
      return $this->Action;
    }

    /**
     * @param string $Action
     * @return FlightPlanCollisionFault
     */
    public function setAction($Action)
    {
      $this->Action = $Action;
      return $this;
    }

    /**
     * @return string
     */
    public function getCollidingAircraftIdent()
    {
      return $this->CollidingAircraftIdent;
    }

    /**
     * @param string $CollidingAircraftIdent
     * @return FlightPlanCollisionFault
     */
    public function setCollidingAircraftIdent($CollidingAircraftIdent)
    {
      $this->CollidingAircraftIdent = $CollidingAircraftIdent;
      return $this;
    }

    /**
     * @return string
     */
    public function getCollidingDepartureICAO()
    {
      return $this->CollidingDepartureICAO;
    }

    /**
     * @param string $CollidingDepartureICAO
     * @return FlightPlanCollisionFault
     */
    public function setCollidingDepartureICAO($CollidingDepartureICAO)
    {
      $this->CollidingDepartureICAO = $CollidingDepartureICAO;
      return $this;
    }

    /**
     * @return string
     */
    public function getCollidingDestinationICAO()
    {
      return $this->CollidingDestinationICAO;
    }

    /**
     * @param string $CollidingDestinationICAO
     * @return FlightPlanCollisionFault
     */
    public function setCollidingDestinationICAO($CollidingDestinationICAO)
    {
      $this->CollidingDestinationICAO = $CollidingDestinationICAO;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCollidingEOBT()
    {
      if ($this->CollidingEOBT == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->CollidingEOBT);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $CollidingEOBT
     * @return FlightPlanCollisionFault
     */
    public function setCollidingEOBT(\DateTime $CollidingEOBT = null)
    {
      if ($CollidingEOBT == null) {
       $this->CollidingEOBT = null;
      } else {
        $this->CollidingEOBT = $CollidingEOBT->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return int
     */
    public function getCollidingFlightPlanId()
    {
      return $this->CollidingFlightPlanId;
    }

    /**
     * @param int $CollidingFlightPlanId
     * @return FlightPlanCollisionFault
     */
    public function setCollidingFlightPlanId($CollidingFlightPlanId)
    {
      $this->CollidingFlightPlanId = $CollidingFlightPlanId;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return FlightPlanCollisionFault
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return FlightPlanCollisionFault
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightPlanId()
    {
      return $this->FlightPlanId;
    }

    /**
     * @param int $FlightPlanId
     * @return FlightPlanCollisionFault
     */
    public function setFlightPlanId($FlightPlanId)
    {
      $this->FlightPlanId = $FlightPlanId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsProduction()
    {
      return $this->IsProduction;
    }

    /**
     * @param boolean $IsProduction
     * @return FlightPlanCollisionFault
     */
    public function setIsProduction($IsProduction)
    {
      $this->IsProduction = $IsProduction;
      return $this;
    }

}
