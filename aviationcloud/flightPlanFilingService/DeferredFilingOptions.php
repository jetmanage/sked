<?php

class DeferredFilingOptions
{

    /**
     * @var boolean $DisableDeferredFiling
     */
    protected $DisableDeferredFiling = null;

    /**
     * @var \DateTime $ScheduledFilingTime
     */
    protected $ScheduledFilingTime = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getDisableDeferredFiling()
    {
      return $this->DisableDeferredFiling;
    }

    /**
     * @param boolean $DisableDeferredFiling
     * @return DeferredFilingOptions
     */
    public function setDisableDeferredFiling($DisableDeferredFiling)
    {
      $this->DisableDeferredFiling = $DisableDeferredFiling;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getScheduledFilingTime()
    {
      if ($this->ScheduledFilingTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ScheduledFilingTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ScheduledFilingTime
     * @return DeferredFilingOptions
     */
    public function setScheduledFilingTime(\DateTime $ScheduledFilingTime = null)
    {
      if ($ScheduledFilingTime == null) {
       $this->ScheduledFilingTime = null;
      } else {
        $this->ScheduledFilingTime = $ScheduledFilingTime->format(\DateTime::ATOM);
      }
      return $this;
    }

}
