<?php

class FlightSegment
{

    /**
     * @var duration $Duration
     */
    protected $Duration = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var float $Risk
     */
    protected $Risk = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return duration
     */
    public function getDuration()
    {
      return $this->Duration;
    }

    /**
     * @param duration $Duration
     * @return FlightSegment
     */
    public function setDuration($Duration)
    {
      $this->Duration = $Duration;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return FlightSegment
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return float
     */
    public function getRisk()
    {
      return $this->Risk;
    }

    /**
     * @param float $Risk
     * @return FlightSegment
     */
    public function setRisk($Risk)
    {
      $this->Risk = $Risk;
      return $this;
    }

}
