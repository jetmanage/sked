<?php

class EUOpsRules extends FlightFuelRules
{

    /**
     * @var boolean $IsGeneralAviation
     */
    protected $IsGeneralAviation = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return boolean
     */
    public function getIsGeneralAviation()
    {
      return $this->IsGeneralAviation;
    }

    /**
     * @param boolean $IsGeneralAviation
     * @return EUOpsRules
     */
    public function setIsGeneralAviation($IsGeneralAviation)
    {
      $this->IsGeneralAviation = $IsGeneralAviation;
      return $this;
    }

}
