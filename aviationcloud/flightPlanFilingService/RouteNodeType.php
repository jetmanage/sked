<?php

class RouteNodeType
{
    const __default = 'Undefined';
    const Undefined = 'Undefined';
    const Waypoint = 'Waypoint';
    const NAVAid = 'NAVAid';
    const SIDPoint = 'SIDPoint';
    const STARPoint = 'STARPoint';
    const Runway = 'Runway';
    const RunwayLiftoff = 'RunwayLiftoff';
    const RunwayStart = 'RunwayStart';
    const RunwayLanding = 'RunwayLanding';
    const RunwayEnd = 'RunwayEnd';
    const AirportTerminal = 'AirportTerminal';


}
