<?php

class DataPoint
{

    /**
     * @var AirwayData $AirwayData
     */
    protected $AirwayData = null;

    /**
     * @var FlightData $FlightData
     */
    protected $FlightData = null;

    /**
     * @var FuelData $FuelData
     */
    protected $FuelData = null;

    /**
     * @var MeteorologicalData $MetrologicalData
     */
    protected $MetrologicalData = null;

    /**
     * @var ParserInformation $ParserData
     */
    protected $ParserData = null;

    /**
     * @var WaypointData $WaypointData
     */
    protected $WaypointData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AirwayData
     */
    public function getAirwayData()
    {
      return $this->AirwayData;
    }

    /**
     * @param AirwayData $AirwayData
     * @return DataPoint
     */
    public function setAirwayData($AirwayData)
    {
      $this->AirwayData = $AirwayData;
      return $this;
    }

    /**
     * @return FlightData
     */
    public function getFlightData()
    {
      return $this->FlightData;
    }

    /**
     * @param FlightData $FlightData
     * @return DataPoint
     */
    public function setFlightData($FlightData)
    {
      $this->FlightData = $FlightData;
      return $this;
    }

    /**
     * @return FuelData
     */
    public function getFuelData()
    {
      return $this->FuelData;
    }

    /**
     * @param FuelData $FuelData
     * @return DataPoint
     */
    public function setFuelData($FuelData)
    {
      $this->FuelData = $FuelData;
      return $this;
    }

    /**
     * @return MeteorologicalData
     */
    public function getMetrologicalData()
    {
      return $this->MetrologicalData;
    }

    /**
     * @param MeteorologicalData $MetrologicalData
     * @return DataPoint
     */
    public function setMetrologicalData($MetrologicalData)
    {
      $this->MetrologicalData = $MetrologicalData;
      return $this;
    }

    /**
     * @return ParserInformation
     */
    public function getParserData()
    {
      return $this->ParserData;
    }

    /**
     * @param ParserInformation $ParserData
     * @return DataPoint
     */
    public function setParserData($ParserData)
    {
      $this->ParserData = $ParserData;
      return $this;
    }

    /**
     * @return WaypointData
     */
    public function getWaypointData()
    {
      return $this->WaypointData;
    }

    /**
     * @param WaypointData $WaypointData
     * @return DataPoint
     */
    public function setWaypointData($WaypointData)
    {
      $this->WaypointData = $WaypointData;
      return $this;
    }

}
