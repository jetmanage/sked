<?php

class Area
{

    /**
     * @var string $AreaShape
     */
    protected $AreaShape = null;

    /**
     * @var int $FlightLevelLowerBound
     */
    protected $FlightLevelLowerBound = null;

    /**
     * @var int $FlightLevelUpperBound
     */
    protected $FlightLevelUpperBound = null;

    /**
     * @var string $Identifier
     */
    protected $Identifier = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAreaShape()
    {
      return $this->AreaShape;
    }

    /**
     * @param string $AreaShape
     * @return Area
     */
    public function setAreaShape($AreaShape)
    {
      $this->AreaShape = $AreaShape;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightLevelLowerBound()
    {
      return $this->FlightLevelLowerBound;
    }

    /**
     * @param int $FlightLevelLowerBound
     * @return Area
     */
    public function setFlightLevelLowerBound($FlightLevelLowerBound)
    {
      $this->FlightLevelLowerBound = $FlightLevelLowerBound;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightLevelUpperBound()
    {
      return $this->FlightLevelUpperBound;
    }

    /**
     * @param int $FlightLevelUpperBound
     * @return Area
     */
    public function setFlightLevelUpperBound($FlightLevelUpperBound)
    {
      $this->FlightLevelUpperBound = $FlightLevelUpperBound;
      return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
      return $this->Identifier;
    }

    /**
     * @param string $Identifier
     * @return Area
     */
    public function setIdentifier($Identifier)
    {
      $this->Identifier = $Identifier;
      return $this;
    }

}
