<?php

class ArrayOfETOPSSettingSpecification implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ETOPSSettingSpecification[] $ETOPSSettingSpecification
     */
    protected $ETOPSSettingSpecification = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ETOPSSettingSpecification[]
     */
    public function getETOPSSettingSpecification()
    {
      return $this->ETOPSSettingSpecification;
    }

    /**
     * @param ETOPSSettingSpecification[] $ETOPSSettingSpecification
     * @return ArrayOfETOPSSettingSpecification
     */
    public function setETOPSSettingSpecification(array $ETOPSSettingSpecification = null)
    {
      $this->ETOPSSettingSpecification = $ETOPSSettingSpecification;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ETOPSSettingSpecification[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ETOPSSettingSpecification
     */
    public function offsetGet($offset)
    {
      return $this->ETOPSSettingSpecification[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ETOPSSettingSpecification $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ETOPSSettingSpecification[] = $value;
      } else {
        $this->ETOPSSettingSpecification[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ETOPSSettingSpecification[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ETOPSSettingSpecification Return the current element
     */
    public function current()
    {
      return current($this->ETOPSSettingSpecification);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ETOPSSettingSpecification);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ETOPSSettingSpecification);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ETOPSSettingSpecification);
    }

    /**
     * Countable implementation
     *
     * @return ETOPSSettingSpecification Return count of elements
     */
    public function count()
    {
      return count($this->ETOPSSettingSpecification);
    }

}
