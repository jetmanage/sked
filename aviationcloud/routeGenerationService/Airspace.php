<?php

class Airspace
{

    /**
     * @var int $LowerFLLimit
     */
    protected $LowerFLLimit = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var string $Shape
     */
    protected $Shape = null;

    /**
     * @var int $UpperFLLimit
     */
    protected $UpperFLLimit = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getLowerFLLimit()
    {
      return $this->LowerFLLimit;
    }

    /**
     * @param int $LowerFLLimit
     * @return Airspace
     */
    public function setLowerFLLimit($LowerFLLimit)
    {
      $this->LowerFLLimit = $LowerFLLimit;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return Airspace
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return string
     */
    public function getShape()
    {
      return $this->Shape;
    }

    /**
     * @param string $Shape
     * @return Airspace
     */
    public function setShape($Shape)
    {
      $this->Shape = $Shape;
      return $this;
    }

    /**
     * @return int
     */
    public function getUpperFLLimit()
    {
      return $this->UpperFLLimit;
    }

    /**
     * @param int $UpperFLLimit
     * @return Airspace
     */
    public function setUpperFLLimit($UpperFLLimit)
    {
      $this->UpperFLLimit = $UpperFLLimit;
      return $this;
    }

}
