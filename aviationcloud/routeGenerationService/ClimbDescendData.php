<?php

class ClimbDescendData
{

    /**
     * @var Length $DistanceCovered
     */
    protected $DistanceCovered = null;

    /**
     * @var Weight $Fuel
     */
    protected $Fuel = null;

    /**
     * @var Time $Time
     */
    protected $Time = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Length
     */
    public function getDistanceCovered()
    {
      return $this->DistanceCovered;
    }

    /**
     * @param Length $DistanceCovered
     * @return ClimbDescendData
     */
    public function setDistanceCovered($DistanceCovered)
    {
      $this->DistanceCovered = $DistanceCovered;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuel()
    {
      return $this->Fuel;
    }

    /**
     * @param Weight $Fuel
     * @return ClimbDescendData
     */
    public function setFuel($Fuel)
    {
      $this->Fuel = $Fuel;
      return $this;
    }

    /**
     * @return Time
     */
    public function getTime()
    {
      return $this->Time;
    }

    /**
     * @param Time $Time
     * @return ClimbDescendData
     */
    public function setTime($Time)
    {
      $this->Time = $Time;
      return $this;
    }

}
