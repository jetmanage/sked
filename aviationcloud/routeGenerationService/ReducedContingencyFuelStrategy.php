<?php

class ReducedContingencyFuelStrategy extends ContingencyStrategy
{

    /**
     * @var int $ContigencyPercentage
     */
    protected $ContigencyPercentage = null;

    /**
     * @var Airport $RCFAirport
     */
    protected $RCFAirport = null;

    /**
     * @var Airport $RCFAirportAlternate
     */
    protected $RCFAirportAlternate = null;

    /**
     * @var Route $RCFAlternateRoute
     */
    protected $RCFAlternateRoute = null;

    /**
     * @var Route $RCFRoute
     */
    protected $RCFRoute = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getContigencyPercentage()
    {
      return $this->ContigencyPercentage;
    }

    /**
     * @param int $ContigencyPercentage
     * @return ReducedContingencyFuelStrategy
     */
    public function setContigencyPercentage($ContigencyPercentage)
    {
      $this->ContigencyPercentage = $ContigencyPercentage;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getRCFAirport()
    {
      return $this->RCFAirport;
    }

    /**
     * @param Airport $RCFAirport
     * @return ReducedContingencyFuelStrategy
     */
    public function setRCFAirport($RCFAirport)
    {
      $this->RCFAirport = $RCFAirport;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getRCFAirportAlternate()
    {
      return $this->RCFAirportAlternate;
    }

    /**
     * @param Airport $RCFAirportAlternate
     * @return ReducedContingencyFuelStrategy
     */
    public function setRCFAirportAlternate($RCFAirportAlternate)
    {
      $this->RCFAirportAlternate = $RCFAirportAlternate;
      return $this;
    }

    /**
     * @return Route
     */
    public function getRCFAlternateRoute()
    {
      return $this->RCFAlternateRoute;
    }

    /**
     * @param Route $RCFAlternateRoute
     * @return ReducedContingencyFuelStrategy
     */
    public function setRCFAlternateRoute($RCFAlternateRoute)
    {
      $this->RCFAlternateRoute = $RCFAlternateRoute;
      return $this;
    }

    /**
     * @return Route
     */
    public function getRCFRoute()
    {
      return $this->RCFRoute;
    }

    /**
     * @param Route $RCFRoute
     * @return ReducedContingencyFuelStrategy
     */
    public function setRCFRoute($RCFRoute)
    {
      $this->RCFRoute = $RCFRoute;
      return $this;
    }

}
