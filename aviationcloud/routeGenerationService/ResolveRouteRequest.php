<?php

class ResolveRouteRequest extends RequestBase
{

    /**
     * @var RouteResolveParameters $Parameters
     */
    protected $Parameters = null;

    /**
     * @var ArrayOfRoute $Routes
     */
    protected $Routes = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return RouteResolveParameters
     */
    public function getParameters()
    {
      return $this->Parameters;
    }

    /**
     * @param RouteResolveParameters $Parameters
     * @return ResolveRouteRequest
     */
    public function setParameters($Parameters)
    {
      $this->Parameters = $Parameters;
      return $this;
    }

    /**
     * @return ArrayOfRoute
     */
    public function getRoutes()
    {
      return $this->Routes;
    }

    /**
     * @param ArrayOfRoute $Routes
     * @return ResolveRouteRequest
     */
    public function setRoutes($Routes)
    {
      $this->Routes = $Routes;
      return $this;
    }

}
