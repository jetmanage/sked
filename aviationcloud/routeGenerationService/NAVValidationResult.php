<?php

class NAVValidationResult
{

    /**
     * @var boolean $IsNavValid
     */
    protected $IsNavValid = null;

    /**
     * @var ArrayOfNAVValidationError $NAVValidationErrors
     */
    protected $NAVValidationErrors = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getIsNavValid()
    {
      return $this->IsNavValid;
    }

    /**
     * @param boolean $IsNavValid
     * @return NAVValidationResult
     */
    public function setIsNavValid($IsNavValid)
    {
      $this->IsNavValid = $IsNavValid;
      return $this;
    }

    /**
     * @return ArrayOfNAVValidationError
     */
    public function getNAVValidationErrors()
    {
      return $this->NAVValidationErrors;
    }

    /**
     * @param ArrayOfNAVValidationError $NAVValidationErrors
     * @return NAVValidationResult
     */
    public function setNAVValidationErrors($NAVValidationErrors)
    {
      $this->NAVValidationErrors = $NAVValidationErrors;
      return $this;
    }

}
