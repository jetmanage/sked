<?php

class GetRADFlightLevelLimit
{

    /**
     * @var RADFlightLevelLimitRequest $request
     */
    protected $request = null;

    /**
     * @param RADFlightLevelLimitRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return RADFlightLevelLimitRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param RADFlightLevelLimitRequest $request
     * @return GetRADFlightLevelLimit
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
