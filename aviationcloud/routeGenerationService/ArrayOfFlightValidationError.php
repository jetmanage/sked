<?php

class ArrayOfFlightValidationError implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var FlightValidationError[] $FlightValidationError
     */
    protected $FlightValidationError = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FlightValidationError[]
     */
    public function getFlightValidationError()
    {
      return $this->FlightValidationError;
    }

    /**
     * @param FlightValidationError[] $FlightValidationError
     * @return ArrayOfFlightValidationError
     */
    public function setFlightValidationError(array $FlightValidationError = null)
    {
      $this->FlightValidationError = $FlightValidationError;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->FlightValidationError[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return FlightValidationError
     */
    public function offsetGet($offset)
    {
      return $this->FlightValidationError[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param FlightValidationError $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->FlightValidationError[] = $value;
      } else {
        $this->FlightValidationError[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->FlightValidationError[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return FlightValidationError Return the current element
     */
    public function current()
    {
      return current($this->FlightValidationError);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->FlightValidationError);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->FlightValidationError);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->FlightValidationError);
    }

    /**
     * Countable implementation
     *
     * @return FlightValidationError Return count of elements
     */
    public function count()
    {
      return count($this->FlightValidationError);
    }

}
