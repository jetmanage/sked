<?php

class RouteRequest extends RequestBase
{

    /**
     * @var boolean $AutoValidate
     */
    protected $AutoValidate = null;

    /**
     * @var boolean $ExtendedValidation
     */
    protected $ExtendedValidation = null;

    /**
     * @var RouteSpecification $RouteSpecification
     */
    protected $RouteSpecification = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return boolean
     */
    public function getAutoValidate()
    {
      return $this->AutoValidate;
    }

    /**
     * @param boolean $AutoValidate
     * @return RouteRequest
     */
    public function setAutoValidate($AutoValidate)
    {
      $this->AutoValidate = $AutoValidate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExtendedValidation()
    {
      return $this->ExtendedValidation;
    }

    /**
     * @param boolean $ExtendedValidation
     * @return RouteRequest
     */
    public function setExtendedValidation($ExtendedValidation)
    {
      $this->ExtendedValidation = $ExtendedValidation;
      return $this;
    }

    /**
     * @return RouteSpecification
     */
    public function getRouteSpecification()
    {
      return $this->RouteSpecification;
    }

    /**
     * @param RouteSpecification $RouteSpecification
     * @return RouteRequest
     */
    public function setRouteSpecification($RouteSpecification)
    {
      $this->RouteSpecification = $RouteSpecification;
      return $this;
    }

}
