<?php

class ArrayOfFiledAircraftTypeCount implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var FiledAircraftTypeCount[] $FiledAircraftTypeCount
     */
    protected $FiledAircraftTypeCount = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FiledAircraftTypeCount[]
     */
    public function getFiledAircraftTypeCount()
    {
      return $this->FiledAircraftTypeCount;
    }

    /**
     * @param FiledAircraftTypeCount[] $FiledAircraftTypeCount
     * @return ArrayOfFiledAircraftTypeCount
     */
    public function setFiledAircraftTypeCount(array $FiledAircraftTypeCount = null)
    {
      $this->FiledAircraftTypeCount = $FiledAircraftTypeCount;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->FiledAircraftTypeCount[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return FiledAircraftTypeCount
     */
    public function offsetGet($offset)
    {
      return $this->FiledAircraftTypeCount[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param FiledAircraftTypeCount $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->FiledAircraftTypeCount[] = $value;
      } else {
        $this->FiledAircraftTypeCount[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->FiledAircraftTypeCount[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return FiledAircraftTypeCount Return the current element
     */
    public function current()
    {
      return current($this->FiledAircraftTypeCount);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->FiledAircraftTypeCount);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->FiledAircraftTypeCount);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->FiledAircraftTypeCount);
    }

    /**
     * Countable implementation
     *
     * @return FiledAircraftTypeCount Return count of elements
     */
    public function count()
    {
      return count($this->FiledAircraftTypeCount);
    }

}
