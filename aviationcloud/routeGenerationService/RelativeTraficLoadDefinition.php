<?php

class RelativeTraficLoadDefinition extends AircraftTraficLoadDefinition
{

    /**
     * @var float $PercentFilled
     */
    protected $PercentFilled = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getPercentFilled()
    {
      return $this->PercentFilled;
    }

    /**
     * @param float $PercentFilled
     * @return RelativeTraficLoadDefinition
     */
    public function setPercentFilled($PercentFilled)
    {
      $this->PercentFilled = $PercentFilled;
      return $this;
    }

}
