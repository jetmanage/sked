<?php

class CFMUValidationWarning
{

    /**
     * @var string $WarningCode
     */
    protected $WarningCode = null;

    /**
     * @var string $WarningDescription
     */
    protected $WarningDescription = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getWarningCode()
    {
      return $this->WarningCode;
    }

    /**
     * @param string $WarningCode
     * @return CFMUValidationWarning
     */
    public function setWarningCode($WarningCode)
    {
      $this->WarningCode = $WarningCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getWarningDescription()
    {
      return $this->WarningDescription;
    }

    /**
     * @param string $WarningDescription
     * @return CFMUValidationWarning
     */
    public function setWarningDescription($WarningDescription)
    {
      $this->WarningDescription = $WarningDescription;
      return $this;
    }

}
