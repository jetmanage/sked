<?php

class ArrayOfRouteNodeIdentifier implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var RouteNodeIdentifier[] $RouteNodeIdentifier
     */
    protected $RouteNodeIdentifier = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RouteNodeIdentifier[]
     */
    public function getRouteNodeIdentifier()
    {
      return $this->RouteNodeIdentifier;
    }

    /**
     * @param RouteNodeIdentifier[] $RouteNodeIdentifier
     * @return ArrayOfRouteNodeIdentifier
     */
    public function setRouteNodeIdentifier(array $RouteNodeIdentifier = null)
    {
      $this->RouteNodeIdentifier = $RouteNodeIdentifier;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->RouteNodeIdentifier[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return RouteNodeIdentifier
     */
    public function offsetGet($offset)
    {
      return $this->RouteNodeIdentifier[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param RouteNodeIdentifier $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->RouteNodeIdentifier[] = $value;
      } else {
        $this->RouteNodeIdentifier[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->RouteNodeIdentifier[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return RouteNodeIdentifier Return the current element
     */
    public function current()
    {
      return current($this->RouteNodeIdentifier);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->RouteNodeIdentifier);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->RouteNodeIdentifier);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->RouteNodeIdentifier);
    }

    /**
     * Countable implementation
     *
     * @return RouteNodeIdentifier Return count of elements
     */
    public function count()
    {
      return count($this->RouteNodeIdentifier);
    }

}
