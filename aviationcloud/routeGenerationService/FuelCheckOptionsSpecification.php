<?php

class FuelCheckOptionsSpecification
{

    /**
     * @var boolean $EnableFuelChecks
     */
    protected $EnableFuelChecks = null;

    /**
     * @var Length $FuelCheckDistance
     */
    protected $FuelCheckDistance = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getEnableFuelChecks()
    {
      return $this->EnableFuelChecks;
    }

    /**
     * @param boolean $EnableFuelChecks
     * @return FuelCheckOptionsSpecification
     */
    public function setEnableFuelChecks($EnableFuelChecks)
    {
      $this->EnableFuelChecks = $EnableFuelChecks;
      return $this;
    }

    /**
     * @return Length
     */
    public function getFuelCheckDistance()
    {
      return $this->FuelCheckDistance;
    }

    /**
     * @param Length $FuelCheckDistance
     * @return FuelCheckOptionsSpecification
     */
    public function setFuelCheckDistance($FuelCheckDistance)
    {
      $this->FuelCheckDistance = $FuelCheckDistance;
      return $this;
    }

}
