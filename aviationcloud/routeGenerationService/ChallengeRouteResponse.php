<?php

class ChallengeRouteResponse
{

    /**
     * @var RouteChallengeResponse $ChallengeRouteResult
     */
    protected $ChallengeRouteResult = null;

    /**
     * @param RouteChallengeResponse $ChallengeRouteResult
     */
    public function __construct($ChallengeRouteResult)
    {
      $this->ChallengeRouteResult = $ChallengeRouteResult;
    }

    /**
     * @return RouteChallengeResponse
     */
    public function getChallengeRouteResult()
    {
      return $this->ChallengeRouteResult;
    }

    /**
     * @param RouteChallengeResponse $ChallengeRouteResult
     * @return ChallengeRouteResponse
     */
    public function setChallengeRouteResult($ChallengeRouteResult)
    {
      $this->ChallengeRouteResult = $ChallengeRouteResult;
      return $this;
    }

}
