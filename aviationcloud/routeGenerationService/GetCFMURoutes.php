<?php

class GetCFMURoutes
{

    /**
     * @var RouteRequest $routeRequest
     */
    protected $routeRequest = null;

    /**
     * @param RouteRequest $routeRequest
     */
    public function __construct($routeRequest)
    {
      $this->routeRequest = $routeRequest;
    }

    /**
     * @return RouteRequest
     */
    public function getRouteRequest()
    {
      return $this->routeRequest;
    }

    /**
     * @param RouteRequest $routeRequest
     * @return GetCFMURoutes
     */
    public function setRouteRequest($routeRequest)
    {
      $this->routeRequest = $routeRequest;
      return $this;
    }

}
