<?php

class RetrieveStoredRoutes
{

    /**
     * @var StoredRouteRequest $request
     */
    protected $request = null;

    /**
     * @param StoredRouteRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return StoredRouteRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param StoredRouteRequest $request
     * @return RetrieveStoredRoutes
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
