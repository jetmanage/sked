<?php

class FAAPreferedRoute extends RouteSourceInformation
{

    /**
     * @var string $AircraftRestrictipDescription
     */
    protected $AircraftRestrictipDescription = null;

    /**
     * @var string $AltitudeDescription
     */
    protected $AltitudeDescription = null;

    /**
     * @var string $AreaDescription
     */
    protected $AreaDescription = null;

    /**
     * @var string $EffectiveHours
     */
    protected $EffectiveHours = null;

    /**
     * @var FAAPreferedRouteInformationFAAPreferedRouteType $FAARouteType
     */
    protected $FAARouteType = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getAircraftRestrictipDescription()
    {
      return $this->AircraftRestrictipDescription;
    }

    /**
     * @param string $AircraftRestrictipDescription
     * @return FAAPreferedRoute
     */
    public function setAircraftRestrictipDescription($AircraftRestrictipDescription)
    {
      $this->AircraftRestrictipDescription = $AircraftRestrictipDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getAltitudeDescription()
    {
      return $this->AltitudeDescription;
    }

    /**
     * @param string $AltitudeDescription
     * @return FAAPreferedRoute
     */
    public function setAltitudeDescription($AltitudeDescription)
    {
      $this->AltitudeDescription = $AltitudeDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getAreaDescription()
    {
      return $this->AreaDescription;
    }

    /**
     * @param string $AreaDescription
     * @return FAAPreferedRoute
     */
    public function setAreaDescription($AreaDescription)
    {
      $this->AreaDescription = $AreaDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getEffectiveHours()
    {
      return $this->EffectiveHours;
    }

    /**
     * @param string $EffectiveHours
     * @return FAAPreferedRoute
     */
    public function setEffectiveHours($EffectiveHours)
    {
      $this->EffectiveHours = $EffectiveHours;
      return $this;
    }

    /**
     * @return FAAPreferedRouteInformationFAAPreferedRouteType
     */
    public function getFAARouteType()
    {
      return $this->FAARouteType;
    }

    /**
     * @param FAAPreferedRouteInformationFAAPreferedRouteType $FAARouteType
     * @return FAAPreferedRoute
     */
    public function setFAARouteType($FAARouteType)
    {
      $this->FAARouteType = $FAARouteType;
      return $this;
    }

}
