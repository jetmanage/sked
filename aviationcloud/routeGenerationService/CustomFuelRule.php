<?php

class CustomFuelRule extends FlightFuelRules
{

    /**
     * @var Weight $AdditionalFuel
     */
    protected $AdditionalFuel = null;

    /**
     * @var Time $AdditionalTime
     */
    protected $AdditionalTime = null;

    /**
     * @var Weight $AlternateFuel
     */
    protected $AlternateFuel = null;

    /**
     * @var string $AlternateMode
     */
    protected $AlternateMode = null;

    /**
     * @var Weight $ContingencyFuel
     */
    protected $ContingencyFuel = null;

    /**
     * @var float $ContingencyPercentage
     */
    protected $ContingencyPercentage = null;

    /**
     * @var Time $ContingencyTime
     */
    protected $ContingencyTime = null;

    /**
     * @var Weight $FinalReserveFuel
     */
    protected $FinalReserveFuel = null;

    /**
     * @var Time $FinalReserveTime
     */
    protected $FinalReserveTime = null;

    /**
     * @var Weight $HoldingFuel
     */
    protected $HoldingFuel = null;

    /**
     * @var Time $HoldingTime
     */
    protected $HoldingTime = null;

    /**
     * @var boolean $Use1500ftForReserve
     */
    protected $Use1500ftForReserve = null;

    /**
     * @var boolean $UseTODForReserve
     */
    protected $UseTODForReserve = null;

    /**
     * @var boolean $UseTripFuelForContingency
     */
    protected $UseTripFuelForContingency = null;

    /**
     * @var boolean $UseTripTimeForContingency
     */
    protected $UseTripTimeForContingency = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Weight
     */
    public function getAdditionalFuel()
    {
      return $this->AdditionalFuel;
    }

    /**
     * @param Weight $AdditionalFuel
     * @return CustomFuelRule
     */
    public function setAdditionalFuel($AdditionalFuel)
    {
      $this->AdditionalFuel = $AdditionalFuel;
      return $this;
    }

    /**
     * @return Time
     */
    public function getAdditionalTime()
    {
      return $this->AdditionalTime;
    }

    /**
     * @param Time $AdditionalTime
     * @return CustomFuelRule
     */
    public function setAdditionalTime($AdditionalTime)
    {
      $this->AdditionalTime = $AdditionalTime;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getAlternateFuel()
    {
      return $this->AlternateFuel;
    }

    /**
     * @param Weight $AlternateFuel
     * @return CustomFuelRule
     */
    public function setAlternateFuel($AlternateFuel)
    {
      $this->AlternateFuel = $AlternateFuel;
      return $this;
    }

    /**
     * @return string
     */
    public function getAlternateMode()
    {
      return $this->AlternateMode;
    }

    /**
     * @param string $AlternateMode
     * @return CustomFuelRule
     */
    public function setAlternateMode($AlternateMode)
    {
      $this->AlternateMode = $AlternateMode;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getContingencyFuel()
    {
      return $this->ContingencyFuel;
    }

    /**
     * @param Weight $ContingencyFuel
     * @return CustomFuelRule
     */
    public function setContingencyFuel($ContingencyFuel)
    {
      $this->ContingencyFuel = $ContingencyFuel;
      return $this;
    }

    /**
     * @return float
     */
    public function getContingencyPercentage()
    {
      return $this->ContingencyPercentage;
    }

    /**
     * @param float $ContingencyPercentage
     * @return CustomFuelRule
     */
    public function setContingencyPercentage($ContingencyPercentage)
    {
      $this->ContingencyPercentage = $ContingencyPercentage;
      return $this;
    }

    /**
     * @return Time
     */
    public function getContingencyTime()
    {
      return $this->ContingencyTime;
    }

    /**
     * @param Time $ContingencyTime
     * @return CustomFuelRule
     */
    public function setContingencyTime($ContingencyTime)
    {
      $this->ContingencyTime = $ContingencyTime;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFinalReserveFuel()
    {
      return $this->FinalReserveFuel;
    }

    /**
     * @param Weight $FinalReserveFuel
     * @return CustomFuelRule
     */
    public function setFinalReserveFuel($FinalReserveFuel)
    {
      $this->FinalReserveFuel = $FinalReserveFuel;
      return $this;
    }

    /**
     * @return Time
     */
    public function getFinalReserveTime()
    {
      return $this->FinalReserveTime;
    }

    /**
     * @param Time $FinalReserveTime
     * @return CustomFuelRule
     */
    public function setFinalReserveTime($FinalReserveTime)
    {
      $this->FinalReserveTime = $FinalReserveTime;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getHoldingFuel()
    {
      return $this->HoldingFuel;
    }

    /**
     * @param Weight $HoldingFuel
     * @return CustomFuelRule
     */
    public function setHoldingFuel($HoldingFuel)
    {
      $this->HoldingFuel = $HoldingFuel;
      return $this;
    }

    /**
     * @return Time
     */
    public function getHoldingTime()
    {
      return $this->HoldingTime;
    }

    /**
     * @param Time $HoldingTime
     * @return CustomFuelRule
     */
    public function setHoldingTime($HoldingTime)
    {
      $this->HoldingTime = $HoldingTime;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUse1500ftForReserve()
    {
      return $this->Use1500ftForReserve;
    }

    /**
     * @param boolean $Use1500ftForReserve
     * @return CustomFuelRule
     */
    public function setUse1500ftForReserve($Use1500ftForReserve)
    {
      $this->Use1500ftForReserve = $Use1500ftForReserve;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseTODForReserve()
    {
      return $this->UseTODForReserve;
    }

    /**
     * @param boolean $UseTODForReserve
     * @return CustomFuelRule
     */
    public function setUseTODForReserve($UseTODForReserve)
    {
      $this->UseTODForReserve = $UseTODForReserve;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseTripFuelForContingency()
    {
      return $this->UseTripFuelForContingency;
    }

    /**
     * @param boolean $UseTripFuelForContingency
     * @return CustomFuelRule
     */
    public function setUseTripFuelForContingency($UseTripFuelForContingency)
    {
      $this->UseTripFuelForContingency = $UseTripFuelForContingency;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseTripTimeForContingency()
    {
      return $this->UseTripTimeForContingency;
    }

    /**
     * @param boolean $UseTripTimeForContingency
     * @return CustomFuelRule
     */
    public function setUseTripTimeForContingency($UseTripTimeForContingency)
    {
      $this->UseTripTimeForContingency = $UseTripTimeForContingency;
      return $this;
    }

}
