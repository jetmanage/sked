<?php

class VerticalOptimizeRoute
{

    /**
     * @var VerticalOptimizationRequest $request
     */
    protected $request = null;

    /**
     * @param VerticalOptimizationRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return VerticalOptimizationRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param VerticalOptimizationRequest $request
     * @return VerticalOptimizeRoute
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
