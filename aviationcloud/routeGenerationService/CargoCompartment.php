<?php

class CargoCompartment extends AircraftStructureUnit
{

    /**
     * @var Weight $MaxCargoWeight
     */
    protected $MaxCargoWeight = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Weight
     */
    public function getMaxCargoWeight()
    {
      return $this->MaxCargoWeight;
    }

    /**
     * @param Weight $MaxCargoWeight
     * @return CargoCompartment
     */
    public function setMaxCargoWeight($MaxCargoWeight)
    {
      $this->MaxCargoWeight = $MaxCargoWeight;
      return $this;
    }

}
