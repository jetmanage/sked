<?php

class ArrayOfAirspace implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Airspace[] $Airspace
     */
    protected $Airspace = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Airspace[]
     */
    public function getAirspace()
    {
      return $this->Airspace;
    }

    /**
     * @param Airspace[] $Airspace
     * @return ArrayOfAirspace
     */
    public function setAirspace(array $Airspace = null)
    {
      $this->Airspace = $Airspace;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Airspace[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Airspace
     */
    public function offsetGet($offset)
    {
      return $this->Airspace[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Airspace $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->Airspace[] = $value;
      } else {
        $this->Airspace[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Airspace[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Airspace Return the current element
     */
    public function current()
    {
      return current($this->Airspace);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Airspace);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Airspace);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Airspace);
    }

    /**
     * Countable implementation
     *
     * @return Airspace Return count of elements
     */
    public function count()
    {
      return count($this->Airspace);
    }

}
