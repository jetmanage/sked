<?php

class GenerateRoutes
{

    /**
     * @var RouteRequest $routeRequest
     */
    protected $routeRequest = null;

    /**
     * @param RouteRequest $routeRequest
     */
    public function __construct($routeRequest)
    {
      $this->routeRequest = $routeRequest;
    }

    /**
     * @return RouteRequest
     */
    public function getRouteRequest()
    {
      return $this->routeRequest;
    }

    /**
     * @param RouteRequest $routeRequest
     * @return GenerateRoutes
     */
    public function setRouteRequest($routeRequest)
    {
      $this->routeRequest = $routeRequest;
      return $this;
    }

}
