<?php

class CalculateRouteMetricsResponse
{

    /**
     * @var RouteMetricsResponse $CalculateRouteMetricsResult
     */
    protected $CalculateRouteMetricsResult = null;

    /**
     * @param RouteMetricsResponse $CalculateRouteMetricsResult
     */
    public function __construct($CalculateRouteMetricsResult)
    {
      $this->CalculateRouteMetricsResult = $CalculateRouteMetricsResult;
    }

    /**
     * @return RouteMetricsResponse
     */
    public function getCalculateRouteMetricsResult()
    {
      return $this->CalculateRouteMetricsResult;
    }

    /**
     * @param RouteMetricsResponse $CalculateRouteMetricsResult
     * @return CalculateRouteMetricsResponse
     */
    public function setCalculateRouteMetricsResult($CalculateRouteMetricsResult)
    {
      $this->CalculateRouteMetricsResult = $CalculateRouteMetricsResult;
      return $this;
    }

}
