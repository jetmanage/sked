<?php

class NAVValidationErrorCode
{
    const __default = 'SyntaxError';
    const SyntaxError = 'SyntaxError';
    const UnkwownNAVAID = 'UnkwownNAVAID';
    const UnkwownAirway = 'UnkwownAirway';
    const AirwayAltitudeRestriction = 'AirwayAltitudeRestriction';
    const NonExistingAirwayConnection = 'NonExistingAirwayConnection';
    const UnkwownSID = 'UnkwownSID';
    const UnkwownSTAR = 'UnkwownSTAR';


}
