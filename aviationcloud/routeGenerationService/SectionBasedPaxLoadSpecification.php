<?php

class SectionBasedPaxLoadSpecification extends PaxLoadSpecification
{

    /**
     * @var ArrayOfPaxSectionLoad $SectionLoads
     */
    protected $SectionLoads = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ArrayOfPaxSectionLoad
     */
    public function getSectionLoads()
    {
      return $this->SectionLoads;
    }

    /**
     * @param ArrayOfPaxSectionLoad $SectionLoads
     * @return SectionBasedPaxLoadSpecification
     */
    public function setSectionLoads($SectionLoads)
    {
      $this->SectionLoads = $SectionLoads;
      return $this;
    }

}
