<?php

class SpecifiedPaxTypeLoad extends PaxLoad
{

    /**
     * @var int $NumberOfChildren
     */
    protected $NumberOfChildren = null;

    /**
     * @var int $NumberOfFemales
     */
    protected $NumberOfFemales = null;

    /**
     * @var int $NumberOfInfants
     */
    protected $NumberOfInfants = null;

    /**
     * @var int $NumberOfMales
     */
    protected $NumberOfMales = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getNumberOfChildren()
    {
      return $this->NumberOfChildren;
    }

    /**
     * @param int $NumberOfChildren
     * @return SpecifiedPaxTypeLoad
     */
    public function setNumberOfChildren($NumberOfChildren)
    {
      $this->NumberOfChildren = $NumberOfChildren;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfFemales()
    {
      return $this->NumberOfFemales;
    }

    /**
     * @param int $NumberOfFemales
     * @return SpecifiedPaxTypeLoad
     */
    public function setNumberOfFemales($NumberOfFemales)
    {
      $this->NumberOfFemales = $NumberOfFemales;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfInfants()
    {
      return $this->NumberOfInfants;
    }

    /**
     * @param int $NumberOfInfants
     * @return SpecifiedPaxTypeLoad
     */
    public function setNumberOfInfants($NumberOfInfants)
    {
      $this->NumberOfInfants = $NumberOfInfants;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfMales()
    {
      return $this->NumberOfMales;
    }

    /**
     * @param int $NumberOfMales
     * @return SpecifiedPaxTypeLoad
     */
    public function setNumberOfMales($NumberOfMales)
    {
      $this->NumberOfMales = $NumberOfMales;
      return $this;
    }

}
