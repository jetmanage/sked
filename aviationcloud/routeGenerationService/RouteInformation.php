<?php

class RouteInformation
{

    /**
     * @var EstimatedFlighRoutetInformation $FlightEstimationData
     */
    protected $FlightEstimationData = null;

    /**
     * @var RouteIntersectionInformation $IntersectionInformation
     */
    protected $IntersectionInformation = null;

    /**
     * @var Route $Route
     */
    protected $Route = null;

    /**
     * @var RouteSourceInformation $Source
     */
    protected $Source = null;

    /**
     * @var RouteValidationInformation $ValidationInformation
     */
    protected $ValidationInformation = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return EstimatedFlighRoutetInformation
     */
    public function getFlightEstimationData()
    {
      return $this->FlightEstimationData;
    }

    /**
     * @param EstimatedFlighRoutetInformation $FlightEstimationData
     * @return RouteInformation
     */
    public function setFlightEstimationData($FlightEstimationData)
    {
      $this->FlightEstimationData = $FlightEstimationData;
      return $this;
    }

    /**
     * @return RouteIntersectionInformation
     */
    public function getIntersectionInformation()
    {
      return $this->IntersectionInformation;
    }

    /**
     * @param RouteIntersectionInformation $IntersectionInformation
     * @return RouteInformation
     */
    public function setIntersectionInformation($IntersectionInformation)
    {
      $this->IntersectionInformation = $IntersectionInformation;
      return $this;
    }

    /**
     * @return Route
     */
    public function getRoute()
    {
      return $this->Route;
    }

    /**
     * @param Route $Route
     * @return RouteInformation
     */
    public function setRoute($Route)
    {
      $this->Route = $Route;
      return $this;
    }

    /**
     * @return RouteSourceInformation
     */
    public function getSource()
    {
      return $this->Source;
    }

    /**
     * @param RouteSourceInformation $Source
     * @return RouteInformation
     */
    public function setSource($Source)
    {
      $this->Source = $Source;
      return $this;
    }

    /**
     * @return RouteValidationInformation
     */
    public function getValidationInformation()
    {
      return $this->ValidationInformation;
    }

    /**
     * @param RouteValidationInformation $ValidationInformation
     * @return RouteInformation
     */
    public function setValidationInformation($ValidationInformation)
    {
      $this->ValidationInformation = $ValidationInformation;
      return $this;
    }

}
