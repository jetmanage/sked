<?php

class DirectOptimizationSettings
{

    /**
     * @var float $AirwayAffinity
     */
    protected $AirwayAffinity = null;

    /**
     * @var boolean $UseAirfieldDirects
     */
    protected $UseAirfieldDirects = null;

    /**
     * @var boolean $UseArrivalAirfieldDirects
     */
    protected $UseArrivalAirfieldDirects = null;

    /**
     * @var boolean $UseDepartureAirfieldDirects
     */
    protected $UseDepartureAirfieldDirects = null;

    /**
     * @var boolean $UseEnrouteDirects
     */
    protected $UseEnrouteDirects = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getAirwayAffinity()
    {
      return $this->AirwayAffinity;
    }

    /**
     * @param float $AirwayAffinity
     * @return DirectOptimizationSettings
     */
    public function setAirwayAffinity($AirwayAffinity)
    {
      $this->AirwayAffinity = $AirwayAffinity;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseAirfieldDirects()
    {
      return $this->UseAirfieldDirects;
    }

    /**
     * @param boolean $UseAirfieldDirects
     * @return DirectOptimizationSettings
     */
    public function setUseAirfieldDirects($UseAirfieldDirects)
    {
      $this->UseAirfieldDirects = $UseAirfieldDirects;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseArrivalAirfieldDirects()
    {
      return $this->UseArrivalAirfieldDirects;
    }

    /**
     * @param boolean $UseArrivalAirfieldDirects
     * @return DirectOptimizationSettings
     */
    public function setUseArrivalAirfieldDirects($UseArrivalAirfieldDirects)
    {
      $this->UseArrivalAirfieldDirects = $UseArrivalAirfieldDirects;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseDepartureAirfieldDirects()
    {
      return $this->UseDepartureAirfieldDirects;
    }

    /**
     * @param boolean $UseDepartureAirfieldDirects
     * @return DirectOptimizationSettings
     */
    public function setUseDepartureAirfieldDirects($UseDepartureAirfieldDirects)
    {
      $this->UseDepartureAirfieldDirects = $UseDepartureAirfieldDirects;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseEnrouteDirects()
    {
      return $this->UseEnrouteDirects;
    }

    /**
     * @param boolean $UseEnrouteDirects
     * @return DirectOptimizationSettings
     */
    public function setUseEnrouteDirects($UseEnrouteDirects)
    {
      $this->UseEnrouteDirects = $UseEnrouteDirects;
      return $this;
    }

}
