<?php

class RouteMetricsRequest extends RequestBase
{

    /**
     * @var RouteEstimationMetricsSpecification $Estimation
     */
    protected $Estimation = null;

    /**
     * @var FlightSpecification $FlightSpecificaion
     */
    protected $FlightSpecificaion = null;

    /**
     * @var RouteIntersectionMetricsSpecification $Intersection
     */
    protected $Intersection = null;

    /**
     * @var ArrayOfRoute $Routes
     */
    protected $Routes = null;

    /**
     * @var RouteValidationMetricsSpecification $Validation
     */
    protected $Validation = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return RouteEstimationMetricsSpecification
     */
    public function getEstimation()
    {
      return $this->Estimation;
    }

    /**
     * @param RouteEstimationMetricsSpecification $Estimation
     * @return RouteMetricsRequest
     */
    public function setEstimation($Estimation)
    {
      $this->Estimation = $Estimation;
      return $this;
    }

    /**
     * @return FlightSpecification
     */
    public function getFlightSpecificaion()
    {
      return $this->FlightSpecificaion;
    }

    /**
     * @param FlightSpecification $FlightSpecificaion
     * @return RouteMetricsRequest
     */
    public function setFlightSpecificaion($FlightSpecificaion)
    {
      $this->FlightSpecificaion = $FlightSpecificaion;
      return $this;
    }

    /**
     * @return RouteIntersectionMetricsSpecification
     */
    public function getIntersection()
    {
      return $this->Intersection;
    }

    /**
     * @param RouteIntersectionMetricsSpecification $Intersection
     * @return RouteMetricsRequest
     */
    public function setIntersection($Intersection)
    {
      $this->Intersection = $Intersection;
      return $this;
    }

    /**
     * @return ArrayOfRoute
     */
    public function getRoutes()
    {
      return $this->Routes;
    }

    /**
     * @param ArrayOfRoute $Routes
     * @return RouteMetricsRequest
     */
    public function setRoutes($Routes)
    {
      $this->Routes = $Routes;
      return $this;
    }

    /**
     * @return RouteValidationMetricsSpecification
     */
    public function getValidation()
    {
      return $this->Validation;
    }

    /**
     * @param RouteValidationMetricsSpecification $Validation
     * @return RouteMetricsRequest
     */
    public function setValidation($Validation)
    {
      $this->Validation = $Validation;
      return $this;
    }

}
