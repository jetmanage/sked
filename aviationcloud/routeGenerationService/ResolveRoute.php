<?php

class ResolveRoute
{

    /**
     * @var ResolveRouteRequest $request
     */
    protected $request = null;

    /**
     * @param ResolveRouteRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return ResolveRouteRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param ResolveRouteRequest $request
     * @return ResolveRoute
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
