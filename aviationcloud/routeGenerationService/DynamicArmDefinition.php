<?php

class DynamicArmDefinition extends ArmDefinition
{

    /**
     * @var ArrayOfWeightDependentArm $Arms
     */
    protected $Arms = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfWeightDependentArm
     */
    public function getArms()
    {
      return $this->Arms;
    }

    /**
     * @param ArrayOfWeightDependentArm $Arms
     * @return DynamicArmDefinition
     */
    public function setArms($Arms)
    {
      $this->Arms = $Arms;
      return $this;
    }

}
