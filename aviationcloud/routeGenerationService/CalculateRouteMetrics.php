<?php

class CalculateRouteMetrics
{

    /**
     * @var RouteMetricsRequest $request
     */
    protected $request = null;

    /**
     * @param RouteMetricsRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return RouteMetricsRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param RouteMetricsRequest $request
     * @return CalculateRouteMetrics
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
