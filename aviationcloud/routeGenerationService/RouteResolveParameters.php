<?php

class RouteResolveParameters
{

    /**
     * @var boolean $UseShortestSIDSTAR
     */
    protected $UseShortestSIDSTAR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getUseShortestSIDSTAR()
    {
      return $this->UseShortestSIDSTAR;
    }

    /**
     * @param boolean $UseShortestSIDSTAR
     * @return RouteResolveParameters
     */
    public function setUseShortestSIDSTAR($UseShortestSIDSTAR)
    {
      $this->UseShortestSIDSTAR = $UseShortestSIDSTAR;
      return $this;
    }

}
