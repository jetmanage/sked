<?php

class IcingSeverity
{
    const __default = 'None';
    const None = 'None';
    const Light = 'Light';
    const Moderate = 'Moderate';
    const Severe = 'Severe';
    const Trace = 'Trace';
    const Heavy = 'Heavy';


}
