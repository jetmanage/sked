<?php

class RouteChallengeResponse
{

    /**
     * @var boolean $FoundBetterRoute
     */
    protected $FoundBetterRoute = null;

    /**
     * @var boolean $ImprovementCriteriaMet
     */
    protected $ImprovementCriteriaMet = null;

    /**
     * @var float $ImprovementValue
     */
    protected $ImprovementValue = null;

    /**
     * @var RouteInformation $NewFlight
     */
    protected $NewFlight = null;

    /**
     * @var RouteInformation $OriginalFlight
     */
    protected $OriginalFlight = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getFoundBetterRoute()
    {
      return $this->FoundBetterRoute;
    }

    /**
     * @param boolean $FoundBetterRoute
     * @return RouteChallengeResponse
     */
    public function setFoundBetterRoute($FoundBetterRoute)
    {
      $this->FoundBetterRoute = $FoundBetterRoute;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getImprovementCriteriaMet()
    {
      return $this->ImprovementCriteriaMet;
    }

    /**
     * @param boolean $ImprovementCriteriaMet
     * @return RouteChallengeResponse
     */
    public function setImprovementCriteriaMet($ImprovementCriteriaMet)
    {
      $this->ImprovementCriteriaMet = $ImprovementCriteriaMet;
      return $this;
    }

    /**
     * @return float
     */
    public function getImprovementValue()
    {
      return $this->ImprovementValue;
    }

    /**
     * @param float $ImprovementValue
     * @return RouteChallengeResponse
     */
    public function setImprovementValue($ImprovementValue)
    {
      $this->ImprovementValue = $ImprovementValue;
      return $this;
    }

    /**
     * @return RouteInformation
     */
    public function getNewFlight()
    {
      return $this->NewFlight;
    }

    /**
     * @param RouteInformation $NewFlight
     * @return RouteChallengeResponse
     */
    public function setNewFlight($NewFlight)
    {
      $this->NewFlight = $NewFlight;
      return $this;
    }

    /**
     * @return RouteInformation
     */
    public function getOriginalFlight()
    {
      return $this->OriginalFlight;
    }

    /**
     * @param RouteInformation $OriginalFlight
     * @return RouteChallengeResponse
     */
    public function setOriginalFlight($OriginalFlight)
    {
      $this->OriginalFlight = $OriginalFlight;
      return $this;
    }

}
