<?php

class RecentlyFiledRouteExtended extends RouteSourceInformation
{

    /**
     * @var ArrayOfFiledAircraftTypeCount $AircraftTypes
     */
    protected $AircraftTypes = null;

    /**
     * @var \DateTime $LastDepartureTime
     */
    protected $LastDepartureTime = null;

    /**
     * @var int $MaxFiledFlightLevel
     */
    protected $MaxFiledFlightLevel = null;

    /**
     * @var int $MinFiledFlightLevel
     */
    protected $MinFiledFlightLevel = null;

    /**
     * @var int $NumberOfFilings
     */
    protected $NumberOfFilings = null;

    /**
     * @var int $NumberOfFilingsPast30Days
     */
    protected $NumberOfFilingsPast30Days = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ArrayOfFiledAircraftTypeCount
     */
    public function getAircraftTypes()
    {
      return $this->AircraftTypes;
    }

    /**
     * @param ArrayOfFiledAircraftTypeCount $AircraftTypes
     * @return RecentlyFiledRouteExtended
     */
    public function setAircraftTypes($AircraftTypes)
    {
      $this->AircraftTypes = $AircraftTypes;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastDepartureTime()
    {
      if ($this->LastDepartureTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->LastDepartureTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $LastDepartureTime
     * @return RecentlyFiledRouteExtended
     */
    public function setLastDepartureTime(\DateTime $LastDepartureTime = null)
    {
      if ($LastDepartureTime == null) {
       $this->LastDepartureTime = null;
      } else {
        $this->LastDepartureTime = $LastDepartureTime->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxFiledFlightLevel()
    {
      return $this->MaxFiledFlightLevel;
    }

    /**
     * @param int $MaxFiledFlightLevel
     * @return RecentlyFiledRouteExtended
     */
    public function setMaxFiledFlightLevel($MaxFiledFlightLevel)
    {
      $this->MaxFiledFlightLevel = $MaxFiledFlightLevel;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinFiledFlightLevel()
    {
      return $this->MinFiledFlightLevel;
    }

    /**
     * @param int $MinFiledFlightLevel
     * @return RecentlyFiledRouteExtended
     */
    public function setMinFiledFlightLevel($MinFiledFlightLevel)
    {
      $this->MinFiledFlightLevel = $MinFiledFlightLevel;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfFilings()
    {
      return $this->NumberOfFilings;
    }

    /**
     * @param int $NumberOfFilings
     * @return RecentlyFiledRouteExtended
     */
    public function setNumberOfFilings($NumberOfFilings)
    {
      $this->NumberOfFilings = $NumberOfFilings;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfFilingsPast30Days()
    {
      return $this->NumberOfFilingsPast30Days;
    }

    /**
     * @param int $NumberOfFilingsPast30Days
     * @return RecentlyFiledRouteExtended
     */
    public function setNumberOfFilingsPast30Days($NumberOfFilingsPast30Days)
    {
      $this->NumberOfFilingsPast30Days = $NumberOfFilingsPast30Days;
      return $this;
    }

}
