<?php

class GetRADFlightLevelLimitResponse
{

    /**
     * @var RADCityPairLevelCapping $GetRADFlightLevelLimitResult
     */
    protected $GetRADFlightLevelLimitResult = null;

    /**
     * @param RADCityPairLevelCapping $GetRADFlightLevelLimitResult
     */
    public function __construct($GetRADFlightLevelLimitResult)
    {
      $this->GetRADFlightLevelLimitResult = $GetRADFlightLevelLimitResult;
    }

    /**
     * @return RADCityPairLevelCapping
     */
    public function getGetRADFlightLevelLimitResult()
    {
      return $this->GetRADFlightLevelLimitResult;
    }

    /**
     * @param RADCityPairLevelCapping $GetRADFlightLevelLimitResult
     * @return GetRADFlightLevelLimitResponse
     */
    public function setGetRADFlightLevelLimitResult($GetRADFlightLevelLimitResult)
    {
      $this->GetRADFlightLevelLimitResult = $GetRADFlightLevelLimitResult;
      return $this;
    }

}
