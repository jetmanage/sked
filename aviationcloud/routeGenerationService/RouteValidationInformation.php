<?php

class RouteValidationInformation
{

    /**
     * @var CFMUValidationResult $EUROCONTROLValidationResults
     */
    protected $EUROCONTROLValidationResults = null;

    /**
     * @var boolean $HasBeenEUROCONTROLValidated
     */
    protected $HasBeenEUROCONTROLValidated = null;

    /**
     * @var boolean $HasBeenNavValidated
     */
    protected $HasBeenNavValidated = null;

    /**
     * @var NAVValidationResult $NAVValidationResult
     */
    protected $NAVValidationResult = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CFMUValidationResult
     */
    public function getEUROCONTROLValidationResults()
    {
      return $this->EUROCONTROLValidationResults;
    }

    /**
     * @param CFMUValidationResult $EUROCONTROLValidationResults
     * @return RouteValidationInformation
     */
    public function setEUROCONTROLValidationResults($EUROCONTROLValidationResults)
    {
      $this->EUROCONTROLValidationResults = $EUROCONTROLValidationResults;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHasBeenEUROCONTROLValidated()
    {
      return $this->HasBeenEUROCONTROLValidated;
    }

    /**
     * @param boolean $HasBeenEUROCONTROLValidated
     * @return RouteValidationInformation
     */
    public function setHasBeenEUROCONTROLValidated($HasBeenEUROCONTROLValidated)
    {
      $this->HasBeenEUROCONTROLValidated = $HasBeenEUROCONTROLValidated;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHasBeenNavValidated()
    {
      return $this->HasBeenNavValidated;
    }

    /**
     * @param boolean $HasBeenNavValidated
     * @return RouteValidationInformation
     */
    public function setHasBeenNavValidated($HasBeenNavValidated)
    {
      $this->HasBeenNavValidated = $HasBeenNavValidated;
      return $this;
    }

    /**
     * @return NAVValidationResult
     */
    public function getNAVValidationResult()
    {
      return $this->NAVValidationResult;
    }

    /**
     * @param NAVValidationResult $NAVValidationResult
     * @return RouteValidationInformation
     */
    public function setNAVValidationResult($NAVValidationResult)
    {
      $this->NAVValidationResult = $NAVValidationResult;
      return $this;
    }

}
