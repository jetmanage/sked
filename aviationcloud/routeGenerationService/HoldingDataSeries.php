<?php

class HoldingDataSeries extends PerformanceDataSeries
{

    /**
     * @var ArrayOfHoldingDataPoint $PerformanceDataPoints
     */
    protected $PerformanceDataPoints = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ArrayOfHoldingDataPoint
     */
    public function getPerformanceDataPoints()
    {
      return $this->PerformanceDataPoints;
    }

    /**
     * @param ArrayOfHoldingDataPoint $PerformanceDataPoints
     * @return HoldingDataSeries
     */
    public function setPerformanceDataPoints($PerformanceDataPoints)
    {
      $this->PerformanceDataPoints = $PerformanceDataPoints;
      return $this;
    }

}
