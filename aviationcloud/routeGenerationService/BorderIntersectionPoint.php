<?php

class BorderIntersectionPoint extends IntersectionPoint
{

    /**
     * @var Airspace $Entry
     */
    protected $Entry = null;

    /**
     * @var Airspace $Exit
     */
    protected $Exit = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Airspace
     */
    public function getEntry()
    {
      return $this->Entry;
    }

    /**
     * @param Airspace $Entry
     * @return BorderIntersectionPoint
     */
    public function setEntry($Entry)
    {
      $this->Entry = $Entry;
      return $this;
    }

    /**
     * @return Airspace
     */
    public function getExit()
    {
      return $this->Exit;
    }

    /**
     * @param Airspace $Exit
     * @return BorderIntersectionPoint
     */
    public function setExit($Exit)
    {
      $this->Exit = $Exit;
      return $this;
    }

}
