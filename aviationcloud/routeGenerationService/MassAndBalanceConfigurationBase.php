<?php

class MassAndBalanceConfigurationBase extends MassAndBalanceConfiguration
{

    /**
     * @var Weight $DryMass
     */
    protected $DryMass = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Weight
     */
    public function getDryMass()
    {
      return $this->DryMass;
    }

    /**
     * @param Weight $DryMass
     * @return MassAndBalanceConfigurationBase
     */
    public function setDryMass($DryMass)
    {
      $this->DryMass = $DryMass;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return MassAndBalanceConfigurationBase
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

}
