<?php

class VerticalOptimizationResult
{

    /**
     * @var Route $OptimizedRoute
     */
    protected $OptimizedRoute = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Route
     */
    public function getOptimizedRoute()
    {
      return $this->OptimizedRoute;
    }

    /**
     * @param Route $OptimizedRoute
     * @return VerticalOptimizationResult
     */
    public function setOptimizedRoute($OptimizedRoute)
    {
      $this->OptimizedRoute = $OptimizedRoute;
      return $this;
    }

}
