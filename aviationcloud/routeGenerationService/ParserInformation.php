<?php

class ParserInformation
{

    /**
     * @var string $ParsedToken
     */
    protected $ParsedToken = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getParsedToken()
    {
      return $this->ParsedToken;
    }

    /**
     * @param string $ParsedToken
     * @return ParserInformation
     */
    public function setParsedToken($ParsedToken)
    {
      $this->ParsedToken = $ParsedToken;
      return $this;
    }

}
