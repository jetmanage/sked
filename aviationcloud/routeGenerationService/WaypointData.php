<?php

class WaypointData
{

    /**
     * @var string $CountryName
     */
    protected $CountryName = null;

    /**
     * @var string $FIR
     */
    protected $FIR = null;

    /**
     * @var string $FIRName
     */
    protected $FIRName = null;

    /**
     * @var string $FixType
     */
    protected $FixType = null;

    /**
     * @var float $GridMORA
     */
    protected $GridMORA = null;

    /**
     * @var string $IcaoAreaCode
     */
    protected $IcaoAreaCode = null;

    /**
     * @var boolean $IsVirtual
     */
    protected $IsVirtual = null;

    /**
     * @var float $Latitude
     */
    protected $Latitude = null;

    /**
     * @var float $Longitude
     */
    protected $Longitude = null;

    /**
     * @var float $Variation
     */
    protected $Variation = null;

    /**
     * @var float $WaypointFrequency
     */
    protected $WaypointFrequency = null;

    /**
     * @var string $WaypointId
     */
    protected $WaypointId = null;

    /**
     * @var string $WaypointName
     */
    protected $WaypointName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCountryName()
    {
      return $this->CountryName;
    }

    /**
     * @param string $CountryName
     * @return WaypointData
     */
    public function setCountryName($CountryName)
    {
      $this->CountryName = $CountryName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFIR()
    {
      return $this->FIR;
    }

    /**
     * @param string $FIR
     * @return WaypointData
     */
    public function setFIR($FIR)
    {
      $this->FIR = $FIR;
      return $this;
    }

    /**
     * @return string
     */
    public function getFIRName()
    {
      return $this->FIRName;
    }

    /**
     * @param string $FIRName
     * @return WaypointData
     */
    public function setFIRName($FIRName)
    {
      $this->FIRName = $FIRName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFixType()
    {
      return $this->FixType;
    }

    /**
     * @param string $FixType
     * @return WaypointData
     */
    public function setFixType($FixType)
    {
      $this->FixType = $FixType;
      return $this;
    }

    /**
     * @return float
     */
    public function getGridMORA()
    {
      return $this->GridMORA;
    }

    /**
     * @param float $GridMORA
     * @return WaypointData
     */
    public function setGridMORA($GridMORA)
    {
      $this->GridMORA = $GridMORA;
      return $this;
    }

    /**
     * @return string
     */
    public function getIcaoAreaCode()
    {
      return $this->IcaoAreaCode;
    }

    /**
     * @param string $IcaoAreaCode
     * @return WaypointData
     */
    public function setIcaoAreaCode($IcaoAreaCode)
    {
      $this->IcaoAreaCode = $IcaoAreaCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsVirtual()
    {
      return $this->IsVirtual;
    }

    /**
     * @param boolean $IsVirtual
     * @return WaypointData
     */
    public function setIsVirtual($IsVirtual)
    {
      $this->IsVirtual = $IsVirtual;
      return $this;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
      return $this->Latitude;
    }

    /**
     * @param float $Latitude
     * @return WaypointData
     */
    public function setLatitude($Latitude)
    {
      $this->Latitude = $Latitude;
      return $this;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
      return $this->Longitude;
    }

    /**
     * @param float $Longitude
     * @return WaypointData
     */
    public function setLongitude($Longitude)
    {
      $this->Longitude = $Longitude;
      return $this;
    }

    /**
     * @return float
     */
    public function getVariation()
    {
      return $this->Variation;
    }

    /**
     * @param float $Variation
     * @return WaypointData
     */
    public function setVariation($Variation)
    {
      $this->Variation = $Variation;
      return $this;
    }

    /**
     * @return float
     */
    public function getWaypointFrequency()
    {
      return $this->WaypointFrequency;
    }

    /**
     * @param float $WaypointFrequency
     * @return WaypointData
     */
    public function setWaypointFrequency($WaypointFrequency)
    {
      $this->WaypointFrequency = $WaypointFrequency;
      return $this;
    }

    /**
     * @return string
     */
    public function getWaypointId()
    {
      return $this->WaypointId;
    }

    /**
     * @param string $WaypointId
     * @return WaypointData
     */
    public function setWaypointId($WaypointId)
    {
      $this->WaypointId = $WaypointId;
      return $this;
    }

    /**
     * @return string
     */
    public function getWaypointName()
    {
      return $this->WaypointName;
    }

    /**
     * @param string $WaypointName
     * @return WaypointData
     */
    public function setWaypointName($WaypointName)
    {
      $this->WaypointName = $WaypointName;
      return $this;
    }

}
