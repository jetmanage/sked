<?php

class AircraftType
{

    /**
     * @var string $AircraftICAOId
     */
    protected $AircraftICAOId = null;

    /**
     * @var string $AircraftMake
     */
    protected $AircraftMake = null;

    /**
     * @var string $AircraftTypeName
     */
    protected $AircraftTypeName = null;

    /**
     * @var string $AircraftUUID
     */
    protected $AircraftUUID = null;

    /**
     * @var int $ID
     */
    protected $ID = null;

    /**
     * @var ATCWakeTurbulanceCategory $WakeTurbulanceCategory
     */
    protected $WakeTurbulanceCategory = null;

    /**
     * @var AircraftFuelType $FuelType
     */
    protected $FuelType = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAircraftICAOId()
    {
      return $this->AircraftICAOId;
    }

    /**
     * @param string $AircraftICAOId
     * @return AircraftType
     */
    public function setAircraftICAOId($AircraftICAOId)
    {
      $this->AircraftICAOId = $AircraftICAOId;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftMake()
    {
      return $this->AircraftMake;
    }

    /**
     * @param string $AircraftMake
     * @return AircraftType
     */
    public function setAircraftMake($AircraftMake)
    {
      $this->AircraftMake = $AircraftMake;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftTypeName()
    {
      return $this->AircraftTypeName;
    }

    /**
     * @param string $AircraftTypeName
     * @return AircraftType
     */
    public function setAircraftTypeName($AircraftTypeName)
    {
      $this->AircraftTypeName = $AircraftTypeName;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftUUID()
    {
      return $this->AircraftUUID;
    }

    /**
     * @param string $AircraftUUID
     * @return AircraftType
     */
    public function setAircraftUUID($AircraftUUID)
    {
      $this->AircraftUUID = $AircraftUUID;
      return $this;
    }

    /**
     * @return int
     */
    public function getID()
    {
      return $this->ID;
    }

    /**
     * @param int $ID
     * @return AircraftType
     */
    public function setID($ID)
    {
      $this->ID = $ID;
      return $this;
    }

    /**
     * @return ATCWakeTurbulanceCategory
     */
    public function getWakeTurbulanceCategory()
    {
      return $this->WakeTurbulanceCategory;
    }

    /**
     * @param ATCWakeTurbulanceCategory $WakeTurbulanceCategory
     * @return AircraftType
     */
    public function setWakeTurbulanceCategory($WakeTurbulanceCategory)
    {
      $this->WakeTurbulanceCategory = $WakeTurbulanceCategory;
      return $this;
    }

    /**
     * @return AircraftFuelType
     */
    public function getFuelType()
    {
      return $this->FuelType;
    }

    /**
     * @param AircraftFuelType $FuelType
     * @return AircraftType
     */
    public function setFuelType($FuelType)
    {
      $this->FuelType = $FuelType;
      return $this;
    }

}
