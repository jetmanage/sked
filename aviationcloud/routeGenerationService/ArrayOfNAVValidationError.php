<?php

class ArrayOfNAVValidationError implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var NAVValidationError[] $NAVValidationError
     */
    protected $NAVValidationError = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return NAVValidationError[]
     */
    public function getNAVValidationError()
    {
      return $this->NAVValidationError;
    }

    /**
     * @param NAVValidationError[] $NAVValidationError
     * @return ArrayOfNAVValidationError
     */
    public function setNAVValidationError(array $NAVValidationError = null)
    {
      $this->NAVValidationError = $NAVValidationError;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->NAVValidationError[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return NAVValidationError
     */
    public function offsetGet($offset)
    {
      return $this->NAVValidationError[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param NAVValidationError $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->NAVValidationError[] = $value;
      } else {
        $this->NAVValidationError[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->NAVValidationError[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return NAVValidationError Return the current element
     */
    public function current()
    {
      return current($this->NAVValidationError);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->NAVValidationError);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->NAVValidationError);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->NAVValidationError);
    }

    /**
     * Countable implementation
     *
     * @return NAVValidationError Return count of elements
     */
    public function count()
    {
      return count($this->NAVValidationError);
    }

}
