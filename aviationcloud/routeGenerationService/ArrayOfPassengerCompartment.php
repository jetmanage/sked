<?php

class ArrayOfPassengerCompartment implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var PassengerCompartment[] $PassengerCompartment
     */
    protected $PassengerCompartment = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return PassengerCompartment[]
     */
    public function getPassengerCompartment()
    {
      return $this->PassengerCompartment;
    }

    /**
     * @param PassengerCompartment[] $PassengerCompartment
     * @return ArrayOfPassengerCompartment
     */
    public function setPassengerCompartment(array $PassengerCompartment = null)
    {
      $this->PassengerCompartment = $PassengerCompartment;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->PassengerCompartment[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return PassengerCompartment
     */
    public function offsetGet($offset)
    {
      return $this->PassengerCompartment[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param PassengerCompartment $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->PassengerCompartment[] = $value;
      } else {
        $this->PassengerCompartment[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->PassengerCompartment[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return PassengerCompartment Return the current element
     */
    public function current()
    {
      return current($this->PassengerCompartment);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->PassengerCompartment);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->PassengerCompartment);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->PassengerCompartment);
    }

    /**
     * Countable implementation
     *
     * @return PassengerCompartment Return count of elements
     */
    public function count()
    {
      return count($this->PassengerCompartment);
    }

}
