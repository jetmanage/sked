<?php

class ProcedureSpecification
{

    /**
     * @var boolean $DisableRNAVProcedures
     */
    protected $DisableRNAVProcedures = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getDisableRNAVProcedures()
    {
      return $this->DisableRNAVProcedures;
    }

    /**
     * @param boolean $DisableRNAVProcedures
     * @return ProcedureSpecification
     */
    public function setDisableRNAVProcedures($DisableRNAVProcedures)
    {
      $this->DisableRNAVProcedures = $DisableRNAVProcedures;
      return $this;
    }

}
