<?php

class ArrayOfAltitude implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Altitude[] $Altitude
     */
    protected $Altitude = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Altitude[]
     */
    public function getAltitude()
    {
      return $this->Altitude;
    }

    /**
     * @param Altitude[] $Altitude
     * @return ArrayOfAltitude
     */
    public function setAltitude(array $Altitude = null)
    {
      $this->Altitude = $Altitude;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Altitude[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Altitude
     */
    public function offsetGet($offset)
    {
      return $this->Altitude[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Altitude $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->Altitude[] = $value;
      } else {
        $this->Altitude[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Altitude[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Altitude Return the current element
     */
    public function current()
    {
      return current($this->Altitude);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Altitude);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Altitude);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Altitude);
    }

    /**
     * Countable implementation
     *
     * @return Altitude Return count of elements
     */
    public function count()
    {
      return count($this->Altitude);
    }

}
