<?php

class ESADCostFunction extends CostFunction
{

    /**
     * @var boolean $AllowFallbackOnHistoricalWinds
     */
    protected $AllowFallbackOnHistoricalWinds = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getAllowFallbackOnHistoricalWinds()
    {
      return $this->AllowFallbackOnHistoricalWinds;
    }

    /**
     * @param boolean $AllowFallbackOnHistoricalWinds
     * @return ESADCostFunction
     */
    public function setAllowFallbackOnHistoricalWinds($AllowFallbackOnHistoricalWinds)
    {
      $this->AllowFallbackOnHistoricalWinds = $AllowFallbackOnHistoricalWinds;
      return $this;
    }

}
