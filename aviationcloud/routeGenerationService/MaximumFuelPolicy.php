<?php

class MaximumFuelPolicy extends FuelPolicy
{

    /**
     * @var Weight $RemainingFuelCapacity
     */
    protected $RemainingFuelCapacity = null;

    /**
     * @var Weight $UpperLimit
     */
    protected $UpperLimit = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Weight
     */
    public function getRemainingFuelCapacity()
    {
      return $this->RemainingFuelCapacity;
    }

    /**
     * @param Weight $RemainingFuelCapacity
     * @return MaximumFuelPolicy
     */
    public function setRemainingFuelCapacity($RemainingFuelCapacity)
    {
      $this->RemainingFuelCapacity = $RemainingFuelCapacity;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getUpperLimit()
    {
      return $this->UpperLimit;
    }

    /**
     * @param Weight $UpperLimit
     * @return MaximumFuelPolicy
     */
    public function setUpperLimit($UpperLimit)
    {
      $this->UpperLimit = $UpperLimit;
      return $this;
    }

}
