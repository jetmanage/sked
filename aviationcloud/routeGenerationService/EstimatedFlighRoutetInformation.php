<?php

class EstimatedFlighRoutetInformation
{

    /**
     * @var Currency $AircraftCost
     */
    protected $AircraftCost = null;

    /**
     * @var float $AverageMach
     */
    protected $AverageMach = null;

    /**
     * @var int $AverageTas
     */
    protected $AverageTas = null;

    /**
     * @var float $AverageWindComponent
     */
    protected $AverageWindComponent = null;

    /**
     * @var CalculationInformation $CalculationReport
     */
    protected $CalculationReport = null;

    /**
     * @var string $CruiseMode
     */
    protected $CruiseMode = null;

    /**
     * @var Length $Distance
     */
    protected $Distance = null;

    /**
     * @var Length $ESADDistance
     */
    protected $ESADDistance = null;

    /**
     * @var Weight $EstimatedExtraFuel
     */
    protected $EstimatedExtraFuel = null;

    /**
     * @var Weight $EstimatedTotalFuel
     */
    protected $EstimatedTotalFuel = null;

    /**
     * @var RouteEstimationLevel $EstimationLevel
     */
    protected $EstimationLevel = null;

    /**
     * @var Time $FlightTime
     */
    protected $FlightTime = null;

    /**
     * @var Weight $Fuel
     */
    protected $Fuel = null;

    /**
     * @var Currency $FuelCost
     */
    protected $FuelCost = null;

    /**
     * @var FlightMetrologicalData $MetrologicalData
     */
    protected $MetrologicalData = null;

    /**
     * @var ArrayOfOverflightCosts $OverflightCosts
     */
    protected $OverflightCosts = null;

    /**
     * @var ArrayOfDataPoint $PathwayData
     */
    protected $PathwayData = null;

    /**
     * @var Currency $TotalCost
     */
    protected $TotalCost = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Currency
     */
    public function getAircraftCost()
    {
      return $this->AircraftCost;
    }

    /**
     * @param Currency $AircraftCost
     * @return EstimatedFlighRoutetInformation
     */
    public function setAircraftCost($AircraftCost)
    {
      $this->AircraftCost = $AircraftCost;
      return $this;
    }

    /**
     * @return float
     */
    public function getAverageMach()
    {
      return $this->AverageMach;
    }

    /**
     * @param float $AverageMach
     * @return EstimatedFlighRoutetInformation
     */
    public function setAverageMach($AverageMach)
    {
      $this->AverageMach = $AverageMach;
      return $this;
    }

    /**
     * @return int
     */
    public function getAverageTas()
    {
      return $this->AverageTas;
    }

    /**
     * @param int $AverageTas
     * @return EstimatedFlighRoutetInformation
     */
    public function setAverageTas($AverageTas)
    {
      $this->AverageTas = $AverageTas;
      return $this;
    }

    /**
     * @return float
     */
    public function getAverageWindComponent()
    {
      return $this->AverageWindComponent;
    }

    /**
     * @param float $AverageWindComponent
     * @return EstimatedFlighRoutetInformation
     */
    public function setAverageWindComponent($AverageWindComponent)
    {
      $this->AverageWindComponent = $AverageWindComponent;
      return $this;
    }

    /**
     * @return CalculationInformation
     */
    public function getCalculationReport()
    {
      return $this->CalculationReport;
    }

    /**
     * @param CalculationInformation $CalculationReport
     * @return EstimatedFlighRoutetInformation
     */
    public function setCalculationReport($CalculationReport)
    {
      $this->CalculationReport = $CalculationReport;
      return $this;
    }

    /**
     * @return string
     */
    public function getCruiseMode()
    {
      return $this->CruiseMode;
    }

    /**
     * @param string $CruiseMode
     * @return EstimatedFlighRoutetInformation
     */
    public function setCruiseMode($CruiseMode)
    {
      $this->CruiseMode = $CruiseMode;
      return $this;
    }

    /**
     * @return Length
     */
    public function getDistance()
    {
      return $this->Distance;
    }

    /**
     * @param Length $Distance
     * @return EstimatedFlighRoutetInformation
     */
    public function setDistance($Distance)
    {
      $this->Distance = $Distance;
      return $this;
    }

    /**
     * @return Length
     */
    public function getESADDistance()
    {
      return $this->ESADDistance;
    }

    /**
     * @param Length $ESADDistance
     * @return EstimatedFlighRoutetInformation
     */
    public function setESADDistance($ESADDistance)
    {
      $this->ESADDistance = $ESADDistance;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getEstimatedExtraFuel()
    {
      return $this->EstimatedExtraFuel;
    }

    /**
     * @param Weight $EstimatedExtraFuel
     * @return EstimatedFlighRoutetInformation
     */
    public function setEstimatedExtraFuel($EstimatedExtraFuel)
    {
      $this->EstimatedExtraFuel = $EstimatedExtraFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getEstimatedTotalFuel()
    {
      return $this->EstimatedTotalFuel;
    }

    /**
     * @param Weight $EstimatedTotalFuel
     * @return EstimatedFlighRoutetInformation
     */
    public function setEstimatedTotalFuel($EstimatedTotalFuel)
    {
      $this->EstimatedTotalFuel = $EstimatedTotalFuel;
      return $this;
    }

    /**
     * @return RouteEstimationLevel
     */
    public function getEstimationLevel()
    {
      return $this->EstimationLevel;
    }

    /**
     * @param RouteEstimationLevel $EstimationLevel
     * @return EstimatedFlighRoutetInformation
     */
    public function setEstimationLevel($EstimationLevel)
    {
      $this->EstimationLevel = $EstimationLevel;
      return $this;
    }

    /**
     * @return Time
     */
    public function getFlightTime()
    {
      return $this->FlightTime;
    }

    /**
     * @param Time $FlightTime
     * @return EstimatedFlighRoutetInformation
     */
    public function setFlightTime($FlightTime)
    {
      $this->FlightTime = $FlightTime;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuel()
    {
      return $this->Fuel;
    }

    /**
     * @param Weight $Fuel
     * @return EstimatedFlighRoutetInformation
     */
    public function setFuel($Fuel)
    {
      $this->Fuel = $Fuel;
      return $this;
    }

    /**
     * @return Currency
     */
    public function getFuelCost()
    {
      return $this->FuelCost;
    }

    /**
     * @param Currency $FuelCost
     * @return EstimatedFlighRoutetInformation
     */
    public function setFuelCost($FuelCost)
    {
      $this->FuelCost = $FuelCost;
      return $this;
    }

    /**
     * @return FlightMetrologicalData
     */
    public function getMetrologicalData()
    {
      return $this->MetrologicalData;
    }

    /**
     * @param FlightMetrologicalData $MetrologicalData
     * @return EstimatedFlighRoutetInformation
     */
    public function setMetrologicalData($MetrologicalData)
    {
      $this->MetrologicalData = $MetrologicalData;
      return $this;
    }

    /**
     * @return ArrayOfOverflightCosts
     */
    public function getOverflightCosts()
    {
      return $this->OverflightCosts;
    }

    /**
     * @param ArrayOfOverflightCosts $OverflightCosts
     * @return EstimatedFlighRoutetInformation
     */
    public function setOverflightCosts($OverflightCosts)
    {
      $this->OverflightCosts = $OverflightCosts;
      return $this;
    }

    /**
     * @return ArrayOfDataPoint
     */
    public function getPathwayData()
    {
      return $this->PathwayData;
    }

    /**
     * @param ArrayOfDataPoint $PathwayData
     * @return EstimatedFlighRoutetInformation
     */
    public function setPathwayData($PathwayData)
    {
      $this->PathwayData = $PathwayData;
      return $this;
    }

    /**
     * @return Currency
     */
    public function getTotalCost()
    {
      return $this->TotalCost;
    }

    /**
     * @param Currency $TotalCost
     * @return EstimatedFlighRoutetInformation
     */
    public function setTotalCost($TotalCost)
    {
      $this->TotalCost = $TotalCost;
      return $this;
    }

}
