<?php

class RouteSpecification
{

    /**
     * @var AircraftSpecification $AircraftDefinition
     */
    protected $AircraftDefinition = null;

    /**
     * @var string $AircraftTailNumber
     */
    protected $AircraftTailNumber = null;

    /**
     * @var ArrayOfAirspaceIdentifier $AvoidFIRS
     */
    protected $AvoidFIRS = null;

    /**
     * @var CostFunctionSpecification $CostFunctionDefinition
     */
    protected $CostFunctionDefinition = null;

    /**
     * @var PerformanceProfileSpecification $CruiseProfile
     */
    protected $CruiseProfile = null;

    /**
     * @var DirectOptimizationSettings $DirectOpimitization
     */
    protected $DirectOpimitization = null;

    /**
     * @var boolean $EnableSETP
     */
    protected $EnableSETP = null;

    /**
     * @var int $FlightLevel
     */
    protected $FlightLevel = null;

    /**
     * @var FlightLevelSpecification $FlightLevelDefinition
     */
    protected $FlightLevelDefinition = null;

    /**
     * @var ATCFlightRule $FlightRule
     */
    protected $FlightRule = null;

    /**
     * @var Airport $FromAirport
     */
    protected $FromAirport = null;

    /**
     * @var RouteNodeIdentifier $FromPoint
     */
    protected $FromPoint = null;

    /**
     * @var FuelPolicy $FuelPolicy
     */
    protected $FuelPolicy = null;

    /**
     * @var boolean $IgnoreConstraints
     */
    protected $IgnoreConstraints = null;

    /**
     * @var ArrayOfRouteNodeIdentifier $NotViaPoints
     */
    protected $NotViaPoints = null;

    /**
     * @var int $NumberOfRoutes
     */
    protected $NumberOfRoutes = null;

    /**
     * @var PathOptions $PathOptions
     */
    protected $PathOptions = null;

    /**
     * @var \DateTime $SceduledTimeOfDeparture
     */
    protected $SceduledTimeOfDeparture = null;

    /**
     * @var Airport $ToAirport
     */
    protected $ToAirport = null;

    /**
     * @var RouteNodeIdentifier $ToPoint
     */
    protected $ToPoint = null;

    /**
     * @var AircraftTraficLoadDefinition $TraficLoad
     */
    protected $TraficLoad = null;

    /**
     * @var ATCFlightType $TypeOfFlight
     */
    protected $TypeOfFlight = null;

    /**
     * @var boolean $UseESADDistance
     */
    protected $UseESADDistance = null;

    /**
     * @var ArrayOfRouteNodeIdentifier $ViaPoints
     */
    protected $ViaPoints = null;

    /**
     * @param \DateTime $SceduledTimeOfDeparture
     */
    public function __construct(\DateTime $SceduledTimeOfDeparture)
    {
      $this->SceduledTimeOfDeparture = $SceduledTimeOfDeparture->format(\DateTime::ATOM);
    }

    /**
     * @return AircraftSpecification
     */
    public function getAircraftDefinition()
    {
      return $this->AircraftDefinition;
    }

    /**
     * @param AircraftSpecification $AircraftDefinition
     * @return RouteSpecification
     */
    public function setAircraftDefinition($AircraftDefinition)
    {
      $this->AircraftDefinition = $AircraftDefinition;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftTailNumber()
    {
      return $this->AircraftTailNumber;
    }

    /**
     * @param string $AircraftTailNumber
     * @return RouteSpecification
     */
    public function setAircraftTailNumber($AircraftTailNumber)
    {
      $this->AircraftTailNumber = $AircraftTailNumber;
      return $this;
    }

    /**
     * @return ArrayOfAirspaceIdentifier
     */
    public function getAvoidFIRS()
    {
      return $this->AvoidFIRS;
    }

    /**
     * @param ArrayOfAirspaceIdentifier $AvoidFIRS
     * @return RouteSpecification
     */
    public function setAvoidFIRS($AvoidFIRS)
    {
      $this->AvoidFIRS = $AvoidFIRS;
      return $this;
    }

    /**
     * @return CostFunctionSpecification
     */
    public function getCostFunctionDefinition()
    {
      return $this->CostFunctionDefinition;
    }

    /**
     * @param CostFunctionSpecification $CostFunctionDefinition
     * @return RouteSpecification
     */
    public function setCostFunctionDefinition($CostFunctionDefinition)
    {
      $this->CostFunctionDefinition = $CostFunctionDefinition;
      return $this;
    }

    /**
     * @return PerformanceProfileSpecification
     */
    public function getCruiseProfile()
    {
      return $this->CruiseProfile;
    }

    /**
     * @param PerformanceProfileSpecification $CruiseProfile
     * @return RouteSpecification
     */
    public function setCruiseProfile($CruiseProfile)
    {
      $this->CruiseProfile = $CruiseProfile;
      return $this;
    }

    /**
     * @return DirectOptimizationSettings
     */
    public function getDirectOpimitization()
    {
      return $this->DirectOpimitization;
    }

    /**
     * @param DirectOptimizationSettings $DirectOpimitization
     * @return RouteSpecification
     */
    public function setDirectOpimitization($DirectOpimitization)
    {
      $this->DirectOpimitization = $DirectOpimitization;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableSETP()
    {
      return $this->EnableSETP;
    }

    /**
     * @param boolean $EnableSETP
     * @return RouteSpecification
     */
    public function setEnableSETP($EnableSETP)
    {
      $this->EnableSETP = $EnableSETP;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightLevel()
    {
      return $this->FlightLevel;
    }

    /**
     * @param int $FlightLevel
     * @return RouteSpecification
     */
    public function setFlightLevel($FlightLevel)
    {
      $this->FlightLevel = $FlightLevel;
      return $this;
    }

    /**
     * @return FlightLevelSpecification
     */
    public function getFlightLevelDefinition()
    {
      return $this->FlightLevelDefinition;
    }

    /**
     * @param FlightLevelSpecification $FlightLevelDefinition
     * @return RouteSpecification
     */
    public function setFlightLevelDefinition($FlightLevelDefinition)
    {
      $this->FlightLevelDefinition = $FlightLevelDefinition;
      return $this;
    }

    /**
     * @return ATCFlightRule
     */
    public function getFlightRule()
    {
      return $this->FlightRule;
    }

    /**
     * @param ATCFlightRule $FlightRule
     * @return RouteSpecification
     */
    public function setFlightRule($FlightRule)
    {
      $this->FlightRule = $FlightRule;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getFromAirport()
    {
      return $this->FromAirport;
    }

    /**
     * @param Airport $FromAirport
     * @return RouteSpecification
     */
    public function setFromAirport($FromAirport)
    {
      $this->FromAirport = $FromAirport;
      return $this;
    }

    /**
     * @return RouteNodeIdentifier
     */
    public function getFromPoint()
    {
      return $this->FromPoint;
    }

    /**
     * @param RouteNodeIdentifier $FromPoint
     * @return RouteSpecification
     */
    public function setFromPoint($FromPoint)
    {
      $this->FromPoint = $FromPoint;
      return $this;
    }

    /**
     * @return FuelPolicy
     */
    public function getFuelPolicy()
    {
      return $this->FuelPolicy;
    }

    /**
     * @param FuelPolicy $FuelPolicy
     * @return RouteSpecification
     */
    public function setFuelPolicy($FuelPolicy)
    {
      $this->FuelPolicy = $FuelPolicy;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIgnoreConstraints()
    {
      return $this->IgnoreConstraints;
    }

    /**
     * @param boolean $IgnoreConstraints
     * @return RouteSpecification
     */
    public function setIgnoreConstraints($IgnoreConstraints)
    {
      $this->IgnoreConstraints = $IgnoreConstraints;
      return $this;
    }

    /**
     * @return ArrayOfRouteNodeIdentifier
     */
    public function getNotViaPoints()
    {
      return $this->NotViaPoints;
    }

    /**
     * @param ArrayOfRouteNodeIdentifier $NotViaPoints
     * @return RouteSpecification
     */
    public function setNotViaPoints($NotViaPoints)
    {
      $this->NotViaPoints = $NotViaPoints;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfRoutes()
    {
      return $this->NumberOfRoutes;
    }

    /**
     * @param int $NumberOfRoutes
     * @return RouteSpecification
     */
    public function setNumberOfRoutes($NumberOfRoutes)
    {
      $this->NumberOfRoutes = $NumberOfRoutes;
      return $this;
    }

    /**
     * @return PathOptions
     */
    public function getPathOptions()
    {
      return $this->PathOptions;
    }

    /**
     * @param PathOptions $PathOptions
     * @return RouteSpecification
     */
    public function setPathOptions($PathOptions)
    {
      $this->PathOptions = $PathOptions;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSceduledTimeOfDeparture()
    {
      if ($this->SceduledTimeOfDeparture == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->SceduledTimeOfDeparture);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $SceduledTimeOfDeparture
     * @return RouteSpecification
     */
    public function setSceduledTimeOfDeparture(\DateTime $SceduledTimeOfDeparture)
    {
      $this->SceduledTimeOfDeparture = $SceduledTimeOfDeparture->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return Airport
     */
    public function getToAirport()
    {
      return $this->ToAirport;
    }

    /**
     * @param Airport $ToAirport
     * @return RouteSpecification
     */
    public function setToAirport($ToAirport)
    {
      $this->ToAirport = $ToAirport;
      return $this;
    }

    /**
     * @return RouteNodeIdentifier
     */
    public function getToPoint()
    {
      return $this->ToPoint;
    }

    /**
     * @param RouteNodeIdentifier $ToPoint
     * @return RouteSpecification
     */
    public function setToPoint($ToPoint)
    {
      $this->ToPoint = $ToPoint;
      return $this;
    }

    /**
     * @return AircraftTraficLoadDefinition
     */
    public function getTraficLoad()
    {
      return $this->TraficLoad;
    }

    /**
     * @param AircraftTraficLoadDefinition $TraficLoad
     * @return RouteSpecification
     */
    public function setTraficLoad($TraficLoad)
    {
      $this->TraficLoad = $TraficLoad;
      return $this;
    }

    /**
     * @return ATCFlightType
     */
    public function getTypeOfFlight()
    {
      return $this->TypeOfFlight;
    }

    /**
     * @param ATCFlightType $TypeOfFlight
     * @return RouteSpecification
     */
    public function setTypeOfFlight($TypeOfFlight)
    {
      $this->TypeOfFlight = $TypeOfFlight;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseESADDistance()
    {
      return $this->UseESADDistance;
    }

    /**
     * @param boolean $UseESADDistance
     * @return RouteSpecification
     */
    public function setUseESADDistance($UseESADDistance)
    {
      $this->UseESADDistance = $UseESADDistance;
      return $this;
    }

    /**
     * @return ArrayOfRouteNodeIdentifier
     */
    public function getViaPoints()
    {
      return $this->ViaPoints;
    }

    /**
     * @param ArrayOfRouteNodeIdentifier $ViaPoints
     * @return RouteSpecification
     */
    public function setViaPoints($ViaPoints)
    {
      $this->ViaPoints = $ViaPoints;
      return $this;
    }

}
