<?php

class ArrayOfWeatherDataRecord implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var WeatherDataRecord[] $WeatherDataRecord
     */
    protected $WeatherDataRecord = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return WeatherDataRecord[]
     */
    public function getWeatherDataRecord()
    {
      return $this->WeatherDataRecord;
    }

    /**
     * @param WeatherDataRecord[] $WeatherDataRecord
     * @return ArrayOfWeatherDataRecord
     */
    public function setWeatherDataRecord(array $WeatherDataRecord = null)
    {
      $this->WeatherDataRecord = $WeatherDataRecord;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->WeatherDataRecord[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return WeatherDataRecord
     */
    public function offsetGet($offset)
    {
      return $this->WeatherDataRecord[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param WeatherDataRecord $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->WeatherDataRecord[] = $value;
      } else {
        $this->WeatherDataRecord[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->WeatherDataRecord[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return WeatherDataRecord Return the current element
     */
    public function current()
    {
      return current($this->WeatherDataRecord);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->WeatherDataRecord);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->WeatherDataRecord);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->WeatherDataRecord);
    }

    /**
     * Countable implementation
     *
     * @return WeatherDataRecord Return count of elements
     */
    public function count()
    {
      return count($this->WeatherDataRecord);
    }

}
