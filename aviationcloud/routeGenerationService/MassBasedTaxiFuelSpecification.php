<?php

class MassBasedTaxiFuelSpecification extends TaxiFuelSpecification
{

    /**
     * @var Weight $TaxiFuel
     */
    protected $TaxiFuel = null;

    /**
     * @var Weight $TaxiInFuel
     */
    protected $TaxiInFuel = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getTaxiFuel()
    {
      return $this->TaxiFuel;
    }

    /**
     * @param Weight $TaxiFuel
     * @return MassBasedTaxiFuelSpecification
     */
    public function setTaxiFuel($TaxiFuel)
    {
      $this->TaxiFuel = $TaxiFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getTaxiInFuel()
    {
      return $this->TaxiInFuel;
    }

    /**
     * @param Weight $TaxiInFuel
     * @return MassBasedTaxiFuelSpecification
     */
    public function setTaxiInFuel($TaxiInFuel)
    {
      $this->TaxiInFuel = $TaxiInFuel;
      return $this;
    }

}
