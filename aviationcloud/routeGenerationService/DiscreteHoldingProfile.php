<?php

class DiscreteHoldingProfile extends HoldingProfile
{

    /**
     * @var ArrayOfHoldingDataSeries $HoldingPerformanceDataSet
     */
    protected $HoldingPerformanceDataSet = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ArrayOfHoldingDataSeries
     */
    public function getHoldingPerformanceDataSet()
    {
      return $this->HoldingPerformanceDataSet;
    }

    /**
     * @param ArrayOfHoldingDataSeries $HoldingPerformanceDataSet
     * @return DiscreteHoldingProfile
     */
    public function setHoldingPerformanceDataSet($HoldingPerformanceDataSet)
    {
      $this->HoldingPerformanceDataSet = $HoldingPerformanceDataSet;
      return $this;
    }

}
