<?php

class RouteResolveResponse
{

    /**
     * @var ArrayOfRouteInformation $RouteInformation
     */
    protected $RouteInformation = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfRouteInformation
     */
    public function getRouteInformation()
    {
      return $this->RouteInformation;
    }

    /**
     * @param ArrayOfRouteInformation $RouteInformation
     * @return RouteResolveResponse
     */
    public function setRouteInformation($RouteInformation)
    {
      $this->RouteInformation = $RouteInformation;
      return $this;
    }

}
