<?php

class RouteSource
{
    const __default = 'Optimized';
    const Optimized = 'Optimized';
    const EUROCONTROL = 'EUROCONTROL';
    const USStandardRoutes = 'USStandardRoutes';
    const FAACodedDepartureRoute = 'FAACodedDepartureRoute';
    const FAAPreferedRoute = 'FAAPreferedRoute';
    const RecentlyFiled = 'RecentlyFiled';
    const EUROCONTROLAssisted = 'EUROCONTROLAssisted';


}
