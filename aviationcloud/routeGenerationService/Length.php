<?php

class Length
{

    /**
     * @var LengthUnit $Unit
     */
    protected $Unit = null;

    /**
     * @var float $Value
     */
    protected $Value = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return LengthUnit
     */
    public function getUnit()
    {
      return $this->Unit;
    }

    /**
     * @param LengthUnit $Unit
     * @return Length
     */
    public function setUnit($Unit)
    {
      $this->Unit = $Unit;
      return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param float $Value
     * @return Length
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

}
