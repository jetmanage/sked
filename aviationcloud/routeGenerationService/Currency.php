<?php

class Currency
{

    /**
     * @var CurrencyUnit $Unit
     */
    protected $Unit = null;

    /**
     * @var float $Value
     */
    protected $Value = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CurrencyUnit
     */
    public function getUnit()
    {
      return $this->Unit;
    }

    /**
     * @param CurrencyUnit $Unit
     * @return Currency
     */
    public function setUnit($Unit)
    {
      $this->Unit = $Unit;
      return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param float $Value
     * @return Currency
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

}
