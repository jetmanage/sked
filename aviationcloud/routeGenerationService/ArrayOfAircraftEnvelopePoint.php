<?php

class ArrayOfAircraftEnvelopePoint implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AircraftEnvelopePoint[] $AircraftEnvelopePoint
     */
    protected $AircraftEnvelopePoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftEnvelopePoint[]
     */
    public function getAircraftEnvelopePoint()
    {
      return $this->AircraftEnvelopePoint;
    }

    /**
     * @param AircraftEnvelopePoint[] $AircraftEnvelopePoint
     * @return ArrayOfAircraftEnvelopePoint
     */
    public function setAircraftEnvelopePoint(array $AircraftEnvelopePoint = null)
    {
      $this->AircraftEnvelopePoint = $AircraftEnvelopePoint;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AircraftEnvelopePoint[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AircraftEnvelopePoint
     */
    public function offsetGet($offset)
    {
      return $this->AircraftEnvelopePoint[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AircraftEnvelopePoint $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AircraftEnvelopePoint[] = $value;
      } else {
        $this->AircraftEnvelopePoint[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AircraftEnvelopePoint[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AircraftEnvelopePoint Return the current element
     */
    public function current()
    {
      return current($this->AircraftEnvelopePoint);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AircraftEnvelopePoint);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AircraftEnvelopePoint);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AircraftEnvelopePoint);
    }

    /**
     * Countable implementation
     *
     * @return AircraftEnvelopePoint Return count of elements
     */
    public function count()
    {
      return count($this->AircraftEnvelopePoint);
    }

}
