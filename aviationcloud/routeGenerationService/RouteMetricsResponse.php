<?php

class RouteMetricsResponse
{

    /**
     * @var ArrayOfRouteInformation $RouteInformation
     */
    protected $RouteInformation = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfRouteInformation
     */
    public function getRouteInformation()
    {
      return $this->RouteInformation;
    }

    /**
     * @param ArrayOfRouteInformation $RouteInformation
     * @return RouteMetricsResponse
     */
    public function setRouteInformation($RouteInformation)
    {
      $this->RouteInformation = $RouteInformation;
      return $this;
    }

}
