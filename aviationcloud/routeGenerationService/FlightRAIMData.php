<?php

class FlightRAIMData
{

    /**
     * @var string $Error
     */
    protected $Error = null;

    /**
     * @var boolean $HasError
     */
    protected $HasError = null;

    /**
     * @var boolean $HasOutageAtAlternate1
     */
    protected $HasOutageAtAlternate1 = null;

    /**
     * @var boolean $HasOutageAtAlternate2
     */
    protected $HasOutageAtAlternate2 = null;

    /**
     * @var boolean $HasOutageAtDeparture
     */
    protected $HasOutageAtDeparture = null;

    /**
     * @var boolean $HasOutageAtDestination
     */
    protected $HasOutageAtDestination = null;

    /**
     * @var ArrayOfSphereicPoint $NodesWithOutage
     */
    protected $NodesWithOutage = null;

    /**
     * @var boolean $RAIMOutage
     */
    protected $RAIMOutage = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getError()
    {
      return $this->Error;
    }

    /**
     * @param string $Error
     * @return FlightRAIMData
     */
    public function setError($Error)
    {
      $this->Error = $Error;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHasError()
    {
      return $this->HasError;
    }

    /**
     * @param boolean $HasError
     * @return FlightRAIMData
     */
    public function setHasError($HasError)
    {
      $this->HasError = $HasError;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHasOutageAtAlternate1()
    {
      return $this->HasOutageAtAlternate1;
    }

    /**
     * @param boolean $HasOutageAtAlternate1
     * @return FlightRAIMData
     */
    public function setHasOutageAtAlternate1($HasOutageAtAlternate1)
    {
      $this->HasOutageAtAlternate1 = $HasOutageAtAlternate1;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHasOutageAtAlternate2()
    {
      return $this->HasOutageAtAlternate2;
    }

    /**
     * @param boolean $HasOutageAtAlternate2
     * @return FlightRAIMData
     */
    public function setHasOutageAtAlternate2($HasOutageAtAlternate2)
    {
      $this->HasOutageAtAlternate2 = $HasOutageAtAlternate2;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHasOutageAtDeparture()
    {
      return $this->HasOutageAtDeparture;
    }

    /**
     * @param boolean $HasOutageAtDeparture
     * @return FlightRAIMData
     */
    public function setHasOutageAtDeparture($HasOutageAtDeparture)
    {
      $this->HasOutageAtDeparture = $HasOutageAtDeparture;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHasOutageAtDestination()
    {
      return $this->HasOutageAtDestination;
    }

    /**
     * @param boolean $HasOutageAtDestination
     * @return FlightRAIMData
     */
    public function setHasOutageAtDestination($HasOutageAtDestination)
    {
      $this->HasOutageAtDestination = $HasOutageAtDestination;
      return $this;
    }

    /**
     * @return ArrayOfSphereicPoint
     */
    public function getNodesWithOutage()
    {
      return $this->NodesWithOutage;
    }

    /**
     * @param ArrayOfSphereicPoint $NodesWithOutage
     * @return FlightRAIMData
     */
    public function setNodesWithOutage($NodesWithOutage)
    {
      $this->NodesWithOutage = $NodesWithOutage;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRAIMOutage()
    {
      return $this->RAIMOutage;
    }

    /**
     * @param boolean $RAIMOutage
     * @return FlightRAIMData
     */
    public function setRAIMOutage($RAIMOutage)
    {
      $this->RAIMOutage = $RAIMOutage;
      return $this;
    }

}
