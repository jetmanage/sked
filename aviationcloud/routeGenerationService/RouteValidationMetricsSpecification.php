<?php

class RouteValidationMetricsSpecification
{

    /**
     * @var boolean $PerformEUROCONTROLValidation
     */
    protected $PerformEUROCONTROLValidation = null;

    /**
     * @var boolean $PerformNAVValidation
     */
    protected $PerformNAVValidation = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getPerformEUROCONTROLValidation()
    {
      return $this->PerformEUROCONTROLValidation;
    }

    /**
     * @param boolean $PerformEUROCONTROLValidation
     * @return RouteValidationMetricsSpecification
     */
    public function setPerformEUROCONTROLValidation($PerformEUROCONTROLValidation)
    {
      $this->PerformEUROCONTROLValidation = $PerformEUROCONTROLValidation;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPerformNAVValidation()
    {
      return $this->PerformNAVValidation;
    }

    /**
     * @param boolean $PerformNAVValidation
     * @return RouteValidationMetricsSpecification
     */
    public function setPerformNAVValidation($PerformNAVValidation)
    {
      $this->PerformNAVValidation = $PerformNAVValidation;
      return $this;
    }

}
