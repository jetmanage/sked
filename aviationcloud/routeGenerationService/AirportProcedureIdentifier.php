<?php

class AirportProcedureIdentifier
{

    /**
     * @var string $ArincIdentifier
     */
    protected $ArincIdentifier = null;

    /**
     * @var string $Id
     */
    protected $Id = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getArincIdentifier()
    {
      return $this->ArincIdentifier;
    }

    /**
     * @param string $ArincIdentifier
     * @return AirportProcedureIdentifier
     */
    public function setArincIdentifier($ArincIdentifier)
    {
      $this->ArincIdentifier = $ArincIdentifier;
      return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return AirportProcedureIdentifier
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

}
