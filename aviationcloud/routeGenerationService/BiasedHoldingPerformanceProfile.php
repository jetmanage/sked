<?php

class BiasedHoldingPerformanceProfile extends HoldingProfile
{

    /**
     * @var int $FuelDeviation
     */
    protected $FuelDeviation = null;

    /**
     * @var int $ReferencedProfile
     */
    protected $ReferencedProfile = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getFuelDeviation()
    {
      return $this->FuelDeviation;
    }

    /**
     * @param int $FuelDeviation
     * @return BiasedHoldingPerformanceProfile
     */
    public function setFuelDeviation($FuelDeviation)
    {
      $this->FuelDeviation = $FuelDeviation;
      return $this;
    }

    /**
     * @return int
     */
    public function getReferencedProfile()
    {
      return $this->ReferencedProfile;
    }

    /**
     * @param int $ReferencedProfile
     * @return BiasedHoldingPerformanceProfile
     */
    public function setReferencedProfile($ReferencedProfile)
    {
      $this->ReferencedProfile = $ReferencedProfile;
      return $this;
    }

}
