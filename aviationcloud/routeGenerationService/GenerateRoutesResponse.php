<?php

class GenerateRoutesResponse
{

    /**
     * @var ArrayOfRoute $GenerateRoutesResult
     */
    protected $GenerateRoutesResult = null;

    /**
     * @param ArrayOfRoute $GenerateRoutesResult
     */
    public function __construct($GenerateRoutesResult)
    {
      $this->GenerateRoutesResult = $GenerateRoutesResult;
    }

    /**
     * @return ArrayOfRoute
     */
    public function getGenerateRoutesResult()
    {
      return $this->GenerateRoutesResult;
    }

    /**
     * @param ArrayOfRoute $GenerateRoutesResult
     * @return GenerateRoutesResponse
     */
    public function setGenerateRoutesResult($GenerateRoutesResult)
    {
      $this->GenerateRoutesResult = $GenerateRoutesResult;
      return $this;
    }

}
