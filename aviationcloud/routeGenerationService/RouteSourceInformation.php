<?php

class RouteSourceInformation
{

    /**
     * @var \DateTime $CreatedAt
     */
    protected $CreatedAt = null;

    /**
     * @var string $RouteIdentifier
     */
    protected $RouteIdentifier = null;

    /**
     * @var RouteSource $Source
     */
    protected $Source = null;

    /**
     * @var string $SourceString
     */
    protected $SourceString = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
      if ($this->CreatedAt == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->CreatedAt);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $CreatedAt
     * @return RouteSourceInformation
     */
    public function setCreatedAt(\DateTime $CreatedAt = null)
    {
      if ($CreatedAt == null) {
       $this->CreatedAt = null;
      } else {
        $this->CreatedAt = $CreatedAt->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getRouteIdentifier()
    {
      return $this->RouteIdentifier;
    }

    /**
     * @param string $RouteIdentifier
     * @return RouteSourceInformation
     */
    public function setRouteIdentifier($RouteIdentifier)
    {
      $this->RouteIdentifier = $RouteIdentifier;
      return $this;
    }

    /**
     * @return RouteSource
     */
    public function getSource()
    {
      return $this->Source;
    }

    /**
     * @param RouteSource $Source
     * @return RouteSourceInformation
     */
    public function setSource($Source)
    {
      $this->Source = $Source;
      return $this;
    }

    /**
     * @return string
     */
    public function getSourceString()
    {
      return $this->SourceString;
    }

    /**
     * @param string $SourceString
     * @return RouteSourceInformation
     */
    public function setSourceString($SourceString)
    {
      $this->SourceString = $SourceString;
      return $this;
    }

}
