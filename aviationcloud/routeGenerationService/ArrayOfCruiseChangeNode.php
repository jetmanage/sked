<?php

class ArrayOfCruiseChangeNode implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CruiseChangeNode[] $CruiseChangeNode
     */
    protected $CruiseChangeNode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CruiseChangeNode[]
     */
    public function getCruiseChangeNode()
    {
      return $this->CruiseChangeNode;
    }

    /**
     * @param CruiseChangeNode[] $CruiseChangeNode
     * @return ArrayOfCruiseChangeNode
     */
    public function setCruiseChangeNode(array $CruiseChangeNode = null)
    {
      $this->CruiseChangeNode = $CruiseChangeNode;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CruiseChangeNode[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CruiseChangeNode
     */
    public function offsetGet($offset)
    {
      return $this->CruiseChangeNode[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CruiseChangeNode $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->CruiseChangeNode[] = $value;
      } else {
        $this->CruiseChangeNode[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CruiseChangeNode[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CruiseChangeNode Return the current element
     */
    public function current()
    {
      return current($this->CruiseChangeNode);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CruiseChangeNode);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CruiseChangeNode);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CruiseChangeNode);
    }

    /**
     * Countable implementation
     *
     * @return CruiseChangeNode Return count of elements
     */
    public function count()
    {
      return count($this->CruiseChangeNode);
    }

}
