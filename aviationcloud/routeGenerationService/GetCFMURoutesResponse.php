<?php

class GetCFMURoutesResponse
{

    /**
     * @var ArrayOfRoute $GetCFMURoutesResult
     */
    protected $GetCFMURoutesResult = null;

    /**
     * @param ArrayOfRoute $GetCFMURoutesResult
     */
    public function __construct($GetCFMURoutesResult)
    {
      $this->GetCFMURoutesResult = $GetCFMURoutesResult;
    }

    /**
     * @return ArrayOfRoute
     */
    public function getGetCFMURoutesResult()
    {
      return $this->GetCFMURoutesResult;
    }

    /**
     * @param ArrayOfRoute $GetCFMURoutesResult
     * @return GetCFMURoutesResponse
     */
    public function setGetCFMURoutesResult($GetCFMURoutesResult)
    {
      $this->GetCFMURoutesResult = $GetCFMURoutesResult;
      return $this;
    }

}
