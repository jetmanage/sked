<?php

class RouteIntersectionMetricsSpecification
{

    /**
     * @var boolean $CalculateAllCountryBorderIntersections
     */
    protected $CalculateAllCountryBorderIntersections = null;

    /**
     * @var boolean $CalculateAllFIRIntersections
     */
    protected $CalculateAllFIRIntersections = null;

    /**
     * @var boolean $FilterCloseRepeatedIntersections
     */
    protected $FilterCloseRepeatedIntersections = null;

    /**
     * @var ArrayOfAirspace $FirsToCalculateIntersections
     */
    protected $FirsToCalculateIntersections = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getCalculateAllCountryBorderIntersections()
    {
      return $this->CalculateAllCountryBorderIntersections;
    }

    /**
     * @param boolean $CalculateAllCountryBorderIntersections
     * @return RouteIntersectionMetricsSpecification
     */
    public function setCalculateAllCountryBorderIntersections($CalculateAllCountryBorderIntersections)
    {
      $this->CalculateAllCountryBorderIntersections = $CalculateAllCountryBorderIntersections;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCalculateAllFIRIntersections()
    {
      return $this->CalculateAllFIRIntersections;
    }

    /**
     * @param boolean $CalculateAllFIRIntersections
     * @return RouteIntersectionMetricsSpecification
     */
    public function setCalculateAllFIRIntersections($CalculateAllFIRIntersections)
    {
      $this->CalculateAllFIRIntersections = $CalculateAllFIRIntersections;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFilterCloseRepeatedIntersections()
    {
      return $this->FilterCloseRepeatedIntersections;
    }

    /**
     * @param boolean $FilterCloseRepeatedIntersections
     * @return RouteIntersectionMetricsSpecification
     */
    public function setFilterCloseRepeatedIntersections($FilterCloseRepeatedIntersections)
    {
      $this->FilterCloseRepeatedIntersections = $FilterCloseRepeatedIntersections;
      return $this;
    }

    /**
     * @return ArrayOfAirspace
     */
    public function getFirsToCalculateIntersections()
    {
      return $this->FirsToCalculateIntersections;
    }

    /**
     * @param ArrayOfAirspace $FirsToCalculateIntersections
     * @return RouteIntersectionMetricsSpecification
     */
    public function setFirsToCalculateIntersections($FirsToCalculateIntersections)
    {
      $this->FirsToCalculateIntersections = $FirsToCalculateIntersections;
      return $this;
    }

}
