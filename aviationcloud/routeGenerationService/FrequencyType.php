<?php

class FrequencyType
{
    const __default = 'CTAF';
    const CTAF = 'CTAF';
    const Ground = 'Ground';
    const Tower = 'Tower';
    const Clearance = 'Clearance';
    const Unicom = 'Unicom';
    const ATIS = 'ATIS';
    const Approach = 'Approach';
    const Arrival = 'Arrival';
    const Departure = 'Departure';
    const RampControl = 'RampControl';
    const Radar = 'Radar';
    const Radio = 'Radio';
    const Information = 'Information';
    const Operation = 'Operation';
    const Director = 'Director';
    const Control = 'Control';
    const AreaControlCenter = 'AreaControlCenter';
    const AirliftCommandPost = 'AirliftCommandPost';
    const ASOS = 'ASOS';
    const AWIB = 'AWIB';
    const AWOS = 'AWOS';
    const AWIS = 'AWIS';
    const Terminal = 'Terminal';
    const FlightServiceStation = 'FlightServiceStation';
    const TerminalArea = 'TerminalArea';
    const ClearencePreTaxi = 'ClearencePreTaxi';
    const TerminalControlArea = 'TerminalControlArea';
    const Emergency = 'Emergency';
    const GateControl = 'GateControl';
    const HelicopterFrequency = 'HelicopterFrequency';


}
