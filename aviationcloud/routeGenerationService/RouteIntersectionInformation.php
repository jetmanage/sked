<?php

class RouteIntersectionInformation
{

    /**
     * @var boolean $CalculatedForFirsToAvoid
     */
    protected $CalculatedForFirsToAvoid = null;

    /**
     * @var boolean $CalculatedWithCountryIntersections
     */
    protected $CalculatedWithCountryIntersections = null;

    /**
     * @var boolean $CalculatedWithFIRIntersections
     */
    protected $CalculatedWithFIRIntersections = null;

    /**
     * @var ArrayOfIntersectionPoint $IntersectionPoints
     */
    protected $IntersectionPoints = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getCalculatedForFirsToAvoid()
    {
      return $this->CalculatedForFirsToAvoid;
    }

    /**
     * @param boolean $CalculatedForFirsToAvoid
     * @return RouteIntersectionInformation
     */
    public function setCalculatedForFirsToAvoid($CalculatedForFirsToAvoid)
    {
      $this->CalculatedForFirsToAvoid = $CalculatedForFirsToAvoid;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCalculatedWithCountryIntersections()
    {
      return $this->CalculatedWithCountryIntersections;
    }

    /**
     * @param boolean $CalculatedWithCountryIntersections
     * @return RouteIntersectionInformation
     */
    public function setCalculatedWithCountryIntersections($CalculatedWithCountryIntersections)
    {
      $this->CalculatedWithCountryIntersections = $CalculatedWithCountryIntersections;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCalculatedWithFIRIntersections()
    {
      return $this->CalculatedWithFIRIntersections;
    }

    /**
     * @param boolean $CalculatedWithFIRIntersections
     * @return RouteIntersectionInformation
     */
    public function setCalculatedWithFIRIntersections($CalculatedWithFIRIntersections)
    {
      $this->CalculatedWithFIRIntersections = $CalculatedWithFIRIntersections;
      return $this;
    }

    /**
     * @return ArrayOfIntersectionPoint
     */
    public function getIntersectionPoints()
    {
      return $this->IntersectionPoints;
    }

    /**
     * @param ArrayOfIntersectionPoint $IntersectionPoints
     * @return RouteIntersectionInformation
     */
    public function setIntersectionPoints($IntersectionPoints)
    {
      $this->IntersectionPoints = $IntersectionPoints;
      return $this;
    }

}
