<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Trip
 * @package App\Models
 * @version October 31, 2017, 2:26 pm UTC
 *
 * @property \App\Models\Aircraft aircraft
 * @property \Illuminate\Database\Eloquent\Collection Leg
 * @property \Illuminate\Database\Eloquent\Collection Passenger
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property integer aircraft_id
 */
class Trip extends Model
{
    use SoftDeletes;

    public $table = 'trips';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'aircraft_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'aircraft_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function aircraft()
    {
        return $this->belongsTo(\App\Models\Aircraft::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function legs()
    {
        return $this->hasMany(\App\Models\Leg::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function passengers()
    {
        return $this->hasMany(\App\Models\Passenger::class);
    }
}
