<?php

namespace App\Models;

trait Common
{
    /**
     * Creating an new table row in database.
     *
     * @author Jesse V
     * @param  string  $table
     * @param  string  $name
     * @param  string  $label
     * @return integer        Return insert id.
     */
    public static function createRow($table, $name, $label)
    {
        return \DB::table($table)->insertGetId([
            'name' => $name,
            'label' => $label,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }

	/**
	 * Laravel scope method, which will get table ids and return them.
	 *
	 * @author Jesse V
	 * @param  Bulider $query Laravel Builder class
	 * @param  array   $ids   Array of ids to be returned, if they exist
	 * @return array
	 */
    public function scopeGetIds($query, $ids)
    {
        $result = $query->whereIn('id', $ids)->get();
        $ids = [];
        foreach ($result as $items) {
            $ids[] = $items->id;
        }

        return $ids;
    }

    /**
     * Getting dropdown array of all countries.
     *
     * @author Jesse V
     * @param  boolean $addPlease
     * @return array
     */
    public static function getDropdownArray($addPlease = true)
    {
        return createDropdownArray(self::all(), 'id', 'name', $addPlease);
    }

    /**
     * Getting record with currency
     *
     * @author Jesse V
     * @param  integer    $id
     * @return Colleciton
     */
    public static function getWithCurrency($id = null)
    {
        if(is_null($id))
            $obj = self::with('currencies')->get();
        else
            $obj = self::with('currencies')->where('id', $id)->first();

        return $obj;
    }
}
