<?php

namespace App\Models;

trait UserRoles
{

    /**
     * The roles that belong to the user.
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Assigning role to user by role name.
     *
     * @author Jesse V
     * @param  string $role Role name
     */
    public function assignRole($role = "")
    {
        return $this->roles()->save(
            Role::whereName($role)->firstOrFail()
        );
    }

    /**
     * Assigning role to user by role id.
     *
     * @author Jesse V
     * @param  integer $roleId Role id
     * @return mixed
     */
    public function assignRoleWithId($roleId)
    {
        return $this->roles()->save(
            Role::whereId($roleId)->firstOrFail()
        );
    }

    /**
     * Checking if the role passed in is assigned to the user.
     *
     * @param  string|array $role It can be string or array of strings
     * @return boolean            return TRUE or FALSE if it gets any roles
     */
    public function hasRole($role)
    {
        if (is_string($role)) {
            // If certain role is assigned to user or not.
            return $this->roles->contains('name', $role);
        }

        // If array is passed in then get all roles and count them.
        return !!$role->intersect($this->roles)->count();
    }

    /**
     * Scope a query to get roles.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string                                $roleName
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetRoles($query, $roleName)
    {
        return $query->whereHas('roles', function ($subQuery) use ($roleName) {
            $subQuery->where('name', $roleName);
        });
    }


    /**
     * Assigning roles to a user.
     *
     * @author Jesse V
     * @param  object/array $roles A single object of roles or array or roles object will be passed
     */
    public function giveRoleTo($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                $this->giveRoleTo($role);
            }
        } else if ($roles instanceof Role) {
            return $this->roles()->save($roles);
        }
    }

    /**
     * Assigning roles to user after detaching previous ones.
     *
     * @author Jesse V
     * @param  array $roles
     * @return void
     */
    public function assignRolesToUser($roles)
    {
        if (is_array($roles)) {
            $this->roles()->detach();
            foreach ($roles as $roleId) {
                $this->assignRoleWithId($roleId);
            }
        }
    }
}
