<?php

namespace App\Models;

use Carbon\Carbon;

trait Attribute
{

    /**
     * Chagneing date time style when getting created date from database.
     *
     * @author Jesse V
     * @param  string $date
     */
    public function getCreatedAtAttribute($date)
    {
        Carbon::setLocale(config('app.locale'));
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('M j, Y');
    }
}
