<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use Attribute, Common;

    /**
     * Many permission are associated with many roles.
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * Assigning permission to a role.
     *
     * @author Jesse V
     * @param  Permission /array $permission Array of Permission obj or just Permission obj
     * @return Model
     */
    public function givePermissionTo($permission)
    {
        if (is_array($permission)) {
            foreach ($permission as $per) {
                $this->givePermissionTo($per);
            }
        } else if ($permission instanceof Permission) {
            return $this->permissions()->save($permission);
        }
    }

    /**
     * Assigning multiple permission.
     *
     * @author Jesse V
     * @param  string $permissionIds Comma separated Permission ids
     * @return void
     */
    public function assignPermissions($permissionIds = "")
    {
        $permissionArray = null;
        if (is_string($permissionIds)) {
            $permissionArray = explode(",", $permissionIds);
        }

        if (is_array($permissionArray)) {
            $this->permissions()->detach();
            foreach ($permissionArray as $pid) {
                if (($pObj = Permission::find($pid)) !== null) {
                    $this->givePermissionTo($pObj);
                }
            }
        }
    }

    /**
     * Creating role.
     *
     * @author Jesse V
     * @param  string     $name  Role name or slug
     * @param  string     $label Role label
     * @return collection
     */
    public static function createRole($name, $label)
    {
        $roleId = self::createRow('roles', $name, $label);

        return $role = Role::find($roleId);
    }

    /**
     * Creating roles from array.
     *
     * @author Jesse V
     * @param  array $roles Array containing roles to be created
     * @return array        Array with role objects
     */
    public static function createRoles($roles)
    {
        $roleObjects = [];
        foreach($roles as $name => $label) {
            $roleObjects[$name] = self::createRole($name, $label);
        }

        return $roleObjects;
    }

    /**
     * Create dropdown array for roles.
     *
     * @author Jesse V
     * @return array
     */
    public static function createRoleDropdownArray()
    {
        return createDropdownArray(self::all(), 'id', 'label');
    }
}
