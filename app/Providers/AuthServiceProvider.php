<?php

namespace App\Providers;

use App\Models\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->definingAndCheckingPermissions();
    }

    /**
     * This method will get permission from database and add permissions to the system
     * So when the middleware checks for any permissions it need to be defined.
     *
     * @author Jesse V
     * @return boolean
     */
    protected function definingAndCheckingPermissions()
    {
        // checking if table exists or not
        if (\Illuminate\Support\Facades\Schema::hasTable('permissions')) {

            // Defining different permissions.
            foreach ($this->getPermissions() as $permission) {

                // IMP NOTE
                // $test VARIABLE IS USED TO PASS IN AS DUMMY VARIABLE OTHERWISE
                // THIS METHOD WILL NO RUN IN CONTROLLER.
                Gate::define($permission->name, function ($user) use ($permission) {

                    // Checking if current user has sufficient role.
                    return $user->hasRole($permission->roles);
                });
            }
        }
    }

    /**
     * Getting all permissions with their roles.
     *
     * @author Jesse V
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getPermissions()
    {
        return Permission::with('roles')->get();
    }
}
