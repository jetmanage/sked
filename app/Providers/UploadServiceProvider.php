<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\UploadOne;

class UploadServiceProvider extends ServiceProvider {
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\Uploads\UploadServiceInterface', function ($app) {
            return new UploadOne();
        });
    }
}