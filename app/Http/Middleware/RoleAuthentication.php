<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;

class RoleAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * Controller mapping array.
         * - Indexes are name of the controller route.
         * - Values are name of the controller class.
         *
         * @var array
         */
        $urlToController = [
            'users' => 'user',
            'roles' => 'role',
            'dashboard' => 'dashboard',
        ];

        /**
         * Controller Action mapping array.
         * - Indexes are name of the action route.
         * - Values are name of the action in controller class.
         *
         * @var array
         */
        $urlToAction = [
            'index' => 'display',
            'create' => 'create',
            'store' => 'create',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'delete',
            'roles' => 'role',
            'user' => 'user',
        ];

        // Getting current page permission name.
        $permission = getPermissionFromRouteName(
            Route::currentRouteName(),
            $urlToController,
            $urlToAction
        );

        // If permission not found then pass on.
        if($permission == null)
            return $next($request);

        // Checking current page permission .
        if(\Gate::denies($permission, "")){
            abort(403, 'You are not allowed to view this page!');
        }

        \App::setLocale(session()->get('user_locale'));

        return $next($request);
    }
}
