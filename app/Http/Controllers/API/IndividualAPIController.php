<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIndividualAPIRequest;
use App\Http\Requests\API\UpdateIndividualAPIRequest;
use App\Models\Individual;
use App\Repositories\IndividualRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class IndividualController
 * @package App\Http\Controllers\API
 */

class IndividualAPIController extends AppBaseController
{
    /** @var  IndividualRepository */
    private $individualRepository;

    public function __construct(IndividualRepository $individualRepo)
    {
        $this->individualRepository = $individualRepo;
    }

    /**
     * Display a listing of the Individual.
     * GET|HEAD /individuals
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->individualRepository->pushCriteria(new RequestCriteria($request));
        $this->individualRepository->pushCriteria(new LimitOffsetCriteria($request));
        $individuals = $this->individualRepository->with(['tags', 'emails'])->all();

        return $this->sendResponse($individuals->toArray(), 'Individuals retrieved successfully');
    }

    /**
     * Store a newly created Individual in storage.
     * POST /individuals
     *
     * @param CreateIndividualAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateIndividualAPIRequest $request)
    {
        $input = $request->all();


        $individuals = $this->individualRepository->create($input);

        return $this->sendResponse($individuals->toArray(), 'Individual saved successfully');
    }

    /**
     * Display the specified Individual.
     * GET|HEAD /individuals/{id}
     *
     * @param  int $id
     *
     * @return Response
     */

    public function show($id)
    {
        /** @var Individual $individual */
        $individual = $this->individualRepository->findWithoutFail($id);
        $individual['address'] = $individual->addresses()->get();
        $individual['email'] = $individual->emails()->get();
        $individual['phone'] = $individual->phones()->get();
        $individual['note'] = $individual->notes()->get();
        $individual['tag'] = $individual->tags()->get();
        //$individual['user'] = $individual->users()->get();

        if (empty($individual)) {
            return $this->sendError('Individual not found');
        }

        return $this->sendResponse($individual->toArray(), 'Individual retrieved successfully');
    }

    /**
     * Update the specified Individual in storage.
     * PUT/PATCH /individuals/{id}
     *
     * @param  int $id
     * @param UpdateIndividualAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIndividualAPIRequest $request)
    {
        $input = $request->all();

        /** @var Individual $individual */
        $individual = $this->individualRepository->findWithoutFail($id);

        if (empty($individual)) {
            return $this->sendError('Individual not found');
        }

        $individual = $this->individualRepository->update($input, $id);

        return $this->sendResponse($individual->toArray(), 'Individual updated successfully');
    }

    /**
     * Remove the specified Individual from storage.
     * DELETE /individuals/{id}
     *
     * @param  int $id
     *
     * @return Response
     */

    public function destroy($id)
    {
        /** @var Individual $individual */
        $individual = $this->individualRepository->findWithoutFail($id);

        if (empty($individual)) {
            return $this->sendError('Individual not found');
        }

        $individual->delete();

        return $this->sendResponse($id, 'Individual deleted successfully');
    }

    /**
     * Add tag to the specified Individual.
     * POST /individuals/tag/{id}/{$tagId}
     *
     * @param  int $id
     *
     * @return Response
     */

    public function addTag($id, $tagId)
    {
        /** @var Individual $individual */
        $individual = $this->individualRepository->findWithoutFail($id);

        if (empty($individual)) {
            return $this->sendError('Individual not found');
        }

        dd($individual->tags()->attach(1));

        return $this->sendResponse($id, 'Individual tagged successfully');
    }

    /**
     * Remove tag to the specified Individual.
     * POST /individuals/tag/{id}/{$tagId}
     *
     * @param  int $id
     *
     * @return Response
     */

    public function removeTag($id, $tagId)
    {
        /** @var Individual $individual */
        $individual = $this->individualRepository->findWithoutFail($id);

        if (empty($individual)) {
            return $this->sendError('Individual not found');
        }

        dd($individual->tags()->detach(1));

        return $this->sendResponse($id, 'Individual tag detached successfully');
    }



}
