<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePassengerAPIRequest;
use App\Http\Requests\API\UpdatePassengerAPIRequest;
use App\Models\Passenger;
use App\Repositories\PassengerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PassengerController
 * @package App\Http\Controllers\API
 */

class PassengerAPIController extends AppBaseController
{
    /** @var  PassengerRepository */
    private $passengerRepository;

    public function __construct(PassengerRepository $passengerRepo)
    {
        $this->passengerRepository = $passengerRepo;
    }

    /**
     * Display a listing of the Passenger.
     * GET|HEAD /passengers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->passengerRepository->pushCriteria(new RequestCriteria($request));
        $this->passengerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $passengers = $this->passengerRepository->all();

        return $this->sendResponse($passengers->toArray(), 'Passengers retrieved successfully');
    }

    /**
     * Store a newly created Passenger in storage.
     * POST /passengers
     *
     * @param CreatePassengerAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePassengerAPIRequest $request)
    {
        $input = $request->all();

        $passengers = $this->passengerRepository->create($input);

        return $this->sendResponse($passengers->toArray(), 'Passenger saved successfully');
    }

    /**
     * Display the specified Passenger.
     * GET|HEAD /passengers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Passenger $passenger */
        $passenger = $this->passengerRepository->findWithoutFail($id);

        if (empty($passenger)) {
            return $this->sendError('Passenger not found');
        }

        return $this->sendResponse($passenger->toArray(), 'Passenger retrieved successfully');
    }

    /**
     * Update the specified Passenger in storage.
     * PUT/PATCH /passengers/{id}
     *
     * @param  int $id
     * @param UpdatePassengerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePassengerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Passenger $passenger */
        $passenger = $this->passengerRepository->findWithoutFail($id);

        if (empty($passenger)) {
            return $this->sendError('Passenger not found');
        }

        $passenger = $this->passengerRepository->update($input, $id);

        return $this->sendResponse($passenger->toArray(), 'Passenger updated successfully');
    }

    /**
     * Remove the specified Passenger from storage.
     * DELETE /passengers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Passenger $passenger */
        $passenger = $this->passengerRepository->findWithoutFail($id);

        if (empty($passenger)) {
            return $this->sendError('Passenger not found');
        }

        $passenger->delete();

        return $this->sendResponse($id, 'Passenger deleted successfully');
    }
}
