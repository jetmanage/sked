<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\UpdateUserCouponRequest;
use App\Models\User;
use App\Models\Role;
use App\Http\Requests;
use App\Models\CallCenter;
use Illuminate\Http\Request;
use App\Services\Admin\UserService;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateUserRequest;
use App\Http\Requests\Admin\UpdateUserRequest;

class UserController extends Controller
{

    /**
     * Variable to store service class object.
     *
     * @author Jesse V
     * @var object
     */
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @author Jesse V
     * @param  UserService $user
     */
    public function __construct(UserService $user)
    {
        $this->middleware('auth');

        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @author Jesse V
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // For header.
        $page = [
            trans('breadcrumbs.users'),
            'userIndex',
        ];

        if (Gate::allows('user-create', '')) {
            $page[] = 'Admin\UserController@create';
        }

        return view('admin.user.index', compact('page'));
    }

    /**
     * Ajax call to show users with their roles.
     *
     * @author Jesse V
     * @param  integer $page
     * @return json
     */
    public function list($page = 20)
    {
        return $this->user->getUserWithRoles($page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @author Jesse V
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // For header.
        $page = [
            trans('breadcrumbs.createAUser'),
            'userCreate',
        ];

        return view('admin.user.create', compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @author Jesse V
     * @param  CreateUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $this->user->create($request->all());

        return $this->user->success(trans('user.messages.store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @author Jesse V
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // For header.
        $page = [
            trans('breadcrumbs.editAUser'),
            'userEdit'
        ];
        $user = $this->user->findById($id);

        return view('admin.user.edit', compact(
            'user', 'page'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @author Jesse V
     * @param  UpdateUserRequest $request
     * @param  int               $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $this->user->update($request->all(), $id);

        return $this->user->success(trans('user.messages.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @author Jesse V
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->delete($id)) {
            return back()->with('success', trans('user.messages.destroy.success'));
        } else {
            return back()->with('error', trans('user.messages.destroy.error'));
        }
    }

    /**
     * Display page to assign roles to user.
     *
     * @author Jesse V
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function roles($id)
    {
        // For header.
        $page = [
            trans('breadcrumbs.assignRolesToAUser'),
            'userRole'
        ];
        $roles = Role::createRoleDropdownArray();
        $user = $this->user->findById($id);
        $selectedRoles = $user->getRoleDropdownArray();

        return view('admin.user.roles', compact('user', 'roles', 'selectedRoles', 'page'));
    }

    /**
     * Assigning Roles to user.
     *
     * @author Jesse V
     * @param  Request $request
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function saveRole(Request $request, $id)
    {
        $this->user->assignRolesToUser($request->roles, $id);

        return $this->user->success(trans('user.messages.role'));
    }
}
