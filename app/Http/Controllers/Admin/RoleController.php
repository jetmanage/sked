<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\Admin\RoleService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\RoleRequest;

class RoleController extends Controller
{

    /**
     * Variable to store service class object.
     *
     * @author Jesse V
     * @var object
     */
    protected $role;

    /**
     * Create a new controller instance.
     *
     * @author Jesse V
     * @param  RoleService $role
     */
    public function __construct(RoleService $role)
    {
        $this->middleware('auth');

        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @author Jesse V
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // For header.
        $page = [
            trans('breadcrumbs.roles'),
            'roleIndex',
        ];

        if (Gate::allows('role-create', '')) {
            $page[] = 'Admin\RoleController@create';
        }

        return view('admin.role.index', compact('page'));
    }

    /**
     * Listing json formate for table view
     *
     * @author Jesse V
     * @param  integer $page
     * @return json
     */
    public function list($page = 20)
    {
        return $this->role->getRoleList($page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @author Jesse V
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // For header.
        $page = [
            trans('breadcrumbs.createARole'),
            'roleCreate',
        ];

        return view('admin.role.create', compact('page'));
    }

    /**
     * Retrieving json for jtree js component.
     *
     * @author Jesse V
     * @param  Request $request
     * @return json
     */
    public function permission(Request $request)
    {
        return $this->role->getPermssions($request->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @author Jesse V
     * @param  RoleRequest               $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $this->role->create($request->all());

        return $this->role->success(trans('role.messages.store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @author Jesse V
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // For header.
        $page = [
            trans('breadcrumbs.editARole'),
            'roleEdit',
        ];
        $role = $this->role->findById($id);

        return view('admin.role.edit', compact('role', 'page'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @author Jesse V
     * @param  RoleRequest               $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        $this->role->update($request->all(), $id);

        return $this->role->success(trans('role.messages.update'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @author Jesse V
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->role->delete($id)) {
            return back()->with('success', trans('role.messages.destroy.success'));
        } else {
            return back()->with('error', trans('role.messages.destroy.error'));
        }
    }
}
