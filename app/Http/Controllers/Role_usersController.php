<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Role_user;

use DB;

class Role_usersController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('role_users.index', []);
	}

	public function create(Request $request)
	{
	    return view('role_users.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$role_user = Role_user::findOrFail($id);
	    return view('role_users.add', [
	        'model' => $role_user	    ]);
	}

	public function show(Request $request, $id)
	{
		$role_user = Role_user::findOrFail($id);
	    return view('role_users.show', [
	        'model' => $role_user	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM role_user a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE role_id LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$role_user = null;
		if($request->id > 0) { $role_user = Role_user::findOrFail($request->id); }
		else { 
			$role_user = new Role_user;
		}
	    

	    		
					    $role_user->user_id = $request->user_id;
		
	    		
					    $role_user->role_id = $request->role_id;
		
	    	    //$role_user->user_id = $request->user()->id;
	    $role_user->save();

	    return redirect('/role_users');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$role_user = Role_user::findOrFail($id);

		$role_user->delete();
		return "OK";
	    
	}

	
}