<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIndividualRequest;
use App\Http\Requests\UpdateIndividualRequest;
use App\Repositories\IndividualRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class IndividualController extends AppBaseController
{
    /** @var  IndividualRepository */
    private $individualRepository;

    public function __construct(IndividualRepository $individualRepo)
    {
        $this->individualRepository = $individualRepo;
    }

    /**
     * Display a listing of the Individual.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->individualRepository->pushCriteria(new RequestCriteria($request));
        $individuals = $this->individualRepository->all();

        return view('individuals.index')
            ->with('individuals', $individuals);
    }

    /**
     * Show the form for creating a new Individual.
     *
     * @return Response
     */
    public function create()
    {
        return view('individuals.create');
    }

    /**
     * Store a newly created Individual in storage.
     *
     * @param CreateIndividualRequest $request
     *
     * @return Response
     */
    public function store(CreateIndividualRequest $request)
    {
        $input = $request->all();

        $individual = $this->individualRepository->create($input);

        Flash::success('Individual saved successfully.');

        return redirect(route('individuals.index'));
    }

    /**
     * Display the specified Individual.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $individual = $this->individualRepository->findWithoutFail($id);

        if (empty($individual)) {
            Flash::error('Individual not found');

            return redirect(route('individuals.index'));
        }

        return view('individuals.show')->with('individual', $individual);
    }

    /**
     * Show the form for editing the specified Individual.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $individual = $this->individualRepository->findWithoutFail($id);

        if (empty($individual)) {
            Flash::error('Individual not found');

            return redirect(route('individuals.index'));
        }

        return view('individuals.edit')->with('individual', $individual);
    }

    /**
     * Update the specified Individual in storage.
     *
     * @param  int              $id
     * @param UpdateIndividualRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIndividualRequest $request)
    {
        $individual = $this->individualRepository->findWithoutFail($id);

        if (empty($individual)) {
            Flash::error('Individual not found');

            return redirect(route('individuals.index'));
        }

        $individual = $this->individualRepository->update($request->all(), $id);

        Flash::success('Individual updated successfully.');

        return redirect(route('individuals.index'));
    }

    /**
     * Remove the specified Individual from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $individual = $this->individualRepository->findWithoutFail($id);

        if (empty($individual)) {
            Flash::error('Individual not found');

            return redirect(route('individuals.index'));
        }

        $this->individualRepository->delete($id);

        Flash::success('Individual deleted successfully.');

        return redirect(route('individuals.index'));
    }
}
