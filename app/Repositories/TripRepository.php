<?php

namespace App\Repositories;

use App\Models\Trip;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TripRepository
 * @package App\Repositories
 * @version October 31, 2017, 2:26 pm UTC
 *
 * @method Trip findWithoutFail($id, $columns = ['*'])
 * @method Trip find($id, $columns = ['*'])
 * @method Trip first($columns = ['*'])
*/
class TripRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'aircraft_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Trip::class;
    }
}
