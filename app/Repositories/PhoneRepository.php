<?php

namespace App\Repositories;

use App\Models\Phone;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PhoneRepository
 * @package App\Repositories
 * @version October 12, 2017, 12:30 pm UTC
 *
 * @method Phone findWithoutFail($id, $columns = ['*'])
 * @method Phone find($id, $columns = ['*'])
 * @method Phone first($columns = ['*'])
*/
class PhoneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ind_id',
        'phone',
        'phone_type',
        'is_textable',
        'is_verified',
        'is_primary'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Phone::class;
    }
}
