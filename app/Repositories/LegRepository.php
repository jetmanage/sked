<?php

namespace App\Repositories;

use App\Models\Leg;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LegRepository
 * @package App\Repositories
 * @version October 31, 2017, 2:26 pm UTC
 *
 * @method Leg findWithoutFail($id, $columns = ['*'])
 * @method Leg find($id, $columns = ['*'])
 * @method Leg first($columns = ['*'])
*/
class LegRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'trip_id',
        'trip_leg_order',
        'from_airport_id',
        'to_airport_id',
        'depart_date',
        'out_time',
        'off_time',
        'arrival_date',
        'on_time',
        'in_time',
        'out_fuel',
        'in_fuel',
        'out_hobbs',
        'in_hobbs',
        'out_tach',
        'in_tach',
        'regs',
        'aircraft_id',
        'registration'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Leg::class;
    }
}
