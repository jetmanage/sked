<?php

namespace App\Repositories;

use App\Models\Aircraft;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AircraftRepository
 * @package App\Repositories
 * @version October 31, 2017, 2:25 pm UTC
 *
 * @method Aircraft findWithoutFail($id, $columns = ['*'])
 * @method Aircraft find($id, $columns = ['*'])
 * @method Aircraft first($columns = ['*'])
*/
class AircraftRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'registration',
        'type_id',
        'serial_number',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Aircraft::class;
    }
}
