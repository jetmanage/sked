<?php

namespace App\Repositories;

use App\Models\Airport;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AirportRepository
 * @package App\Repositories
 * @version November 8, 2017, 8:41 am UTC
 *
 * @method Airport findWithoutFail($id, $columns = ['*'])
 * @method Airport find($id, $columns = ['*'])
 * @method Airport first($columns = ['*'])
*/
class AirportRepository extends BaseRepository
{
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'airport_entry_id',
        'code',
        'city',
        'country',
        'faaCode',
        'iata',
        'icao',
        'landingFacilityId',
        'latitude',
        'longitude',
        'typicallyHasMetar',
        'typicallyHasTaf',
        'name',
        'state',
        'zip',
        'fullCountryNameLowerCase',
        'magVarWest',
        'access',
        'type',
        'hasFuel',
        'fuelTypes',
        'longestRunwayLength',
        'longestRunwaySurfaceType',
        'nearestlandingFacilityIds',
        'elevationFt',
        'procedureTypes',
        'towered'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Airport::class;
    }
}
