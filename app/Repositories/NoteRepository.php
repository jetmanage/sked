<?php

namespace App\Repositories;

use App\Models\Note;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class NoteRepository
 * @package App\Repositories
 * @version October 10, 2017, 9:24 am UTC
 *
 * @method Note findWithoutFail($id, $columns = ['*'])
 * @method Note find($id, $columns = ['*'])
 * @method Note first($columns = ['*'])
*/
class NoteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ind_id',
        'note_type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Note::class;
    }
}
