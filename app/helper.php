<?php

/**
 * Breaking down permission name.
 *
 * @author Jesse V
 * @param  string $name Permission name
 * @return void
 */
function explodePermissionName($name = "")
{
	$val = null;
	if (strpos($name, '-') !== false) {
        list($mainkey, $val) = explode("-", $name);
    } else {
        $mainkey = $name;
    }

    return [$mainkey, $val];
}

/**
 * Breaking down the url into array with 3 values.
 *
 * @author Jesse V
 * @param  string $path
 * @return array
 */
function convertUrlToArray($path = "")
{
    $explode = explode("/", $path);

    $return = [];
    for ($index = 0; $index < 3; $index++) {
        $return[$index] = isset($explode[$index]) ? $explode[$index] : null;
    }

    return $return;
}

/**
 * Splitting route name and getting correct controller and action name to check
 * user's permission.
 *
 * @author Jesse V
 * @param  string $routeName       Laravel's route name
 * @param  array  $controllerArray Array containing controller names
 * @param  array  $actionArray     Array containing action names
 * @return string|null             Permission's name or null
 */
function getPermissionFromRouteName($routeName, $controllerArray, $actionArray)
{
    if (strpos($routeName, ".")) {
        list($controller, $action) = explode("." , $routeName);

        $first = isset($controllerArray[$controller])
            ? $controllerArray[$controller]
            : null;

        $second = isset($actionArray[$action]) ? $actionArray[$action] : null;

        if ($first != null && $second != null)
            return "$first-$second";
        else
            return null;
    }

    return null;
}

/**
 * This method will take db object and create array to show drop down in view.
 *
 * @author Jesse V
 * @param  collection $dbObject  Database Collection object
 * @param  string     $id        Value for the array's index
 * @param  string     $value     Value for the array's value
 * @param  boolean    $addPlease To add "Please select" in the array or not
 * @return array
 */
function createDropdownArray(
    $dbObject = null,
    $id = 'id',
    $value = 'name',
    $addPlease = true
) {
    $array = [];

    if ($addPlease === true)
        $array[""] = trans('crm.select');

    if ($dbObject) {
        foreach ($dbObject as $obj) {
            if ($obj != null) {
                if ($id == null) {
                    $array[] = arrayToString($obj, $value);
                } else {
                    $array[$obj->$id] = arrayToString($obj, $value);
                }
            }
        }
    }

    return $array;
}

/**
 * Converting array of objects to string.
 *
 * @author Jesse V
 * @param  collection $obj   Database collection object
 * @param  array      $array Array of objects or string
 * @return string
 */
function arrayToString($obj, $array)
{
    $string = "";

    if (is_array($array)) {
        foreach ($array as $val) {
            $string .= " {$obj->$val} ";
        }
    } else {
        $string = $obj->$array;
    }

    return $string;
}

/**
 * Getting attribute from database object relationship and then added it to existing
 * database object. This is done because we cannot retrieve any attribute from an
 * object's relationship. Eg. Getting phone number from different table and adding
 * it to user's object and then displaying it on a page in one join query.
 *
 * @author Jesse V
 * @param  collection $dbCollection Database collection object
 * @param  string     $relationship Name of the eloquent relationship
 * @param  string     $oldAttribute Name of the attribute from join table
 * @param  string     $newAttribute Name of the new attribute
 * @return collection
 */
function addNewAttributeToCollection(
    $dbCollection,
    $relationship,
    $oldAttribute,
    $newAttribute
) {

    $oldFirst = null;
    $oldSecond = null;

    // If old attribute is an array break down into two variable.
    if (is_array($oldAttribute))
        list($oldFirst, $oldSecond) = $oldAttribute;

    foreach ($dbCollection as $row) {

        // Checking if there are multiple relationships.
        if ($row->$relationship instanceof \Illuminate\Database\Eloquent\Collection) {
            $retrunArray = [];

            // Getting values from relationship table and saving it as a new attribute
            // to an existing database object.
            foreach ($row->$relationship as $object) {
                if (is_array($oldAttribute))
                    $retrunArray[] = $object->$oldFirst . " " . $object->$oldSecond;
                else
                    $retrunArray[] = $object->$oldAttribute;
            }

            $row->$newAttribute = implode(", ", $retrunArray);
        } else {

            // If it is a single relationship, then get value from relationship table
            // and add as a new attribute to existing database object.
            if ($row->$relationship) {
                if (is_array($oldAttribute)) {
                    $row->$newAttribute = $row->$relationship->$oldFirst
                        . " "
                        . $row->$relationship->$oldSecond;
                }
                else {
                    $row->$newAttribute = $row->$relationship->$oldAttribute;
                }
            }
        }
    }

    // Returning existing database object with new attribute, which as retrieved from
    // the relationship table.
    return $dbCollection;
}

/**
 * Counting the rows from relationship table and saving it as a new attribute to an
 * exiting database object.
 *
 * @author Jesse V
 * @param  collection $dbCollection Database object in which new attribute will be added
 * @param  string     $relationship Name of the relationship
 * @param  string     $newAttribute Name of the new attribute to be added
 * @param  boolean    $remove       To remove relationship
 * @return collection
 */
function countRelationshipRows($dbCollection, $relationship, $newAttribute, $remove = false)
{
    foreach ($dbCollection as $row) {
        if ($row->$relationship instanceof \Illuminate\Database\Eloquent\Collection) {
            $row->$newAttribute = count($row->$relationship);
        } else {
            $row->$newAttribute = 0;
        }

        if($remove == true)
            unset($row->$relationship);
    }

    return $dbCollection;
}

/**
 * This is an automated method to save new attribute to a Model class object.
 *
 * @author Jesse V
 * @param  array      $request
 * @param  collection $classObject
 * @param  array      $array
 * @return collection
 */
function assignRequestValue($request, $classObject, $array)
{
    if (is_array($request)) {
        foreach ($array as $item) {
            $classObject->$item = empty($request[$item]) ? null : $request[$item];
        }
    } else {
        foreach ($array as $item) {
            $classObject->$item = empty($request->$item) ? null : $request->$item;
        }
    }
    return $classObject;
}

/**
 * Get order status.
 *
 * @author Jesse V
 * @return array
 */
function getOrderStatusDropdownArray()
{
    return [
        'unpaid' => trans('crm.order.status.unpaid'),
        'paid' => trans('crm.order.status.paid'),
        'refunded' => trans('order.status.refund'),
        'chargeback' => trans('order.status.chargeback')
    ];
}

/**
 * This method will check if the current user's role is same to the roles passed
 * and return true or false.
 *
 * @author Jesse V
 * @param  string  $role_name
 * @return boolean
 */
function checkUserRole($role_name = null)
{
    $user_role = session()->get('user_role');

    if (is_string($role_name))
        $role_name = [$role_name];

    if ($user_role != null) {
        foreach ($user_role as $role) {
            if (in_array($role->name, $role_name))
                return true;
        }
    }

    return false;
}

/**
 * Checking if the user is or super admin or admin role.
 *
 * @author Jesse V
 * @return boolean
 */
function isCurrentUserAdmin()
{
    return checkUserRole(['super-admin', 'admin']);
}

/**
 * Get hours in an array.
 *
 * @author Jesse V
 * @return array
 */
function getHours()
{
    return [
        ""=> "Select",
        "1"=> "1 : 00",
        "2"=> "2 : 00",
        "3"=> "3 : 00",
        "4"=> "4 : 00",
        "5"=> "5 : 00",
        "6"=> "6 : 00",
        "7"=> "7 : 00",
        "8"=> "8 : 00",
        "9"=> "9 : 00",
        "10"=> "10 : 00",
        "11"=> "11 : 00",
        "12"=> "12 : 00",
        "13"=> "13 : 00",
        "14"=> "14 : 00",
        "15"=> "15 : 00",
        "16"=> "16 : 00",
        "17"=> "17 : 00",
        "18"=> "18 : 00",
        "19"=> "19 : 00",
        "20"=> "20 : 00",
        "21"=> "21 : 00",
        "22"=> "22 : 00",
        "23"=> "23 : 00",
        "24"=> "24 : 00"
    ];
}

/**
 * This function is use to add "required" to Form element.
 *
 * @author Jesse V
 * @param  string $required Boolean value to add "required" or not
 * @param  array  $array    Array in which "required" will be added
 * @return array
 */
function addRequiredAttribute($required = "", $array = []) {

    $array['class'] = 'form-control';
    $req = $required;

    if (is_string($required)) {
        if ($required == "true")
            $req = true;

        if ($required == "false")
            $req = false;
    }

    if ($req == true) {
        $array['required'] = 'required';
    }

    return $array;
}

/**
 * Get reporting range filter.
 *
 * @author Jesse V
 * @return array
 */
function getReportRangeFilter()
{
    return [
        'today' => trans('report.agent.view.range.today'),
        'yesterday' => trans('report.agent.view.range.yesterday'),
        'this_week' => trans('report.agent.view.range.thisWeek'),
        'last_week' => trans('report.agent.view.range.lastWeek'),
        'this_month' => trans('report.agent.view.range.thisMonth'),
        'late_month' => trans('report.agent.view.range.lateMonth'),
        'year_to_date' => trans('report.agent.view.range.yearToDate'),
        'late_year' => trans('report.agent.view.range.lateYear'),
        'custom_range' => trans('report.agent.view.range.customRange'),
    ];
}

/**
 * Adding prefix to array its key or to its value.
 *
 * @author Jesse V
 * @param  array $array
 * @param  string $index
 * @param  string $value
 * @return array
 */
function addPrefix($array, $index = null, $value = null)
{
    $ret = [];
    foreach ($array as $key => $item) {
        if ($index != null) {
            $ret[$index.$key] = $item;
        }

        if ($value != null) {
            $ret[$key] = $value.$item;
        }
    }

    return $ret;
}

/**
 * Getting locale for bootstrap js table
 *
 * @author Jesse V
 * @return string
 */
function getLocale()
{
    return config('app.fallback_locale'). '-' . strtoupper(config('app.fallback_locale'));
}

/**
 * Getting required locale for js table
 *
 * @author Jesse V
 * @return string
 */
function getJsLocale()
{
    if (config('app.locale') == 'en')
        return '//cdn.datatables.net/plug-ins/1.10.13/i18n/English.json';

    else if (config('app.locale') == 'es')
        return '//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json';

    else if (config('app.locale') == 'fr')
        return '//cdn.datatables.net/plug-ins/1.10.13/i18n/French.json';
}

/**
 * Getting locale array.
 *
 * @author Jesse V
 * @return array
 */
function getLocaleArray()
{
    return [
        'en' => trans('crm.locale.english'),
        'es' => trans('crm.locale.spanish'),
        'fr' => trans('crm.locale.french'),
    ];
}

/**
 * Getting page value from url
 *
 * @author Jesse V
 * @param  string $url
 * @return string
 */
function getPage($url)
{
    $array = parse_url($url);
    if (isset($array['query'])){
        parse_str($array['query'], $query);
        $query = $query['page'];
    } else {
        $query = null;
    }

    return $query;
}

/**
 * Cleaning phone number and adding value to db.
 *
 * @author Jesse V
 * @param  string $string
 * @return integer
 */
function cleanPhone($string) {
    // Replaces all spaces with hyphens.
    $string = str_replace(' ', '-', $string);

    // Removes special chars.
    return preg_replace('/[^0-9]/', '', $string);
}
