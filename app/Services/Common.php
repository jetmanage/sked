<?php

namespace App\Services;

use App\Models\Order;

trait Common
{

    /**
     * Variable to save model class used by service class.
     *
     * @author Jesse V
     * @var object
     */
    protected $model;

    /**
     * Variable to save current controller route, which will be used for redirection.
     *
     * @author Jesse V
     * @var string
     */
    protected $controllerRoute;

    /**
     * Setter for Controller Route.
     *
     * @author Jesse V
     * @param  string $route
     * @return void
     */
    public function setControllerRoute($route)
    {
        $this->controllerRoute = $route;
    }

    /**
     * Getting the controller route.
     *
     * @author Jesse V
     * @return string
     */
    public function getControllerRoute()
    {
        return $this->controllerRoute;
    }

    /**
     * Setting model for current service class.
     *
     * @author Jesse V
     * @param  $model
     * @return void
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * Getting the model class.
     *
     * @author Jesse V
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Getting current class string name
     *
     * @author Jesse V
     * @return string
     */
    public function getClassName()
    {
        $obj = $this->getModel();
        $name = explode('\\', get_class($obj));

        return array_pop($name);
    }

    /**
     * Common method to find rows or row of any model by id.
     *
     * @author Jesse V
     * @param  integer $id
     * @return collection
     */
    public function findById($id)
    {
        $obj = $this->getModel();
        $name = $this->getClassName();

        // Will throw ModelNotFoundException exception
        $row = $obj->findOrFail($id);

        return $row;
    }

    /**
     * Deleting row from database using model.
     *
     * @author Jesse V
     * @param  integer $id
     * @return boolean
     */
    public function delete($id)
    {
        $obj = $this->findById($id);

        return $obj->delete();
    }
}
