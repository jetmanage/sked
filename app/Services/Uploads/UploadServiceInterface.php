<?php
namespace App\Services\Uploads;

Interface UploadServiceInterface
{
    public function upload();
}