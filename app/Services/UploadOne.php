<?php
namespace App\Services;

use App\Services\Uploads\UploadServiceInterface;

class UploadOne implements UploadServiceInterface
{
    public function upload()
    {
        return 'Output from ONE';
    }
}