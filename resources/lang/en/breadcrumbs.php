<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Site Breadcrumbs
    |--------------------------------------------------------------------------
    */

    'home' => 'Home',
    'dashboard' => 'Dashboard',
    'roles' => 'Roles',
    'edit' => 'Edit',
    'create' => 'Create',
    'users' => 'Users',
    'role' => 'Role',
    'user' => 'User',

    'createAUser' => 'Create a User',
    'editAUser' => 'Edit a User',
    'assignRolesToAUser' => 'Assign Roles to a User',

    'createARole' => 'Create a Role',
    'editARole' => 'Edit a Role',
];
