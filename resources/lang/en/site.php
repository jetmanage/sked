<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Site
    |--------------------------------------------------------------------------
    |
    */
    'rights' => 'All RIGHT RESERVED',
    'select' => 'Please Select',
    'locale' => [
        'english' => 'English',
        'spanish' => 'Spanish',
        'french' => 'French',
    ],
    'name'     => 'InApp',
    'leftMenu' => [
        'users'         => 'User Management',
        'roles'         => 'Role Management',
    ],
    'login'    => [
        'title'         => 'Sign In',
        'username'      => 'Username',
        'email'         => 'Email',
        'password'      => 'Password',
        'rememberMe'    => 'Remember me',
        'forgot'        => 'Forgot password',
        'signIn'        => 'Sign in',
        'copyRightSite' => 'Sked',
    ],
    'error' => [
        'list' => 'Errors listed below',
        'page' => 'Page not found!',
    ],
    'view' => [
        'list' => [
            'createdAt' => 'Created At',
            'action' => 'Action',
        ],
        'button' => [
            'close' => 'Close',
            'saveChanges' => 'Save Changes',
        ],
    ],
    'js' => [
        'delete' => [
            'yes' => 'Yes',
            'cancel' => 'Cancel',
        ],
    ],
];