<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CRM Role
    |--------------------------------------------------------------------------
    */
    'messages' => [
        'store'   => 'Role added successfully.',
        'update'  => 'Role updated successfully.',
        'destroy' => [
            'success' => 'Role deleted successfully.',
            'error'   => 'Something went wrong while deleting role.',
        ],
    ],
    'view'     => [
        'list' => [
            'label' => 'Label',
        ],
        'form' => [
            'create'          => 'Create Role',
            'edit'            => 'Edit Role',
            'title'           => 'Role Display Name',
            'roleName'        => 'Role Name',
            'title2'          => 'Role Permissions',
            'permissionsTree' => 'Permissions Tree',
        ],
    ],
    'js'       => [
        'delete'     => [
            'message' => 'Are you sure you want to do delete this role?',
        ],
        'validation' => [
            'label' => 'The role name is required and cannot be empty',
        ],
    ],
];
