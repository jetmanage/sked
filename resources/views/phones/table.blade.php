<table class="table table-responsive" id="phones-table">
    <thead>
        <tr>
            <th>Ind Id</th>
        <th>Phone</th>
        <th>Phone Type</th>
        <th>Is Textable</th>
        <th>Is Verified</th>
        <th>Is Primary</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($phones as $phone)
        <tr>
            <td>{!! $phone->ind_id !!}</td>
            <td>{!! $phone->phone !!}</td>
            <td>{!! $phone->phone_type !!}</td>
            <td>{!! $phone->is_textable !!}</td>
            <td>{!! $phone->is_verified !!}</td>
            <td>{!! $phone->is_primary !!}</td>
            <td>
                {!! Form::open(['route' => ['phones.destroy', $phone->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('phones.show', [$phone->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('phones.edit', [$phone->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>