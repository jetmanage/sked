<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $phone->id !!}</p>
</div>

<!-- Ind Id Field -->
<div class="form-group">
    {!! Form::label('ind_id', 'Ind Id:') !!}
    <p>{!! $phone->ind_id !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $phone->phone !!}</p>
</div>

<!-- Phone Type Field -->
<div class="form-group">
    {!! Form::label('phone_type', 'Phone Type:') !!}
    <p>{!! $phone->phone_type !!}</p>
</div>

<!-- Is Textable Field -->
<div class="form-group">
    {!! Form::label('is_textable', 'Is Textable:') !!}
    <p>{!! $phone->is_textable !!}</p>
</div>

<!-- Is Verified Field -->
<div class="form-group">
    {!! Form::label('is_verified', 'Is Verified:') !!}
    <p>{!! $phone->is_verified !!}</p>
</div>

<!-- Is Primary Field -->
<div class="form-group">
    {!! Form::label('is_primary', 'Is Primary:') !!}
    <p>{!! $phone->is_primary !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $phone->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $phone->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $phone->deleted_at !!}</p>
</div>

