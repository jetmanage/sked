@if( count($errors->all()) )
<div class="panel panel-primary panel-line">
  <div class="panel-heading">
    <h3 class="panel-title">@lang('site.error.list')</h3>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <blockquote class="blockquote custom-blockquote blockquote-danger">
          <ul>
            @foreach($errors->all() as $error)
            <li >{{ $error }}</li>
            @endforeach
            </ul>
          </blockquote>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
