@extends('admin.role.view')


@section("roles")
  {{ Form::model($role, ['route' => ['roles.update', $role->id], 'method' => 'patch', 'id' => 'roleform', 'name' => 'roleform', "class" => "form-horizontal"]) }}

    {{ Form::hidden('jsfields', null, ["id" => "jsfields"]) }}
    {{ Form::hidden('id', null, ["id" => "rid"]) }}

    @include('admin.role.partials.form', ['submitButtonText'=>  trans('role.view.form.edit')])

  {{ Form::close() }}
@stop
