@extends('admin.role.view')


@section("roles")
<div class="col-xs-12">
  <div class="panel panel-primary panel-line">
    <div class="panel-heading">&nbsp;</div>
    <div class="panel-body">
      <div class="row">

        <div class="col-md-12 col-xs-12">

        	<!-- Table load -->
        	<div class="wrap m-sm-0">
          <div class="">
            <table data-toggle="table" data-url="{{ url('roles/list/') }}" data-height="500" data-mobile-responsive="true" data-pagination="true" data-locale="{{ getLocale() }}">

              <thead>
                <tr>
                  <th data-field="label">@lang('role.view.list.label')</th>
                  <th data-field="created_at">@lang('site.view.list.createdAt')</th>
                  @if(Auth::user()->can('role-update', '') || Auth::user()->can('role-delete', ''))
                    <th data-field="action" data-formatter="actionRoleFormatter" data-events="actionRoleEvents">@lang('site.view.list.action')</th>
                  @endif
                </tr>
              </thead>
            </table>
          </div>
        </div>
        <!-- End Table load -->

        </div>

      </div>
    </div>
  </div>
</div>
@stop
