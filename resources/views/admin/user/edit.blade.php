@extends('admin.user.view')


@section("users")
    {{ Form::model($user, ['route' => [isset($route) ? $route : 'users.update', $user->id], 'method' => 'patch', 'id' => 'userformedit', 'name' => 'userformedit', "class" => "form-horizontal"]) }}

    @include('admin.user.partials.form', [
        'submitButtonText' => trans('user.view.form.edit'),
        'type' => 'edit'
    ])

    {{ Form::close() }}
@stop
