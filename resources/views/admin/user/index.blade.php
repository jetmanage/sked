@extends('admin.user.view')


@section("users")
<div class="col-xs-12">
  <div class="panel panel-primary panel-line">
    <div class="panel-heading">&nbsp;</div>
    <div class="panel-body">
      <div class="row">

        <div class="col-md-12 col-xs-12">

          <!-- Table load -->
          <div class="wrap m-sm-0">
            <div class="">
              <table data-toggle="table" data-url="{{ url('users/list/') }}" data-height="500" data-mobile-responsive="true" data-pagination="true"  data-locale="{{ getLocale() }}">
                <thead>
                  <tr>
                    <!-- <th data-field="id">Number</th> -->
                    <th data-field="name">@lang('user.view.list.fullName')</th>
                    <th data-field="username">@lang('user.view.list.username')</th>
                    <th data-field="email">@lang('user.view.list.email')</th>
                    <th data-field="role_labels">@lang('user.view.list.roles')</th>
                    <th data-field="created_at">@lang('site.view.list.createdAt')</th>
                    @if(Auth::user()->can('user-update', '') || Auth::user()->can('user-delete', '') || Auth::user()->can('user-role', ''))
                    <th data-field="action" data-formatter="actionFormatter" data-events="actionEvents">@lang('site.view.list.action')</th>
                    @endif
                  </tr>
                </thead>
              </table>
            </div>
          </div>
          <!-- End Table load -->

        </div>

      </div>
    </div>
  </div>
</div>
@stop

