@include('errors.formerror')

<div class="panel panel-primary panel-line">
    <div class="panel-heading">
        <h3 class="panel-title">@lang('user.view.form.title')</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">

            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    {{ Form::label(trans('user.view.form.email').':') }}
                    @if($type == 'create')
                        {{ Form::email('email', null, ['class' => 'form-control', 'required' => "required"]) }}
                    @else
                        {{ Form::email('email', null, ['class' => 'form-control', 'disabled' => "disabled"]) }}
                    @endif
                </div>
                <div class="form-group">
                    {{ Form::label(trans('user.view.form.password').':') }}
                    {{ Form::password('password',  ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    {{ Form::label(trans('user.view.form.fullName').':') }}
                    {{ Form::text('name', null , ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::label(trans('user.view.form.rePassword').':') }}
                    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    {{ Form::button($submitButtonText, ['type' => 'submit', 'class' => "form-control btn btn-block btn-primary waves-effect",  'id' => "sub"]) }}
                </div>
            </div>

        </div>
    </div>
</div>


