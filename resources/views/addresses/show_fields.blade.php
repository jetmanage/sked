<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $address->id !!}</p>
</div>

<!-- Ind Id Field -->
<div class="form-group">
    {!! Form::label('ind_id', 'Ind Id:') !!}
    <p>{!! $address->ind_id !!}</p>
</div>

<!-- Address 1 Field -->
<div class="form-group">
    {!! Form::label('address_1', 'Address 1:') !!}
    <p>{!! $address->address_1 !!}</p>
</div>

<!-- Address 2 Field -->
<div class="form-group">
    {!! Form::label('address_2', 'Address 2:') !!}
    <p>{!! $address->address_2 !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $address->city !!}</p>
</div>

<!-- State Field -->
<div class="form-group">
    {!! Form::label('state', 'State:') !!}
    <p>{!! $address->state !!}</p>
</div>

<!-- Zip Field -->
<div class="form-group">
    {!! Form::label('zip', 'Zip:') !!}
    <p>{!! $address->zip !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $address->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $address->updated_at !!}</p>
</div>

