<!-- Ind Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ind_id', 'Ind Id:') !!}
    {!! Form::number('ind_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Address 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_1', 'Address 1:') !!}
    {!! Form::text('address_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Address 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_2', 'Address 2:') !!}
    {!! Form::text('address_2', null, ['class' => 'form-control']) !!}
</div>

<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<!-- State Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state', 'State:') !!}
    {!! Form::text('state', null, ['class' => 'form-control']) !!}
</div>

<!-- Zip Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zip', 'Zip:') !!}
    {!! Form::text('zip', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('addresses.index') !!}" class="btn btn-default">Cancel</a>
</div>
