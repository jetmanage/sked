<!-- Ind Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ind_id', 'Ind Id:') !!}
    {!! Form::number('ind_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note_type', 'Note Type:') !!}
    {!! Form::number('note_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('notes.index') !!}" class="btn btn-default">Cancel</a>
</div>
