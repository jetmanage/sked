<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $country->id !!}</p>
</div>

<!-- Full Name Field -->
<div class="form-group">
    {!! Form::label('full_name', 'Full Name:') !!}
    <p>{!! $country->full_name !!}</p>
</div>

<!-- Iso 3166 2 Field -->
<div class="form-group">
    {!! Form::label('iso_3166_2', 'Iso 3166 2:') !!}
    <p>{!! $country->iso_3166_2 !!}</p>
</div>

