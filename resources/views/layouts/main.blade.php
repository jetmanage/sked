<!DOCTYPE html>
<html class="no-js css-menubar" lang="{{ config('app.locale') }}">
<head>
    @include('layouts.partials.header', ['cssFile' => 'vendor'])
</head>


<body class="animsition site-menubar-unfold site-menubar-keep">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


@include('layouts.partials.menu')


<div class="page">
    @include('layouts.partials.pageheader')
    <div class="page-content container-fluid">
        @yield('content')
    </div>
</div>


@include('layouts.partials.footer', ['jsFile' => 'vendor'])


</body>
</html>