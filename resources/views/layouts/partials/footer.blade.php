<script src="{{ asset('/js/app.js') }}"></script>
<script src="{{ asset('/js/'.$jsFile.'.js') }}"></script>

@if(session()->has('success') || session()->has('error'))
    <script>
        jQuery(function($){
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
            };

            @if(session()->has('success'))
                toastr.success('{{ session()->get("success") }}');
            @endif

            @if(session()->has('error'))
                toastr.error('{{ session()->get("error") }}');
            @endif
        });
    </script>
@endif

@yield('bottom_js')
