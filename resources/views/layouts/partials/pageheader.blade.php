<div class="page-header">
    {!! Breadcrumbs::render($page[1]) !!}
    <h1 class="page-title">{!! $page[0] !!}</h1>
    <div class="page-header-actions">
        @if(isset($page[2]))
            @if(!is_array($page[2]))
            <a class="btn btn-sm btn-icon btn-primary btn-round waves-effect" data-tooltip-content="Create" data-toggle="tooltip" data-original-title="Create" href="{!! action($page[2]) !!}">
                <i class="icon md-plus" aria-hidden="true"></i>
            </a>
            @else
            <a class="btn btn-sm btn-icon btn-primary btn-round waves-effect" data-tooltip-content="Create" data-toggle="tooltip" data-original-title="Create" href="{!! action($page[2][0], $page[2][1]) !!}">
                <i class="icon md-plus" aria-hidden="true"></i>
            </a>
            @endif
        @endif
        @if(isset($page[3]))
            <a class="btn btn-sm btn-icon btn-primary btn-round waves-effect" data-toggle="tooltip" data-original-title="Bulk" href="{!! action($page[3]) !!}">
                <i class="icon md-playlist-plus" aria-hidden="true"></i>
            </a>
        @endif
    </div>
</div>
