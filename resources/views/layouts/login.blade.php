<!DOCTYPE html>
<html class="no-js css-menubar" lang="{{ config('app.locale') }}">
<head>
    @include('layouts.partials.header', ['cssFile' => 'login'])
</head>


<body class="animsition page-login-v2 layout-full page-dark">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->


@yield('content')


@include('layouts.partials.footer', ['jsFile' => 'login'])

</body>
</html>