<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $passenger->id !!}</p>
</div>

<!-- Leg Id Field -->
<div class="form-group">
    {!! Form::label('leg_id', 'Leg Id:') !!}
    <p>{!! $passenger->leg_id !!}</p>
</div>

<!-- Trip Id Field -->
<div class="form-group">
    {!! Form::label('trip_id', 'Trip Id:') !!}
    <p>{!! $passenger->trip_id !!}</p>
</div>

<!-- Ind Id Field -->
<div class="form-group">
    {!! Form::label('ind_id', 'Ind Id:') !!}
    <p>{!! $passenger->ind_id !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $passenger->deleted_at !!}</p>
</div>

