<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $trip->id !!}</p>
</div>

<!-- Aircraft Id Field -->
<div class="form-group">
    {!! Form::label('aircraft_id', 'Aircraft Id:') !!}
    <p>{!! $trip->aircraft_id !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $trip->deleted_at !!}</p>
</div>

