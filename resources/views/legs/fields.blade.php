<!-- Trip Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trip_id', 'Trip Id:') !!}
    {!! Form::number('trip_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Trip Leg Order Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trip_leg_order', 'Trip Leg Order:') !!}
    {!! Form::number('trip_leg_order', null, ['class' => 'form-control']) !!}
</div>

<!-- From Airport Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('from_airport_id', 'From Airport Id:') !!}
    {!! Form::number('from_airport_id', null, ['class' => 'form-control']) !!}
</div>

<!-- To Airport Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('to_airport_id', 'To Airport Id:') !!}
    {!! Form::number('to_airport_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Depart Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('depart_date', 'Depart Date:') !!}
    {!! Form::date('depart_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Out Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('out_time', 'Out Time:') !!}
    {!! Form::date('out_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Off Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('off_time', 'Off Time:') !!}
    {!! Form::date('off_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Arrival Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('arrival_date', 'Arrival Date:') !!}
    {!! Form::date('arrival_date', null, ['class' => 'form-control']) !!}
</div>

<!-- On Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('on_time', 'On Time:') !!}
    {!! Form::date('on_time', null, ['class' => 'form-control']) !!}
</div>

<!-- In Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('in_time', 'In Time:') !!}
    {!! Form::date('in_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Out Fuel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('out_fuel', 'Out Fuel:') !!}
    {!! Form::number('out_fuel', null, ['class' => 'form-control']) !!}
</div>

<!-- In Fuel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('in_fuel', 'In Fuel:') !!}
    {!! Form::number('in_fuel', null, ['class' => 'form-control']) !!}
</div>

<!-- Out Hobbs Field -->
<div class="form-group col-sm-6">
    {!! Form::label('out_hobbs', 'Out Hobbs:') !!}
    {!! Form::number('out_hobbs', null, ['class' => 'form-control']) !!}
</div>

<!-- In Hobbs Field -->
<div class="form-group col-sm-6">
    {!! Form::label('in_hobbs', 'In Hobbs:') !!}
    {!! Form::number('in_hobbs', null, ['class' => 'form-control']) !!}
</div>

<!-- Out Tach Field -->
<div class="form-group col-sm-6">
    {!! Form::label('out_tach', 'Out Tach:') !!}
    {!! Form::number('out_tach', null, ['class' => 'form-control']) !!}
</div>

<!-- In Tach Field -->
<div class="form-group col-sm-6">
    {!! Form::label('in_tach', 'In Tach:') !!}
    {!! Form::number('in_tach', null, ['class' => 'form-control']) !!}
</div>

<!-- Regs Field -->
<div class="form-group col-sm-6">
    {!! Form::label('regs', 'Regs:') !!}
    {!! Form::text('regs', null, ['class' => 'form-control']) !!}
</div>

<!-- Aircraft Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('aircraft_id', 'Aircraft Id:') !!}
    {!! Form::number('aircraft_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Registration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('registration', 'Registration:') !!}
    {!! Form::text('registration', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('legs.index') !!}" class="btn btn-default">Cancel</a>
</div>
