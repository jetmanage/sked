@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Leg
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($leg, ['route' => ['legs.update', $leg->id], 'method' => 'patch']) !!}

                        @include('legs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection