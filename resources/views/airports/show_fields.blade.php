<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $airport->id !!}</p>
</div>

<!-- Airport Entry Id Field -->
<div class="form-group">
    {!! Form::label('airport_entry_id', 'Airport Entry Id:') !!}
    <p>{!! $airport->airport_entry_id !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $airport->code !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $airport->city !!}</p>
</div>

<!-- Country Field -->
<div class="form-group">
    {!! Form::label('country', 'Country:') !!}
    <p>{!! $airport->country !!}</p>
</div>

<!-- Faacode Field -->
<div class="form-group">
    {!! Form::label('faaCode', 'Faacode:') !!}
    <p>{!! $airport->faaCode !!}</p>
</div>

<!-- Iata Field -->
<div class="form-group">
    {!! Form::label('iata', 'Iata:') !!}
    <p>{!! $airport->iata !!}</p>
</div>

<!-- Icao Field -->
<div class="form-group">
    {!! Form::label('icao', 'Icao:') !!}
    <p>{!! $airport->icao !!}</p>
</div>

<!-- Landingfacilityid Field -->
<div class="form-group">
    {!! Form::label('landingFacilityId', 'Landingfacilityid:') !!}
    <p>{!! $airport->landingFacilityId !!}</p>
</div>

<!-- Latitude Field -->
<div class="form-group">
    {!! Form::label('latitude', 'Latitude:') !!}
    <p>{!! $airport->latitude !!}</p>
</div>

<!-- Longitude Field -->
<div class="form-group">
    {!! Form::label('longitude', 'Longitude:') !!}
    <p>{!! $airport->longitude !!}</p>
</div>

<!-- Typicallyhasmetar Field -->
<div class="form-group">
    {!! Form::label('typicallyHasMetar', 'Typicallyhasmetar:') !!}
    <p>{!! $airport->typicallyHasMetar !!}</p>
</div>

<!-- Typicallyhastaf Field -->
<div class="form-group">
    {!! Form::label('typicallyHasTaf', 'Typicallyhastaf:') !!}
    <p>{!! $airport->typicallyHasTaf !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $airport->name !!}</p>
</div>

<!-- State Field -->
<div class="form-group">
    {!! Form::label('state', 'State:') !!}
    <p>{!! $airport->state !!}</p>
</div>

<!-- Zip Field -->
<div class="form-group">
    {!! Form::label('zip', 'Zip:') !!}
    <p>{!! $airport->zip !!}</p>
</div>

<!-- Fullcountrynamelowercase Field -->
<div class="form-group">
    {!! Form::label('fullCountryNameLowerCase', 'Fullcountrynamelowercase:') !!}
    <p>{!! $airport->fullCountryNameLowerCase !!}</p>
</div>

<!-- Magvarwest Field -->
<div class="form-group">
    {!! Form::label('magVarWest', 'Magvarwest:') !!}
    <p>{!! $airport->magVarWest !!}</p>
</div>

<!-- Access Field -->
<div class="form-group">
    {!! Form::label('access', 'Access:') !!}
    <p>{!! $airport->access !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $airport->type !!}</p>
</div>

<!-- Hasfuel Field -->
<div class="form-group">
    {!! Form::label('hasFuel', 'Hasfuel:') !!}
    <p>{!! $airport->hasFuel !!}</p>
</div>

<!-- Fueltypes Field -->
<div class="form-group">
    {!! Form::label('fuelTypes', 'Fueltypes:') !!}
    <p>{!! $airport->fuelTypes !!}</p>
</div>

<!-- Longestrunwaylength Field -->
<div class="form-group">
    {!! Form::label('longestRunwayLength', 'Longestrunwaylength:') !!}
    <p>{!! $airport->longestRunwayLength !!}</p>
</div>

<!-- Longestrunwaysurfacetype Field -->
<div class="form-group">
    {!! Form::label('longestRunwaySurfaceType', 'Longestrunwaysurfacetype:') !!}
    <p>{!! $airport->longestRunwaySurfaceType !!}</p>
</div>

<!-- Nearestlandingfacilityids Field -->
<div class="form-group">
    {!! Form::label('nearestlandingFacilityIds', 'Nearestlandingfacilityids:') !!}
    <p>{!! $airport->nearestlandingFacilityIds !!}</p>
</div>

<!-- Elevationft Field -->
<div class="form-group">
    {!! Form::label('elevationFt', 'Elevationft:') !!}
    <p>{!! $airport->elevationFt !!}</p>
</div>

<!-- Proceduretypes Field -->
<div class="form-group">
    {!! Form::label('procedureTypes', 'Proceduretypes:') !!}
    <p>{!! $airport->procedureTypes !!}</p>
</div>

<!-- Towered Field -->
<div class="form-group">
    {!! Form::label('towered', 'Towered:') !!}
    <p>{!! $airport->towered !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $airport->deleted_at !!}</p>
</div>

