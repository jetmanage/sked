<table class="table table-responsive" id="emails-table">
    <thead>
        <tr>
            <th>Ind Id</th>
        <th>Email</th>
        <th>Is Verified</th>
        <th>Is Primary</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($emails as $email)
        <tr>
            <td>{!! $email->ind_id !!}</td>
            <td>{!! $email->email !!}</td>
            <td>{!! $email->is_verified !!}</td>
            <td>{!! $email->is_primary !!}</td>
            <td>
                {!! Form::open(['route' => ['emails.destroy', $email->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('emails.show', [$email->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('emails.edit', [$email->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>