@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Individual
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($individual, ['route' => ['individuals.update', $individual->id], 'method' => 'patch']) !!}

                        @include('individuals.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection