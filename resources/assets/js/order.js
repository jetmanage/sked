
jQuery(function($){

  $(".selectpicker").selectpicker();
  $("#productplan").select2();

  $("#productplan").on('select2:selecting', function (evt) {
    // getting parent label
    var parentLabel=evt.params.args.data.element.parentNode.label;
    var currentOpt=evt.params.args.data.element.value;

    // Allowing only one plan
    if ($("#productplan optgroup[label=Plans] option:selected").length == 1 && parentLabel == 'Plans'){
      return false;
    }
  });

  $("#js-data-ajax").select2({
    ajax: {
      url: "/customers/select",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term, // search term
          page: params.page
        };
      },
      processResults: function (data, params) {

        // parse the results into the format expected by Select2
        // since we are using custom formatting functions we do not need to
        // alter the remote JSON data, except to indicate that infinite
        // scrolling can be used
        params.page = params.page || 1;

        return {
          results: data,
          pagination: {
            more: (params.page * 30) < data.total_count
          }
        };
      },
      cache: true
    }
    ,
    escapeMarkup: function (markup) { return markup; },
    minimumInputLength: 1,
    templateResult: formatRepo,
    // templateSelection: formatRepoSelection
  });

  var $eventSelect = $("select.profiles");

  // fired when option is selected on select2 element
  $eventSelect.on("select2:select", function (e) {
    callAjax();
  });

  // fired when option is unselected on select2 element
  $eventSelect.on("select2:unselect", function (e) {
    callAjax();
  });

  // Currency select object
  var $currencySelect = $("select.currency");

  // fired when option is selected on select2 element
  $currencySelect.on("change", function (e) {
    console.log("her")
    callAjax();
  });

  // Calliing at the start of the page
  callAjax();
});

/**
 * Ajax call to show product pricing for order's page.
 */
function callAjax(){
  if ($("select.profiles").length > 0 && $("select.currency").length > 0) {
    jQuery(".val").html('<div class="loader loader-size vertical-align-middle loader-ellipsis"></div>');
    $.ajax({
      method: "GET",
      url: "/orders/products",
      data: {
        product_id: $("select.profiles").val(),
        currency: $("select.currency").val()
      },
      success: function($result){

        if($("select.currency").val() == 'usd')
          currency = "$"
        if($("select.currency").val() == 'eur')
          currency = "&euro;"

        jQuery(".val").html(currency+$result);
      }
    });
  }
}

function formatRepo (repo) {
  if (repo.loading) return repo.text;
  var markup =  "<div class='select2-result-repository clearfix'>" +
                  "<div class='select2-result-repository__meta'>" +
                    "<div class='select2-result-repository__title'>" +
                      repo.first_name + " " + repo.last_name + " (" + repo.id + " - " + repo.email + ") " +
                    "</div>" +
                  "</div>"+
                "</div>";
  return markup;
}

function formatRepoSelection (repo) {
  return repo.first_name || repo.text;
}
