/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(document, window, $) {
  'use strict';

  // ------------------------------------
  // (function() {
  //   $('#treeviews').jstree({

  //     'core': {
  //       'data': [{
  //         'text': 'Simple root node',
  //         "icon": "wb-folder"
  //       }, {
  //         'text': 'Root node 2',
  //         "icon": "wb-folder",
  //         'state': {
  //           'opened': true,
  //           'selected': true
  //         },
  //         'children': [{
  //           'text': 'Child 1',
  //           "icon": "wb-folder"
  //         }, {
  //           'text': 'Child 2',
  //           "icon": "wb-folder"
  //         }]
  //       }]
  //     },
  //     'plugins': ['checkbox']
  //   })
  // })();

  $('#treeview').jstree({
      'core': {
        'data': {
          'url': getBaseUrl() + '/roles/permission',
          'dataType': 'json',
          'data' : function (node) {
            return { 'id' : $("#rid").val() };
          }
        },
        "themes": {
            "icons": false
        }
      },
      'plugins': ['checkbox']
    });


})(document, window, jQuery);


/**
 * saving checkbox value in hidden variable when form submits
 */
function submitMe() {

  var checked_ids = [];
  var ids = jQuery('#treeview').jstree('get_checked',null,true);

  jQuery(ids).each(function( index, value){
      checked_ids.push(value);
  });
  
  // document.getElementById('jsfields').value = checked_ids.join(",");
  jQuery('#jsfields').val( checked_ids.join(",") );
}
