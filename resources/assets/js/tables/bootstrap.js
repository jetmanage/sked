(function (document, window, $) {
    'use strict';

    // var Site = window.Site;

    // $(document).ready(function($) {
    //   Site.run();
    // });

})(document, window, jQuery);

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

window.actionEvents = {

    'click .assign': function (e, value, row, index) {
        document.location.href = '/users/' + row.id + "/roles";
    },
    'click .edit': function (e, value, row, index) {
        document.location.href = '/users/' + row.id + "/edit";
    },
    'click .remove': function (e, value, row, index) {
        notie.confirm(
            Lang.get('user.js.delete.message'),
            Lang.get('crm.js.delete.yes'),
            Lang.get('crm.js.delete.cancel'),
            function () {
                document.location.href = '/users/' + row.id + "/delete";
            });
    }
};

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

window.actionRoleEvents = {

    'click .edit': function (e, value, row, index) {
        document.location.href = '/roles/' + row.id + "/edit";
    },
    'click .remove': function (e, value, row, index) {
        notie.confirm(
            Lang.get('role.js.delete.message'),
            Lang.get('crm.js.delete.yes'),
            Lang.get('crm.js.delete.cancel'),
            function () {
                document.location.href = '/roles/' + row.id + "/delete";
            });
    }
};

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

window.actionCallCenter = {

    'click .assign': function (e, value, row, index) {
        document.location.href = '/callcenters/' + row.id + "/user";
    },
    'click .edit': function (e, value, row, index) {
        document.location.href = '/callcenters/' + row.id + "/edit";
    },
    'click .remove': function (e, value, row, index) {
        notie.confirm(
            Lang.get('callcenter.js.delete.message'),
            Lang.get('crm.js.delete.yes'),
            Lang.get('crm.js.delete.cancel'),
            function () {
                document.location.href = '/callcenters/' + row.id + "/delete";
            });
    }
};

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

window.actionComputer = {

    'click .edit': function (e, value, row, index) {
        document.location.href = '/customers/' + row.customer_id + "/computer/" + row.id + "/edit";
    },
    'click .remove': function (e, value, row, index) {
        notie.confirm(
            Lang.get('computer.js.delete.message'),
            Lang.get('crm.js.delete.yes'),
            Lang.get('crm.js.delete.cancel'),
            function () {
                document.location.href = '/customers/' + row.customer_id + "/computer/" + row.id + "/delete";
            });
    }
};

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

window.actionCustomer = {

    'click .case': function (e, value, row, index) {
        document.location.href = '/cases/' + row.id + "/customer";
    },
    'click .computer': function (e, value, row, index) {
        document.location.href = '/customers/' + row.id + "/computer";
    },
    'click .details': function (e, value, row, index) {
        document.location.href = '/customers/plan/' + row.id + "/details";
    },
    'click .edit': function (e, value, row, index) {
        document.location.href = '/customers/' + row.id + "/edit";
    },
    'click .remove': function (e, value, row, index) {
        notie.confirm(
            Lang.get('customers.js.delete.message'),
            Lang.get('crm.js.delete.yes'),
            Lang.get('crm.js.delete.cancel'),
            function () {
                document.location.href = '/customers/' + row.id + "/delete";
            });
    }
};

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
//
window.actionProduct = {

    'click .edit': function (e, value, row, index) {
        document.location.href = '/products/' + row.id + "/edit";
    },
    'click .remove': function (e, value, row, index) {
        notie.confirm(
            Lang.get('product.js.delete.message'),
            Lang.get('crm.js.delete.yes'),
            Lang.get('crm.js.delete.cancel'),
            function () {
                document.location.href = '/products/' + row.id + "/delete";
            });
    }
};

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
//
window.actionOrder = {

    'click .edit': function (e, value, row, index) {
        document.location.href = '/orders/' + row.id + "/edit";
    },
    'click .remove': function (e, value, row, index) {
        notie.confirm('Are you sure you want to do delete this order ?', 'Yes', 'Cancel', function () {
            document.location.href = '/orders/' + row.id + "/delete";
        });
    },
    'click .details': function (e, value, row, index) {
        document.location.href = '/orders/' + row.id + "/details";
    }
};

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

window.actionPlan = {

    'click .edit': function (e, value, row, index) {
        document.location.href = '/plans/' + row.id + "/edit";
    },
    'click .remove': function (e, value, row, index) {
        notie.confirm(
            Lang.get('plan.js.delete.message'),
            Lang.get('crm.js.delete.yes'),
            Lang.get('crm.js.delete.cancel'),
            function () {
                document.location.href = '/plans/' + row.id + "/delete";
            });
    }
};

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

window.actionCustomerPlan = {

    'click .cancel': function (e, value, row, index) {
        notie.confirm(
            Lang.get('customers.js.plan.message'),
            Lang.get('crm.js.delete.yes'),
            Lang.get('crm.js.delete.cancel'),
            function () {
                document.location.href = '/customers/plan/' + row.id + "/inactive";
            });
    },
    'click .refresh': function (e, value, row, index) {
        $("#pid").val(row.id)
        $('#reactiveform').data('formValidation').resetForm();
        $('#customerPlan').modal('show');
    }

};

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
//
window.actionComments = {

    'click .edit': function (e, value, row, index) {
        document.location.href = '/orders/comment/' + row.id + "/edit";
    }

};

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

window.actionCase = {

    'click .edit': function (e, value, row, index) {
        document.location.href = '/cases/' + row.id + "/edit";
    },
    'click .remove': function (e, value, row, index) {
        notie.confirm(
            Lang.get('case.js.delete.message'),
            Lang.get('crm.js.delete.yes'),
            Lang.get('crm.js.delete.cancel'),
            function () {
                document.location.href = '/cases/' + row.id + "/delete";
            });
    }
};


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

window.actionKey = {

    'click .assign': function (e, value, row, index) {
        document.location.href = '/keys/' + row.id + "/assign";
    },
    'click .edit': function (e, value, row, index) {
        document.location.href = '/keys/' + row.id + "/edit";
    },
    'click .remove': function (e, value, row, index) {
        notie.confirm(
            Lang.get('key.js.delete.message'),
            Lang.get('crm.js.delete.yes'),
            Lang.get('crm.js.delete.cancel'),
            function () {
                document.location.href = '/keys/' + row.id + "/delete";
            });
    }
};


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
//
window.actionCustomerViewComments = {

    'click .edit': function (e, value, row, index) {
        document.location.href = '/customers/' + row.customer_id + "/view/"+ row.id + "/edit";
    }

};
// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
window.actionCustomerEditComments = {

    'click .edit': function (e, value, row, index) {
        document.location.href = '/customers/' + row.customer_id + "/edit/"+ row.id + "/edit";
    }

};
// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

window.actionCoupon = {

    'click .edit': function (e, value, row, index) {
        document.location.href = '/coupons/' + row.id + "/edit";
    },
    'click .remove': function (e, value, row, index) {
        notie.confirm(
            Lang.get('coupon.js.delete.message'),
            Lang.get('crm.js.delete.yes'),
            Lang.get('crm.js.delete.cancel'),
            function () {
                document.location.href = '/coupons/' + row.id + "/delete";
            });
    }
};