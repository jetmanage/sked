Events CRUD
Events Views
Trip Quotes CRUD
Trip Quotes Views
Airport IATA Auto-Complete
Trip Schedule CRUD
Trip Schedule Views
Quotes / Request Lookup
Flight Logs CRUD
Flight Logs Views
Aircraft Fleet CRUD
Aircraft Fleet Views
Performance Profiles
Airport Bias Time
Companies CRUD
Companies Views
Users CRUD
Users Views
Customers CRUD
Cutomsers Views
Pilots CRUD
Pilots Views
Settings Forms
Settings Views

user
    id
    first_name
    last_name
    email
    role_id
    last_sign_in
    company
    password
    cuostmer
    created_at
    updated_at
    deleted_at
        
customer
    id
    title
    first
    middle
    last
    gender
    dob
    email
    phone
    fax
    notes_aircraft
    notes_catering
    notes_general
    notes_handling
    notes_lodging
    notes_pilot_trip_sheet
    notes_transportation
    pricing_profile_type_id
    is_shared_customer
    is_shared_passenger
    weight
    weight_unit
    account_email
    account_exists
    account_fax
    account_first
    account_last
    account_middle
    account_phone
    company_name
    company_type
    contact_company
    formatted_primary_address
    notes_html
    relationships
    company
    created_at
    updated_at
    deleted_at
    
documents
    id
    type
    number
    expiry
    uploaded_file
    
uploads
    id
    file_hash
    
    
companies
    name
    phone
    mobile
    fax
    email
    url
    created_at
    updated_at
    deleted_at
    
aircrafts
    id
    aircraft_type
    call_sign
    description
    dispatch_office
    exterior_refurbishment_date
    fact_sheet_document
    helicopter
    homebase
    interior_refurbishment_date
    mass_take_off_weight
    mass_take_off_weight_unit
    max_pax
    max_range_n_m
    min_pax
    number_of_pilot_seats
    owner_approval
    pax_max_range
    price_info
    range_max_pax_n_m
    registration_number
    serial_number
    year_of_make
    created_at
    updated_at
    deleted_at
    
    #CO-Ownership Issues